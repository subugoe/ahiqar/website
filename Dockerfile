FROM node:lts-alpine AS build
ARG VITE_APP_BASE_URL_API=http://api.ahiqar.local
ARG VITE_APP_BASE_URL_VIEWER=http://ahiqar.local
ARG VITE_APP_MATOMO_SITE_ID=NA
ARG VITE_APP_PUBLIC_PATH=/

COPY . /

RUN npm ci
RUN npm run build

FROM nginx:alpine-slim

COPY --from=build src/.vuepress/dist /usr/share/nginx/html