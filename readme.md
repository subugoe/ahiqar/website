# Ahiqar Website

The main entrypoint for the Ahiqar project. It is built with the Vuepress 2 static site generator and 
implements the [TIDO Viewer](https://gitlab.gwdg.de/subugoe/emo/tido) to display the manuscripts.

## Get it running

- `$ git clone git@gitlab.gwdg.de:subugoe/ahiqar/website.git`
- `$ nvm use 18` *to make sure you use the required node version*
- `$ npm i` *to install all dependencies*
- Create `.env.local` file and copy everything from `.env` file
- Replace your desired values in `.env.local` file
- `$ npm run dev` *to get a running (locally) website at [`http://localhost:8080/`](http://localhost:8080/)*

### Container Way

You can also run the build and the application in a container environment.
Keep in mind that the env vars set URLs to be written into static files. Adjust these variables for your need. A typical image meant for local testing is also generated by the CI pipeline and will use the domain `ahiqar.local` and for accessing the TextAPI it uses `api.ahiqar.local`. These are the default values, if no others are provided via `--build-arg`

```sh
docker build \
  --file Dockerfile \
  --tag harbor.gwdg.de/sub/subugoe/ahiqar/website:local-test \
  .
```

Start the server with

```sh
docker run -p 9099:80 harbor.gwdg.de/sub/subugoe/ahiqar/website:local-test
```

Now you can access the application via http://localhost:9099/.

Limitations: All functionality that requires a running database will not work, mainly Tido and Search. Read at the respective repository how to set up.

## Edit Content

Page content can be found in various markdown files in [`src/`](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/tree/main/src) and [`src/de/`](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/tree/main/src/de).

Gitlab's Web IDE can be used as well as a checkout-edit-locally-and-commit-workflow.

You can either open a branch for edits and use [Gitlab's environment feature](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/environments) to preview changes or use the master branch.

Be aware that if you use Gitlab's web IDE the changelog can't reflect the changes. This is not ideal, so please go the checkout-edit-locally-and-commit-workflow route and don't skip commit hooks and use the appropriate commit message category and write clear commit messages.

Basically the general rules on how to format markdown apply, but to have it properly styled you have to use vuetify styles (see below). The general markdown formatting rules are available e. g. [at Github](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

If you want to get an idea why things look the way they look already, peek into existing files and inspect the { } parts - [like here at the homepage](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/blob/main/src/index.md).

## Content via API

Some pages are prepared form TEI source files. HTML is provided via backend.

### Manuscripts

To update the [list of manuscripts](src/manuscripts.md), run the following code.

```bash
head -$(grep " HTML " src/manuscripts.md -n | cut -f1 -d:) src/manuscripts.md >> src/tmp.md
echo "" >> src/tmp.md # newline
curl https://ahiqar.uni-goettingen.de/api/website/manuscripts | sed "s#xhtml:##g" >> src/tmp.md
# check result at tmp.md and than
mv src/tmp.md src/manuscripts.md
```

### Collation Results

To update the [collation results](src/collation.md), run the following code.

```bash
head -$(grep " HTML " src/collation.md -n | cut -f1 -d:) src/collation.md >> src/tmp.md
echo "" >> src/tmp.md # newline
curl https://ahiqar.uni-goettingen.de/api/website/collation | sed "s#xhtml:##g" >> src/tmp.md
# check result at tmp.md and than
mv src/tmp.md src/collation.md
```

## Release

```bash
git checkout develop
git pull

# This updates the changelog file.
npm run changelog

git push --follow-tags origin develop
```

We add a tag to develop what will provide a stage deployment. If everything is fine
this tag version is going to merged to main what triggers a production release.

## Adding a New Collection to the Website

To add a new collection to the website proceed as follows:

1. create a new Markdown file for the collection `collection_name` at `src/collection_name.md`.
The content of the file will be

    ```markdown
    ---
    title: TIDO
    layout: Layout
    is_viewer: true
    ---
    <ClientOnly>
        <TidoPage/>
    </ClientOnly>
    ```

2. add a new entry point to `src/.vuepress/components/TidoPage.vue` that point to the correct TextAPI Collection Object
3. add a new entry to navigation at `src/.vuepress/config.js`

## Customize the TIDO viewer

The configuration for the TIDO viewer can be found at `/src/.vuepress/utilities/tido-config.js`.
