# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.6.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v2.5.0...v2.6.0) (2025-02-14)


### Features

* update TIDO to v4.6.0 ([7650a6e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7650a6ef61ad813a2f5deefc352cf41b8367b648))

## [2.5.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v2.4.0...v2.5.0) (2025-02-07)


### Features

* add button for Neo-Aramaic on landing page ([ec41552](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ec41552860942f7ef8a388bb0379ab575eb2f7c8))
* add dictionary annotation tab for syriac page ([a4a38cd](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a4a38cd8043ed9a3579a9a5bacfb9c63a8b338ce))
* first draft ([2146f8f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2146f8f7d243c8fd259bbf627421bfda43157d91))
* update TIDO to v4.5.0 ([68c77f7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/68c77f763e8fe8c79b902698d0fb554314aa488b))


### Bug Fixes

* **ci:** dev deployment on all branches ([0d09a71](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/0d09a71263b2b11f674c46a71df2c7509805ee13))
* **ci:** helm login ([a2aeef6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a2aeef633557a68369dfa0cb0de6d0f7e9b3afe7))
* **ci:** path to chart package ([3e73485](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/3e734854281a45591f14210c85b197e6f3cbeda2))
* **ci:** syntax ([69cd838](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/69cd838e2406d94f301dba6f68589c26e22ad734))
* **ci:** syntax ([7cd0278](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7cd0278535599f9e85d9cc8a1c588553b9f7a34a))
* **ci:** syntax ([07c412b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/07c412ba1f36350cea59a586efd65a19e3829b8e))
* **ci:** syntax ([667dd63](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/667dd63d6d5424389774955df616067019477867))
* **ci:** syntax ([3f703f3](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/3f703f300c8745c3d9b0be5d1b8478c4265c6487))
* **ci:** vault ([ccd8d22](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ccd8d223b8ff6ac9118ea1c1f075b3b8240be06c))


### Docs

* typo ([da85a37](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/da85a3775ec8362ac867285f13ac270e72fe8206))


### Continuos Integration

* add destination tag wo branch name ([c670f71](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c670f7108785f3c4a1859c68d3628fc7b1c9b6f0))
* fix path ([1c27aab](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1c27aab713dcc819d5644e31cf31cd248e64907b))
* fix path ([99b8c36](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/99b8c36e9737779f774ecf395949aa8e1818ee61))
* fix path ([b848825](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b848825c883a6369e2d8629ec58c1b124f6b67b7))
* fix path ([a1e31ae](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a1e31ae5dcd142e3ca90f3d6b3ad005fbd2b5136))
* fix path to values files ([3a33a72](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/3a33a7262ab4698fea355c35703f4fee91b2a31e))
* fix syntax ([9340dc1](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9340dc1acd0dee77b97df812f944148d6b122e5f))
* fix syntax ([d11153d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d11153dc39e06757e98a33ebe2b16d9664b36a3b))
* fix syntax ([1e0870c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1e0870c05a1b544fe816e6b6f0bc00cdb19fc6af))
* include helm chart here ([6b7c8fc](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6b7c8fca855f94d14498e52fa5a8ac580baeb2ff))
* include helm chart here ([7ccef87](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7ccef871684bf57c008434fab21259591f224c44))
* set values for instances ([5e590c8](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5e590c87b8481b35aec0df205f93f63d52220e86))
* update instances and chart on demand ([c46e977](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c46e97720c40e8603941210419e3c189553f54a0))
* verbosity ([64b177d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/64b177d29add19ff199715d3e4d9c3a2dddb1ab1))
* verbosity ([1f6091e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1f6091eec89809c3dcfe9e373e945e0a0c40e8a5))


### Chore

* deactivate cache when not needed ([7ae056a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7ae056aea3e323804d1b170fbb1bd599d6965a79))

## [2.4.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v2.3.3...v2.4.0) (2024-12-11)


### Features

* add information about variants ([6f0683c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6f0683ca60366ae2e52e4e1b7411b757af1bbf9c))

### [2.3.3](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v2.3.2...v2.3.3) (2024-12-09)


### Chore

* update Syriac stemma ([e4d6257](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e4d625791b69ec20b6efe1e78f7a6a63d1f7e272))

### [2.3.2](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v2.3.0...v2.3.2) (2024-11-29)

## [2.3.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v2.0.0...v2.1.0) (2024-09-03)

### Features

* 🍪 cookie banner added ([b773a92](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b773a9206f9d7fa64f8578c10ef6a559ea8e87ea))

### Bug Fixes

* adjust URLs to external resources to match the target enironment ([b1f6def](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b1f6defd3639ffc7ec2e0e38430ed2d87fe38bb3))

## [2.2.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v2.0.0...v2.1.0) (2024-09-03)

### Features

* update tido to v4.3.0 ([bce3987](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/bce3987cf6d6fd46789400372b2028184a472332))

## [2.1.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v2.0.0...v2.1.0) (2024-09-03)


### Features

* add downstream pipeline ([aa273b9](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/aa273b9ce3a253d642825930e212f1bdaaa9d579))
* **ci:** workflow ([ea62486](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ea62486cbefdac38de1d874f440604e9173a9b05))


### Bug Fixes

* **ci:** syntax ([299a398](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/299a398a8ac9655a1242bcf35d06ccf2e8dcf70a))
* **ci:** var usage ([a3a4db3](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a3a4db32ff7a66afbd1c34dd74ab18c47d61fdb0))
* **ci:** var usage ([dca023d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/dca023d38b23e8b31b07ca626691a9c1ea729dd6))


### Docs

* refine release workflow description ([dc0a17e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/dc0a17ed9bd01b3da67c2225f84bb3bba5ea7ece))

## [2.0.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.12.1...v2.0.0) (2024-08-21)


### ⚠ BREAKING CHANGES

* upgrade to Vuepress 2

### Features

* **abbr:** complete siglum long forms ([2bc68f8](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2bc68f8f5be5752fac3ae4d277b1b82300565695))
* add new pages with special Tido config for translation panel ([63724cb](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/63724cb639b435b96ea888bf31c194050a6f722e))
* add robots entries to pages with translation to prevent search engine indexing ([a7d3fb6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a7d3fb6c6d76026fc41ef7e9e7d36db6723c38a0))
* add sbom creation and upload to deps ([aa36a11](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/aa36a118848eeb5eb16b71dc34a98ea56880efe4))
* Display the 'Translation' panel on the right of 'Transcription' ([1a20224](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1a202247113df382698e6a29418f824f946cf085))
* enlarge TidoPage container widths ([ee6e797](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ee6e7976bdf313edea374010cf2f9c455d410751))
* get TextAPI base URL from env variable ([d8d01c2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d8d01c273a34cb6d011cbd4630d2ef61dd365ede))
* hide tables in modals ([13a05ee](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/13a05ee40bb7ac1aa6688f8c24255abd4b4db2ad))
* Implement TIDO load using instances. ([e9e5246](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e9e524653a4d5c2e668e7e55e402e8dae112519b))
* implement TIDO v4.0.2 ([6aa9db2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6aa9db2891680e523c35f6a266ec7e587aa664c9))
* integrate TIDO v3 ([c52b6bc](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c52b6bca4b201fc4e1999e9317038b1002467d26))
* **modal:** add abbr tags for sigla ([10e968a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/10e968a22c1293a149e861300f9125710c17bdd9))
* **modal:** add sticker header for tables ([5191816](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5191816b305d99c444548975f0c020e040f34330))
* **sayings:** add abbr tags ([e470d75](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e470d75194b3c6772506822417a3566247186c83))
* **sayings:** add sigla ([155a63c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/155a63c0ee15b16f886a69c69a4f1267734242c0))
* update edition guidelines ([d8eece4](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d8eece43b4b27dc1aab9115a1d77b931c925ce2a))
* Update manuscipts and sigla ([9b2a82a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9b2a82ada5a680349d1942c536057af021848ca0))
* update readme ([2f36f41](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2f36f41c8118f8c9a8c4fc4ea8042b7c4b907ee1))
* update staff member list ([37f960d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/37f960de2cc2c4111029511f74a13077d1dd3ec5))
* update technical info ([978684b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/978684b237c17e363be60e1e2f6c60be01f61fdf))
* update TIDO to v3.2.1 ([28146ee](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/28146ee2c08a7709a128141e7d3a2e7fc4721506))
* update TIDO to v3.3.0 ([64961d3](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/64961d3e026c399dcedb1c5d4a6cb0493470cd73))
* upgrade to Vuepress 2 ([8b272e2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/8b272e2b7628223d60a9620b2c96663d9274eae5))
* use Tido v4.0.6 - fixes switching between editions ([7befec7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7befec7fc1b037da089422be1ee9a1c019743f17))


### Bug Fixes

* **abbr:** update positioning of tooltip ([1692bb7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1692bb7720ba7f945742181717afa45dff2b2f17))
* add heading and sigla ro proverbs tables ([82414b1](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/82414b18d011a67c090110d3e7cbeb0afc30050f))
* add static keyboard layouts because of build problem ([cc5d680](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cc5d68092d13210c0ddd1cc4a085a2d3aca251c6))
* **ci:** adjust paths ([5847780](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/584778081000e29f33f34598aa4897b9bbbd1199))
* **ci:** audience scoped JWT ([b737b32](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b737b32f7bb59734328c6cac2b0c7fe96b39f82c))
* **ci:** duplicate URL part as image name ([179e424](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/179e424537a4cb27b5044e4c0b126f5f9afd7d50))
* dark mode issues at keyboard ([e838b6e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e838b6eafb12f4da3b404d62ac0d699a243f34cf))
* error that result from new tech stack ([c818f72](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c818f72b8c7ddc00f1bc069c9778e721f625c182))
* layout switch when closing keyboard ([e0a63fa](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e0a63fa1c0b5d8cca0fb7dc61ce0cb3341bfc48a))
* Menu items vanish when combining ahiqar website white mode and tido darkmode ([01e910e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/01e910ebce338ed5594f8a9e1552a5a1725b0162))
* **proverbs:** add spaces between abbr tags ([e48c4dc](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e48c4dcadf245d3dd785b7cedc6a160afa747c67))
* **proverbs:** remove linebreaks between abbr tags ([7c94bda](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7c94bda7b48081bf64d1db828c8d21d0503c2956))
* quotes ([7ccaeb4](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7ccaeb4e653e370f18f076efbaae238cb3edffea))
* quotes ([1e6250e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1e6250eb283af879de7e9f97d37a16b204c3ea54))
* remove ".com" key from keyboard ([5467cd6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5467cd6feb4d9887cb343aa2c28c44af87723ff4))
* remove error of favicon by showing favicon ([349d69f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/349d69fc98cc117bab63744024c6a52e115f74f9))
* remove query params when switching between TidoPages ([93a9b09](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/93a9b09a4c3ea9433e12142ef83d8ee53082dd2b))
* remove TIDO bg color when navigating to other pages ([01ac6cc](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/01ac6cc6e747b7301a811af81aa4d71307e5afb9))
* remove TIDO params only when switch to other edition ([44314fd](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/44314fde4f94c9c377c8f5b20fe95ec07c6487cb))
* replace CI_COMMIT_REF_SLUG ([032888e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/032888e9ee80d901a06dd0b65e704239d3752617))
* **sayings:** add sigla to second table ([69b35b7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/69b35b72a0458e10dd3091c646407f5b30fc0197))
* **sayings:** format html ([b7672a7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b7672a78b6b8418ac863ae5c679609e3ee754707))
* set build args ([ee7cf45](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ee7cf45693d847e18254f7a5b965f12023af0a93))
* set PATH for Dependency Track ([9ee9fb2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9ee9fb2e72217c7d29557090a6c2ed723dfad3c6))
* update CTA style ([c56d93a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c56d93ac3e0d1b78a9da9cc0d96d16517627f22b))
* update TextAPI endpoint ([bfcedc9](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/bfcedc98817c13d9c78810410be5cdb10aaa354f))


### Styling

* format ([e52f297](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e52f2973d1f30dffae0b4b772665bfa41432fa4b))


### Docs

* nvm version ([2d2fdec](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2d2fdece5c2d9c115fe76ebc3a2dbb1df1f9231c))
* **readme.md:** add section about how to add a new collection ([69c743e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/69c743e7dd42c225da2e6f35146f81f4f964e9be))


### Refactoring

* **abbr:** use variable for padding ([f4d58bd](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f4d58bd0655aa9b95200645fae2587c00098ccdf))
* **modal:** remove css importants ([51d78af](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/51d78afec2cab3cbc1ff80f8ed305b3f94ea70c9))
* removed unused config - breadcrumb navigations. ([cbd7f64](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cbd7f643ed2cb7174eb4c917d885e55bb414c2f3))


### Continuos Integration

* add develop build target ([7cc88cb](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7cc88cbc1e6afcd4edae6f520648f691ecd6ba82))
* add docker build ([1433305](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/143330554f18ceb490d9179ac4ff04f716a9089c))
* add prod and stage env ([d63d9f7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d63d9f77bddab83136baf3191efc3df9e2de03ef))
* artifacts expire after 2 weeks ([83202bb](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/83202bb36f8e35e4b2e24da9924b7720974f3669))
* check file type ([26293ce](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/26293cee25ca3abbb6161ea70a80306e41162880))
* **container:** build w/o npm ci, as we get the artifact from the previouse job ([f0f69e4](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f0f69e4e68cf0a1b37dc09daf063505465f7c4e1))
* deactivate sbom for now ([bddb25b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/bddb25b00d7d94bfce683ae60be11a40f71157f0))
* **docker:** image name ([c8193a1](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c8193a151f4ae97b9bfae53f7ec5dfc87c8e89be))
* env vars to work with local k8s deployment ([007868b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/007868bdd141b2472043d9542964b78a184289d4))
* error handling for artifacts ([ac953ad](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ac953ad24e509d4bdc4ba68e923ed69b45ace791))
* export envs ([1c720a2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1c720a24d723adfa95c522fd4dfeac52d5ee31ed))
* **fix:** use wget ([679cede](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/679cedef81af3f54e7200bd854fe40c714012faa))
* introduce parallel build ([8ba7f0e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/8ba7f0edb9e56b0e935052125437c249929d9f38))
* keep main artifacts ([6db2b89](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6db2b89e9c2a7b043ed4f8a8bb921f2e230b264c))
* make env file work ([a49be4c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a49be4c9b74de6b7177479f402d37dce3c53651c))
* remove artifacts from older branches ([8e500dd](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/8e500dd7d3edbb3652c74724db9190ba526b6bb1))
* remove VITE_APP_BASE_URL_VIEWER, remove file transfer to frontend/website ([2890d52](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2890d52b975a345db951b101e10f00639a05f825))
* secrets from vault ([af35ebc](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/af35ebc13b306cbaaa2122d5fc632534f6604d1d))
* set image name ([3fc70f0](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/3fc70f075f8118309726fad4fd91b69206efcf94))
*** update image ([2bc6406](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2bc64065121e9e449ee2cedbf28a01eae3300b28))**
* **vault:** set secret path ([1c510e7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1c510e7f55f71a3c7fa7ec7957205b54feb08ca6))


### Chore

* adjust local domain ([69e9fdc](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/69e9fdcde20c3562cf63f3d358ac7a39ac26d4b6))
* adjust path ([3de12d9](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/3de12d906d44cfd1651ffebd7d943544e631ee25))
* adjust static base URLs ([2b42d2d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2b42d2dd1899dce628ca518265b0d1ac8de4c886))
* **ci:** set artifact expiry ([d7b9b43](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d7b9b43efdc0a9011da3fd290e5fe2e870fe3a0b))
* npm audit fix ([dad0248](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/dad0248f19954fa3c40d41e23d7ffebcb681aabb))
* remove comments from config ([5aac94e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5aac94ed2aeeb0e75b4983f5c5cf93e62656dd43))
* run package CI stage only on certain develop, main and tags ([659982e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/659982ee4de2117c00d9d95e0f5428e2b02dcc15))
* set placeholder value in env again ([15371d3](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/15371d3788add6b375686c509f5283ee05ce1167))
* upate package lock file. ([aa81568](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/aa81568517df98ce996d7d099063bc10aba15323))
* update changelog command ([e839e7f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e839e7f63dceee522ae353aabe9048f42918cf78))
* update vuepress and fix Tido styles ([c79e89c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c79e89cfece53228a40e45f1abe3fbc7fab74959))

### [1.12.1](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.12.0...v1.12.1) (2023-01-26)


### Bug Fixes

* **relatedliterature.md:** remove tracing ([81eaf27](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/81eaf27c49002f06068f7ae558ba4e6305c1cd55))

## [1.12.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.11.4...v1.12.0) (2023-01-26)


### Features

* add citation instruction to static pages ([0724f0b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/0724f0bf11c762b36e122dd913562cb5e8528ca5))
* **package.json:** update Sass ([bc9c92e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/bc9c92e8e23729007fa8582fca1c2e558f8d51c5))


### Styling

* headings ([0eba63a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/0eba63aeac0a1d64e509d230dfba68af4674cf01))
* lint README ([49ec5bd](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/49ec5bd6c1f9c9119949151a4637cb3eb93bb4d1))


### Docs

* **readme.md:** add requirements ([d0eab39](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d0eab393cc547405ee3de08690a3b88d406f63e9))
* **readme.md:** typos ([0dc3f13](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/0dc3f13f2463f57229735ade99badfb44be4f0a1))

### [1.11.4](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.11.3...v1.11.4) (2022-09-20)

### [1.11.3](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.11.2...v1.11.3) (2022-05-25)

### [1.11.2](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.11.1...v1.11.2) (2022-05-25)


### Bug Fixes

* link to contact ([7cfc428](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7cfc4284013df806ce2b2206f879db8ff588ebdd)), closes [#144](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/144)
* typos ([1cef8d1](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1cef8d1916a670582e2eede79f0d1d7209b50a47))

### [1.11.1](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.11.0...v1.11.1) (2021-12-07)


### Chore

* simplify copy text ([60a3168](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/60a31686b645686fe3f885ae6ff5f7017f1e11ac))

## [1.11.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.9...v1.11.0) (2021-11-26)


### Features

* add section on Tido to the tech doc ([520c46e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/520c46e899a5b52afe27ac807f7a830b0b0940d0))

### [1.10.9](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.8...v1.10.9) (2021-11-22)


### Chore

* remove obsolete project.md page ([d373f14](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d373f145dd5bed6619f9681ccf34408f72fa49c8))

### [1.10.8](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.7...v1.10.8) (2021-11-22)

### [1.10.7](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.6...v1.10.7) (2021-11-19)


### Chore

* minor markdown issues ([e8ecb2f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e8ecb2fc03112abbb4457322110004e9d33fc97b))

### [1.10.6](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.5...v1.10.6) (2021-11-19)


### Chore

* make links clickable ([b1553ad](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b1553ad5ce826a772a6024fcdfa9a29a73dfae76))

### [1.10.5](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.4...v1.10.5) (2021-11-16)

### [1.10.4](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.3...v1.10.4) (2021-11-09)

### [1.10.3](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.2...v1.10.3) (2021-11-09)

### [1.10.2](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.0...v1.10.2) (2021-11-08)


### Bug Fixes

* ms page ([a160b99](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a160b9926513bca229031443d044bce61b82afc0))
* update filename. ([cd1f2ed](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cd1f2ed912933e8bf79f2e0807cfb76052196217))
* url spelling correction. ([abfab10](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/abfab10c79a1bc3ad5f5e990adcf2407ae8c71b9))


### Chore

* correct menu item title ([02f6379](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/02f6379b729a74553add670088dba348a7ece2f4)), closes [#140](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/140)
* minor addition re [#138](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/138) ([37e078b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/37e078b0bf5f5f4406c913458761a300e2f2e773))
* remove terms of use ([02ab459](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/02ab459c1d1135090cb7ddd6041d5ecb23f36792)), closes [#139](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/139)
* **release:** 1.10.1 ([9f0f369](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9f0f369853832029247983aa62a5ef2f45daf319))

### [1.10.1](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.10.0...v1.10.1) (2021-10-12)


### Bug Fixes

* ms page ([a160b99](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a160b9926513bca229031443d044bce61b82afc0))

## [1.10.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.9.2...v1.10.0) (2021-10-12)


### Features

* update ms page ([9cfbdaa](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9cfbdaa6d474671d80199507d21ae0413d68b83e))


### Styling

* fix copy styling issues ([485e8d4](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/485e8d4d6b10eccfadafac6952fa6abd1eb03d5b))


### Refactoring

* how content is styled ([64d7044](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/64d70449cce2299f0aa6a674b442747cd32490a7))

### [1.9.2](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.9.1...v1.9.2) (2021-10-06)


### Bug Fixes

* markdown issues ([db81d12](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/db81d1240bc3e3a4ba921f476660f337e797f9ee))


### Chore

* change licence text under Ahiqar Image ([6c7a8c5](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6c7a8c5fa3fed46ccd5f94973c366aba172f052d)), closes [#137](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/137)

### [1.9.1](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.9.0...v1.9.1) (2021-09-29)


### Styling

* rework styling headings and stuff ([cc09744](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cc09744157346a41b0d760d7ea3d3a95456f8e80))


### Chore

* add sub pages to project and add technical docu ([3d6544a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/3d6544a85c62d2f54f74c2c88e6ede6a73a5b66b)), closes [#132](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/132)
* change menue item order in Meta Edition ([5ec5750](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5ec5750c7f394d5ae2488ce2712b72089eb77951)), closes [#134](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/134)
* change menue Item order in Translation ([95a0498](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/95a0498c82bb683626e2dd663ec002eed7689568)), closes [#133](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/133)
* fix markdown ([84414ae](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/84414aeec86c229487068cd5b6ff3d75533db845))
* replace images on stemmata ([38820eb](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/38820ebcf2c05f2a705d40a11e113ce85957eeab))

## [1.9.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.8.0...v1.9.0) (2021-09-21)


### Features

* add karshuni to languages map at search results ([e59c714](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e59c714a8920134dfe09fb0d49b751585c85f7e6))

## [1.8.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.7.0...v1.8.0) (2021-09-21)


### Features

* check for karshuni in search result item ([5fb05c0](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5fb05c0679ec63abf7789bbe8c28a187871cb729))
* new menue button "Translations" with two menue items ([7ab4cc8](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7ab4cc88b123a23d9182cdb1136e8151cbb2345d)), closes [#130](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/130)


### Bug Fixes

* markdown issues ([5d8e241](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5d8e241bc18a4af136e65dd3fcbdd5579e6ad583))
* new button navigation ([ec6f4a6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ec6f4a6d9cf3aa3f455358f874a9c7b2f4a8a4b9))


### Chore

* add links in markown ([5ea27ca](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5ea27ca6fda7a42c9094e878bacaeeb9c7fc6177))
* replace image stemmata3 ([9d44d8a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9d44d8a3d557a1cef760b0fa3d68fd345aa6a418))
* update content delivered via API ([91f999f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/91f999f0a3da0c351a44b727277633d4c2155ef3))

## [1.7.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.6.0...v1.7.0) (2021-09-13)


### Features

* move to new domain ([b7378b5](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b7378b55cddb4deed953ea68e64abf4b106351c9))
* update collation results ([a195225](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a195225d18e71c672e03e2fe1953f38d624904e1))


### Refactoring

* renaming parables to proverbs according to requirements ([c7bc0c9](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c7bc0c99af8b481b127d995e487ef2c1d84dc5d2))


### Continuos Integration

* enable dev deployment on test ([88ef830](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/88ef8306193bb0788b3ac4d77bde65a05a5e6485))

## [1.6.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.5.0...v1.6.0) (2021-09-09)


### Features

* add collation results ([87358f9](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/87358f92799db40393856c7fbc6bbc130bad80af))
* returning from tido to the respective search hit list page ([d09ad53](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d09ad5350a6a2bd41729efb5c23375f5fc71f8a5))


### Continuos Integration

* add build and deploy for test ([cbaa0f8](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cbaa0f843ef0bc319cc74ce5a4d22b95dbe6c11d))
* add build and deployment for test instane ([eb19de9](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/eb19de9043cc4e392f9d21400783bb9329399c5e))


### Chore

* pictures for stemmata page ([093f6a3](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/093f6a39297193e1f28d424b437229de729110ba)), closes [#112](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/112)


### Styling

* formatting and markdown ([f17a6ee](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f17a6ee84a2c8870704b78f5088b9f243c0b4a18))
* formatting and styling ([f70c0ec](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f70c0ec96f86c7177841226d416ad601a958b0d6))
* formatting, markdown and tables ([73e0ee6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/73e0ee62b8a79ca346dd1514dcc3d010ec29412f))
* minor ([f7df12f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f7df12fe2162ae073cbbf3a3ba99b654842330e0))
* minor markdown formatting ([a46dce0](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a46dce094bd4705cc27109ec61474cc7646994f5))


### Docs

* renaming menu items according to requirements. ([00c38c8](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/00c38c863a76d3bee198e888865add87cf21c0bc))


### Refactoring

* adding some blank spaces. ([d9046f9](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d9046f9a09e7f54b62ef6dfe157c9034871cd7c0))
* addressing review comments. ([30e5950](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/30e5950c9cf95e777b63442714ca10e21d99c4b1))
* fixing console errors. ([8020773](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/80207739fa1c1185dc41077fd200bf37479bebec))
* optimize back to top arrow on website ([534418c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/534418c6bafa4771d17d6417d66a8b8f92a6a5d6))
* renaming a method name. ([78c9380](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/78c93807f808a896a9768a79f07186a382b4fdf1))

## [1.5.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.4.0...v1.5.0) (2021-08-31)


### Features

* add landing message to search page ([f7b586d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f7b586de2cb91f93dfb9f668019bd6e69169b499))
* add list of manuscripts to a single page via API ([8a230c7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/8a230c70e1cfa9fc4bdb2e969a168be6fb1d62c3))
* add Menue Item "Arabic Translation" to Meta Edition ([1bccb04](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1bccb04d82e6d7e60fbfee1fd4e648855912bd43)), closes [#118](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/118)
* add menue item "Syriac Translation" to meta edition ([5b8cd4c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5b8cd4cb40711badf93b4591ce2bfe29d30137ca))
* control element to get back to the top from the bottom of the page ([62f4102](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/62f4102c7f060257cfb781c708e4855c8c818fd7)), closes [#110](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/110)
* image on the homepage smaller ([1fcb8f8](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1fcb8f8b4aead7d92053f81415544449b3af6044)), closes [#113](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/113)
* lists of manuscripts ([011b8fb](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/011b8fb2f63177306af8b277ed90fc24d41bf78b))


### Bug Fixes

* heading levels ([29cd66a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/29cd66aa77c611b0632a416f3c9d3c889b8fde8b))
* remove namespace, bump version ([881c115](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/881c115046dcd2ed951bf8b1d51812e0f615594f))
* remove xhtml namespace ([5f72172](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5f721729c60021df36078227c14318fb7b7e6046))
* remove xhtml prefix ([cc7712b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cc7712be4449a018a29ca3d1809b04ee0e7966d3))
* usage of wrong styling method ([5763c02](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5763c021dac8e63a282df73d42e1936534dcddd9))


### Reverts

* version bump ([63d56e8](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/63d56e834ec06efa8efca6a661cf694b1aac35e1))


### Styling

* add some styling ([c3a493e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c3a493e0d8e3e8ca5f9488a104deecd317a4f55f))


### Docs

* refine instructions on how to load content from API ([4f5783a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/4f5783a6af41c9dae2ac70f4e7ee0fa7d5af4727))


### Chore

* add missing image ([d75bd53](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d75bd53c8acf6e049a8d8bef44e80b9d008531aa)), closes [#112](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/112)
* minor formatting ([b3d877d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b3d877d683cc184b3f870023f0033336d6b5f06f))
* pictures for stemmata page ([ae283b1](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ae283b1435115c5e66bd625085fc674b3a25c46f))

## [1.4.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.3.0...v1.4.0) (2021-08-23)


### Features

* add circular progress to the search list ([fc29c07](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/fc29c0724d0b5c751c4ac2277fc20a8119548c98))
* add incremental counter to either match ([e29e28f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e29e28f72118f0a92c9d36d592ddaf0002d9c184))
* add version number to page ([dcece01](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/dcece01a25057efdb2758eaa4dc2444f028e44de)), closes [#106](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/106)
* hits per sheet optimisation ([846dd29](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/846dd2957e0d999a76057ea98d1b55645830a46a))
* only show notifaction of *no results* if there aren't any in effect; not by default ([7079a0b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7079a0b7f664062948e0d44d266e248b44fe0b3a))
* update logos on website ([b5c0b16](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b5c0b164bfd8c48ae7551bd7aef8e6481b82982d)), closes [#98](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/98)


### Bug Fixes

* adding a slash key ([a185b1e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a185b1ee96d1550bba3c68ea98159533163fe939))
* amount of total hits and distinctive sheets ([e6eb71d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e6eb71dec72ebc7a34c79a9b5d25692b44d8ef3e))
* display error message for empty search and search containing whitespace only ([20a41e0](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/20a41e080c4f211530a18654203e819d9a63767b))
* don't show an error message by default ([cba20fc](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cba20fcac6b6d15bd4fa970f5d24888d2e9acd9c))
* implement filter to fix numerus for sheet/s and manuscript/s ([9cdb2ae](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9cdb2ae81d3f8e609ab4ee465af38e60e64af5d7))
* logic to display any items on empty search ([0d3a9a2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/0d3a9a29ee84a38d602c597da2935c6cb19dec8b))
* markdown formatting ([5a25591](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5a25591e1d2093b0a56545e3746341fcfe391a22))
* modifying the method names ([ee1d451](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ee1d451d83fe37842ade267874cf42c7fb6d1473))
* navigating from tido homepage to website ([4154c09](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/4154c091399151aeebf12b828fc26ed41fff220f))
* remove word boundary for whitespace ([50f7b5c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/50f7b5c0d67a964eb9226732dbb5e1519ad5f9ad))
* removed the obsolete code. ([4ec1fc3](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/4ec1fc3422d6c0c38d9fbcd50a2c84f2bc9a9a13))
* replace page path with empty. ([19feac6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/19feac6ad665ed6bcbce31d8876225824d5eecfb))
* scroll to top after using the pagination ([3c76fd6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/3c76fd629208a01b1d63ba183f81876e1d16a0b9))
* search result list navigation error ([93a3010](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/93a30107504f1be71f34dbaead0ac3c80622dfcf))
* show pagination ony if needed ([f5010e7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f5010e739e78c8decada372c24f54debb3b4909c))
* update home page links ([b677e4c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b677e4ca0213308aabbfc12374b1834afe0d4f1f))
* update route path condition. ([d15f303](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d15f30307a25d6b611f930327247990127f4cdc7))


### Reverts

* open TIDO on the page (botched during last merge) ([2d31b77](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2d31b77cd018a97295a4e18e38d69b133ca3301e))
* Revert "Remove artifact updating" ([88fcff3](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/88fcff3e03385b5a96c99ce563e2d9b8c621efe9))
* Revert "Update .gitlab-ci.yml" ([5f6e634](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5f6e6348f3127398812e407716fd346a11ffa1e2))


### Styling

* add missing key to loop structure ([5feba24](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5feba24ce3c5aceaf2078350cd94f97e4cc900c5))
* change website color to (University) Blue ([d97a68d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d97a68d281a35925e766ce88125342577fcd86fc)), closes [#97](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/97)
* improve display of result number ([a512944](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a512944a310fcf446348a3f506e64341c34619a4))
* minor ([e758fae](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e758fae11484c33c68d5871b044dd8209531cbda))
* minor formatting / prop ordering ([f67356b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f67356b75426410156faa426ccb82597b5cd9324))
* minor formatting / whitespacing for better readability ([10388eb](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/10388eb227be3de4968cbb172f92bb2462bf5120))
* optimize heading/ structure on website ([dff5cc2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/dff5cc22096fe8e513158eb080af47d61eeef9fa)), closes [#99](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/99)
* replace dfg logo and make it clickable ([b6492be](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b6492be71a58c9fa021916fb29fea93a0ae0ee8b))
* style loading animation ([7f43a99](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7f43a999ca91eed6dad55270ea1e90ab053aa5e2))


### Refactoring

* add line break for multiple hits ([cbacab6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cbacab60c737f030a772617d0039a4077840c84d))
* add the number of distinctive manuscripts delivered by the api ([56f37bd](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/56f37bd9fb153378c4c9221db8d63dd4994c32e5))
* adding white spaces according to review comments ([2ab8fef](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2ab8fef88100e260844179cab4e8fd35d4f63919))
* fix path url ([daf9f91](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/daf9f91f71bb58f838a9d2df4a059710442c3770))
* move logic of valid query check to parent component ([561e862](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/561e86213c0a64a9852eb07f75831e9bc07d443f))
* moving the method to mixin. ([088c768](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/088c768a50d27b62741bc66058837acc366a211f))
* only render resultlist on match; not by default ([22006f2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/22006f26019416034234d114c3aaf45cab0e6cf6))
* open TIDO on same page from search results ([fbb471d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/fbb471d78dcc0411d05a0f1d908a3bdd27307138))
* provide singular / plural term (hit/s) according to the number of matches in each sheet ([4b566de](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/4b566de3c884618b6f22c85885fa7269a224100f))
* updating the paths ([9843f11](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9843f119232cf1d5f446cbfc7aee3d46feffd528))
* use try-catch-finally according to the review proposal ([dcb3361](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/dcb3361155d48aefaa2113dc777b17307f2dd9e6)), closes [/gitlab.gwdg.de/subugoe/ahiqar/website/-/merge_requests/60#note_370134](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/note_370134)
* wrap request logic into try-catch-block ([e5b0a52](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e5b0a527f56b4380c5706152f89301f880ea12e1))


### Chore

* add  "Sayings" and "Parables" as Pages under Meta Edition ([217cc6f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/217cc6f9b145cd2a77f3fa629663393da70c4ad4)), closes [#108](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/108)
* add info to the search ([6fced06](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6fced06b004dae0fcfbc2cff29234b38f20caadf)), closes [#111](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/111)
* fix some minor typos ([763bf00](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/763bf00ff8e783c5953ae6ec356de9457b394fb2))
* merge develop ([e6437a3](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e6437a334e429ed78cc53e24f5651dfef678601f))
* merge develop into working branch ([297bc76](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/297bc766764e7d9ffef70bcf29eac39c18a9c62d))
* merge latest changes from develop ([7bd6d45](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7bd6d45aa3205b05897bf872c93e09fedfad016c))
* merge latest state from develop branch ([047536f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/047536fde3aa6cdea83040344d977a3e9310218a))
* merge latest state of develop branch ([ad0439f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ad0439f8d5333842313740c080d705d7301056ec))
* merge latest state of develop branch ([4f6752c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/4f6752cc3d6ce45a8034728fae0e9d4a23dc5708))
* minor typo ([e6a17b6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e6a17b6aa1a3fe2c2d23cecff73f47eb1123f535))
* minor wording ([a14396f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a14396f1f740d754ecf965aac2c01574f4053644))
* remove log stmt ([15e6b6e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/15e6b6e4d8706fb609a8366417cb970867b56e66))
* remove log stmt ([701ac8f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/701ac8f22e369dbbf0ff2ba1882e88bc77d56a52))

## [1.3.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.2.0...v1.3.0) (2021-08-03)


### Features

* edit urls to navigate to respected page ([21442ef](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/21442ef2543f16932b3169c357f4d57ea3182974))


### Bug Fixes

* linking back to the url from where we came from ([eada1d4](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/eada1d4606d2ab0bba2748d82fc92b6c9fb261f0))


### Refactoring

* fixing the build ([2706fae](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2706fae57a34fdaa115911bed6291565eff893c3))
* remove search form in top bar ([155ba35](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/155ba3568c756055906c0a4f43a3dff2f553f5c0)), closes [#88](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/88)

## [1.2.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.1.0...v1.2.0) (2021-07-29)


### Features

* add total results display at search page ([92a6dc3](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/92a6dc3c1582145fa2f5121c9cb19d4d9d292a38)), closes [#86](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/86)
* implementing of search in header and navigating to home or search page accordingly ([a93ffe5](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a93ffe511cf7f2b914169e44a442f5fd081e3c13))
* trigger search upon enter key ([11d1f19](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/11d1f19c7f064709d184642e4b117be4c46ebd07))
* update syriac keyboard layout ([6ebe03e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6ebe03ef972e780840d14a653afc0d997422469c))


### Bug Fixes

* pages amount display ([19e557d](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/19e557d3ef2e2dcbcbbf662e6a95668f69e3f921))


### Refactoring

* adding urls to the home page links as well. ([ee69648](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ee69648eb0df04849813d78d3005fa09475c2b85))
* modified url's and logic according to the recent develop changes. ([d454806](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d4548061d6babd16366f4d1213800ebb151fd261))
* remove obsolete code ([a32156a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a32156a2f43dd0e1c7277dd40e0eda1744ae2ac5))


### Styling

* style project page ([b4c3870](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b4c3870d5ab51b7e724ee1931e9607a0ffa0e541))

## [1.1.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v1.0.1...v1.1.0) (2021-07-26)


### Features

* reuse edition links with base URL ([9e03407](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9e03407a902d18c8770d8df52039321f13bc87d2)), closes [#84](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/84)


### Chore

* changelog and version num ([711f70f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/711f70fdff2e33f5d68187990d19ccf53af65fe1))
* **release:** 1.0.2 ([6390448](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6390448f127272abb10c80c3b3135ef16737579b))


### Continuos Integration

* add verbosity ([84abac0](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/84abac02e3136001183864656c302b5076ebf9f3))
* deployment to main ([80f875b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/80f875b0dc69b393bb686a7a277a1c33d953f69f))
* extend condition for main build targeting backend ([c336e2b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c336e2b4879e426ff51825a89cb63345f543ac50))
* fix slash-ending var ([35bd816](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/35bd8169f2ea711d448e06772f7945f01192ddae))
* fix syntax ([d2358d5](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d2358d5afbbcdb6657d1a1f6338058460d257cde))
* fix var resolution in global vars ([98f3534](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/98f3534981dbee696f22420e980c9a8aa817ce7f))
* move deployment strategy ([0f8dfb0](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/0f8dfb0ecc5835afbff9cfd19d111424a9791df7))
* refine target paths ([6e4dd35](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6e4dd35a36f1ccab403a28369fe038d21809987f))
* remove alias with vars ([966bf64](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/966bf6475671e206ef8d2bbf7100b6561bb2e6ba))
* remove alias, as they do not work for unknown reasons ([e70a9cd](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e70a9cd1e6ba7e8f177affb4578adb391e5e4d51))


### Refactoring

* move apiService to utilities ([f03aa2f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f03aa2f4c89387ee506d924d336b08adbcf9f6b6))

## [1.0.2](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v0.1.0...v1.0.2) (2021-07-23)

### CI

* minor change in the pipeline to get working path on back end site

## [1.0.1](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v0.1.0...v1.0.1) (2021-07-23)

## [0.1.0](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v0.0.6...v0.1.0) (2021-07-23)


### ⚠ BREAKING CHANGES

* Pass the base URL of API server to the ApiService class and inject the viewer base
URL at search result list items instead of hardcoded strings.

### Features

* add build with base path for backend ([95d93e7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/95d93e7707b3fd11bff8bb418ff23ca4e7d8e296))
* add env variables for URLs ([3c3083f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/3c3083f991dfe756506a1eb7700bdfa4e1484f2f)), closes [#75](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/75)
* add matches to search result ([8ab0b60](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/8ab0b60783de10d91c373acd5bdbf6559812afe2)), closes [#76](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/76)
* add public path to env variable ([a0c317f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a0c317ffb95cdb60d846bcf3738f0dd0e18d898e))
* display "no search results" message ([804383c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/804383c871d6a45e8d1888b24eae3748d8069ae0)), closes [#60](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/60)
* fake logic that renders search result view ([c4b8a79](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c4b8a795f4ba3c8213f987f5aef35565521b3567)), closes [#55](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/55)
* implement keyboard with arabic and syriac letters ([7dba2b7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7dba2b7ee85c10772900e3b37d0b2e3f09080e18))
* info for the search ([f2ec6f7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f2ec6f7153c615255538c48acf21cf32e696bb76))
* search results pagination ([79b8a28](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/79b8a280be13e37f5123b59d4a62394e0b25dfda)), closes [#61](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/61)
* set real search  API URL ([61fc5f5](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/61fc5f559164b9d775df762e9a56afb33e94ad38)), closes [#66](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/66)
* update search result ([5aa6941](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5aa6941220b22a45e2bf0e869f714d114d55c59c))
* update search result item content ([263d588](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/263d5880eb68a84e3895a800547651c3baef8b89))
* update search result item link ([1ef5975](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1ef59754d8d1202bb015c4a0880973951a2dcc06))


### Bug Fixes

* ci var resolution issue (reasonable workaround) ([1364c3b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1364c3bd1312a9dd55892693ac500db8e99c4ae8)), closes [#79](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/79)
* cp for main only ([541ea11](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/541ea11ae85b9093d3877369efdad51a83e4611c))
* dependency from missing job ([c806caa](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c806caa8c2053078cae239f3206ccb9982980680))
* dependency from missing job (main) ([be4be0b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/be4be0b422f9b725ba5ea0bd53c0ce77d5ab64d0))
* faulty title on motif page ([e6b7249](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e6b72490a410d2de8fc709fca2e84d23bad53585)), closes [#64](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/64)
* keyboard position ([ace6be8](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ace6be89aa39462a33698bcfdf4c82c458203295))
* remove env variable at ApiService constructor ([c64cf70](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/c64cf70d9ad5b5563e064fe5f818eb6de2b8726b))
* runner concurrency ([e86a970](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e86a970b1055a6683adad7f6dc510e682ef90eb7))
* syntax ([93eb9ad](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/93eb9ad6138a85ca8e4e17fd248a18e526cda65d))
* var resolution ([7219957](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/7219957071de0c42b4d8cf78146ff105cfea4676))


### Styling

* expand Search Hit Data Display ([dabc28f](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/dabc28f3137f18a612b0737c1fbf283e1ad9f17c)), closes [#58](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/58)


### Continuos Integration

* get project id from predefined vars on CI ([b6b442b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b6b442b6c556498e065aedf72b1d489bb6c2431f))
* set env vars ([d5851f2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d5851f21221e3c5f12e057bd7ed095f7302fa7c0))
* update public path variable ([b3d775c](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b3d775c756fed8584582e1162023786bfe154a2c))


### Chore

* edit info texts ([816e59e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/816e59ed7a9684f37d75364ac93163dd2bde2773))
* merge develop into issue branch ([ba853ee](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ba853ee2ca7c9f76b90ccd3a0653aeef598a9e30))
* move vars to gitlab ci configuration, provide jobs picked up by back end ([db624e6](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/db624e6fbf000f9835f442b80bb13efe3f8072b3))
* nice qutation marks in ci file ([fc0aede](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/fc0aedeb7417d7d88d3234e413b2ff933d2e889a))
* typo ([f349752](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f349752e4986ed1059a72920399aa0696263f9c6))
* use npm script ([b123971](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b123971d5cb016dd01e1551a5e3d1a1f7957f2ee))

### [0.0.6](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v0.0.5...v0.0.6) (2021-06-17)


### Features

* adding link in home-page ([1753b99](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/1753b998809e39c5163f61e87d80da16fcd2fd81))
* implementation of ahikar links ([a49e42e](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/a49e42e109d886a95f6a1160d603e83b2dcce9d2))
* implementation of links to ahikar collection ([12c853a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/12c853a90abcc2962d34824c8c02df326e618fd8))
* search related ([2c84128](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/2c84128a72f841be246192f4bb03b9cf41aa1992))


### Continuos Integration

* move CI scripts to separate dir ([71479c1](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/71479c1ef643e69eab0856dd453f3ecacd936cf1))


### Docs

* add docs about stages ([aaf4692](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/aaf4692525a238cccb98f7b30478dc1bce4ea30f))
* add docs for CI scripts ([f21fe95](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f21fe95586d74b5aa6c73190b4484094ff1fd292))


### Refactoring

* contact linking and content ([d7d4705](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/d7d4705b0a9b65fc57606750a4ed9fdd77ee7118)), closes [#41](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/41)
* minor formatting ([b3216e2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b3216e24fdacac554d4cf37e8bb08e86af50a0c8))


### Chore

* add code todo remarks ([262b2de](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/262b2de5def0cccb7c23fd1e0dd44a766f40beeb))
* add de content ([e58f43a](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e58f43af0a5440e547b1d0452e5dc184a540900b))
* add de content (literatur) ([e921ec9](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e921ec9011b197bbf32a3fd94c684f1d141987a4))
* add de content (motive, stemmata) ([b857b3b](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/b857b3b675d217fd54d7aa736cad032a8c43de24))
* dE content of website (WIP) ([5de9ac0](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/5de9ac056c9b4447112f8d3128c53ae91b1cb63d))
* merge branch 'develop' into feature/[#36](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/36)-ahikar-links ([6445b63](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/6445b6357986424dafdf45e12b60bf771e508759))
* minor formatting ([04a65ee](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/04a65ee0d0507f16803206f26a954eeceddc2b50))
* remove obsolete code ([27047c2](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/27047c2f93808a32e1a30814307780176ca2490d))

### [0.0.5](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v0.0.4...v0.0.5) (2021-06-11)


### Features

* search input field ([e0e13cd](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/e0e13cdb0d9513a8f9fa4e80b2a491d58dd7e806)), closes [#44](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/44)


### Chore

* update .gitignore ([ce3f0dd](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/ce3f0dd28ef1bee182bbc97165a4f3365d2e9cb6))

### [0.0.4](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v0.0.3...v0.0.4) (2021-06-11)


### Chore

* content Meta Edition: Edition Guidelines ([f8f3942](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f8f39424abd17b9ed5ca2bea6d0a1c0e871738e7)), closes [#39](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/39)
* content Meta Edition: Motifs ([9d82523](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/9d825231eaa536af0ca49b233f026f9b532bdec5)), closes [#37](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/37)
* content: Meta Edition 6. Related Literature ([edce998](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/edce99866bd33454872ba37d12e360698922d2ce)), closes [#40](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/40)
* content: Meta Edition: Stemmata ([8e631d7](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/8e631d72e0d7d45304efafacf47e74bd45138670)), closes [#38](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/issues/38)

### [0.0.3](https://gitlab.gwdg.de/subugoe/ahiqar/website/compare/v0.0.2...v0.0.3) (2021-06-10)


### Features

* add various new pages ([cb13097](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cb130970d2bb12690b5b1c295fa12711c614395e))


### Bug Fixes

* gitlab references in changelog files ([afebb55](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/afebb5573c8a3425c38a2e4f7e815ea19d2f7c71))


### Refactoring

* menu for new page structure ([f5548ff](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/f5548ff900cf1caae716db9a7558ea43b328c7dc))
* remove obsolete pages ([cf85ace](https://gitlab.gwdg.de/subugoe/ahiqar/website/-/commit/cf85acef77f9bf5e2bec0b60a66b5338ced45b11))

### [0.0.2](https://gitlab.gwdg.de/subugoe/ahikar/website/compare/v0.0.1...v0.0.2) (2021-06-09)


### Chore

* update npm packages ([be39b24](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/be39b24a11fb6076827bc09c79f63df4f17fd079))

### 0.0.1 (2021-06-09)


### ⚠ BREAKING CHANGES

* url structure changes
* Add husky and changelog config

### Features

* add husky and changelog config ([76c4213](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/76c4213053dc2ce24a2e3dd93bfac8bc273dd0be))
* add page privacy ([67ac1fd](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/67ac1fdc5aadaaa22f78e1c8b9d588be7f834f3a))
* allow for keyboard-only navigation ([209085b](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/209085be8d0cad5916f673ddbef1b12731a4916b)), closes [#27](https://gitlab.gwdg.de/subugoe/ahikar/website/-/issues/27)
* allow for scoped scss in vue files ([54e6763](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/54e676366faf1e993ed4199ac1695ddf4f0e21ee))
* insert interim entrypoints for the different collections ([5bb9e7d](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/5bb9e7d9d4c3a4ae2bfdb212ee5fa3d1610c91a2))


### Bug Fixes

* add Meta Charset ([c59262d](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/c59262da672add653104f6daf48a8cba7d8082c3)), closes [#31](https://gitlab.gwdg.de/subugoe/ahikar/website/-/issues/31)
* fix broken DFG logo ([f688cbd](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/f688cbd238081c62e8070a775f151fcd86a435a2)), closes [#20](https://gitlab.gwdg.de/subugoe/ahikar/website/-/issues/20)
* fix Lighthouse a11y issues ([428792c](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/428792cdd64071486c25c974a7357d88d9ea0423))
* fix package.json ([f6ca7b8](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/f6ca7b88bb7abe6a97bba3c95e875cd656dd2755))
* fix typo ([7b0f12f](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/7b0f12ff590551bf717450e3cb44958c504d6624))
* hide-separators-in-contact.vue ([031d86e](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/031d86e4c9631e18e062f5db51b3a885cbd24599)), closes [#33](https://gitlab.gwdg.de/subugoe/ahikar/website/-/issues/33)
* invalid vuetify color value ([25b4964](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/25b4964025f8eaddc46946eb18b3140c7c69b4cb)), closes [#28](https://gitlab.gwdg.de/subugoe/ahikar/website/-/issues/28)
* minor ([17a62e8](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/17a62e8a051044812f922f6c02eb9d730bc1a6b4))
* remove page "Terms of Services" ([d57b7bc](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/d57b7bc30e24a9e220d219c16d354989f7bef672))
* set correct base path for GitLab Page ([c1044c4](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/c1044c49f408338a17ee0629141755eb1278a1d5))
* set-meta-viewport ([da4f714](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/da4f71434e1f59c250775f04d6d78bd3e7a77281)), closes [#29](https://gitlab.gwdg.de/subugoe/ahikar/website/-/issues/29)
* typo ([c890f1c](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/c890f1ca15f0ceca4b97bf812e0d52f2069fd73a))


### Styling

* add project colors ([4febf4e](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/4febf4ee328ece66a62e301662b16f91f1a3cfd1))


### Performance

* optimize image size on homepage ([f7e9c74](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/f7e9c74a03c0cbde52fbe51685acc37404dcdc70))


### Continuos Integration

* add missing pkg ([96cb716](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/96cb71699b7379e9b26c9833e9e5bc428eb3e54b))
* build Vuepress ([84601e1](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/84601e17dc5bb4fd01eed25b58e3f3ae02f61f51))
* first draft ([5ba5487](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/5ba54871f42622ff748bf8072672090be3cf5532))
* fix base path ([8eecb49](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/8eecb4961b8a1f509b86c6bb95da9498181af299))
* fix paths to assets ([4aa1d72](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/4aa1d72beef31e8442366f6b644f6cc7c1548cf8))
* fix typo ([b0d0686](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/b0d0686588f2f04d580ddfa694df75d3fbdfae0c))
* include subdirs in copy ([ae7e2d6](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/ae7e2d69a0b655a44cf98b986d9b9c6e16ea97c5))
* stop env before removing it ([7c211ae](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/7c211aec8386679d4f5fd396f3950e1d9e1a0f05))


### Chore

* add Hero Image Copyright Info ([53307f3](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/53307f34959da71516418de509951eb80c34bf2f)), closes [#24](https://gitlab.gwdg.de/subugoe/ahikar/website/-/issues/24)
* clean up package.json ([a62d8bb](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/a62d8bb636b08bd2dc93c5c4f2653c9935494aee)), closes [#25](https://gitlab.gwdg.de/subugoe/ahikar/website/-/issues/25)


### Docs

* add content editing info ([2492cf1](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/2492cf1da9fde564c3aa2eb2a23ad30b8273bcac))
* fix typo ([cdf4632](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/cdf4632f3d153fb45a599ea0708c75529e1ecd55))
* readme content refinement ([cc32e7b](https://gitlab.gwdg.de/subugoe/ahikar/website/-/commit/cc32e7bf2bb73475ec16dfba8cdba4563919f45f))
