---
title: Sayings
layout: Layout
hide_sidebar: true
---

# {{ $frontmatter.title }}

## In the Syriac Version of the Story of Ahiqar

Author: Simon Birol

## Introductory remarks

The following tables provide a comparative overview of the order and existence of the sayings in the story of Ahiqar. The sayings have been numbered (see below) and the various correspondences (cf. Tab. 2) have been noted on the basis of content rather than exact verbal correspondence. Biblical parallels are also listed in Tab. 1. A synopsis of the sayings has not yet been implemented and will be introduced at a later date, along with the tables for the Arabic versions.

### How to Cite This Page

Birol, Simon. „Sayings. In the Syriac Version of the Story of Ahiqar“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/sayings.html.

## Tab. 1: A comparative order of the sayings of Ahiqar in the Syriac tradition

The manuscript BnF syr. 434 does not contain any sayings. Moreover, the existing folios of BL Or. 2313 do not contain the sayings either. The sayings have been numbered and transcribed as follows:

* Nos. 1-75: Syriac text from Cambridge Add. 2020 (C)
* Nos. A1-7: Syriac text from Aleppo SCAA 7/229 (A)
* Nos. S1: Syriac text from Sachau 162 (S)
* No. U1: Syriac text from Sachau 336 (U)
* Nos. M1-5: Syriac text from Mingana syr. 433 (M)
* Nos. D1-4: Syriac text from Mosul DFM 430 (D)
* Further sigla: BnF syr. 434 (B), Notre-Dame des Semences, mss. 611 (K), Notre-Dame des Semences, mss. 612 (I), Ms. Graffin (G), BnF syr. 422 (N), Mingana syr. 433 (M), Strasbourg S4122 (T), Harvard syr. 80 (H), St. Petersburg, Sado 9 (P), Tellkepe, QACCT 135 (Q)

<ModalTrigger modal-element="#table1" trigger-element="#table1-trigger" />
<div class="modal fade" id="table1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
<div class="modal-dialog card">
<div class="modal-header">
<h2>A comparative order of the sayings of Ahiqar in the Syriac tradition</h2>
<CloseButton/>
</div>
<div class="modal-content">

<h3>Sigla</h3>

<table>
    <tr>
        <td>
            <ul>
                <li><strong>A:</strong> <a href="/manuscripts.html#aleppo-scaa-7-229" target="_blank">Aleppo SCAA 7/229</a></li>
                <li><strong>B:</strong> <a href="/manuscripts.html#paris-bibliotheque-nationale-de-france-ms-syr-434" target="_blank">BnF syr. 434</a></li>
                <li><strong>C:</strong> <a href="/manuscripts.html#cambridge-univ-cant-add-2020" target="_blank">Cambridge Add. 2020</a></li>
                <li><strong>D:</strong> <a href="/manuscripts.html#mosul-dfm-430" target="_blank">Mosul DFM 430</a></li>
                <li><strong>G:</strong> <a href="/manuscripts.html#ms-graffin" target="_blank">Ms. Graffin</a></li>
                <li><strong>H:</strong> <a href="/manuscripts.html#cambridge-ma-harvard-university-houghton-library-syr-80" target="_blank">Harvard syr. 80</a></li>
                <li><strong>I:</strong> <a href="/manuscripts.html#alqosh-notre-dame-des-semences-mss-syr-612-codex-207" target="_blank">Notre-Dame des Semences, mss. 612</a></li>
            </ul>
        </td>
        <td>
            <ul>
                <li><strong>K:</strong> <a href="/manuscripts.html#alqosh-notre-dame-des-semences-mss-syr-611-codex-205">Notre-Dame des Semences, mss. 611</a></li>
                <li><strong>M:</strong> <a href="/manuscripts.html#birmingham-mingana-syriac-433">Mingana syr. 433</a></li>
                <li><strong>N:</strong> <a href="/manuscripts.html#paris-bibliotheque-nationale-de-france-ms-syr-422-ms-pognon">BnF syr. 422</a></li>
                <li><strong>O:</strong> <a href="/manuscripts.html#london-brit-libr-or-2313">Oxford BL Or. 2313</a></li>
                <li><strong>P:</strong> <a href="/manuscripts.html#st-petersburg-sado-no-9">St. Petersburg, Sado 9</a></li>
                <li><strong>T:</strong> <a href="/manuscripts.html#bibliotheque-nationale-de-france-et-universitaire-de-strasbourg-ms-4122">Strasbourg S4122</a></li>
                <li><strong>U:</strong> <a href="/manuscripts.html#staatsbibliothek-zu-berlin-sachau-336">Sachau 336</a></li>
            </ul>
        </td>
    </tr>
</table>

<table>
        <tr class="ro1">
            <th>
                <p>Nos.</p>
            </th>
            <th>
                <p>English Translation</p>
            </th>
            <th>
                <p>Syriac</p>
            </th>
            <th>
                <p>Biblical Parallels</p>
            </th>
            <th>
                <p><abbr class="siglum C">C</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum J">J</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum A">A</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum L">L</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum S">S</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum U">U</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum M">M</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum N">N</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum T">T</abbr> &amp; <abbr class="siglum H">H</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> &amp; <abbr class="siglum I">I</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum P">P</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum D">D</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum Q">Q</abbr></p>
            </th>
        </tr>
        <tr class="ro2">
            <td>
                <p>1</p>
            </td>
            <td>
                <p>Hear, my son Nadan, and come to my understanding, and consider my words as the words of God.</p>
            </td>
            <td>
                <p>ܫܼܡܥ [ܒ]ܪܝ ܒܪܝ ܢܕܢ ܘܬܐ ܠܬܪܥܝܼܬܼܝ ܘܗܘܝܬܿ ܥܗܿܕ ܠܡ̈ܠܝ ܐܝܟ ܡܠܝ̈ ܐܠܗ[ܐ]</p>
            </td>
            <td> </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1-2</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1-2</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>19</p>
            </td>
        </tr>
        <tr class="ro3">
            <td>
                <p>2</p>
            </td>
            <td>
                <p>My son Nadan, when you have heard a word, let it die in your heart, and do not reveal it to anyone,
                    lest it become a hot coal in your mouth and burn you and you brand yourself with disgrace and
                    complain angrily against God. </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܕܢ ܐܢ ܫܼܡܥܬܿ ܡܠܬܼܐ ܬܡܘܼܬܼ ܒܠܒܟ ܘܠܐܢܫ ܠܐ ܬܓܼܠܝܗܿ ܕܠܡܐ ܬܗܘܐ ܓܡܘܼܪܬܐ ܒܦܘܼܡܟ ܘܬܼܟܼܘܝܟ ܘܡܘܼܡܐ ܬܣܝܼܡ
                    ܒܢܦܫܟ ܘܥܠ ܐܠܗܐ ܬܬܼܪܥܡ</p>
            </td>
            <td>
                <p><span class="T1">Sir 19,10:</span><span class="T2"> ܫܡܼܥܬ ܡܠܬܼܐ ܬܡܘܬ ܒܠܒܟ܂ ܠܐ ܗܘܬ ܓܐܪܐ ܕܬܒܙܥܟ
                        ܘܬܦܘܩ܂</span></p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>18</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>3</p>
            </td>
            <td>
                <p>My son, do not tell all that you hear and do not reveal all that you see. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܠ ܕܫܡܥ ܐܢܬܿ ܠܐ ܬܐܡܪ ܘܕܿܚܙܐ ܐܢܬܿ ܠܐ ܬܓܼܠܐ</p>
            </td>
            <td> </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>19</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>4</p>
            </td>
            <td>
                <p>My son, do not loosen a bond that is sealed, nor seal one that is loosened. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܣܪܐ ܕܚܬܼܝܼܡ ܠܐ ܬܫܪܐ ܘܕܫܪܐ ܠܐ ܬܚܬܼܘܿܡ</p>
            </td>
            <td> </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>A1</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>A1</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>7</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>5</p>
            </td>
            <td>
                <p>My son, do not raise your eyes and look at a woman who is bedizened and painted; do not desire her in
                    your heart. For if you give her all that is in your hands, you will find no benefit in her, and you
                    will be guilty of a sin against God. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܪܝܼܡ ܥܝ̈ܢܝܟ ܘܬܚܙܐ ܐܢܬܿܬܼܐ ܕܣܩܝܼܠܐ ܘܟܼܚܝܼܠܐ ܠܐ ܬܪܓܼܝܼܗܿ ܒܠܒܟ ܡܛܠ ܕܐܢ ܬܬܿܠ ܠܗܿ ܟܠܡܕܡ ܕܐܝܼܬܼ
                    ܒܐܝܼ̈ܕܝܟ ܡܕܡ ܝܘܼܬܼܪܢܐ ܒܗܿ ܠܐ ܡܫܟܚ ܐܢܬܿ ܘܚܛܗܐ ܠܐܠܗܐ ܬܚܘܿܒܼ</p>
            </td>
            <td> </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>66</p>
            </td>
            <td>
                <p>A1</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>N1</p>
            </td>
            <td>
                <p>M1</p>
            </td>
            <td>
                <p>M1</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>M1</p>
            </td>
            <td>
                <p>8</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>6</p>
            </td>
            <td>
                <p>My son, do not commit adultery with your friend's wife, so that others may not commit adultery with
                    your wife. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܓܼܘܿܪ ܒܐܢܬܬܼ ܚܒܼܪܟ ܕܕܠܡܐ ܢܓܼܘܼܪܘܼܢ ܐܚܪ̈ܢܐ ܒܐܢܬܿܬܼܟ</p>
            </td>
            <td> </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>66</p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>9</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>7</p>
            </td>
            <td>
                <p>My son, do not be in a hurry like the almond tree, which is the first to blossom, but whose fruit is
                    the last to ripen [lit: be eaten]. Rather, be balanced and prudent, like the mulberry tree, which
                    blooms last, but whose fruit is the first to be eaten. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܗܘܐ ܡܣܪܗܒܼܐ ܐܝܟ ܠܘܼܙܐ ܕܠܘܼܩܕܼܡ ܥܦܼܿܐ ܘܠܚܪܬܼܐ ܐܒܿܗ ܡܬܼܐܟܠ ܐܠܐ ܗܘܝ ܫܘܐ ܘܛܥܡܢ ܐܝܟ ܬܘܼܬܼܐ ܕܠܚܪܬܼܐ
                    ܥܦܿܐ ܘܠܘܼܩܕܼܡ ܐܒܿܗ ܡܬܼܐܟܠ</p>
            </td>
            <td> </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>72</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>72</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>10</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>8</p>
            </td>
            <td>
                <p>My son, lower your eyes and lower your voice, and look from under your eyelids, for a house is not
                    built by a loud voice; for if a house were built by a loud voice, a donkey would build two houses in
                    one day; and if the plough were driven by sheer force, its share would never be loosed from a
                    camel's armpit. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܪܟܢ ܥܝ̈ܢܝܟ ܘܐܡܟ ܩܠܟ ܘܚܘܼܪ ܬܚܝܬ ܥܝ̈ܢܝܟ ܡܛܠ ܕܠܘ ܒܩܠܐ ܪܡܐ ܡܬܼܒܿܢܐ ܒܝܬܐ ܕܐܠܘܼ ܒܩܠܐ ܪܡܐ ܡܬܼܒܿܢܐ ܗܘܼܐ
                    ܒܝܬܐ ܚܡܪܐ ܕܝܢ ܬܪܝܢ ܒܬ̈ܝܼܢ ܒܢܿܐ ܗܘܼܐ ܒܚܕ ܝܘܡܐ ܘܐܠܘܼ ܒܚܝܠܐ ܬܩܝܼܦܐ ܡܬܿܕܒܼܪܐ ܗܘܼܬܼ ܦܕܢܐ ܚ[ܪ]ܒܗܿ ܡܼܢ ܫܚܬܗ
                    ܕܓܡܠܐ ܠܐ ܡܫܬܪܝܐ</p>
            </td>
            <td> </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>12</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>9</p>
            </td>
            <td>
                <p>My son, it is better to roll over stones with a wise man than to drink wine with a fool. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܼ ܠܡܗܦܟܼܘܼ ܟܐ̈ܦܐ ܥܡ ܓܒܼܪܐ ܚܟܿܝܼܡܐ ܡܼܢ ܕܠܡܫܬܐ ܚܡܪܐ ܥܡ ܓܒܼܪܐ ܣܟܼܠܐ</p>
            </td>
            <td> </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>A4</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>10</p>
            </td>
            <td>
                <p>My son, pour your wine on the graves of the righteous rather than drink it with the wicked. </p>
            </td>
            <td>
                <p>ܒܼܪܝ ܐܫܘܿܕ ܚܡܪܟ ܥܠ ܩܒܼܪ̈ܐ ܕܙܕܝܼܩ̈ܐ ܘܠܐ ܬܫܬܝܘܗܝ ܥܡ ܐܢܫܐ ܥܘ̈ܿܠܐ</p>
            </td>
            <td> </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>45-46 &amp; 10</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>12</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>11</p>
            </td>
            <td>
                <p>My son, you will not be defiled with a wise man, nor will you be wise with a defiled man. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܡ ܚܟܿܝܼܡܐ ܠܐ ܬܣܪܘܿܚ ܘܥܡ ܣܪܘܿܚܐ ܠܐ ܬܬܼܚܟܡ</p>
            </td>
            <td>
                <p><span class="T1">Prov 13,20:</span><span class="T2"> ܕܡܗܠܟ ܥܡ ܚܟܝܡܐ ܢܗܘܐ ܚܟܝܡ܂ ܘܕܡܗܠܟ ܥܡ ܣܟܠܐ ܢܒܐܫ
                        ܠܗ܂</span></p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td> </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>10 (2x)</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>13</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>12</p>
            </td>
            <td>
                <p>My son, associate yourself with a wise man in order to become wise like him, and do not associate
                    yourself with a loquacious and talkative man in order not to be numbered with him. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܬܼܠܘܐ ܠܓܒܼܪܐ ܚܟܿܝܼܡܐ ܕܬܬܼܚܟܡ ܐܟܼܘܬܗ ܘܠܐ ܬܬܼܠܘܐ ܠܓܒܼܪܐ ܦܟܢܐ ܘܠܫܢܐ ܕܠܐ ܬܬܼܡܢܐ ܥܡܗ</p>
            </td>
            <td> </td>
            <td>
                <p>12</p>
            </td>
            <td> </td>
            <td>
                <p>A2</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>A5</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>13</p>
            </td>
            <td>
                <p>My son, while you have shoes on your feet, tread down the thorns and make a path for your sons and
                    grandsons. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܕ ܐܝܼܬܼ ܡܣ̈ܢܐ ܒܪ̈ܓܠܝܟ ܕܘܼܫ ܕܪܕܪ̈ܐ ܘܥܒܕ ܐܘܼܪܚܐ ܠܒܼ̈ܢܝܟ ܘܠܒܼ̈ܢܿܝ ܒ̈ܢܝܟ</p>
            </td>
            <td> </td>
            <td>
                <p>13</p>
            </td>
            <td> </td>
            <td>
                <p>A3</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>A3</p>
            </td>
            <td>
                <p>A4</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>21</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>21</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>14</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>14</p>
            </td>
            <td>
                <p>My son, the rich man has eaten a serpent, and they are saying: "He ate it for medicine". But the poor
                    ate it, and they were saying: "He ate it for his hunger". </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܪ ܥܬܿܝܼܪ̈ܐ ܐܟܠ ܚܘܝܐ ܘܐܡܪܝܼܢ ܠܐܣܝܘܼܬܼܐ ܐܟܼܠܗ ܘܐܟܼܠܗ ܒܪ ܡܣ̈ܟܿܢܐ ܘܐܡܪܝܼܢ ܕܠܟܦܢܗ ܐܟܼܠܗ</p>
            </td>
            <td> </td>
            <td>
                <p>14</p>
            </td>
            <td> </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>A4</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>A4</p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>A5</p>
            </td>
            <td>
                <p>15</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>15</p>
            </td>
            <td>
                <p>My son, eat your portion and do not reproach your friends. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܟܼܘܿܠ ܡܢܬܟ ܘܥܠ ܚܒܼܪ̈ܝܟ ܬܒܣܪ</p>
            </td>
            <td>
                <p><span class="T1">Prov 23,6:</span><span class="T2"> ܠܐ ܬܚܫܡ ܥܡ ܓܒܪܐ ܚܘܪܐ܂ ܘܠܐ ܬܪܓ ܡܢ ܡܟܘܠܬܗ܂</span>
                </p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td> </td>
            <td>
                <p>A4</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>17</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>16</p>
            </td>
            <td>
                <p>My son, do not even eat bread with him who is not ashamed. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܡ ܡܿܢ ܕܠܐ ܡܬܼܚܡܨ ܐܦܠܐ ܠܚܡܐ ܠܡܐܟܠ</p>
            </td>
            <td>
                <p><span class="T1">Prov 24,17:</span><span class="T2"> ܡܐ ܕܢܦܠ ܒܥܠܕܒܒܟ ܠܐ ܬܚܕܐ܂ ܘܡܐ ܕܡܣܬܚܦ ܠܐ ܢܕܘܨ
                        ܠܒܟ܂</span></p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td> </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>A5</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>18</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>17</p>
            </td>
            <td>
                <p>My son, do not envy your enemy's happiness, nor rejoice in his misfortune. </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܛܒܼ̈ܬܗ ܕܣܢܐܟ ܠܐ ܬܩܨܦ ܘܐܦ ܒܒܼܝܼܫ̈ܬܗ ܠܐ ܬܚܕܿܐ</p>
            </td>
            <td> </td>
            <td>
                <p>17</p>
            </td>
            <td> </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>17 (2x)</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>A5</p>
            </td>
            <td>
                <p>25</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>25</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>19</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>18</p>
            </td>
            <td>
                <p>My son, do not go near a whispering woman or one whose voice is loud. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܬܩܪܒ ܠܐܢܬܬܐ ܠܚܘܫܬܢܝܬܐ܂ ܘܠܐܝܕܐ ܕܪܡ ܩܠܗܿ</p>
            </td>
            <td> </td>
            <td>
                <p>18</p>
            </td>
            <td> </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>18 &amp; 19</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>20</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>19</p>
            </td>
            <td>
                <p>My son, do not follow the beauty of a woman, nor desire her in your heart, for the [true] beauty of a
                    woman is her mind, and the word of her mouth is her ornament. </p>
            </td>
            <td>
                <p>ܒܬܪ ܫܘܦܪܐ ܕܐܢܬܬܐ ܠܐ ܬܐܙܠ܂ ܘܠܐ ܬܪܓܼܝܼܗܿ ܒܠܒܟ ܡܛܠ ܕܫܘܼܦܪܐ ܕܐܢܬܿܬܼܐ ܛܥܡܗܿ ܘܡܠܬܼ ܦܘܼܡܗܿ ܗܕܪܗܿ</p>
            </td>
            <td> </td>
            <td>
                <p>19</p>
            </td>
            <td> </td>
            <td>
                <p>A5</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>A3 &amp; 16</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td>
                <p>21</p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>21</p>
            </td>
            <td>
                <p>21</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>20</p>
            </td>
            <td>
                <p>My son, when your enemy meets you with evil, meet him with wisdom. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܐܪܥܟ ܣܢܐܟ ܒܒܼܝܼܫܬܐ ܐܪܘܿܥܝܗܝ ܐܢܬܿ ܒܚܟܡܬܼܐ</p>
            </td>
            <td>
                <p><span class="T1">Prov 24,16:</span><span class="T2"> ܡܛܠ ܕܫܒܥ ܙܒ̈ܢܝܢ ܢܦܠ ܙܕܝܩܐ ܘܩܐܡ܂ ܘܪ̈ܫܝܥܐ ܒܒܝܫܬܐ
                        ܢܣܬܚܦܘܢ܂</span></p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td> </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>61</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>A3 &amp; 16</p>
            </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>M2</p>
            </td>
            <td>
                <p>M2</p>
            </td>
        </tr>
        <tr class="ro4">
            <td>
                <p>21</p>
            </td>
            <td>
                <p>My son, the wicked falls and does not rise, but the righteous is not shaken, for God is with him.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܦܿܠ ܥܘܠܐܼ ܘܠܐ ܩܐܡ ܘܟܐܢܐ ܠܐ ܡܬܿܬܿܙܝܼܥ ܡܛܠ ܕܐܠܗ ܥܡܗ</p>
            </td>
            <td>
                <p><span class="T1">Prov 23,13:</span><span class="T2"> ܠܐ ܬܟܠܐ ܡܪܕܘܬܐ ܡܢ ܛܠܝܐ܂ ܡܛܠ ܕܐܢ ܡܚܐ ܐܢܬ ܠܗ ܠܐ
                        ܡܐܬ܂</span></p>
            </td>
            <td>
                <p>21</p>
            </td>
            <td> </td>
            <td>
                <p>17</p>
            </td>
            <td> </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>29</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>40</p>
            </td>
            <td>
                <p>21</p>
            </td>
            <td>
                <p>61</p>
            </td>
            <td>
                <p>61</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>22</p>
            </td>
            <td>
                <p>My son, do not withhold your son from the chastisement, for the chastisement of a boy is like dung in
                    the garden, and like a bridle for a donkey or any beast, and like a fetter on the foot of a donkey.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܪܟ ܡܼܢ ܡܚ̈ܘܢ ܠܐ ܬܟܼܠܐ ܡܛܠ ܕܡܚܘ̈ܬܼܐ ܠܛܠܝܐ ܐܝܟ ܙܒܠܐ ܠܦܪܕܝܣܐܼ ܘܐܝܟ ܐܣܪܐ ܠܚܡܪܐ ܐܘ ܠܟܼܠ ܚܝܘܬܼܐ ܘܐܝܟ
                    ܚܒܼܠܐ ܒܪܓܼܠܗ ܕܚܡܪܐ</p>
            </td>
            <td>
                <p><span class="T1">Sir 30,12:</span><span class="T2"> ܟܘܦܼ ܪܫܗܼ ܥܕ ܗܼܘ ܛܠܐܼ܂ ܘܦܩܿܥ ܡܬܢ̈ܬܗܼ ܥܕ ܗܼܘ ܙܥܩܪ܂
                        ܕܠܐ ܢܥܼܫܢ ܘܢܡܼܪܕ ܡܢܟ܂</span></p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td> </td>
            <td>
                <p>18</p>
            </td>
            <td> </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>18-19</p>
            </td>
            <td>
                <p>30</p>
            </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>41</p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td>
                <p>22</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>23</p>
            </td>
            <td>
                <p>My son, subdue your son while he is a boy, before he becomes stronger than you and rebels against
                    you, and you are ashamed of all his evil deeds. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܒܼܘܿܫ ܒܪܟ ܥܕ ܗܘ ܛܠܝܐ ܥܕܠܐ ܢܥܫܢ ܡܢܟ ܘܢܡܪܕ ܥܠܝܟ ܘܒܼܟܼܠ ܣܘܼܪ̈ܚܢܘܗܝ ܬܬܼܢܟܦ</p>
            </td>
            <td> </td>
            <td>
                <p>23</p>
            </td>
            <td> </td>
            <td>
                <p>19</p>
            </td>
            <td> </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>25</p>
            </td>
            <td>
                <p>42</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>23</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>24</p>
            </td>
            <td>
                <p>My son, get a strong bull and a donkey with good hooves, but do not get a runaway slave or a thieving
                    maid, lest they cause you to lose all that you have acquired. </p>
            </td>
            <td>
                <p>ܒܪܝ ܩܢܝܼ ܬܘܪܐ ܕܡܪܒܥ ܘܚܡܪܐ ܕܦܪܣܬܿܢ ܘܠܐ ܬܩܢܐ ܥܒܼܕܐ ܥܪܘܿܩܐ ܘܐܡܬܼܐ ܓܢܒܼܬܐ ܥܠ ܕܠܐ ܟܠ ܡܕܡ ܕܩܢܝܬܿ ܡܘܒܿܕܝܼܢ
                    ܠܗ ܡܢܟ</p>
            </td>
            <td> </td>
            <td>
                <p>24</p>
            </td>
            <td> </td>
            <td>
                <p>20</p>
            </td>
            <td> </td>
            <td>
                <p>21</p>
            </td>
            <td>
                <p>21</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>33</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>43</p>
            </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>24</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>25</p>
            </td>
            <td>
                <p>My son, the words of a liar are like fat sparrows, and he who is without understanding eats them
                    [i.e. believes them]. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܠܝ ܐܢܫܐ ܕܓܠܐ ܐܝܟ ܨܦܪ̈ܐ ܫܡܝܼ̈ܢܢ ܘܡܿܢ ܕܠܝܬܿ ܠܗ ܠܒܐ ܐܟܠ ܠܗܝܢ</p>
            </td>
            <td>
                <p><span class="T1">Sir 3,4-5:</span><span class="T2"> ܘܣܐܡ ܣܝ̈ܡܬܐܼ ܡܿܢ ܕܡܿܝܩܪ ܠܐܡܗ܂ ܕܡܿܝܩܪ ܠܐܒܘܗܼܝ
                        ܢܚܼܕܐ ܡܢ ܒܪܗ܂ ܘܟܕ ܡܨܠܿܐ ܢܫܬܼܡܥܼ ܘܢܬܥܢܐ܂</span></p>
            </td>
            <td>
                <p>25</p>
            </td>
            <td> </td>
            <td>
                <p>21</p>
            </td>
            <td> </td>
            <td>
                <p>22</p>
            </td>
            <td>
                <p>M5</p>
            </td>
            <td>
                <p>21</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td>
                <p>44</p>
            </td>
            <td>
                <p>25</p>
            </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>24</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>26</p>
            </td>
            <td>
                <p>My son, do not bring upon yourself the curses of your father and mother, so that you may rejoice in
                    the blessings of your children.</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܘ̈ܛܬܼܐ ܕܐܒܼܘܼܟ ܘܕܐܡܟ ܥܠܝܟ ܠܐ ܬܝܬܿܿܐ ܕܠܡܐ ܒܛܒܼ̈ܬܼܐ ܕܒܼ̈ܢܝܟ ܠܐ ܬܚܕܿܐ</p>
            </td>
            <td> </td>
            <td>
                <p>26</p>
            </td>
            <td> </td>
            <td>
                <p>22</p>
            </td>
            <td> </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>61</p>
            </td>
            <td>
                <p>M2</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>45</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>25</p>
            </td>
            <td>
                <p>25</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>27</p>
            </td>
            <td>
                <p>My son, do not go unarmed on the way, for you do not know when your enemy may come upon you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܐܘܼܪܚܐ ܕܠܐ ܙܝܢܐ ܠܐ ܬܐܙܠ ܡܛܠ ܕܠܐ ܝܕܥ ܐܢܬܿ ܐܡܬܼܝ ܐܪܥ ܠܟ ܒܥܠܕܒܼܒܼܟ</p>
            </td>
            <td>
                <p><span class="T1">Ps 127,3-5:</span><span class="T2"> ܝܪܬܘܬܗ ܓܝܪ ܕܡܪܝܐ ܒ̈ܢܝܐ ܐܢܘܢ܂ ܐܓܪܐ ܕܦܐܪ̈ܐ
                        ܕܒܡܪܒܥܐ܂ ܐܝܟ ܓܐܪܐ ܒܐܝܕܗ ܕܚܝܠܬܢܐ܂ ܗܟܢ ܐܢܘܢ ܒܢ̈ܝ ܥܠܝܡܘܬܐ܂ ܛܘܒܘܗܝ ܠܓܒܪܐ ܕܢܡܠܐ ܩܛܪܩܗ ܡܢܗܘܢ܂ ܘܠܐ
                        ܢܒܗܬܘܢ ܟܕ ܡܡܠܠܝܢ ܥܡ ܒܥܠܕܒܒܐ ܒܬܪܥܐ܂</span></p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td> </td>
            <td>
                <p>23</p>
            </td>
            <td> </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td>
                <p>61</p>
            </td>
            <td>
                <p>37</p>
            </td>
            <td>
                <p>29</p>
            </td>
            <td>
                <p>46</p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>26</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>28</p>
            </td>
            <td>
                <p>My son, as a tree is adorned with its branches and fruit, and a mountain with trees, so is a man
                    adorned with his wife and children; and a man without brothers, wife and children is despised and
                    scorned by his enemies; and he is likened to a tree by the roadside, which every passer-by plucks
                    and every beast of the field tears off its leaves. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܟܼܡܐ ܕܗܕܝܼܪ ܐܝܼܠܢܐ ܒܣܘܟܘ̈ܗܝ ܘܒܼܐܒܿܗ ܘܛܘܼܪܐ ܕܣܥܝܼܪ ܒܐܝܼ̈ܠܢܐ ܗܟܢ ܗܕܝܼܪ ܓܒܼܪܐ ܒܐܢܬܿܬܗ ܘܒܒܼܢ̈ܘܗܝ
                    ܘܓܒܼܪܐ ܕܐܚ̈ܐ ܘܐܢܬܿܬܼܐ ܘܒܼ̈ܢܝܐ ܠܝܬܿ ܠܗ ܫܝܼܛ ܘܒܣܝܼܪ ܩܕܼܡ ܒܥܠܕܒܼܒܼܘ̈ܗܝ ܘܕܼܡܿܐ ܠܐܝܼܠܢܐ ܕܥܠ ܝܕ ܐܘܼܪܚܐ
                    ܕܟܼܠ ܕܥܒܪ ܡܢܗ ܢܣܿܒܼ ܘܟܼܠܗܿ ܚܝܘܬܼܐ ܕܕܒܼܪܐ ܛܪ̈ܦܘܗܝ ܡܬܿܪܐ</p>
            </td>
            <td> </td>
            <td>
                <p>28</p>
            </td>
            <td> </td>
            <td>
                <p>25</p>
            </td>
            <td> </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>30</p>
            </td>
            <td>
                <p>47</p>
            </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td>
                <p>27</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>29</p>
            </td>
            <td>
                <p>My son, do not say, "My lord is a fool, and I am wise," but take him in his faults, and you shall be
                    loved. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܐܡܪ ܕܡܪܝ ܣܟܠ ܘܐܢܐ ܚܟܿܝܼܡ ܐܠܐ ܠܒܼܘܿܟܝܗܝ ܒܡܘܼܡܘ̈ܗܝ ܘܬܬܼܪܚܡ</p>
            </td>
            <td> </td>
            <td>
                <p>29</p>
            </td>
            <td> </td>
            <td>
                <p>26</p>
            </td>
            <td> </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>24 (2x)</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>48</p>
            </td>
            <td>
                <p>29</p>
            </td>
            <td>
                <p>D1</p>
            </td>
            <td>
                <p>28</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>30</p>
            </td>
            <td>
                <p>My son, do not count yourself wise if others do not count you wise. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܚܫܘܿܒܼ ܢܦܫܟ ܚܟܿܝܼܡܐ ܟܕ ܐܚܪ̈ܢܐ ܠܐ ܚܫܒܿܝܼܢ ܠܟ ܚܟܿܝܼܡܐ</p>
            </td>
            <td> </td>
            <td>
                <p>30</p>
            </td>
            <td> </td>
            <td>
                <p>29</p>
            </td>
            <td> </td>
            <td>
                <p>27</p>
            </td>
            <td>
                <p>25</p>
            </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>40</p>
            </td>
            <td>
                <p>32</p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>30</p>
            </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>29</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>31</p>
            </td>
            <td>
                <p>My son, do not lie with your words before your Lord, lest He despise you and say to you: "Get out of
                    my sight!" </p>
            </td>
            <td>
                <p>ܠܐ ܬܕܓܠ ܒܡ̈ܠܝܟ ܩܕܡ ܡܪܟ ܕܠܡܐ ܬܬܒܣܪ܂ ܘܢܐܡܪ ܠܟ ܕܙܠ ܡܼܢ ܩܕܼܡ ܥܝ̈ܢܝ</p>
            </td>
            <td> </td>
            <td>
                <p>31</p>
            </td>
            <td> </td>
            <td>
                <p>31</p>
            </td>
            <td> </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>25</p>
            </td>
            <td>
                <p>41</p>
            </td>
            <td>
                <p>33</p>
            </td>
            <td>
                <p>50</p>
            </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>29</p>
            </td>
            <td>
                <p>31</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>32</p>
            </td>
            <td>
                <p>My son, let your words be true, so that your Lord may say to you: "Come near to Me," and you shall
                    live. </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܗܘ̈ܝܢ ܫܪܝܪ̈ܢ ܡܠܝܟ ܕܢܐܡܪ ܠܟ ܡܪܟ ܩܪܘܿܒܼ ܠܘܬܼܝ ܘܬܚܐ</p>
            </td>
            <td> </td>
            <td>
                <p>32</p>
            </td>
            <td> </td>
            <td>
                <p>33</p>
            </td>
            <td> </td>
            <td>
                <p>29</p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>42</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>32</p>
            </td>
            <td>
                <p>30</p>
            </td>
            <td>
                <p>31 &amp; 32</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>33</p>
            </td>
            <td>
                <p>My son, do not revile God on the day of your affliction, in order that We may not be angry with you
                    when He hears you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܝܘܿܡ ܒܝܼܫܬܟ ܠܐ ܬܨܚܐ ܠܐܠܗܐ ܕܠܡܐ ܟܕ ܢܫܡܥܟ ܢܪܓܙ ܥܠܝܟ</p>
            </td>
            <td> </td>
            <td>
                <p>33</p>
            </td>
            <td> </td>
            <td>
                <p>34</p>
            </td>
            <td> </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td>
                <p>43</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>52</p>
            </td>
            <td>
                <p>33</p>
            </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>34</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>34</p>
            </td>
            <td>
                <p>My son, do not treat your slave better than his fellow, for you do not know which of them you will
                    need in the end. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܥܒܼܕܟ ܛܒܼ ܡܼܢ ܚܒܼܪܗ ܠܐ ܬܛܐܒܼ ܡܛܠ ܕܠܐ ܝܕܥ ܐܢܬܿ ܐܝܢܐ ܡܢܗܘܿܢ ܡܬܼܒܿܥܐ ܠܟ ܠܚܪܬܼܐ</p>
            </td>
            <td> </td>
            <td>
                <p>34</p>
            </td>
            <td> </td>
            <td>
                <p>35</p>
            </td>
            <td> </td>
            <td>
                <p>33</p>
            </td>
            <td>
                <p>29-30</p>
            </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>44</p>
            </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>53</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>35</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>35</p>
            </td>
            <td>
                <p>My son, smite with stones the dog that leaves its master and follows you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܠܒܐ ܕܫܒܿܩ ܡܪܗ ܘܐܬܿܐ ܒܬܼܪܟ ܒܟܐ̈ܦܐ ܡܚܝܼܘܗܝ</p>
            </td>
            <td> </td>
            <td>
                <p>35</p>
            </td>
            <td> </td>
            <td>
                <p>36</p>
            </td>
            <td> </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>29-30</p>
            </td>
            <td>
                <p>45</p>
            </td>
            <td>
                <p>37</p>
            </td>
            <td>
                <p>54</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>36</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>36</p>
            </td>
            <td>
                <p>My son, the flock of many tracks will become the prey of the wolves. </p>
            </td>
            <td>
                <p>ܒܪܝ ܓܙܪܐ ܕܣܓܿܝܼܐܢ ܐܘܼܪ̈ܚܬܗ ܡܢܬܼܐ ܕܕܐܒܐ ܗܘܿܐ</p>
            </td>
            <td> </td>
            <td>
                <p>36</p>
            </td>
            <td> </td>
            <td>
                <p>37</p>
            </td>
            <td> </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>32</p>
            </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>46</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>37</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>37</p>
            </td>
            <td>
                <p>My son, judge righteously in your youth, that you may have honour in your old age. </p>
            </td>
            <td>
                <p>ܒܪܝ ܕܘܼܢ ܕܝܼܢܐ ܬܪܝܼܨܐ ܒܛܠܝܘܼܬܼܟ ܕܒܼܣܝܒܿܘܼܬܼܟ ܐܝܼܩܪܐ ܢܗܘܐ ܠܟ</p>
            </td>
            <td>
                <p><span class="T1">Prov 8,6:</span><span class="T2"> ܠܐ ܬܟܠܐ ܡܪܕܘܬܐ ܡܢ ܛܠܝܐ܂ ܡܛܠ ܕܐܢ ܡܚܐ ܐܢܬ ܠܗ ܠܐ
                        ܡܐܬ܂</span></p>
            </td>
            <td>
                <p>37</p>
            </td>
            <td> </td>
            <td>
                <p>38</p>
            </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>33</p>
            </td>
            <td>
                <p>31-32</p>
            </td>
            <td>
                <p>47</p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>61</p>
            </td>
            <td>
                <p>37</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>38</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>38</p>
            </td>
            <td>
                <p>My son, sweeten your tongue and make the opening of your mouth savoury, for the tail of a dog gives
                    him bread, but his mouth blows. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܚܠܐ ܠܫܢܟ ܘܐܛܥܡ ܡܦܬܚ ܦܘܼܡܟ ܡܛܠ ܕܕܘܼܢܒܿܗ ܕܟܠܒܐ ܝܗܿܒܼ ܠܗ ܠܚܡܐ ܘܦܘܼܡܗ ܡܚ̈ܘܢ</p>
            </td>
            <td> </td>
            <td>
                <p>38</p>
            </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
            <td> </td>
            <td>
                <p>41</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>33</p>
            </td>
            <td>
                <p>48</p>
            </td>
            <td>
                <p>40</p>
            </td>
            <td>
                <p>67</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>42</p>
            </td>
            <td>
                <p>39</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>39</p>
            </td>
            <td>
                <p>My son, do not let your neighbour tread on your foot, lest he tread on your throat. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܫܒܿܘܿܩ ܠܚܒܼܪܟ ܕܕܼܪܟ ܥܠ ܪܓܼܠܟ ܕܕܠܡܐ ܢܕܪܘܿܟ ܥܠ ܨܘܪܟ</p>
            </td>
            <td>
                <p><span class="T1">Prov 17,10:</span><span class="T2"> ܠܘܚܡܐ ܫܚܩ ܠܒܗ ܕܚܟܝܡܐ܂ ܘܚܠܦ ܟܐܬܐ ܡܬܢܓܕ ܣܟܠܐ ܘܠܐ
                        †ܪܓܫ†܂</span></p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td> </td>
            <td>
                <p>40 (2x)</p>
            </td>
            <td> </td>
            <td>
                <p>42</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>41</p>
            </td>
            <td>
                <p>68</p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>44</p>
            </td>
            <td>
                <p>40</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>40</p>
            </td>
            <td>
                <p>My son, strike a man with a wise speech, so that it may be in his heart like a fever in summer: for
                    even if you strike the fool with many rods, he will not understand. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܚܝܼ ܠܓܒܼܪܐ ܒܡܠܬܼܐ ܚܟܿܝܼܡܬܐ ܕܬܗܘܐ ܒܠܒܿܗ ܐܝܟ ܐܫܬܐ ܕܒܼܩܝܛܐ ܕܐܢ ܬܡܚܐ ܠܣܟܼܠܐ ܚܘܼܛܪ̈ܝܼܢ ܣܓܿ̈ܝܼܐܢ ܠܐ
                    ܝܕܥ</p>
            </td>
            <td> </td>
            <td>
                <p>40</p>
            </td>
            <td> </td>
            <td>
                <p>11</p>
            </td>
            <td> </td>
            <td>
                <p>43</p>
            </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>50</p>
            </td>
            <td>
                <p>42</p>
            </td>
            <td>
                <p>69</p>
            </td>
            <td>
                <p>40</p>
            </td>
            <td>
                <p>S1</p>
            </td>
            <td>
                <p>41</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>41</p>
            </td>
            <td>
                <p>My son, send a wise man, and give him no orders: but if thou wilt send a fool, go thy way, and do not
                    send him. </p>
            </td>
            <td>
                <p>ܒܪܝ ܫܕܪ ܚܟܿܝܼܡܐ ܘܠܐ ܬܦܩܕܝܼܘܗܝ ܘܐܢ ܣܟܼܠܐ ܡܫܕܪ ܐܢܬܿ ܙܠ ܐܢܬܿ ܘܠܐ ܬܫܕܪܝܼܘܗܝ</p>
            </td>
            <td> </td>
            <td>
                <p>41</p>
            </td>
            <td> </td>
            <td>
                <p>41</p>
            </td>
            <td> </td>
            <td>
                <p>A6</p>
            </td>
            <td>
                <p>37</p>
            </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>43</p>
            </td>
            <td>
                <p>70</p>
            </td>
            <td>
                <p>41</p>
            </td>
            <td>
                <p>45 &amp; 46</p>
            </td>
            <td>
                <p>42</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>42</p>
            </td>
            <td>
                <p>My son, test your son with bread and water, and then leave your property and goods in his hands. </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܣܐ ܒܪܟ ܒܠܚܡܐ ܘܒܼ̈ܡܝܐ ܘܗܝܕܿܝܢ ܬܫܒܿܘܿܩ ܒܐܝܼܕܼܘ̈ܗܝ ܩܢ̈ܝܢܝܟ ܘܢܟܼܣܝ̈ܟ</p>
            </td>
            <td>
                <p><span class="T1">Sir 32,11:</span><span class="T2"> ܒܥܕܢܐ ܕܦܬܘܪܐ ܠܐ ܬܣܓܐ ܠܡܡܠܠܘ܂ ܘܥܕ ܐܝܬ ܒܟ ܥܘܗܕܢܐ
                        ܦܼܛܪ ܠܒܝܬܟ܂</span></p>
            </td>
            <td>
                <p>42</p>
            </td>
            <td> </td>
            <td>
                <p>42</p>
            </td>
            <td> </td>
            <td>
                <p>44</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>37</p>
            </td>
            <td>
                <p>52</p>
            </td>
            <td>
                <p>44</p>
            </td>
            <td>
                <p>71</p>
            </td>
            <td>
                <p>42</p>
            </td>
            <td>
                <p>48</p>
            </td>
            <td>
                <p>43</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>43</p>
            </td>
            <td>
                <p>My son, withdraw first from a marriage, and do not tarry for pleasant ointments, lest they should
                    become bruises in your head. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܼܢ ܡܫܬܿܘܼܬܼܐ ܩܕܡܝܐ ܦܛܪ ܘܠܐ ܬܟܬܪ ܠܡܫܚܢ̈ܝܼܢ ܒܣܝܼܡ̈ܐ ܕܠܡܐ ܢܗܘ̈ܝܢ ܠܟ ܨܘܼ̈ܠܦܬܼܐ ܒܪܫܟ</p>
            </td>
            <td> </td>
            <td>
                <p>43</p>
            </td>
            <td> </td>
            <td>
                <p>43</p>
            </td>
            <td> </td>
            <td>
                <p>S1</p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>53</p>
            </td>
            <td>
                <p>S1</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>43</p>
            </td>
            <td>
                <p>47</p>
            </td>
            <td>
                <p>44</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>44</p>
            </td>
            <td>
                <p>My son, he whose hand is full is called wise and honourable; and he whose hand is scarce is called
                    foolish and weak. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܿܢ ܕܐܝܼܕܗ ܡܠܝܐ ܡܬܩܪܐ ܚܟܿܝܼܡܐ ܘܡܝܩܪܐ ܘܡܿܢ ܕܐܝܼܕܗ ܚܣܝܼܪܐ ܡܬܩܪܐ ܡܣܟܿܠܢܐ ܘܫܦܠܐ</p>
            </td>
            <td>
                <p><span class="T1">Sir 22,15:</span><span class="T2"> ܚܠܐ ܘܡܠܚܐ ܘܛܥܢܐ ܕܦܪܙܠܐܼ ܢܝܼܚ ܠܡܫܩܠ܇ ܡܢ ܕܠܡܥܼܡܪ ܥܡ
                        ܓܒܪܐ ܣܟܠܐ܂</span></p>
            </td>
            <td>
                <p>44</p>
            </td>
            <td> </td>
            <td>
                <p>A6</p>
            </td>
            <td> </td>
            <td>
                <p>46</p>
            </td>
            <td>
                <p>40</p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>54</p>
            </td>
            <td>
                <p>45</p>
            </td>
            <td>
                <p>73</p>
            </td>
            <td>
                <p>44</p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>S1</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>45</p>
            </td>
            <td>
                <p>My son, I have carried salt, and I have turned over lead; but I have seen nothing heavier than a debt
                    which a man has to pay without having borrowed. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܥܢܿܬܼ ܡܠܚܐ ܘܗܦܿܟܿܬܼ ܐܒܼܪܐ ܘܠܐ ܚܙܿܝܬܼ ܕܝܩܝܼܪ ܡܼܢ ܚܘܒܿܬܼܐ ܕܢܦܪܘܿܥ ܐܢܫ ܟܕ ܠܐ ܝܼܙܦ</p>
            </td>
            <td> </td>
            <td>
                <p>45</p>
            </td>
            <td> </td>
            <td>
                <p>46</p>
            </td>
            <td> </td>
            <td>
                <p>47</p>
            </td>
            <td>
                <p>41</p>
            </td>
            <td>
                <p>40</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>47</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>S1</p>
            </td>
            <td>
                <p>50</p>
            </td>
            <td>
                <p>45</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>46</p>
            </td>
            <td>
                <p>My son, I have borne iron and rolled stones, but they did not weigh so heavily on me as a man who
                    settles in his father-in-law's house. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܥܢܬܼ ܦܪܙܠܐ ܘܗܦܿܟܿܬ ܟܐ̈ܦܐ ܘܠܐ ܝܼܩܪܘ ܥܠܝ ܐܝܟ ܓܒܼܪܐ ܕܝܬܒܼ ܒܝܬܼ ܚܡܘܼܗܝ</p>
            </td>
            <td> </td>
            <td>
                <p>46</p>
            </td>
            <td> </td>
            <td>
                <p>47</p>
            </td>
            <td> </td>
            <td>
                <p>48</p>
            </td>
            <td>
                <p>U2</p>
            </td>
            <td>
                <p>41</p>
            </td>
            <td>
                <p>61</p>
            </td>
            <td>
                <p>48</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>45</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>46</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>47</p>
            </td>
            <td>
                <p>My son, teach your son hunger and thirst, so that he may manage his house according to what his eye
                    sees. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܠܦ ܟܦܢܐ ܘܨܗܝܐ ܠܒܼܪܟ ܕܐܝܟ ܕܚܙܝܐ ܥܝܢܗ ܢܕܒܪ ܒܝܬܿܗ</p>
            </td>
            <td> </td>
            <td>
                <p>47</p>
            </td>
            <td> </td>
            <td>
                <p>48</p>
            </td>
            <td> </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>42</p>
            </td>
            <td>
                <p>M3</p>
            </td>
            <td>
                <p>67</p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>47</p>
            </td>
            <td>
                <p>D1</p>
            </td>
            <td>
                <p>47</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>48</p>
            </td>
            <td>
                <p>My son, a blind man is better than one who is blind in his heart, for the blind of the eyes quickly
                    learn the way and walk in it, but the blind of the heart leave the right way and go astray. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܼ ܥܘܝܼܪ ܡܼܢ ܗܘܿ ܕܥܘܝܼܪ ܒܠܒܿܗ ܥܘܝܼܪ ܥܝ̈ܢܐ ܕܝܢ ܒܥܓܠ ܝܠܦ ܐܘܼܪܚܐ ܘܐܙܿܠ ܒܗܿ ܘܥܘܝܼܪ ܠܒܐ ܫܒܿܩ ܐܘܼܪܚܐ
                    ܬܪܝܼܨܬܐ ܘܐܙܿܠ ܒܬܼܘܫܐ</p>
            </td>
            <td>
                <p><span class="T1">Prov 27,10:</span><span class="T2"> ܪܚܡܟ ܘܪܚܡܗ ܕܐܒܘܟ ܠܐ ܬܫܒܘܩ܂ ܘܠܒܝܬ ܐܚܘܟ ܠܐ ܬܥܘܠ
                        ܒܝܘܡܐ ܕܬܒܪܟ܂ ܛܒ ܗܘ ܫܒܒܐ ܕܩܪܝܒ ܡܢ ܐܚܐ ܕܪܚܝܩ܂</span></p>
            </td>
            <td>
                <p>48</p>
            </td>
            <td> </td>
            <td>
                <p>49</p>
            </td>
            <td> </td>
            <td>
                <p>50</p>
            </td>
            <td>
                <p>43</p>
            </td>
            <td>
                <p>42</p>
            </td>
            <td>
                <p>68</p>
            </td>
            <td>
                <p>50</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>48</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>48</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>49</p>
            </td>
            <td>
                <p>My son, better is a friend who is near than a brother who is far away; and better is a good name than
                    much beauty, for a good name lasts forever, but beauty decays and fades. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܼ ܪܚܡܐ ܕܩܪܝܼܒܼ ܡܼܢ ܐܚܐ ܕܪܚܝܼܩ ܘܛܒܼ ܫܡܐ ܛܒܼܐܼ ܡܼܢ ܫܘܼܦܪܐ ܣܓܿܝܼܐܐ ܡܛܠ ܕܫܡܐ ܛܒܼܐܼ ܩܐ݂ܿܡ ܠܥܠܡ
                    ܘܫܘܼܦܪܐ ܒܠܐ ܘܡܬܚܒܠ</p>
            </td>
            <td>
                <p><span class="T1">A. Sir 30,17:</span><span class="T2"> ܦܩܚ ܠܡܡܼܬ ܡܢ ܚܝ̈ܐ ܒܝܼܫ̈ܐܼ ܘܠܡܚܬ ܠܫܝܘܠܼ ܡܢ ܟܐܒܐ
                        ܕܩܿܝܡ܂ </span><span class="T3">// B. Prov 7,2:</span><span class="T2"> ܛܒ ܠܡܐܙܠ ܠܒܝܬ ܒ̈ܟܐ܂ ܡܢ
                        ܕܠܡܐܙܠ ܠܒܝܬ ܡܫܬܘܬܐ܂ ܡܛܠ ܕܗܕܐ ܗܝ ܚܪܬܐ ܕܟܠܗܘܢ ܒ̈ܢܝ ܐܢܫܐ܂ ܘܕܚܝ ܝܗܒ ܛܒܬܐ ܠܠܒܗ܂</span></p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td> </td>
            <td>
                <p>51</p>
            </td>
            <td> </td>
            <td>
                <p>50</p>
            </td>
            <td>
                <p>44</p>
            </td>
            <td>
                <p>43</p>
            </td>
            <td>
                <p>69</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>D2</p>
            </td>
            <td>
                <p>49</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>50</p>
            </td>
            <td>
                <p>My son, better is death than life for a man who has no rest, and better is the voice of wailing in
                    the ears of a fool than singing and joy. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܼܘܼ ܡܘܬܐ ܡܼܢ ܚܝܐ ܠܓܒܼܪܐ ܕܢܝܚܐ ܠܝܬܿ ܠܗ ܘܛܒܼܘܼ ܩܠܐ ܕܐܘܼܠ̈ܝܬܼܐ ܒܐܕ̈ܢܝ ܣܟܼܠܐ ܡܢ ܙܡܪܐ ܘܚܕܘܼܬܼܐ</p>
            </td>
            <td>
                <p><span class="T1">Eccl. 9,4:</span><span class="T2"> ܟܠ ܕܢܫܬܘܬܦ ܠܟܠ ܚ̈ܝܐ ܐܝܬ ܬܘܟܠܢܐ܂ ܡܛܠ ܕܟܠܒܐ ܕܚܝ ܛܒ
                        ܗܘ ܡܢ ܐܪܝܐ ܕܡܝܝܬ܂</span></p>
            </td>
            <td>
                <p>50</p>
            </td>
            <td> </td>
            <td>
                <p>52</p>
            </td>
            <td> </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>S1</p>
            </td>
            <td>
                <p>44</p>
            </td>
            <td>
                <p>70</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>50</p>
            </td>
            <td>
                <p>D3</p>
            </td>
            <td>
                <p>50</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>51</p>
            </td>
            <td>
                <p>My son, better is a leg in your hand than a goose in another's pot, and better is a sheep near you
                    than a cow far away, and better is a sparrow in your hand than thousands on the wing, and better is
                    poverty that gathers than wealth that is scattered, and better is woollen clothing on you than fine
                    linen and silk of others. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܐ ܟܪܥܐ ܕܒܼܐܝܼܕܟ ܡܼܢ ܙܦ̮ܬܐ ܒܩܕܪܐ ܕܐܚܪ̈ܢܐ ܘܛܒܐ ܢܩܝܐ ܕܩܪܝܼܒܼܐ ܡܼܢ ܬܘܼܪܬܐ ܕܪܚܝܼܩܐ ܘܛܒܐ ܚܕܼܐ ܨܦܪܐ
                    ܕܒܼܐܝܼܕܼܟ ܡܼܢ ܐܠܦ ܕܦܪ̈ܚܢ ܘܛܒܐ ܡܣܟܿܢܘܼܬܼܐ ܕܡܟܢܫܐ ܡܼܢ ܥܘܼܬܪܐ ܕܡܒܕܪ ܘܛܒܼ ܡܐܢܐ ܕܥܡܪܐ ܕܥܠܝܟ ܡܼܢ ܒܘܼܨܐ
                    ܘܫܐܪ̈ܝܐ ܕܐܚܪ̈ܢܐ</p>
            </td>
            <td>
                <p><span class="T1">Sir 27,16:</span><span class="T2"> ܒܝܬ ܪ̈ܫܝܥܐ ܠܐ ܬܬܒ܂ ܘܡܐ ܕܓܚܟܝܼܢ ܣܿܟܪ ܐܕܢ̈ܟ܂ ܕܓܠܿܐ
                        ܪܐܙܐ ܡܘܒܕ ܗܝܡܢܘܬܗ܂ ܘܠܐ ܢܫܟܚ ܠܗ ܪܚܡܿܐ ܐܝܟ ܢܦܫܗ܂</span></p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td> </td>
            <td>
                <p>53</p>
            </td>
            <td> </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>46</p>
            </td>
            <td>
                <p>S1</p>
            </td>
            <td>
                <p>71</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>57</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>D4</p>
            </td>
            <td>
                <p>51</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>52</p>
            </td>
            <td>
                <p>My son, hold back a word in your heart, and that will make you feel well, for once you have exchanged
                    your word, you will have lost your friend. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܒܼܘܿܫ ܡܠܬܼܐ ܒܠܒܟ ܘܢܛܐܒܼ ܠܟ ܡܛܠ ܕܡܐ ܕܚܠܦܼܬܿ ܡܠܬܼܟܼ ܐܘܒܿܕܬܿ ܪܚܡܟ</p>
            </td>
            <td>
                <p>A. Sir 19,10: ܫܼܡܥܬ ܡܠܬܼܐ ܬܡܘܬ ܒܠܒܟ܂ ܠܐ ܗܘܼܬ ܓܐܪܐ ܕܬܼܒܙܥܟ ܘܬܦܘܩ܂ // B. Sir 20,17-9: ܐܠܼܐ ܐܟܘܬܗ ܢܡܝܩܘܢ
                    ܥܠܘܗܝ܂ ܐܝܟ ܡܝ̈ܐ ܕܐܫܕܝܢ ܥܠ ܫܘܥܐ ܕܟܐܦܐ܂ ܗܟܢܐ ܠܫܢܗ ܕܥܘܿܠܐ ܒܝܬ ܙܕܝܩ̈ܐ܂ ܐܝܟܢܐ ܕܠܐ ܡܫܟܚܐ ܐܼܠܝܬܐ ܕܬܬܐܟܠ ܕܠܐ
                    ܡܠܚܐܼ܂ ܗܟܢܐ܂ ܡܠܬܐ ܕܠܐ ܡܬܐܡܪܐ ܒܥܕܢܗܿ܂ </p>
            </td>
            <td>
                <p>52</p>
            </td>
            <td> </td>
            <td>
                <p>54</p>
            </td>
            <td> </td>
            <td>
                <p>52</p>
            </td>
            <td>
                <p>46</p>
            </td>
            <td>
                <p>46 (2x)</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>52</p>
            </td>
            <td>
                <p>58</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td> </td>
            <td>
                <p>52</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>53</p>
            </td>
            <td>
                <p>My son, do not let a word go out of your mouth until you have taken counsel in your heart, for it is
                    better for a man to stumble in his heart than to stumble with his tongue.</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܦܘܿܩ ܡܠܬܐ ܡܼܢ ܦܘܼܡܟ ܥܕܡܐ ܕܬܬܼܡܠܟ ܒܓܼܘ ܠܒܟ ܡܛܠ ܕܛܒܼܘܼ ܠܓܒܼܪܐ ܕܡܬܿܬܿܩܠ ܒܠܒܿܗ ܘܠܐ ܡܬܿܬܿܩܠ ܒܠܫܢܗ
                </p>
            </td>
            <td> </td>
            <td>
                <p>53</p>
            </td>
            <td> </td>
            <td>
                <p>55</p>
            </td>
            <td> </td>
            <td>
                <p>53</p>
            </td>
            <td>
                <p>47</p>
            </td>
            <td> </td>
            <td>
                <p>73</p>
            </td>
            <td>
                <p>53</p>
            </td>
            <td>
                <p>59</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td> </td>
            <td>
                <p>53</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>54</p>
            </td>
            <td>
                <p>My son, when you hear an evil word, bury it seven cubits deep in the ground. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܬܫܡܥ ܡܠܬܼܐ ܒܝܼܫܬܐ ܐܥܠܝܗܿ ܒܐܪܥܐ ܫܒܥ ܐܡ̈ܝܼܢ</p>
            </td>
            <td> </td>
            <td>
                <p>54</p>
            </td>
            <td> </td>
            <td>
                <p>A7</p>
            </td>
            <td> </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>47</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>54</p>
            </td>
            <td>
                <p>60</p>
            </td>
            <td>
                <p>52</p>
            </td>
            <td> </td>
            <td>
                <p>54 (2x)</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>55</p>
            </td>
            <td>
                <p>My son, do not linger where there is strife, for out of strife comes murder. </p>
            </td>
            <td>
                <p>ܒܪܝ ܨܝܕ ܡܨܘܼܬܼܐܼ ܠܐ ܬܟܬܪ ܡܛܠ ܕܡܼܢ ܬܟܼܬܿܘܼܫܐ ܗܘܿܐ ܩܛܠܐ</p>
            </td>
            <td> </td>
            <td>
                <p>55</p>
            </td>
            <td> </td>
            <td>
                <p>61</p>
            </td>
            <td> </td>
            <td>
                <p>A7 (2x)</p>
            </td>
            <td>
                <p>U3</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>62</p>
            </td>
            <td>
                <p>53</p>
            </td>
            <td> </td>
            <td>
                <p>55</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>56</p>
            </td>
            <td>
                <p>My son, he who does not execute a just judgement angers God. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܠ ܕܠܐ ܕܐܿܢ ܕܝܼܢܐ ܬܪܝܼܨܐ ܡܪܓܿܙ ܠܐܠܗܐ</p>
            </td>
            <td>
                <p>A. Prov 27,10: ܪܚܡܟ ܘܪܚܡܗ ܕܐܒܘܟ ܠܐ ܬܫܒܘܩ܂ ܘܠܒܝܬ ܐܚܘܟ ܠܐ ܬܥܘܠ ܒܝܘܡܐ ܕܬܒܪܟ܂ ܛܒ ܗܘ ܫܒܒܐ ܕܩܪܝܒ ܡܢ ܐܚܐ
                    ܕܪܚܝܩ܂ // B. Sir 9,10: ܠܐ ܬܫܒܘܩ ܪܚܡܿܟ ܥܬܝܩܐ܂ ܡܛܠ ܕܚܕܬܐ ܠܐ ܡܿܛܐ ܠܗ܂ ܪܚܡܿܐ ܚܕܬܐܼ ܐܝܟ ܚܡܪܐ ܚܕܬܐ܂ ܕܟܕ
                    ܢܥܿܬܩܼ ܬܫܬܝܘܗܝ܀</p>
            </td>
            <td>
                <p>56</p>
            </td>
            <td> </td>
            <td>
                <p>67</p>
            </td>
            <td> </td>
            <td>
                <p>61</p>
            </td>
            <td>
                <p>48</p>
            </td>
            <td>
                <p>M4</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>61</p>
            </td>
            <td>
                <p>63</p>
            </td>
            <td>
                <p>54</p>
            </td>
            <td> </td>
            <td>
                <p>A7</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>57</p>
            </td>
            <td>
                <p>My son, do not go away from your father's friend, lest your (own) friend join you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܬܼܪܚܩ ܡܼܢ ܪܚܡܗ ܕܐܒܼܘܼܟ ܕܠܡܐ ܪܚܡܟ ܠܐ ܡܛܿܐ ܠܟ</p>
            </td>
            <td> </td>
            <td>
                <p>57</p>
            </td>
            <td> </td>
            <td>
                <p>68</p>
            </td>
            <td> </td>
            <td>
                <p>67</p>
            </td>
            <td>
                <p>U4</p>
            </td>
            <td>
                <p>48</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>67</p>
            </td>
            <td>
                <p>W1</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td> </td>
            <td>
                <p>M5</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>58</p>
            </td>
            <td>
                <p>My son, do not go down into the garden of the nobles, nor approach the daughters of the nobles. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܓܢܬܼ ܪܘܪ̈ܒܼܢܐ ܠܐ ܬܚܘܿܬܼ ܘܠܒܼ̈ܢܬܼ ܪܘܪ̈ܒܼܢܐ ܠܐ ܬܬܼܩܪܒܼ</p>
            </td>
            <td> </td>
            <td>
                <p>58</p>
            </td>
            <td> </td>
            <td>
                <p>35</p>
            </td>
            <td> </td>
            <td>
                <p>68</p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>53</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td>
                <p>68</p>
            </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>61</p>
            </td>
            <td> </td>
            <td>
                <p>57</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>59</p>
            </td>
            <td>
                <p>My son, help your friend before the ruler, that you may save him from the lion. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܕܪ ܪܚܡܟ ܩܕܼܡ ܫܠܝܼܛܐ ܕܬܫܟܚ ܬܥܕܪܝܼܘܗܝ<span class="T1"> </span><span class="T2">ܡܼܢ ܐܪܝܐ</span></p>
            </td>
            <td>
                <p>Prov 24,17: ܡܐ ܕܢܦܠ ܒܥܠܕܒܒܟ ܠܐ ܬܚܕܐ܂ ܘܡܐ ܕܡܣܬܚܦ ܠܐ ܢܕܘܨ ܠܒܟ܂</p>
            </td>
            <td>
                <p>59</p>
            </td>
            <td> </td>
            <td>
                <p>73</p>
            </td>
            <td> </td>
            <td>
                <p>69</p>
            </td>
            <td>
                <p>49</p>
            </td>
            <td>
                <p>49 (2x)</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>69</p>
            </td>
            <td>
                <p>29</p>
            </td>
            <td>
                <p>67</p>
            </td>
            <td> </td>
            <td>
                <p>59</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>60</p>
            </td>
            <td>
                <p>My son, do not rejoice over your enemy when he dies. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܚܕܿܐ ܠܗ ܠܒܼܥܠܕܒܼܒܼܟ ܟܕ ܢܡܘܼܬܼ</p>
            </td>
            <td>
                <p>Lev 19,32: ܡܢ ܩܕܡ ܣܒܐ ܗܘܝܬ ܩܿܐܡ܂ ܘܗܘܝܬ ܡܝܩܪ ܠܡܿܢ ܕܩܫܝܫ ܡܢܟ܂</p>
            </td>
            <td>
                <p>60</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>50</p>
            </td>
            <td>
                <p>50 (2x)</p>
            </td>
            <td>
                <p>57</p>
            </td>
            <td>
                <p>70</p>
            </td>
            <td>
                <p>30</p>
            </td>
            <td>
                <p>68</p>
            </td>
            <td> </td>
            <td>
                <p>60</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>61</p>
            </td>
            <td>
                <p>My son, when you see a man who is older than you, stand up before him. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܕ ܬܚܙܐ ܓܒܼܪܐ ܕܩܫܝܼܫ ܡܢܟ ܩܘܼܡ ܡܼܢ ܩܕܼܡܘܗܝ</p>
            </td>
            <td> </td>
            <td>
                <p>61</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>73</p>
            </td>
            <td>
                <p>50</p>
            </td>
            <td>
                <p>51 (2x)</p>
            </td>
            <td>
                <p>58</p>
            </td>
            <td>
                <p>71</p>
            </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>69</p>
            </td>
            <td> </td>
            <td>
                <p>61</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>62</p>
            </td>
            <td>
                <p>My son, if the waters were to rise up without a bottom, and the sparrow fly without wings, and the
                    raven become white as snow, and the bitter become sweet as honey, then the fool would become wise.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܢܩܘܼܡܘܼܢ ܡܝ̈ܐ ܕܠܐ ܐܪܥܐ ܘܢܦܪܚ ܨܦܪܐ ܕܠܐ ܓܦܐ ܘܢܚܘܪ ܢܥܒܼܐ ܐܝܟ ܬܠܓܿܐ ܘܢܚܠܐ ܡܪܝܼܪܐ ܐܝܟ ܕܒܼܫܐ ܗܝܕܝܢ
                    ܢܬܼܚܟܡ ܣܟܼܠܐ</p>
            </td>
            <td>
                <p>Eccl 4,17: ܛܪ ܪܓܠܟ ܡܐ ܕܐܙܠ ܐܢܬ ܠܒܝܬ ܐܠܗܐ܂ ܘܩܪܘܒ ܠܡܫܡܥ܂ ܛܒ ܡܢ ܡܘܗ̈ܒܬܐ ܕܕܒܚ̈ܐ ܕܣܟ̈ܠܐ܂ ܡܛܠ ܕܠܐ ܝܕܥܝܢ
                    ܠܡܥܒܕ ܕܛܒ܀܂</p>
            </td>
            <td>
                <p>62</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>59</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>52</p>
            </td>
            <td>
                <p>59</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>33</p>
            </td>
            <td>
                <p>71</p>
            </td>
            <td> </td>
            <td>
                <p>62</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>63</p>
            </td>
            <td>
                <p>My son, if you are a priest of God, be on guard before Him, and come before Him in purity, and do not
                    depart from His presence. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܟܘܼܡܪܐ ܐܢܬܿ ܕܐܠܗܐ ܗܘܝܬܿ ܙܗܝܼܪ ܡܢܗ ܘܒܕܟܼܝܘܼܬܼܐ ܥܘܿܠ ܩܕܼܡܘܗܝ ܘܡܼܢ ܩܕܼܡܘܗܝ ܠܐ ܬܥܢܕ</p>
            </td>
            <td> </td>
            <td>
                <p>63</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>66</p>
            </td>
            <td>
                <p>51</p>
            </td>
            <td>
                <p>53</p>
            </td>
            <td>
                <p>60</p>
            </td>
            <td>
                <p>73</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td> </td>
            <td>
                <p>67</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>64</p>
            </td>
            <td>
                <p>My son, to whom God does good, honour him also. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܿܢ ܕܐܠܗܐ ܐܛܐܒܼ ܠܗ ܘܐܦ ܐܢܬܿ ܝܩܪܝܗܝ</p>
            </td>
            <td> </td>
            <td>
                <p>64</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>52</p>
            </td>
            <td>
                <p>54</p>
            </td>
            <td>
                <p>62</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>73</p>
            </td>
            <td> </td>
            <td>
                <p>D2</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>65</p>
            </td>
            <td>
                <p>My son, do not quarrel with anyone on his day, nor stand against a river in its flow. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܕܘܼܢ ܥܡ ܐܢܫ ܒܝܘܡܗ ܘܠܐ ܬܩܘܼܡ ܠܘܼܩܒܠ ܢܗܪܐ ܒܡܠܐܗ</p>
            </td>
            <td>
                <p>A. Prov 27,20: ܫܝܘܠ ܘܐܒܕܢܐ ܠܐ ܣܒܥܝܢ܂ ܗܟܢܐ ܐܦ ܥܝܢܗܘܢ ܕܒܢ̈ܝ ܐܢܫܐ ܠܐ ܣܒܥܐ // B.Eccl 1,8: ܟܠܗܘܢ ܦܬ̈ܓܡܐ
                    ܠܐܝܢ܂ † ܠܐ † ܢܣܒܥ ܓܒܪܐ ܠܡܡܠܠܘ܂ ܘܠܐ ܣܒܥܐ ܥܝܢܐ ܠܡܚܙܐ܂ ܘܠܐ ܡܠܝܐ ܐܕܢܐ ܠܡܫܡܥ܂</p>
            </td>
            <td>
                <p>65</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>53</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>63</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td>
                <p>37</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td> </td>
            <td>
                <p>65</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>66</p>
            </td>
            <td>
                <p>My son, the human eye is like a well of water, and it is not satisfied with riches until it is filled
                    with dust. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܝܢܗ ܕܒܪܢܫܐ ܐܝܟ ܡܒܿܘܼܥܐ ܕܡܝ̈ܐ ܘܠܐ ܣܒܼܥܐ ܢܟܼ̈ܣܐ ܥܕܡܐ ܕܡܠܝܐ ܥܦܪܐ</p>
            </td>
            <td> </td>
            <td>
                <p>66</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>54</p>
            </td>
            <td>
                <p>A7</p>
            </td>
            <td>
                <p>64</p>
            </td>
            <td>
                <p>57</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td> </td>
            <td>
                <p>66</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>67</p>
            </td>
            <td>
                <p>My son, if you wish to be wise, keep your tongue from lying and your hand from stealing, and you will
                    be wise. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܨܒܿܐ ܐܢܬܿ ܕܬܗܘܐ ܚܟܿܝܼܡ ܟܠܝܼ ܦܘܼܡܟ ܡܼܢ ܕܓܠܘܼܬܼܐ ܘܐܝܼܕܼܟ ܡܼܢ ܓܢܒܼܘܼܬܼܐ ܘܬܗܘܐ ܚܟܿܝܼܡܐ</p>
            </td>
            <td> </td>
            <td>
                <p>67</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>M5</p>
            </td>
            <td>
                <p>65</p>
            </td>
            <td>
                <p>58</p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td> </td>
            <td>
                <p>68</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>68</p>
            </td>
            <td>
                <p>My son, be not a mediator in a woman's marriage, for if it goes badly for her, she will curse you;
                    and if it goes well for her, she will not remember you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܡܟܼܘܼܪܝܐ ܕܐܢܬܿܬܼܐ ܠܐ ܬܗܘܐ ܡܬܡܥܡܨܢܐ ܡܛܠ ܕܐܢ ܢܒܼܐܫ ܠܗܿ ܬܠܘܼܛܟ ܘܐܢ ܢܛܐܒܼ ܠܗܿ ܠܐ ܬܬܿܕܟܪܟ</p>
            </td>
            <td> </td>
            <td>
                <p>68</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>N7</p>
            </td>
            <td>
                <p>57</p>
            </td>
            <td>
                <p>66</p>
            </td>
            <td>
                <p>59</p>
            </td>
            <td>
                <p>64</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td> </td>
            <td>
                <p>69</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>69</p>
            </td>
            <td>
                <p>My son, he who is elegant in dress is also elegant in speech, and he who is despicable in dress is
                    also despicable in speech. </p>
            </td>
            <td>
                <p>ܒܪܝ ܕܗܕܝܼܪ ܒܠܒܼܘܼܫܗ ܗܕܝܼܪ ܐܦ ܒܡܠܬܗ ܘܕܫܝܼܛ ܒܠܒܼܘܼܫܗ ܫܝܼܛ ܐܦ ܒܡܠܬܗ</p>
            </td>
            <td> </td>
            <td>
                <p>69</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>U5</p>
            </td>
            <td>
                <p>58</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>60</p>
            </td>
            <td>
                <p>65</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td> </td>
            <td>
                <p>70</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>70</p>
            </td>
            <td>
                <p>My son, if you discover a find in front of an idol, offer [the idol] its share of it. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܬܫܟܚ ܫܟܼܚܬܐ ܩܕܼܡ ܨܠܡܐ ܡܢܬܗ ܩܪܒܼ ܠܗ</p>
            </td>
            <td> </td>
            <td>
                <p>70</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>57</p>
            </td>
            <td>
                <p>59</p>
            </td>
            <td> </td>
            <td>
                <p>62</p>
            </td>
            <td>
                <p>66</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td> </td>
            <td>
                <p>71</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>71</p>
            </td>
            <td>
                <p>My son, neither the hand that was satiated and is (now) hungry will give, nor the one that was hungry
                    and is (now) satiated. </p>
            </td>
            <td>
                <p>ܒܪܝ ܬܬܿܠ ܐܝܕܐ ܕܣܒܼܥܐ ܗܘܼܬܼ ܘܟܦܢܬ ܘܠܐ ܗܝܿ ܕܟܼܦܢܐ ܗܘܼܬܼ ܘܣܒܼܥܬ</p>
            </td>
            <td>
                <p>Sir 9,8: ܐܦ̈ܝܟ ܒܐܢܬܬܐ ܕܫܦܝܪܐܼ ܠܐ ܢܚܘܪ̈ܢ܂ ܘܠܐ ܬܬܒܿܩܐ ܒܫܘܦܪܐ ܕܠܐ ܕܝܠܟ܂ ܒܫܘܦܪܗܿ ܓܝܪ ܕܐܢܬܬܐܼ ܣܓܝ̈ܐܐ ܐܒܕܘ܂
                    ܘܪܚܡܬܗܼܿ ܐܝܟ ܢܘܪܐ ܝܩܕܐ܂</p>
            </td>
            <td>
                <p>71</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>58</p>
            </td>
            <td>
                <p>60</p>
            </td>
            <td> </td>
            <td>
                <p>63</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td>
                <p>57</p>
            </td>
            <td> </td>
            <td>
                <p>35</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>72</p>
            </td>
            <td>
                <p>My son, our eyes must not look at a beautiful woman, nor must we gaze at beauty that does not belong
                    to you, for many have perished because of a woman's beauty, and love for her is like a burning fire.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܐܢܬܿܬܼܐ ܕܫܦܝܼܪܐ ܠܐ ܢܚܘܼܪ̈ܢ ܥܝ̈ܢܝܟ ܘܠܐ ܬܬܼܒܩܐ ܒܫܘܼܦܪܐ ܕܠܐ ܕܝܼܠܟ ܡܛܠ ܕܣܓܿܝܼ̈ܐܐ ܐܒܕܘ ܒܫܘܼܦܪܐ
                    ܕܐܢܬܿܬܼܐ ܘܪܚܡܬܼܗܿ ܐܝܟ ܢܘܼܪܐ ܕܝܩܕܐ</p>
            </td>
            <td>
                <p>Eccl 7,5: ܛܒ ܠܡܫܡܥ ܟܐܬܐ ܕܚ̈ܟܝܡܐ܂ ܡܢ ܓܒܪܐ ܕܫܡܥ ܙܡܪܐ ܕܣ̈ܟܠܐ܂</p>
            </td>
            <td>
                <p>72</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>59</p>
            </td>
            <td>
                <p>62</p>
            </td>
            <td> </td>
            <td>
                <p>64</p>
            </td>
            <td> </td>
            <td>
                <p>58</p>
            </td>
            <td> </td>
            <td>
                <p>73</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>73</p>
            </td>
            <td>
                <p>My son, let the wise beat you with many blows, but do not let the fool soothe you with sweet perfume.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܡܚܝܟ ܚܟܿܝܼܡܐ ܚܘܼܛܪ̈ܝܼܢ ܣܓܿܝܼ̈ܐܢ ܘܠܐ ܢܡܫܚܟ ܣܟܼܠܐ ܡܫܚܐ ܡܒܣܡܐ</p>
            </td>
            <td>
                <p>Prov 25,17: ܠܐ † ܬܬܟܒ ܪܓܠܟ ܠܒܝܬ ܪܚܡܟ܂ ܕܠܐ ܢܣܒܥܟ ܘܢܣܢܝܟ܂ †</p>
            </td>
            <td>
                <p>73</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>60</p>
            </td>
            <td>
                <p>63</p>
            </td>
            <td> </td>
            <td>
                <p>65</p>
            </td>
            <td> </td>
            <td>
                <p>59</p>
            </td>
            <td> </td>
            <td>
                <p>D2</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>74</p>
            </td>
            <td>
                <p>My son, do not let your foot run after your friend, lest he be sated with you and hate you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܪܗܛ ܪܓܼܠܟ ܠܘܬܼ ܪܚܡܟ ܕܠܐ ܢܣܒܥ ܡܢܟ ܘܢܣܢܝܟ</p>
            </td>
            <td> </td>
            <td>
                <p>74</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>62</p>
            </td>
            <td>
                <p>64</p>
            </td>
            <td> </td>
            <td>
                <p>66</p>
            </td>
            <td> </td>
            <td>
                <p>60</p>
            </td>
            <td> </td>
            <td>
                <p>D3</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>75</p>
            </td>
            <td>
                <p>My son, put not a golden ring on your finger when you have nothing, lest the fools mock you.</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܣܝܼܡ ܥܙܩܬܼܐ ܕܕܗܒܼܐ ܒܐܝܼܕܼܟ ܟܕ ܠܝܬܿ ܠܟ ܕܠܐ ܢܓܚܟܿܘܼܢ ܒܟ ܣܟܼ̈ܠܐ</p>
            </td>
            <td> </td>
            <td>
                <p>75</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>63</p>
            </td>
            <td>
                <p>65</p>
            </td>
            <td> </td>
            <td>
                <p>55</p>
            </td>
            <td> </td>
            <td>
                <p>62</p>
            </td>
            <td> </td>
            <td>
                <p>D4</p>
            </td>
        </tr>
        <tr class="ro5">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>64</p>
            </td>
            <td>
                <p>66</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>63</p>
            </td>
            <td> </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A1</p>
            </td>
            <td>
                <p>My son, words that are not yours should not come from your lips</p>
            </td>
            <td>
                <p>ܒܪܝ ܡ̈ܠܐ ܕܠܐ ܕܝܠܟ܆ ܣܦܘ̈ܬܟ ܠܐ ܢܡ̈ܠܠܢ܀</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>65</p>
            </td>
            <td>
                <p>55</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>64</p>
            </td>
            <td> </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A2</p>
            </td>
            <td>
                <p>My son, pass over the threshold of the wise [man] and go out of the house of the rich.</p>
            </td>
            <td>
                <p>ܒܪܝ ܕܘܫ ܐܣܟܘܦܬܐ ܕܚܟܝܡܐ܆ ܘܡܢ ܒܝܬܐ ܕܥܬܝܪ̈ܐ ܐܪܚܸܩ܇</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>66</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>65</p>
            </td>
            <td> </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A3</p>
            </td>
            <td>
                <p>My son, do not reveal a secret to the foolish, for they cannot keep it.</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܩܛܘܪ ܪܐܙܐ ܥܡ ܐܢ̈ܫܐ ܣܟ̈ܠܐ ܡܛܠ ܕܠܐ ܡܫܟܚܝܼܢ ܠܡܟܲܣܵܝܘܬܗ܀</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>55</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>66</p>
            </td>
            <td> </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A4</p>
            </td>
            <td>
                <p>My son, if you are about to acquire a friend, acquire him with a trial. Do not praise anyone, before
                    you have tested him. Test and then acquire!</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܩܵܢܐ ܐܢܬ ܪܵܚܡܐ܇ ܒܢܣܝܘܢܐ ܩܵܢܝܗܝ܂ ܘܥܕܠܐ<span class="T1">  </span><span
                        class="T2">ܬܸܒܩܐ܆</span><span class="T3">  </span><span class="T2">ܠܐܢܫ ܠܐ ܬܫܲܒܚ܂ ܒܩܝܼ܆ ܘܗܵܝܕܝܢ
                        ܩܢܝܼ܀</span></p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>55</p>
            </td>
            <td> </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A5</p>
            </td>
            <td>
                <p>My son, while the wind is blowing and the sea is calm from storms, set your ship straight in the
                    harbour, before the sea is troubled and the waves lie down and sink.</p>
            </td>
            <td>
                <p>ܒܪ[ܝ]<span class="T1">  </span><span class="T2">ܥܕ ܢܿܫܒ ܐܐܪ ܘܫܦܸܐ ܝܲܡܐ ܡܼܢ ܡܚܫܘ̈ܠܐ܇ ܬܪܘܨ ܠܠܡܐܢܐ ܥܕܠܐ
                        ܡܫܬܓܫ ܝܡܐ ܘܡܬܬܪܝܡܝܢ ܓ̈ܠܠܘܗܝ ܘܡܬܛܒܥܐ</span><span class="T3"> </span><span class="T2">ܐܠܦܐ ܡܼܢ
                        ܟܐܡܘ̈ܢܐ܀</span></p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>A6</p>
            </td>
            <td>
                <p>My son, for what trouble and for what woe, and for what bruising, and contention and strife? But for
                    those who tarry and multiply the drinking of wine, and for those who seek where to drink.</p>
            </td>
            <td>
                <p>ܒܪܝ܆ ܠܡܿܢ ܕܘܼܘܵܕܳܐ ܘܠܡܢ ܘܝܵܐ܂ ܘܠܡܿܢ ܨܘܠܦܵܬܐ ܘܡܨܘ̈ܬܐ ܘܚܪ̈ܝܢܐ܂ ܐܠܐ ܠܐܝܠܝܢ ܕܡܘܚܪܝܢ ܘܠܡܲܣܓܐܝܘ ܠܡܸܫܬܐ
                    ܚܡܪܐ܂ ܘܠܐܝܠܝܢ ܕܡܥܩܒܝܢ ܐܝܟܐ ܕܐܝܬ ܡܲܫܬܝܐ܀</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>A7</p>
            </td>
            <td>
                <p>Do not contend with those who are stronger than you; and put aside the word and cut out the evil; and
                    conquer the evil with a good [manner] and obtain a humble heart, patience, endurance [and] a good
                    will, and the end of this life and rest will be yours.</p>
            </td>
            <td>
                <p>ܠܐ ܬܩܘܡ ܒܚܪܝܢܐ ܠܘܼܩܒܠ ܡܿܢ ܕܥܲܫܝܢ ܡܢܟ܂ ܘܐܥܒܪ ܡܠܬܐ܂ ܘܦܣܘܩ ܒܝܫܬܐ܂ ܘܙܟܝܺ ܠܒܝܫܬܐ ܒܛܵܒܬܐ܂ ܘܰܩܢܝ ܠܟ ܠܒܐ
                    ܡܟܝ[ܟ]ܐ<span class="T1">  </span><span class="T2">ܘܢܰܓܝܪܘܬ ܪܘܚܐ ܘܡܣܝܒܪܳܢܘܬܐ ܘܨܒܝܿܢܐ ܛܒܐ܆ ܘܚܰܪܬܐ
                        ܕܗܠܝܢ ܚ̈ܝܐ ܘܢܝܿܚܐ ܢܗܘܘܢ ܠܟ܀</span></p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro5">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>S1</p>
            </td>
            <td>
                <p>My son, I have eaten bitterness and swallowed aloe, [but] I have seen nothing more bitter than
                    poverty and need.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܟܿܠܬ ܕܡܪܝܪܢ ܘܒܿܠܥܬ [ܕ]ܥܠܝܩܢ܂ ܘܠܐ ܚܙܝܿܬ ܡܕܡ ܕܡܪܝܪ ܡܢ ܡܣܟܢܘܬܐ܀</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro5">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>M1</p>
            </td>
            <td>
                <p>My son, when a doctor gets ill a doctor can cure him, and a wrathful [man] does not have a cure for
                    his illness and wounds.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܟܪܗܿ ܐܣܝܐ ܡܫܟܚ ܐܣܝܐ ܕܢܐܣܝܘܗܝ</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro6">
            <td>
                <p>M2</p>
            </td>
            <td>
                <p>My son, when they send you after people outside from you[r circle of acquaintance], do not go to the
                    mighty man, unless he will befall you with evil, because you do not know [him].</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܡܫܕܪܝܼܢ ܠܟ ܒܬܪ ܐ̄ܢܫܐ ܠܒܪ ܡܢܟ܂ ܠܐ ܬܐܙܠ ܠܘܬܼ ܓܒܼܪܐ ܪܒܿܐ ܕܠܡܐ ܢܐܪܥܟ ܒܒܼܝܼܫܬܿܐ ܡܛܠ ܕܐܢ̄ܬܿ ܠܐ ܝܕܥ
                    ܐܢ̄ܬܿ</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro6">
            <td>
                <p>M3</p>
            </td>
            <td>
                <p>If you are in need and impoverished, do not reveal your situation [lit.: secret] to your friend, so
                    that you will not be avaricious towards him.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܣܢܝܼܩܬܿ ܐܘ ܢܟܝܬܿ ܠܐ ܬܓܼܠܐ ܐ̄ܪܙܟ ܠܚܒܼܪܟ ܕܠܐ ܬܗܘܐ ܠܘܬܗ ܩܠܘܼܛܐ</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro6">
            <td>
                <p>M4</p>
            </td>
            <td>
                <p>My son, acquire an innocent and humble heart, a vestment and a mighty spirit and possess patience.
                    Acquire a good will, for there is nothing better than it in the world and you will have a restful
                    and delightful life.</p>
            </td>
            <td>
                <p>ܒܪܝ ܩܢܝܼ ܠܟ ܠܒܐ ܬܡܝܼܡܐ ܘܡܟܿܝܼܟܼܵܐ ܘܡܐܢܵܐ ܘܪܘܼܚܐ ܫܠܝܼܬܼܐ ܘܛܥܢ ܡܣܝܒܿܪܢܘܼܬܼܐ ܘܨܒܼܝܢܐ ܛܒܼܐ ܩܢܝܼ ܠܟ܂ ܒܪܡ
                    ܠܝܬܿ ܡܕܿܡ ܒܓܼܘ ܥܠܡܐ ܛܒܼ ܡܢܗܿ܂ ܘܬܚܐ ܚܝ̈ܐ ܢܝܼܚܐ ܘܒܣܝܼܡ̈ܐ</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>M5</p>
            </td>
            <td>
                <p>O my son, make your way and your speech straight, hasten for listening, but do not hasten for giving
                    an answer.</p>
            </td>
            <td>
                <p>ܐܘ [ܒܪܝ]<span class="T4"> ܫܘܐ</span><span class="T5"> </span><span class="T2">ܐܘܼܪܚܟܿ</span><span
                        class="T5"> </span><span class="T2">ܘܫܒܼܝܼܠܟ</span><span class="T6"> </span><span
                        class="T7">ܘܣܪܗܒ</span><span class="T8"> </span><span class="T9">ܠܡܫܡܥ</span><span class="T8">
                    </span><span class="T9">ܘܠܐ</span><span class="T8"> </span><span class="T9">ܠܡܬܠ</span><span
                        class="T8"> </span><span class="T9">ܬܣܪܗܒܼ</span><span class="T6"> </span><span
                        class="T7">ܦܬܼܓܼܡܐ܂</span></p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro5">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>D1</p>
            </td>
            <td>
                <p>Be humble in your youth and obedient, so that you will be honoured in old age.</p>
            </td>
            <td>
                <p>ܗܘܝܼ ܒܥܠܝܼܡܘܼܬܼܟܼ ܡܟܿܝܼܟܼܐ ܘܡܫܬܡܥܢܐ܆ ܕܒܼܣܝܒܘܼܬܼܟܼ ܬܬܼܝܩܪ</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>D2</p>
            </td>
            <td>
                <p>My son, he who was sent in true friendship received honour here and a legacy in the future.</p>
            </td>
            <td>
                <p>ܒܪܝ ܡ̇ܢ ܕܐܫܬܕܪ ܒܪܚܡܘܼܬܼܐ ܫܪܝܼܪܬܐ ܩܒܿܠ ܐܝܼܩܪܐ ܕܗܪܟܐ ܘܝܪܬܿܘܼܬܼܐ ܒܕܥ[ܬ]ܝܼܕܼ</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>D3</p>
            </td>
            <td>
                <p>My son, authority cannot withstand four [things]: armies of satraps, an evil lifestyle, a crafty
                    conscience and a flock of fraudsters.</p>
            </td>
            <td>
                <p>ܒܿܪܝ ܥܡ ܐܪ̈ܒܥ ܠܐ ܡܨܝܐ ܕܢܟܬܪ ܐܘܚܕܢܐ܂ ܚܝ̈ܠܘܬܼܐ ܕܣܛܪ̈ܦܐ܂ ܘܒܼܝܼܫܘܼܬܼ ܕܘܼܒܪܐ܂ ܘܬܐܪܬܐ ܢܟܼܝܼܠܬܐ܂ ܘܥܘܼܠܒܢ
                    ܡܪܥܝܼܬܼܐ ܘܡܣܟܿܢܐ܂܂</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>D4</p>
            </td>
            <td>
                <p>My son, four other things: the intelligent and the foolish, the rich and the poor</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܪܒܥ ܨܒܼܘܼܬܼܐ ܐ̄ܚܪ̈ܢܝܬܼܐ܂ ܝܕܼܘܿܥܐ ܘܣܟܼܠܐ܂ ܘܥܬܿܝܼܪܐ</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro5">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro5">
            <td>
                <p>W1</p>
            </td>
            <td>
                <p>My son, if you lend money to the poor, you have bought him for yourself and his children.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܬܘܙܦ ܠܡܣܟܢܐ ܟܣܦܐ܂ ܙܒܢܿܬܝܗܝ ܠܟ ܗܘ ܘܒܢܘ̈ܗܝ܀</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
    </table>

</div>
</div>
</div>

1-75: Syriac text from Cambridge Add. 2020

N: Syriac text from Aleppo SCAA 7/229

S: Syriac text from Sachau 162

B: Syriac text from Sachau 336

U: Syriac text from Mingana 433

D: Syriac text from Mosul DFM 430

The proverbs from the first part are not preserved in the manuscripts Paris BnF syr. 434 and BL Or. 2313

## Tab. 2: The distribution of the sayings of Ahiqar in the Syriac manuscripts

<ModalTrigger modal-element="#table2" trigger-element="#table2-trigger" />
<div class="modal fade" id="table2" tabindex="-1" aria-hidden="true" role="dialog">
<div class="modal-dialog card">
<div class="modal-header">
<h2>The distribution of the sayings of Ahiqar in the Syriac manuscripts</h2>
<CloseButton/>
</div>
<div class="modal-content">

<h3>Sigla</h3>

<table>
    <tr>
        <td>
            <ul>
                <li><strong>A:</strong> <a href="/manuscripts.html#aleppo-scaa-7-229" target="_blank">Aleppo SCAA 7/229</a></li>
                <li><strong>B:</strong> <a href="/manuscripts.html#paris-bibliotheque-nationale-de-france-ms-syr-434" target="_blank">BnF syr. 434</a></li>
                <li><strong>C:</strong> <a href="/manuscripts.html#cambridge-univ-cant-add-2020" target="_blank">Cambridge Add. 2020</a></li>
                <li><strong>D:</strong> <a href="/manuscripts.html#mosul-dfm-430" target="_blank">Mosul DFM 430</a></li>
                <li><strong>G:</strong> <a href="/manuscripts.html#ms-graffin" target="_blank">Ms. Graffin</a></li>
                <li><strong>H:</strong> <a href="/manuscripts.html#cambridge-ma-harvard-university-houghton-library-syr-80" target="_blank">Harvard syr. 80</a></li>
                <li><strong>I:</strong> <a href="/manuscripts.html#alqosh-notre-dame-des-semences-mss-syr-612-codex-207" target="_blank">Notre-Dame des Semences, mss. 612</a></li>
            </ul>
        </td>
        <td>
            <ul>
                <li><strong>K:</strong> <a href="/manuscripts.html#alqosh-notre-dame-des-semences-mss-syr-611-codex-205">Notre-Dame des Semences, mss. 611</a></li>
                <li><strong>M:</strong> <a href="/manuscripts.html#birmingham-mingana-syriac-433">Mingana syr. 433</a></li>
                <li><strong>N:</strong> <a href="/manuscripts.html#paris-bibliotheque-nationale-de-france-ms-syr-422-ms-pognon">BnF syr. 422</a></li>
                <li><strong>O:</strong> <a href="/manuscripts.html#london-brit-libr-or-2313">Oxford BL Or. 2313</a></li>
                <li><strong>P:</strong> <a href="/manuscripts.html#st-petersburg-sado-no-9">St. Petersburg, Sado 9</a></li>
                <li><strong>T:</strong> <a href="/manuscripts.html#bibliotheque-nationale-de-france-et-universitaire-de-strasbourg-ms-4122">Strasbourg S4122</a></li>
                <li><strong>U:</strong> <a href="/manuscripts.html#staatsbibliothek-zu-berlin-sachau-336">Sachau 336</a></li>
            </ul>
        </td>
    </tr>
</table>

<table>
        <tr class="ro1">
            <th>
                <p>Nos.</p>
            </th>
            <th>
                <p>English Translation</p>
            </th>
            <th>
                <p>Syriac</p>
            </th>
            <th>
                <p>Appearance</p>
            </th>
            <th>
                <p>Textual Witnesses</p>
            </th>
        </tr>
        <tr class="ro2">
            <td>
                <p>1</p>
            </td>
            <td>
                <p>Hear, my son Nadan, and come to my understanding, and consider my words as the words of God.</p>
            </td>
            <td>
                <p>ܫܼܡܥ [ܒ]ܪܝ ܒܪܝ ܢܕܢ ܘܬܐ ܠܬܪܥܝܼܬܼܝ ܘܗܘܝܬܿ ܥܗܿܕ ܠܡ̈ܠܝ ܐܝܟ ܡܠܝ̈ ܐܠܗ[ܐ]</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr></p>
            </td>
        </tr>
        <tr class="ro3">
            <td>
                <p>2</p>
            </td>
            <td>
                <p>My son Nadan, when you have heard a word, let it die in your heart, and do not reveal it to anyone,
                    lest it become a hot coal in your mouth and burn you and you brand yourself with disgrace and
                    complain angrily against God. </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܕܢ ܐܢ ܫܼܡܥܬܿ ܡܠܬܼܐ ܬܡܘܼܬܼ ܒܠܒܟ ܘܠܐܢܫ ܠܐ ܬܓܼܠܝܗܿ ܕܠܡܐ ܬܗܘܐ ܓܡܘܼܪܬܐ ܒܦܘܼܡܟ ܘܬܼܟܼܘܝܟ ܘܡܘܼܡܐ ܬܣܝܼܡ
                    ܒܢܦܫܟ ܘܥܠ ܐܠܗܐ ܬܬܼܪܥܡ</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>3</p>
            </td>
            <td>
                <p>My son, do not tell all that you hear and do not reveal all that you see. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܠ ܕܫܡܥ ܐܢܬܿ ܠܐ ܬܐܡܪ ܘܕܿܚܙܐ ܐܢܬܿ ܠܐ ܬܓܼܠܐ</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>4</p>
            </td>
            <td>
                <p>My son, do not loosen a bond that is sealed, nor seal one that is loosened. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܣܪܐ ܕܚܬܼܝܼܡ ܠܐ ܬܫܪܐ ܘܕܫܪܐ ܠܐ ܬܚܬܼܘܿܡ</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>5</p>
            </td>
            <td>
                <p>My son, do not raise your eyes and look at a woman who is bedizened and painted; do not desire her in
                    your heart. For if you give her all that is in your hands, you will find no benefit in her, and you
                    will be guilty of a sin against God. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܪܝܼܡ ܥܝ̈ܢܝܟ ܘܬܚܙܐ ܐܢܬܿܬܼܐ ܕܣܩܝܼܠܐ ܘܟܼܚܝܼܠܐ ܠܐ ܬܪܓܼܝܼܗܿ ܒܠܒܟ ܡܛܠ ܕܐܢ ܬܬܿܠ ܠܗܿ ܟܠܡܕܡ ܕܐܝܼܬܼ
                    ܒܐܝܼ̈ܕܝܟ ܡܕܡ ܝܘܼܬܼܪܢܐ ܒܗܿ ܠܐ ܡܫܟܚ ܐܢܬܿ ܘܚܛܗܐ ܠܐܠܗܐ ܬܚܘܿܒܼ</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>6</p>
            </td>
            <td>
                <p>My son, do not commit adultery with your friend's wife, so that others may not commit adultery with
                    your wife. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܓܼܘܿܪ ܒܐܢܬܬܼ ܚܒܼܪܟ ܕܕܠܡܐ ܢܓܼܘܼܪܘܼܢ ܐܚܪ̈ܢܐ ܒܐܢܬܿܬܼܟ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>7</p>
            </td>
            <td>
                <p>My son, do not be in a hurry like the almond tree, which is the first to blossom, but whose fruit is
                    the last to ripen [lit: be eaten]. Rather, be balanced and prudent, like the mulberry tree, which
                    blooms last, but whose fruit is the first to be eaten. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܗܘܐ ܡܣܪܗܒܼܐ ܐܝܟ ܠܘܼܙܐ ܕܠܘܼܩܕܼܡ ܥܦܼܿܐ ܘܠܚܪܬܼܐ ܐܒܿܗ ܡܬܼܐܟܠ ܐܠܐ ܗܘܝ ܫܘܐ ܘܛܥܡܢ ܐܝܟ ܬܘܼܬܼܐ ܕܠܚܪܬܼܐ
                    ܥܦܿܐ ܘܠܘܼܩܕܼܡ ܐܒܿܗ ܡܬܼܐܟܠ</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>8</p>
            </td>
            <td>
                <p>My son, lower your eyes and lower your voice, and look from under your eyelids, for a house is not
                    built by a loud voice; for if a house were built by a loud voice, a donkey would build two houses in
                    one day; and if the plough were driven by sheer force, its share would never be loosed from a
                    camel's armpit. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܪܟܢ ܥܝ̈ܢܝܟ ܘܐܡܟ ܩܠܟ ܘܚܘܼܪ ܬܚܝܬ ܥܝ̈ܢܝܟ ܡܛܠ ܕܠܘ ܒܩܠܐ ܪܡܐ ܡܬܼܒܿܢܐ ܒܝܬܐ ܕܐܠܘܼ ܒܩܠܐ ܪܡܐ ܡܬܼܒܿܢܐ ܗܘܼܐ
                    ܒܝܬܐ ܚܡܪܐ ܕܝܢ ܬܪܝܢ ܒܬ̈ܝܼܢ ܒܢܿܐ ܗܘܼܐ ܒܚܕ ܝܘܡܐ ܘܐܠܘܼ ܒܚܝܠܐ ܬܩܝܼܦܐ ܡܬܿܕܒܼܪܐ ܗܘܼܬܼ ܦܕܢܐ ܚ[ܪ]ܒܗܿ ܡܼܢ ܫܚܬܗ
                    ܕܓܡܠܐ ܠܐ ܡܫܬܪܝܐ</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>9</p>
            </td>
            <td>
                <p>My son, it is better to roll over stones with a wise man than to drink wine with a fool. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܼ ܠܡܗܦܟܼܘܼ ܟܐ̈ܦܐ ܥܡ ܓܒܼܪܐ ܚܟܿܝܼܡܐ ܡܼܢ ܕܠܡܫܬܐ ܚܡܪܐ ܥܡ ܓܒܼܪܐ ܣܟܼܠܐ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>10</p>
            </td>
            <td>
                <p>My son, pour your wine on the graves of the righteous rather than drink it with the wicked. </p>
            </td>
            <td>
                <p>ܒܼܪܝ ܐܫܘܿܕ ܚܡܪܟ ܥܠ ܩܒܼܪ̈ܐ ܕܙܕܝܼܩ̈ܐ ܘܠܐ ܬܫܬܝܘܗܝ ܥܡ ܐܢܫܐ ܥܘ̈ܿܠܐ</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> (2x), <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>11</p>
            </td>
            <td>
                <p>My son, you will not be defiled with a wise man, nor will you be wise with a defiled man. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܡ ܚܟܿܝܼܡܐ ܠܐ ܬܣܪܘܿܚ ܘܥܡ ܣܪܘܿܚܐ ܠܐ ܬܬܼܚܟܡ</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> (2x), <abbr class="siglum H">H</abbr> (2x), <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> (2x), <abbr class="siglum D">D</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>12</p>
            </td>
            <td>
                <p>My son, associate yourself with a wise man in order to become wise like him, and do not associate
                    yourself with a loquacious and talkative man in order not to be numbered with him. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܬܼܠܘܐ ܠܓܒܼܪܐ ܚܟܿܝܼܡܐ ܕܬܬܼܚܟܡ ܐܟܼܘܬܗ ܘܠܐ ܬܬܼܠܘܐ ܠܓܒܼܪܐ ܦܟܢܐ ܘܠܫܢܐ ܕܠܐ ܬܬܼܡܢܐ ܥܡܗ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> (2x), <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> (2x), <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr> (2x)</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>13</p>
            </td>
            <td>
                <p>My son, while you have shoes on your feet, tread down the thorns and make a path for your sons and
                    grandsons. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܕ ܐܝܼܬܼ ܡܣ̈ܢܐ ܒܪ̈ܓܠܝܟ ܕܘܼܫ ܕܪܕܪ̈ܐ ܘܥܒܕ ܐܘܼܪܚܐ ܠܒܼ̈ܢܝܟ ܘܠܒܼ̈ܢܿܝ ܒ̈ܢܝܟ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> (2x), <abbr class="siglum D">D</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>14</p>
            </td>
            <td>
                <p>My son, the rich man has eaten a serpent, and they are saying: "He ate it for medicine". But the poor
                    ate it, and they were saying: "He ate it for his hunger". </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܪ ܥܬܿܝܼܪ̈ܐ ܐܟܠ ܚܘܝܐ ܘܐܡܪܝܼܢ ܠܐܣܝܘܼܬܼܐ ܐܟܼܠܗ ܘܐܟܼܠܗ ܒܪ ܡܣ̈ܟܿܢܐ ܘܐܡܪܝܼܢ ܕܠܟܦܢܗ ܐܟܼܠܗ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> (2x), <abbr class="siglum D">D</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>15</p>
            </td>
            <td>
                <p>My son, eat your portion and do not reproach your friends. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܟܼܘܿܠ ܡܢܬܟ ܘܥܠ ܚܒܼܪ̈ܝܟ ܬܒܣܪ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> (2x), <abbr class="siglum D">D</abbr> (2x), <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>16</p>
            </td>
            <td>
                <p>My son, do not even eat bread with him who is not ashamed. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܡ ܡܿܢ ܕܠܐ ܡܬܼܚܡܨ ܐܦܠܐ ܠܚܡܐ ܠܡܐܟܠ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> (2x)</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>17</p>
            </td>
            <td>
                <p>My son, do not envy your enemy's happiness, nor rejoice in his misfortune. </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܛܒܼ̈ܬܗ ܕܣܢܐܟ ܠܐ ܬܩܨܦ ܘܐܦ ܒܒܼܝܼܫ̈ܬܗ ܠܐ ܬܚܕܿܐ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> (2x) <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> (2x) <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>18</p>
            </td>
            <td>
                <p>My son, do not go near a whispering woman or one whose voice is loud. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܬܩܪܒ ܠܐܢܬܬܐ ܠܚܘܫܬܢܝܬܐ܂ ܘܠܐܝܕܐ ܕܪܡ ܩܠܗܿ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> (2x), <abbr class="siglum Q">Q</abbr> (2x)</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>19</p>
            </td>
            <td>
                <p>My son, do not follow the beauty of a woman, nor desire her in your heart, for the [true] beauty of a
                    woman is her mind, and the word of her mouth is her ornament. </p>
            </td>
            <td>
                <p>ܒܬܪ ܫܘܦܪܐ ܕܐܢܬܬܐ ܠܐ ܬܐܙܠ܂ ܘܠܐ ܬܪܓܼܝܼܗܿ ܒܠܒܟ ܡܛܠ ܕܫܘܼܦܪܐ ܕܐܢܬܿܬܼܐ ܛܥܡܗܿ ܘܡܠܬܼ ܦܘܼܡܗܿ ܗܕܪܗܿ</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> (2x), <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> (2x), <abbr class="siglum M">M</abbr> (2x), <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr> (3x)</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>20</p>
            </td>
            <td>
                <p>My son, when your enemy meets you with evil, meet him with wisdom. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܐܪܥܟ ܣܢܐܟ ܒܒܼܝܼܫܬܐ ܐܪܘܿܥܝܗܝ ܐܢܬܿ ܒܚܟܡܬܼܐ</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro4">
            <td>
                <p>21</p>
            </td>
            <td>
                <p>My son, the wicked falls and does not rise, but the righteous is not shaken, for God is with him.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܦܿܠ ܥܘܠܐܼ ܘܠܐ ܩܐܡ ܘܟܐܢܐ ܠܐ ܡܬܿܬܿܙܝܼܥ ܡܛܠ ܕܐܠܗ ܥܡܗ</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>22</p>
            </td>
            <td>
                <p>My son, do not withhold your son from the chastisement, for the chastisement of a boy is like dung in
                    the garden, and like a bridle for a donkey or any beast, and like a fetter on the foot of a donkey.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܪܟ ܡܼܢ ܡܚ̈ܘܢ ܠܐ ܬܟܼܠܐ ܡܛܠ ܕܡܚܘ̈ܬܼܐ ܠܛܠܝܐ ܐܝܟ ܙܒܠܐ ܠܦܪܕܝܣܐܼ ܘܐܝܟ ܐܣܪܐ ܠܚܡܪܐ ܐܘ ܠܟܼܠ ܚܝܘܬܼܐ ܘܐܝܟ
                    ܚܒܼܠܐ ܒܪܓܼܠܗ ܕܚܡܪܐ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>23</p>
            </td>
            <td>
                <p>My son, subdue your son while he is a boy, before he becomes stronger than you and rebels against
                    you, and you are ashamed of all his evil deeds. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܒܼܘܿܫ ܒܪܟ ܥܕ ܗܘ ܛܠܝܐ ܥܕܠܐ ܢܥܫܢ ܡܢܟ ܘܢܡܪܕ ܥܠܝܟ ܘܒܼܟܼܠ ܣܘܼܪ̈ܚܢܘܗܝ ܬܬܼܢܟܦ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>24</p>
            </td>
            <td>
                <p>My son, get a strong bull and a donkey with good hooves, but do not get a runaway slave or a thieving
                    maid, lest they cause you to lose all that you have acquired. </p>
            </td>
            <td>
                <p>ܒܪܝ ܩܢܝܼ ܬܘܪܐ ܕܡܪܒܥ ܘܚܡܪܐ ܕܦܪܣܬܿܢ ܘܠܐ ܬܩܢܐ ܥܒܼܕܐ ܥܪܘܿܩܐ ܘܐܡܬܼܐ ܓܢܒܼܬܐ ܥܠ ܕܠܐ ܟܠ ܡܕܡ ܕܩܢܝܬܿ ܡܘܒܿܕܝܼܢ
                    ܠܗ ܡܢܟ</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum S">S</abbr> (2x), <abbr class="siglum U">U</abbr> (2x), <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> (2x), <abbr class="siglum Q">Q</abbr> (2x)</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>25</p>
            </td>
            <td>
                <p>My son, the words of a liar are like fat sparrows, and he who is without understanding eats them
                    [i.e. believes them]. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܠܝ ܐܢܫܐ ܕܓܠܐ ܐܝܟ ܨܦܪ̈ܐ ܫܡܝܼ̈ܢܢ ܘܡܿܢ ܕܠܝܬܿ ܠܗ ܠܒܐ ܐܟܠ ܠܗܝܢ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>26</p>
            </td>
            <td>
                <p>My son, do not bring upon yourself the curses of your father and mother, so that you may rejoice in
                    the blessings of your children.</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܘ̈ܛܬܼܐ ܕܐܒܼܘܼܟ ܘܕܐܡܟ ܥܠܝܟ ܠܐ ܬܝܬܿܿܐ ܕܠܡܐ ܒܛܒܼ̈ܬܼܐ ܕܒܼ̈ܢܝܟ ܠܐ ܬܚܕܿܐ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>27</p>
            </td>
            <td>
                <p>My son, do not go unarmed on the way, for you do not know when your enemy may come upon you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܐܘܼܪܚܐ ܕܠܐ ܙܝܢܐ ܠܐ ܬܐܙܠ ܡܛܠ ܕܠܐ ܝܕܥ ܐܢܬܿ ܐܡܬܼܝ ܐܪܥ ܠܟ ܒܥܠܕܒܼܒܼܟ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>28</p>
            </td>
            <td>
                <p>My son, as a tree is adorned with its branches and fruit, and a mountain with trees, so is a man
                    adorned with his wife and children; and a man without brothers, wife and children is despised and
                    scorned by his enemies; and he is likened to a tree by the roadside, which every passer-by plucks
                    and every beast of the field tears off its leaves. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܟܼܡܐ ܕܗܕܝܼܪ ܐܝܼܠܢܐ ܒܣܘܟܘ̈ܗܝ ܘܒܼܐܒܿܗ ܘܛܘܼܪܐ ܕܣܥܝܼܪ ܒܐܝܼ̈ܠܢܐ ܗܟܢ ܗܕܝܼܪ ܓܒܼܪܐ ܒܐܢܬܿܬܗ ܘܒܒܼܢ̈ܘܗܝ
                    ܘܓܒܼܪܐ ܕܐܚ̈ܐ ܘܐܢܬܿܬܼܐ ܘܒܼ̈ܢܝܐ ܠܝܬܿ ܠܗ ܫܝܼܛ ܘܒܣܝܼܪ ܩܕܼܡ ܒܥܠܕܒܼܒܼܘ̈ܗܝ ܘܕܼܡܿܐ ܠܐܝܼܠܢܐ ܕܥܠ ܝܕ ܐܘܼܪܚܐ
                    ܕܟܼܠ ܕܥܒܪ ܡܢܗ ܢܣܿܒܼ ܘܟܼܠܗܿ ܚܝܘܬܼܐ ܕܕܒܼܪܐ ܛܪ̈ܦܘܗܝ ܡܬܿܪܐ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> (2x), <abbr class="siglum K">K</abbr> (2x), <abbr class="siglum I">I</abbr> (2x), <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>29</p>
            </td>
            <td>
                <p>My son, do not say, "My lord is a fool, and I am wise," but take him in his faults, and you shall be
                    loved. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܐܡܪ ܕܡܪܝ ܣܟܠ ܘܐܢܐ ܚܟܿܝܼܡ ܐܠܐ ܠܒܼܘܿܟܝܗܝ ܒܡܘܼܡܘ̈ܗܝ ܘܬܬܼܪܚܡ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>30</p>
            </td>
            <td>
                <p>My son, do not count yourself wise if others do not count you wise. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܚܫܘܿܒܼ ܢܦܫܟ ܚܟܿܝܼܡܐ ܟܕ ܐܚܪ̈ܢܐ ܠܐ ܚܫܒܿܝܼܢ ܠܟ ܚܟܿܝܼܡܐ</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>31</p>
            </td>
            <td>
                <p>My son, do not lie with your words before your Lord, lest He despise you and say to you: "Get out of
                    my sight!" </p>
            </td>
            <td>
                <p>ܠܐ ܬܕܓܠ ܒܡ̈ܠܝܟ ܩܕܡ ܡܪܟ ܕܠܡܐ ܬܬܒܣܪ܂ ܘܢܐܡܪ ܠܟ ܕܙܠ ܡܼܢ ܩܕܼܡ ܥܝ̈ܢܝ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> (2x) <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr> (2x)</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>32</p>
            </td>
            <td>
                <p>My son, let your words be true, so that your Lord may say to you: "Come near to Me," and you shall
                    live. </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܗܘ̈ܝܢ ܫܪܝܪ̈ܢ ܡܠܝܟ ܕܢܐܡܪ ܠܟ ܡܪܟ ܩܪܘܿܒܼ ܠܘܬܼܝ ܘܬܚܐ</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>33</p>
            </td>
            <td>
                <p>My son, do not revile God on the day of your affliction, in order that We may not be angry with you
                    when He hears you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܝܘܿܡ ܒܝܼܫܬܟ ܠܐ ܬܨܚܐ ܠܐܠܗܐ ܕܠܡܐ ܟܕ ܢܫܡܥܟ ܢܪܓܙ ܥܠܝܟ</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>34</p>
            </td>
            <td>
                <p>My son, do not treat your slave better than his fellow, for you do not know which of them you will
                    need in the end. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܥܒܼܕܟ ܛܒܼ ܡܼܢ ܚܒܼܪܗ ܠܐ ܬܛܐܒܼ ܡܛܠ ܕܠܐ ܝܕܥ ܐܢܬܿ ܐܝܢܐ ܡܢܗܘܿܢ ܡܬܼܒܿܥܐ ܠܟ ܠܚܪܬܼܐ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro5">
            <td>
                <p>35</p>
            </td>
            <td>
                <p>My son, smite with stones the dog that leaves its master and follows you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܠܒܐ ܕܫܒܿܩ ܡܪܗ ܘܐܬܿܐ ܒܬܼܪܟ ܒܟܐ̈ܦܐ ܡܚܝܼܘܗܝ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> (2x), <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> (2x), <abbr class="siglum T">T</abbr> (2x), <abbr class="siglum H">H</abbr> (2x), <abbr class="siglum P">P</abbr> (2x), <abbr class="siglum D">D</abbr> (2x), <abbr class="siglum G">G</abbr> (2x), <abbr class="siglum K">K</abbr> (2x), <abbr class="siglum I">I</abbr> (2x), <abbr class="siglum Q">Q</abbr> (2x)</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>36</p>
            </td>
            <td>
                <p>My son, the flock of many tracks will become the prey of the wolves. </p>
            </td>
            <td>
                <p>ܒܪܝ ܓܙܪܐ ܕܣܓܿܝܼܐܢ ܐܘܼܪ̈ܚܬܗ ܡܢܬܼܐ ܕܕܐܒܐ ܗܘܿܐ</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>37</p>
            </td>
            <td>
                <p>My son, judge righteously in your youth, that you may have honour in your old age. </p>
            </td>
            <td>
                <p>ܒܪܝ ܕܘܼܢ ܕܝܼܢܐ ܬܪܝܼܨܐ ܒܛܠܝܘܼܬܼܟ ܕܒܼܣܝܒܿܘܼܬܼܟ ܐܝܼܩܪܐ ܢܗܘܐ ܠܟ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>38</p>
            </td>
            <td>
                <p>My son, sweeten your tongue and make the opening of your mouth savoury, for the tail of a dog gives
                    him bread, but his mouth blows. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܚܠܐ ܠܫܢܟ ܘܐܛܥܡ ܡܦܬܚ ܦܘܼܡܟ ܡܛܠ ܕܕܘܼܢܒܿܗ ܕܟܠܒܐ ܝܗܿܒܼ ܠܗ ܠܚܡܐ ܘܦܘܼܡܗ ܡܚ̈ܘܢ</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>39</p>
            </td>
            <td>
                <p>My son, do not let your neighbour tread on your foot, lest he tread on your throat. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܫܒܿܘܿܩ ܠܚܒܼܪܟ ܕܕܼܪܟ ܥܠ ܪܓܼܠܟ ܕܕܠܡܐ ܢܕܪܘܿܟ ܥܠ ܨܘܪܟ</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>40</p>
            </td>
            <td>
                <p>My son, strike a man with a wise speech, so that it may be in his heart like a fever in summer: for
                    even if you strike the fool with many rods, he will not understand. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܚܝܼ ܠܓܒܼܪܐ ܒܡܠܬܼܐ ܚܟܿܝܼܡܬܐ ܕܬܗܘܐ ܒܠܒܿܗ ܐܝܟ ܐܫܬܐ ܕܒܼܩܝܛܐ ܕܐܢ ܬܡܚܐ ܠܣܟܼܠܐ ܚܘܼܛܪ̈ܝܼܢ ܣܓܿ̈ܝܼܐܢ ܠܐ
                    ܝܕܥ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> (2x), <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>41</p>
            </td>
            <td>
                <p>My son, send a wise man, and give him no orders: but if thou wilt send a fool, go thy way, and do not
                    send him. </p>
            </td>
            <td>
                <p>ܒܪܝ ܫܕܪ ܚܟܿܝܼܡܐ ܘܠܐ ܬܦܩܕܝܼܘܗܝ ܘܐܢ ܣܟܼܠܐ ܡܫܕܪ ܐܢܬܿ ܙܠ ܐܢܬܿ ܘܠܐ ܬܫܕܪܝܼܘܗܝ</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>42</p>
            </td>
            <td>
                <p>My son, test your son with bread and water, and then leave your property and goods in his hands. </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܣܐ ܒܪܟ ܒܠܚܡܐ ܘܒܼ̈ܡܝܐ ܘܗܝܕܿܝܢ ܬܫܒܿܘܿܩ ܒܐܝܼܕܼܘ̈ܗܝ ܩܢ̈ܝܢܝܟ ܘܢܟܼܣܝ̈ܟ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>43</p>
            </td>
            <td>
                <p>My son, withdraw first from a marriage, and do not tarry for pleasant ointments, lest they should
                    become bruises in your head. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܼܢ ܡܫܬܿܘܼܬܼܐ ܩܕܡܝܐ ܦܛܪ ܘܠܐ ܬܟܬܪ ܠܡܫܚܢ̈ܝܼܢ ܒܣܝܼܡ̈ܐ ܕܠܡܐ ܢܗܘ̈ܝܢ ܠܟ ܨܘܼ̈ܠܦܬܼܐ ܒܪܫܟ</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>44</p>
            </td>
            <td>
                <p>My son, he whose hand is full is called wise and honourable; and he whose hand is scarce is called
                    foolish and weak. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܿܢ ܕܐܝܼܕܗ ܡܠܝܐ ܡܬܩܪܐ ܚܟܿܝܼܡܐ ܘܡܝܩܪܐ ܘܡܿܢ ܕܐܝܼܕܗ ܚܣܝܼܪܐ ܡܬܩܪܐ ܡܣܟܿܠܢܐ ܘܫܦܠܐ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>45</p>
            </td>
            <td>
                <p>My son, I have carried salt, and I have turned over lead; but I have seen nothing heavier than a debt
                    which a man has to pay without having borrowed. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܥܢܿܬܼ ܡܠܚܐ ܘܗܦܿܟܿܬܼ ܐܒܼܪܐ ܘܠܐ ܚܙܿܝܬܼ ܕܝܩܝܼܪ ܡܼܢ ܚܘܒܿܬܼܐ ܕܢܦܪܘܿܥ ܐܢܫ ܟܕ ܠܐ ܝܼܙܦ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>46</p>
            </td>
            <td>
                <p>My son, I have borne iron and rolled stones, but they did not weigh so heavily on me as a man who
                    settles in his father-in-law's house. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܥܢܬܼ ܦܪܙܠܐ ܘܗܦܿܟܿܬ ܟܐ̈ܦܐ ܘܠܐ ܝܼܩܪܘ ܥܠܝ ܐܝܟ ܓܒܼܪܐ ܕܝܬܒܼ ܒܝܬܼ ܚܡܘܼܗܝ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> (2x), <abbr class="siglum M">M</abbr> (2x), <abbr class="siglum N">N</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>47</p>
            </td>
            <td>
                <p>My son, teach your son hunger and thirst, so that he may manage his house according to what his eye
                    sees. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܠܦ ܟܦܢܐ ܘܨܗܝܐ ܠܒܼܪܟ ܕܐܝܟ ܕܚܙܝܐ ܥܝܢܗ ܢܕܒܪ ܒܝܬܿܗ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>48</p>
            </td>
            <td>
                <p>My son, a blind man is better than one who is blind in his heart, for the blind of the eyes quickly
                    learn the way and walk in it, but the blind of the heart leave the right way and go astray. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܼ ܥܘܝܼܪ ܡܼܢ ܗܘܿ ܕܥܘܝܼܪ ܒܠܒܿܗ ܥܘܝܼܪ ܥܝ̈ܢܐ ܕܝܢ ܒܥܓܠ ܝܠܦ ܐܘܼܪܚܐ ܘܐܙܿܠ ܒܗܿ ܘܥܘܝܼܪ ܠܒܐ ܫܒܿܩ ܐܘܼܪܚܐ
                    ܬܪܝܼܨܬܐ ܘܐܙܿܠ ܒܬܼܘܫܐ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>49</p>
            </td>
            <td>
                <p>My son, better is a friend who is near than a brother who is far away; and better is a good name than
                    much beauty, for a good name lasts forever, but beauty decays and fades. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܼ ܪܚܡܐ ܕܩܪܝܼܒܼ ܡܼܢ ܐܚܐ ܕܪܚܝܼܩ ܘܛܒܼ ܫܡܐ ܛܒܼܐܼ ܡܼܢ ܫܘܼܦܪܐ ܣܓܿܝܼܐܐ ܡܛܠ ܕܫܡܐ ܛܒܼܐܼ ܩܐ݂ܿܡ ܠܥܠܡ
                    ܘܫܘܼܦܪܐ ܒܠܐ ܘܡܬܚܒܠ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> (2x) <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>50</p>
            </td>
            <td>
                <p>My son, better is death than life for a man who has no rest, and better is the voice of wailing in
                    the ears of a fool than singing and joy. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܼܘܼ ܡܘܬܐ ܡܼܢ ܚܝܐ ܠܓܒܼܪܐ ܕܢܝܚܐ ܠܝܬܿ ܠܗ ܘܛܒܼܘܼ ܩܠܐ ܕܐܘܼܠ̈ܝܬܼܐ ܒܐܕ̈ܢܝ ܣܟܼܠܐ ܡܢ ܙܡܪܐ ܘܚܕܘܼܬܼܐ</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> (2x), <abbr class="siglum S">S</abbr> (2x), <abbr class="siglum U">U</abbr> (2x), <abbr class="siglum M">M</abbr> (2x), <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>51</p>
            </td>
            <td>
                <p>My son, better is a leg in your hand than a goose in another's pot, and better is a sheep near you
                    than a cow far away, and better is a sparrow in your hand than thousands on the wing, and better is
                    poverty that gathers than wealth that is scattered, and better is woollen clothing on you than fine
                    linen and silk of others. </p>
            </td>
            <td>
                <p>ܒܪܝ ܛܒܐ ܟܪܥܐ ܕܒܼܐܝܼܕܟ ܡܼܢ ܙܦ̮ܬܐ ܒܩܕܪܐ ܕܐܚܪ̈ܢܐ ܘܛܒܐ ܢܩܝܐ ܕܩܪܝܼܒܼܐ ܡܼܢ ܬܘܼܪܬܐ ܕܪܚܝܼܩܐ ܘܛܒܐ ܚܕܼܐ ܨܦܪܐ
                    ܕܒܼܐܝܼܕܼܟ ܡܼܢ ܐܠܦ ܕܦܪ̈ܚܢ ܘܛܒܐ ܡܣܟܿܢܘܼܬܼܐ ܕܡܟܢܫܐ ܡܼܢ ܥܘܼܬܪܐ ܕܡܒܕܪ ܘܛܒܼ ܡܐܢܐ ܕܥܡܪܐ ܕܥܠܝܟ ܡܼܢ ܒܘܼܨܐ
                    ܘܫܐܪ̈ܝܐ ܕܐܚܪ̈ܢܐ</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> (2x), <abbr class="siglum U">U</abbr> (2x), <abbr class="siglum M">M</abbr> (2x), <abbr class="siglum T">T</abbr> (3x), <abbr class="siglum H">H</abbr> (3x), <abbr class="siglum P">P</abbr> (3x), <abbr class="siglum D">D</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>52</p>
            </td>
            <td>
                <p>My son, hold back a word in your heart, and that will make you feel well, for once you have exchanged
                    your word, you will have lost your friend. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܒܼܘܿܫ ܡܠܬܼܐ ܒܠܒܟ ܘܢܛܐܒܼ ܠܟ ܡܛܠ ܕܡܐ ܕܚܠܦܼܬܿ ܡܠܬܼܟܼ ܐܘܒܿܕܬܿ ܪܚܡܟ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>53</p>
            </td>
            <td>
                <p>My son, do not let a word go out of your mouth until you have taken counsel in your heart, for it is
                    better for a man to stumble in his heart than to stumble with his tongue.</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܦܘܿܩ ܡܠܬܐ ܡܼܢ ܦܘܼܡܟ ܥܕܡܐ ܕܬܬܼܡܠܟ ܒܓܼܘ ܠܒܟ ܡܛܠ ܕܛܒܼܘܼ ܠܓܒܼܪܐ ܕܡܬܿܬܿܩܠ ܒܠܒܿܗ ܘܠܐ ܡܬܿܬܿܩܠ ܒܠܫܢܗ
                </p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>54</p>
            </td>
            <td>
                <p>My son, when you hear an evil word, bury it seven cubits deep in the ground. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܬܫܡܥ ܡܠܬܼܐ ܒܝܼܫܬܐ ܐܥܠܝܗܿ ܒܐܪܥܐ ܫܒܥ ܐܡ̈ܝܼܢ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr> (2x)</p>
            </td>
        </tr>
        <tr class="ro5">
            <td>
                <p>55</p>
            </td>
            <td>
                <p>My son, do not linger where there is strife, for out of strife comes murder. </p>
            </td>
            <td>
                <p>ܒܪܝ ܨܝܕ ܡܨܘܼܬܼܐܼ ܠܐ ܬܟܬܪ ܡܛܠ ܕܡܼܢ ܬܟܼܬܿܘܼܫܐ ܗܘܿܐ ܩܛܠܐ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> (2x), <abbr class="siglum M">M</abbr> (2x), <abbr class="siglum N">N</abbr> (2x), <abbr class="siglum T">T</abbr> (2x), <abbr class="siglum H">H</abbr> (2x), <abbr class="siglum P">P</abbr> (2x) <abbr class="siglum G">G</abbr> (2x), <abbr class="siglum K">K</abbr> (2x), <abbr class="siglum I">I</abbr> (2x), <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>56</p>
            </td>
            <td>
                <p>My son, he who does not execute a just judgement angers God. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܠ ܕܠܐ ܕܐܿܢ ܕܝܼܢܐ ܬܪܝܼܨܐ ܡܪܓܿܙ ܠܐܠܗܐ</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>57</p>
            </td>
            <td>
                <p>My son, do not go away from your father's friend, lest your (own) friend join you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܬܼܪܚܩ ܡܼܢ ܪܚܡܗ ܕܐܒܼܘܼܟ ܕܠܡܐ ܪܚܡܟ ܠܐ ܡܛܿܐ ܠܟ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>58</p>
            </td>
            <td>
                <p>My son, do not go down into the garden of the nobles, nor approach the daughters of the nobles. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܓܢܬܼ ܪܘܪ̈ܒܼܢܐ ܠܐ ܬܚܘܿܬܼ ܘܠܒܼ̈ܢܬܼ ܪܘܪ̈ܒܼܢܐ ܠܐ ܬܬܼܩܪܒܼ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>59</p>
            </td>
            <td>
                <p>My son, help your friend before the ruler, that you may save him from the lion. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܕܪ ܪܚܡܟ ܩܕܼܡ ܫܠܝܼܛܐ ܕܬܫܟܚ ܬܥܕܪܝܼܘܗܝ<span class="T1"> </span><span class="T2">ܡܼܢ ܐܪܝܐ</span></p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>60</p>
            </td>
            <td>
                <p>My son, do not rejoice over your enemy when he dies. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܚܕܿܐ ܠܗ ܠܒܼܥܠܕܒܼܒܼܟ ܟܕ ܢܡܘܼܬܼ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>61</p>
            </td>
            <td>
                <p>My son, when you see a man who is older than you, stand up before him. </p>
            </td>
            <td>
                <p>ܒܪܝ ܟܕ ܬܚܙܐ ܓܒܼܪܐ ܕܩܫܝܼܫ ܡܢܟ ܩܘܼܡ ܡܼܢ ܩܕܼܡܘܗܝ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>C A L S U M N T H P D G K I Q (2)</p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>62</p>
            </td>
            <td>
                <p>My son, if the waters were to rise up without a bottom, and the sparrow fly without wings, and the
                    raven become white as snow, and the bitter become sweet as honey, then the fool would become wise.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܢܩܘܼܡܘܼܢ ܡܝ̈ܐ ܕܠܐ ܐܪܥܐ ܘܢܦܪܚ ܨܦܪܐ ܕܠܐ ܓܦܐ ܘܢܚܘܪ ܢܥܒܼܐ ܐܝܟ ܬܠܓܿܐ ܘܢܚܠܐ ܡܪܝܼܪܐ ܐܝܟ ܕܒܼܫܐ ܗܝܕܝܢ
                    ܢܬܼܚܟܡ ܣܟܼܠܐ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>63</p>
            </td>
            <td>
                <p>My son, if you are a priest of God, be on guard before Him, and come before Him in purity, and do not
                    depart from His presence. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܟܘܼܡܪܐ ܐܢܬܿ ܕܐܠܗܐ ܗܘܝܬܿ ܙܗܝܼܪ ܡܢܗ ܘܒܕܟܼܝܘܼܬܼܐ ܥܘܿܠ ܩܕܼܡܘܗܝ ܘܡܼܢ ܩܕܼܡܘܗܝ ܠܐ ܬܥܢܕ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> I </p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>64</p>
            </td>
            <td>
                <p>My son, to whom God does good, honour him also. </p>
            </td>
            <td>
                <p>ܒܪܝ ܡܿܢ ܕܐܠܗܐ ܐܛܐܒܼ ܠܗ ܘܐܦ ܐܢܬܿ ܝܩܪܝܗܝ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>65</p>
            </td>
            <td>
                <p>My son, do not quarrel with anyone on his day, nor stand against a river in its flow. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܕܘܼܢ ܥܡ ܐܢܫ ܒܝܘܡܗ ܘܠܐ ܬܩܘܼܡ ܠܘܼܩܒܠ ܢܗܪܐ ܒܡܠܐܗ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>66</p>
            </td>
            <td>
                <p>My son, the human eye is like a well of water, and it is not satisfied with riches until it is filled
                    with dust. </p>
            </td>
            <td>
                <p>ܒܪܝ ܥܝܢܗ ܕܒܪܢܫܐ ܐܝܟ ܡܒܿܘܼܥܐ ܕܡܝ̈ܐ ܘܠܐ ܣܒܼܥܐ ܢܟܼ̈ܣܐ ܥܕܡܐ ܕܡܠܝܐ ܥܦܪܐ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum J">J</abbr> <abbr class="siglum L">L</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>67</p>
            </td>
            <td>
                <p>My son, if you wish to be wise, keep your tongue from lying and your hand from stealing, and you will
                    be wise. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܨܒܿܐ ܐܢܬܿ ܕܬܗܘܐ ܚܟܿܝܼܡ ܟܠܝܼ ܦܘܼܡܟ ܡܼܢ ܕܓܠܘܼܬܼܐ ܘܐܝܼܕܼܟ ܡܼܢ ܓܢܒܼܘܼܬܼܐ ܘܬܗܘܐ ܚܟܿܝܼܡܐ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>68</p>
            </td>
            <td>
                <p>My son, be not a mediator in a woman's marriage, for if it goes badly for her, she will curse you;
                    and if it goes well for her, she will not remember you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܡܟܼܘܼܪܝܐ ܕܐܢܬܿܬܼܐ ܠܐ ܬܗܘܐ ܡܬܡܥܡܨܢܐ ܡܛܠ ܕܐܢ ܢܒܼܐܫ ܠܗܿ ܬܠܘܼܛܟ ܘܐܢ ܢܛܐܒܼ ܠܗܿ ܠܐ ܬܬܿܕܟܪܟ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>69</p>
            </td>
            <td>
                <p>My son, he who is elegant in dress is also elegant in speech, and he who is despicable in dress is
                    also despicable in speech. </p>
            </td>
            <td>
                <p>ܒܪܝ ܕܗܕܝܼܪ ܒܠܒܼܘܼܫܗ ܗܕܝܼܪ ܐܦ ܒܡܠܬܗ ܘܕܫܝܼܛ ܒܠܒܼܘܼܫܗ ܫܝܼܛ ܐܦ ܒܡܠܬܗ</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>70</p>
            </td>
            <td>
                <p>My son, if you discover a find in front of an idol, offer [the idol] its share of it. </p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܬܫܟܚ ܫܟܼܚܬܐ ܩܕܼܡ ܨܠܡܐ ܡܢܬܗ ܩܪܒܼ ܠܗ</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>71</p>
            </td>
            <td>
                <p>My son, neither the hand that was satiated and is (now) hungry will give, nor the one that was hungry
                    and is (now) satiated. </p>
            </td>
            <td>
                <p>ܒܪܝ ܬܬܿܠ ܐܝܕܐ ܕܣܒܼܥܐ ܗܘܼܬܼ ܘܟܦܢܬ ܘܠܐ ܗܝܿ ܕܟܼܦܢܐ ܗܘܼܬܼ ܘܣܒܼܥܬ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>72</p>
            </td>
            <td>
                <p>My son, our eyes must not look at a beautiful woman, nor must we gaze at beauty that does not belong
                    to you, for many have perished because of a woman's beauty, and love for her is like a burning fire.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܒܐܢܬܿܬܼܐ ܕܫܦܝܼܪܐ ܠܐ ܢܚܘܼܪ̈ܢ ܥܝ̈ܢܝܟ ܘܠܐ ܬܬܼܒܩܐ ܒܫܘܼܦܪܐ ܕܠܐ ܕܝܼܠܟ ܡܛܠ ܕܣܓܿܝܼ̈ܐܐ ܐܒܕܘ ܒܫܘܼܦܪܐ
                    ܕܐܢܬܿܬܼܐ ܘܪܚܡܬܼܗܿ ܐܝܟ ܢܘܼܪܐ ܕܝܩܕܐ</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>73</p>
            </td>
            <td>
                <p>My son, let the wise beat you with many blows, but do not let the fool soothe you with sweet perfume.
                </p>
            </td>
            <td>
                <p>ܒܪܝ ܢܡܚܝܟ ܚܟܿܝܼܡܐ ܚܘܼܛܪ̈ܝܼܢ ܣܓܿܝܼ̈ܐܢ ܘܠܐ ܢܡܫܚܟ ܣܟܼܠܐ ܡܫܚܐ ܡܒܣܡܐ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>74</p>
            </td>
            <td>
                <p>My son, do not let your foot run after your friend, lest he be sated with you and hate you. </p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܪܗܛ ܪܓܼܠܟ ܠܘܬܼ ܪܚܡܟ ܕܠܐ ܢܣܒܥ ܡܢܟ ܘܢܣܢܝܟ</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>75</p>
            </td>
            <td>
                <p>My son, put not a golden ring on your finger when you have nothing, lest the fools mock you.</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܣܝܼܡ ܥܙܩܬܼܐ ܕܕܗܒܼܐ ܒܐܝܼܕܼܟ ܟܕ ܠܝܬܿ ܠܟ ܕܠܐ ܢܓܚܟܿܘܼܢ ܒܟ ܣܟܼ̈ܠܐ</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A1</p>
            </td>
            <td>
                <p>My son, words that are not yours should not come from your lips</p>
            </td>
            <td>
                <p>ܒܪܝ ܡ̈ܠܐ ܕܠܐ ܕܝܠܟ܆ ܣܦܘ̈ܬܟ ܠܐ ܢܡ̈ܠܠܢ܀</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A2</p>
            </td>
            <td>
                <p>My son, pass over the threshold of the wise [man] and go out of the house of the rich.</p>
            </td>
            <td>
                <p>ܒܪܝ ܕܘܫ ܐܣܟܘܦܬܐ ܕܚܟܝܡܐ܆ ܘܡܢ ܒܝܬܐ ܕܥܬܝܪ̈ܐ ܐܪܚܸܩ܇</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum A">A</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A3</p>
            </td>
            <td>
                <p>My son, do not reveal a secret to the foolish, for they cannot keep it.</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܐ ܬܩܛܘܪ ܪܐܙܐ ܥܡ ܐܢ̈ܫܐ ܣܟ̈ܠܐ ܡܛܠ ܕܠܐ ܡܫܟܚܝܼܢ ܠܡܟܲܣܵܝܘܬܗ܀</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p><abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum D">D</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A4</p>
            </td>
            <td>
                <p>My son, if you are about to acquire a friend, acquire him with a trial. Do not praise anyone, before
                    you have tested him. Test and then acquire!</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܩܵܢܐ ܐܢܬ ܪܵܚܡܐ܇ ܒܢܣܝܘܢܐ ܩܵܢܝܗܝ܂ ܘܥܕܠܐ<span class="T1">  </span><span
                        class="T2">ܬܸܒܩܐ܆</span><span class="T3">  </span><span class="T2">ܠܐܢܫ ܠܐ ܬܫܲܒܚ܂ ܒܩܝܼ܆ ܘܗܵܝܕܝܢ
                        ܩܢܝܼ܀</span></p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p><abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A5</p>
            </td>
            <td>
                <p>My son, while the wind is blowing and the sea is calm from storms, set your ship straight in the
                    harbour, before the sea is troubled and the waves lie down and sink.</p>
            </td>
            <td>
                <p>ܒܪ[ܝ]<span class="T1">  </span><span class="T2">ܥܕ ܢܿܫܒ ܐܐܪ ܘܫܦܸܐ ܝܲܡܐ ܡܼܢ ܡܚܫܘ̈ܠܐ܇ ܬܪܘܨ ܠܠܡܐܢܐ ܥܕܠܐ
                        ܡܫܬܓܫ ܝܡܐ ܘܡܬܬܪܝܡܝܢ ܓ̈ܠܠܘܗܝ ܘܡܬܛܒܥܐ</span><span class="T3"> </span><span class="T2">ܐܠܦܐ ܡܼܢ
                        ܟܐܡܘ̈ܢܐ܀</span></p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p><abbr class="siglum A">A</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A6</p>
            </td>
            <td>
                <p>My son, for what trouble and for what woe, and for what bruising, and contention and strife? But for
                    those who tarry and multiply the drinking of wine, and for those who seek where to drink.</p>
            </td>
            <td>
                <p>ܒܪܝ܆ ܠܡܿܢ ܕܘܼܘܵܕܳܐ ܘܠܡܢ ܘܝܵܐ܂ ܘܠܡܿܢ ܨܘܠܦܵܬܐ ܘܡܨܘ̈ܬܐ ܘܚܪ̈ܝܢܐ܂ ܐܠܐ ܠܐܝܠܝܢ ܕܡܘܚܪܝܢ ܘܠܡܲܣܓܐܝܘ ܠܡܸܫܬܐ
                    ܚܡܪܐ܂ ܘܠܐܝܠܝܢ ܕܡܥܩܒܝܢ ܐܝܟܐ ܕܐܝܬ ܡܲܫܬܝܐ܀</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p><abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>A7</p>
            </td>
            <td>
                <p>Do not contend with those who are stronger than you; and put aside the word and cut out the evil; and
                    conquer the evil with a good [manner] and obtain a humble heart, patience, endurance [and] a good
                    will, and the end of this life and rest will be yours.</p>
            </td>
            <td>
                <p>ܠܐ ܬܩܘܡ ܒܚܪܝܢܐ ܠܘܼܩܒܠ ܡܿܢ ܕܥܲܫܝܢ ܡܢܟ܂ ܘܐܥܒܪ ܡܠܬܐ܂ ܘܦܣܘܩ ܒܝܫܬܐ܂ ܘܙܟܝܺ ܠܒܝܫܬܐ ܒܛܵܒܬܐ܂ ܘܰܩܢܝ ܠܟ ܠܒܐ
                    ܡܟܝ[ܟ]ܐ<span class="T1">  </span><span class="T2">ܘܢܰܓܝܪܘܬ ܪܘܚܐ ܘܡܣܝܒܪܳܢܘܬܐ ܘܨܒܝܿܢܐ ܛܒܐ܆ ܘܚܰܪܬܐ
                        ܕܗܠܝܢ ܚ̈ܝܐ ܘܢܝܿܚܐ ܢܗܘܘܢ ܠܟ܀</span></p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p><abbr class="siglum A">A</abbr> <abbr class="siglum S">S</abbr> (2x), <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>S1</p>
            </td>
            <td>
                <p>My son, I have eaten bitterness and swallowed aloe, [but] I have seen nothing more bitter than
                    poverty and need.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܟܿܠܬ ܕܡܪܝܪܢ ܘܒܿܠܥܬ [ܕ]ܥܠܝܩܢ܂ ܘܠܐ ܚܙܝܿܬ ܡܕܡ ܕܡܪܝܪ ܡܢ ܡܣܟܢܘܬܐ܀</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p><abbr class="siglum S">S</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>M1</p>
            </td>
            <td>
                <p>My son, when a doctor gets ill a doctor can cure him, and a wrathful [man] does not have a cure for
                    his illness and wounds.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܟܪܗܿ ܐܣܝܐ ܡܫܟܚ ܐܣܝܐ ܕܢܐܣܝܘܗܝ</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p><abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum D">D</abbr></p>
            </td>
        </tr>
        <tr class="ro6">
            <td>
                <p>M2</p>
            </td>
            <td>
                <p>My son, when they send you after people outside from you[r circle of acquaintance], do not go to the
                    mighty man, unless he will befall you with evil, because you do not know [him].</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܡܫܕܪܝܼܢ ܠܟ ܒܬܪ ܐ̄ܢܫܐ ܠܒܪ ܡܢܟ܂ ܠܐ ܬܐܙܠ ܠܘܬܼ ܓܒܼܪܐ ܪܒܿܐ ܕܠܡܐ ܢܐܪܥܟ ܒܒܼܝܼܫܬܿܐ ܡܛܠ ܕܐܢ̄ܬܿ ܠܐ ܝܕܥ
                    ܐܢ̄ܬܿ</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p><abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro6">
            <td>
                <p>M3</p>
            </td>
            <td>
                <p>If you are in need and impoverished, do not reveal your situation [lit.: secret] to your friend, so
                    that you will not be avaricious towards him.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܣܢܝܼܩܬܿ ܐܘ ܢܟܝܬܿ ܠܐ ܬܓܼܠܐ ܐ̄ܪܙܟ ܠܚܒܼܪܟ ܕܠܐ ܬܗܘܐ ܠܘܬܗ ܩܠܘܼܛܐ</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p><abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr></p>
            </td>
        </tr>
        <tr class="ro6">
            <td>
                <p>M4</p>
            </td>
            <td>
                <p>My son, acquire an innocent and humble heart, a vestment and a mighty spirit and possess patience.
                    Acquire a good will, for there is nothing better than it in the world and you will have a restful
                    and delightful life.</p>
            </td>
            <td>
                <p>ܒܪܝ ܩܢܝܼ ܠܟ ܠܒܐ ܬܡܝܼܡܐ ܘܡܟܿܝܼܟܼܵܐ ܘܡܐܢܵܐ ܘܪܘܼܚܐ ܫܠܝܼܬܼܐ ܘܛܥܢ ܡܣܝܒܿܪܢܘܼܬܼܐ ܘܨܒܼܝܢܐ ܛܒܼܐ ܩܢܝܼ ܠܟ܂ ܒܪܡ
                    ܠܝܬܿ ܡܕܿܡ ܒܓܼܘ ܥܠܡܐ ܛܒܼ ܡܢܗܿ܂ ܘܬܚܐ ܚܝ̈ܐ ܢܝܼܚܐ ܘܒܣܝܼܡ̈ܐ</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p><abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>M5</p>
            </td>
            <td>
                <p>O my son, make your way and your speech straight, hasten for listening, but do not hasten for giving
                    an answer.</p>
            </td>
            <td>
                <p>ܐܘ [ܒܪܝ]<span class="T4"> ܫܘܐ</span><span class="T5"> </span><span class="T2">ܐܘܼܪܚܟܿ</span><span
                        class="T5"> </span><span class="T2">ܘܫܒܼܝܼܠܟ</span><span class="T6"> </span><span
                        class="T7">ܘܣܪܗܒ</span><span class="T8"> </span><span class="T9">ܠܡܫܡܥ</span><span class="T8">
                    </span><span class="T9">ܘܠܐ</span><span class="T8"> </span><span class="T9">ܠܡܬܠ</span><span
                        class="T8"> </span><span class="T9">ܬܣܪܗܒܼ</span><span class="T6"> </span><span
                        class="T7">ܦܬܼܓܼܡܐ܂</span></p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p><abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>D1</p>
            </td>
            <td>
                <p>Be humble in your youth and obedient, so that you will be honoured in old age.</p>
            </td>
            <td>
                <p>ܗܘܝܼ ܒܥܠܝܼܡܘܼܬܼܟܼ ܡܟܿܝܼܟܼܐ ܘܡܫܬܡܥܢܐ܆ ܕܒܼܣܝܒܘܼܬܼܟܼ ܬܬܼܝܩܪ</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>D2</p>
            </td>
            <td>
                <p>My son, he who was sent in true friendship received honour here and a legacy in the future.</p>
            </td>
            <td>
                <p>ܒܪܝ ܡ̇ܢ ܕܐܫܬܕܪ ܒܪܚܡܘܼܬܼܐ ܫܪܝܼܪܬܐ ܩܒܿܠ ܐܝܼܩܪܐ ܕܗܪܟܐ ܘܝܪܬܿܘܼܬܼܐ ܒܕܥ[ܬ]ܝܼܕܼ</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>D3</p>
            </td>
            <td>
                <p>My son, authority cannot withstand four [things]: armies of satraps, an evil lifestyle, a crafty
                    conscience and a flock of fraudsters.</p>
            </td>
            <td>
                <p>ܒܿܪܝ ܥܡ ܐܪ̈ܒܥ ܠܐ ܡܨܝܐ ܕܢܟܬܪ ܐܘܚܕܢܐ܂ ܚܝ̈ܠܘܬܼܐ ܕܣܛܪ̈ܦܐ܂ ܘܒܼܝܼܫܘܼܬܼ ܕܘܼܒܪܐ܂ ܘܬܐܪܬܐ ܢܟܼܝܼܠܬܐ܂ ܘܥܘܼܠܒܢ
                    ܡܪܥܝܼܬܼܐ ܘܡܣܟܿܢܐ܂܂</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro2">
            <td>
                <p>D4</p>
            </td>
            <td>
                <p>My son, four other things: the intelligent and the foolish, the rich and the poor</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܪܒܥ ܨܒܼܘܼܬܼܐ ܐ̄ܚܪ̈ܢܝܬܼܐ܂ ܝܕܼܘܿܥܐ ܘܣܟܼܠܐ܂ ܘܥܬܿܝܼܪܐ</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum D">D</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>W1</p>
            </td>
            <td>
                <p>My son, if you lend money to the poor, you have bought him for yourself and his children.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܬܘܙܦ ܠܡܣܟܢܐ ܟܣܦܐ܂ ܙܒܢܿܬܝܗܝ ܠܟ ܗܘ ܘܒܢܘ̈ܗܝ܀</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p><abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr></p>
            </td>
        </tr>
    </table>
</div>
</div>
</div>
