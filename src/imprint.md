---
home: false
title: Imprint
layout: Layout
---

# {{ $frontmatter.title }}

## English Version

This is an English translation of the German Impressum (see below). The German version is the legally binding version.

### Provider

The provider of this website in the legal sense is the Göttingen State and University Library:

**Georg-August-Universität Göttingen**  
Niedersächsische Staats- und Universitätsbibliothek Göttingen  
Platz der Göttinger Sieben 1  
37073 Göttingen

Phone: <a href="rel:+49551395212">+49 (0) 551 – 39 5212</a>  
Fax: +49 (0) 551 – 39 5222  
[sekretariat@sub.uni-goettingen.de](mailto:sekretariat@sub.uni-goettingen.de)  
[www.sub.uni-goettingen.de](http://www.sub.uni-goettingen.de/)

The Göttingen State and University Library is represented by the Assistant Director Kathrin Brannemann. As a central institution, the SUB Göttingen is an organizational, legally non-independent unit of the Georg-August-Universität Göttingen. The Georg-August-Universität Göttingen is a corporation under public law. It is legally represented by the President Prof. Dr. Metin Tolan.

Georg-August-Universität Göttingen  
Wilhelmsplatz 1  
37073 Göttingen

Phone: <a href="tel:+49551390">+49 (0) 551 – 39 0</a>  
Fax: +49 (0) 551 – 39 9612  
[poststelle@uni-goettingen.de](mailto:poststelle@uni-goettingen.de)

### Responsible supervisory authority

Georg-August-Universität Göttingen Stiftung Öffentlichen Rechts, Stiftungsausschuss Universität (§§ 59 Abs. 2, 60 Abs. 2 Satz 2 Nr. 7,60 a Abs. 1 NHG), Wilhelmsplatz 1, 37073 Göttingen

Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz: DE 152 336 201

Responsible for content according to § 55 para. 2 RStV:

Prof. Dr. Reinhard Gregor Kratz  
Georg-August-Universität Göttingen  
Theologische Fakultät  
Platz der Göttinger Sieben 2  
37073 Göttingen

Tel.: <a href="tel:+495513927130">+49 (0) 551 – 39 27130</a>

and

Zeki Mustafa Doğan  
Niedersächsische Staats- und Universitätsbibliothek  
Abteilung Digitale Bibliothek  
Platz der Göttinger Sieben 1  
37073 Göttingen

Tel.: <a href="tel:+495513924924">+49 (0) 551 39-24924</a>

### Responsible data protection officer

[Data Protection Officer of the University of Göttingen (without UMG)](https://www.uni-goettingen.de/en/576209.html)

### Privacy policy

Information on data privacy can be found on the [Data privacy](https://ahiqar.uni-goettingen.de/website/privacy.html) page.

### Disclaimer

Despite careful control of the content, we assume no liability for the content of external links. The operators of the linked pages are solely responsible for their content.

### Licence

All content developed as part of the Ahiqar Project and accessible on this website is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0).

## German Version

Diese deutsche Fassung ist die rechtlich verbindliche Fassung.

### Anbieter

Anbieter dieser Internetpräsenz ist im Rechtssinne die Niedersächsische Staats- und Universitätsbibliothek Göttingen:

**Georg-August-Universität Göttingen**  
Niedersächsische Staats- und Universitätsbibliothek Göttingen  
Platz der Göttinger Sieben 1  
37073 Göttingen

Tel.: <a href="tel:+49551395212">+49 (0) 551 – 39 5212</a>  
Fax: +49 (0) 551 – 39 5222  
[sekretariat@sub.uni-goettingen.de](mailto:sekretariat@sub.uni-goettingen.de)  
[www.sub.uni-goettingen.de](http://www.sub.uni-goettingen.de/)

Die Niedersächsische Staats- und Universitätsbibliothek Göttingen (SUB Göttingen) wird vertreten durch die stellvertretende Direktorin Kathrin Brannemann. Die SUB Göttingen ist als zentrale Einrichtung eine organisatorische, rechtlich nicht selbständige Einheit der Georg-August-Universität Göttingen. Die Georg-August-Universität Göttingen ist eine Körperschaft des öffentlichen Rechts. Sie wird durch den Präsidenten Prof. Dr. Metin Tolan gesetzlich vertreten.

Georg-August-Universität Göttingen  
Wilhelmsplatz 1  
37073 Göttingen

Tel.: <a href="+49551390">+49 (0) 551 – 39 0</a>  
Fax: +49 (0) 551 – 39 9612  
[poststelle@uni-goettingen.de](mailto:poststelle@uni-goettingen.de)

### Zuständige Aufsichtsbehörde

Georg-August-Universität Göttingen Stiftung Öffentlichen Rechts, Stiftungsausschuss Universität (§§ 59 Abs. 2, 60 Abs. 2 Satz 2 Nr. 7,60 a Abs. 1 NHG), Wilhelmsplatz 1, 37073 Göttingen

Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz: DE 152 336 201

Inhaltlich Verantwortliche gemäß § 55 Abs. 2 RStV:

Prof. Dr. Reinhard Gregor Kratz  
Georg-August-Universität Göttingen  
Theologische Fakultät  
Platz der Göttinger Sieben 2  
37073 Göttingen  

Tel.: <a href="tel:+495513927130">+49 (0) 551 – 39 27130</a>

und

Zeki Mustafa Doğan  
Niedersächsische Staats- und Universitätsbibliothek  
Abteilung Digitale Bibliothek  
Platz der Göttinger Sieben 1  
37073 Göttingen

Tel.: <a href="tel:+495513924924">+49 (0) 551 39-24924</a>

### Zuständiger Datenschutzbeauftragter

[Datenschutzbeauftragter der Universität Göttingen (ohne UMG)](https://www.uni-goettingen.de/de/576209.html)

### Datenschutzhinweis

Hinweise zum Datenschutz finden Sie auf der Seite [Data privacy](https://ahiqar.uni-goettingen.de/website/privacy.html). 

### Haftungshinweis

Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren BetreiberInnen verantwortlich.

### Lizenz

Alle im Rahmen des Ahiqar Projektes erarbeiteten und auf dieser Website zugänglichen Inhalte sind gemäß [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0) lizenziert.
