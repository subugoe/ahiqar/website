---
home: false
title: Technical Documentation
layout: Layout
---

# {{ $frontmatter.title }}

The software implemented for the Ahiqar project is realized with several components, which are mainly the following:

* Website
* back end (all ETL processes / offering TextAPI, AnnotationAPI and SearchAPI)
* TIDO Viewer (a viewer meant to display digital editions - and everything else delivered via TextAPI) and
* Collation

This is the very fist implementation of the [TextAPI](https://subugoe.pages.gwdg.de/emo/text-api/) - a generic API specification to deliver all kinds of texts and related material like images, serializations and annotations.
A back end made with eXist-db is meant to provide the TextAPI.
A client application within the browser, TIDO, interacts with the TextAPI to present all the data in a well-known order (e.g. synoptic view with image and a transcription converted to HTML).  
Beforehand the data is prepared with TextGrid.

![UML diagram](https://yuml.me/7474569d.svg)

## Git Repository

All the code used for this project is available at our GitLab: [https://gitlab.gwdg.de/subugoe/ahiqar](https://gitlab.gwdg.de/subugoe/ahiqar)

### GitLab CI

Everything is assembled, tested and deployed with the help of GitLab CI. Please see the `.gitlab-ci.yaml` files in all the repositories.

### Editing TEI

For editing, storing and exporting the TEI documents the virtual research environment [TextGrid](https://textgrid.de/) is used.
TextGrid offers all facilities to encode, validate, interlink, store and export the data.

### Schema

Based on the ODD file a conversion to RNG is used for validating documents during the process of encoding utilizing oXygen XML editor as part of the TextGrid Laboratory.
A complementary Schematron files ensures data quality.

### Export Interfaces

Communicating with TextGrid's REST interfaces an XQuery client is available to transfer data to eXist-db.
The process can be triggered from within the TextGrid Laboratory (requires the “SADE” plug-in) or from eXist-db (manually or programmatically).

## eXist-db

The main component for the backend implementation is eXist-db offering a performant XQuery processor.
ETL-processes are bound together in our backend application ([git repo](https://gitlab.gwdg.de/subugoe/ahiqar/backend), [Expath Package](https://ci.de.dariah.eu/exist-repo/packages/ahikar.html)) as well as the instructions for the REST API created with [RESTXQ](http://exquery.github.io/exquery/exquery-restxq-specification/restxq-1.0-specification.html).

## Interfaces Available to End-Users

Most of the interfaces available to end-users are documented with OpenAPI: [https://ahiqar.uni-goettingen.de/openapi/](https://ahiqar.uni-goettingen.de/openapi/)

In addition, particulary where a desciption is missing in the OpenAPI descriptor file, more extensive documentation can be found via the following URLs:

* [TextAPI especially for Ahiqar](https://subugoe.pages.gwdg.de/ahiqar/api-documentation/page/text-api-specs/)
* [AnnotationAPI for Ahiqar](https://subugoe.pages.gwdg.de/ahiqar/api-documentation/page/annotation-api-specs/)
* [SearchAPI](https://subugoe.pages.gwdg.de/ahiqar/api-documentation/page/search-api-specs/)

A restricted version of eXist-db's REST interface is available via [https://ahiqar.uni-goettingen.de/rest/](https://ahiqar.uni-goettingen.de/rest/).

## Website

This website, offering static content and the user interface to search only, is prepared with Vue.js and VuePress2.  
Pages are written in Markdown.

## TIDO Viewer

The front end counterpart to the TextAPI is the [TIDO Viewer](https://gitlab.gwdg.de/subugoe/emo/tido).
It is embedded into the website to display the actual digital edition.
TIDO Viewer's code is available at our GitLab: [https://gitlab.gwdg.de/subugoe/emo/tido)](https://gitlab.gwdg.de/subugoe/emo/tido)

## Collation

Using [CollateX](https://github.com/interedition/collatex) with a [tokenized version of the manuscripts](https://ahiqar.uni-goettingen.de/api/content/ahikar-json.zip) the output presented at [Meta Edition > Collation](/collation.html) is prepared.
