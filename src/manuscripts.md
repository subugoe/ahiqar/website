---
home: false
title: Manuscripts
layout: Layout
---

# {{ $frontmatter.title }}

## How to Cite This Page

Birol, Simon and Aly Elrefaei. „Manuscripts“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/manuscripts.html.

## Overview of the Manuscripts

<!-- DO NOT TOUCH FOLLOWING LINES! -->
<!-- following XHTML can be downloaded from https://ahikar-test.sub.uni-goettingen.de/api/website/manuscripts -->
<!-- HTML -->
## The Syriac witnesses

### Paris, Bibliothèque Nationale de France, ms. syr. 434
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r678/186v/latest/item.json)


#### Manuscript Identifier

* Identifier: Bibliothèque Nationale de France (France Paris), ms. syr. 434 ([Digital Source](https://gallica.bnf.fr/ark:/12148/btv1b53110176r))
* Language: Classical Syriac
* Responsible: unknown (scribe)

#### History

* Object type: codex
* Origin: unknown
* Date of creation: unknown; not before 1799, not after 1899

#### Bibliography

* Briquel-Chatonnet, F. 1997. Manuscrits syriaque de la Bibliothèque nationale de France (nos 356 - 435, entrés depuis 1911), de la bibliothèque Méjanes d'Aix-en-Provence, de la bibliothèque muncipale de Lyon et de la Bibliothèque nationale et universitaire de Strasbourg, Paris: Bibliothèque nationale de France, 178-183.


#### Editor's statements

* 2 pp. (fol. 186v-187r)
* East Syriac script
* This manuscript was part of Graffin's collection.


### Staatsbibliothek zu Berlin, Sachau 162
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r145/86r/latest/item.json)


#### Manuscript Identifier

* Identifier: Staatsbibliothek zu Berlin (Germany), Sachau 162 ([Digital Source](https://digital.staatsbibliothek-berlin.de/werkansicht?PPN=PPN862950937&PHYSID=PHYS_0173&DMDID=DMDLOG_0001))
* Language: Classical Syriac
* Responsible: unknown (scribe)

#### History

* Object type: codex
* Origin: Tur Abdin[?]
* Date of creation: unknown [15th cent. (?); not after 1583]

#### Bibliography

* Sachau, E. 1899. Verzeichniss der syrischen Handschriften der Königlichen Bibliothek zu Berlin, 2 vols., Berlin: A. Asher & Co., 2:514-9.
* Nau, F. 1920. Documents relatifs à Aḥikar. Textes syriaques édités et traduits. Extrait de la Revue de l’Orient chrétien T. XXI, Paris: Librairie Auguste Picard, 1–13.


#### Editor's statements

* 12 pp. (fol. 86v-92r)
* West Syriac script
* The manuscript is badly damaged at the margins and the text is incomplete.
* Metadata (with a link to the digitized images) can be viewed at: [http://orient-digital.staatsbibliothek-berlin.de/receive/SBBMSBook_islamhs_00010205](http://orient-digital.staatsbibliothek-berlin.de/receive/SBBMSBook_islamhs_00010205)
* A transcription of the Ahiqar story from this manuscript has been published with a French translation in: Nau 1920, 1–13.


### London, Brit. Libr. Add. 7200
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r679/114r/latest/item.json)


#### Manuscript Identifier

* Identifier: British Library (Great Britain), Add. 7200 
* Language: Classical Syriac
* Responsible: unknown (scribe)

#### History

* Object type: codex
* Origin: unknown
* Date of creation: unknown; not before 1100, not after 1299

#### Bibliography

* Forshall, J./ Rosen, F. A. 1838. Catalogus codicum manuscriptorum orientalium qui in Museo Britannico asservantur. Pars prima: codices syriacos et carshunicos amplectens, London: British Museum, 93.
* Wright, W. 1870-2. Catalogue of Syriac Manuscripts in the British Museum acquired since the year 1838, 3 vols., London: British Museum, 3:1207.


#### Editor's statements

* 2 fols. (fol. 114rv)
* East Syriac script
* The manuscript is badly damaged and an unknown number of pages have been lost.


### Cambridge (MA), Harvard University, Houghton Library, syr. 80
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r67b/1/latest/item.json)


#### Manuscript Identifier

* Identifier: Harvard University (Cambridge/MA) (United States of America), Syr 80 
* Language: Classical Syriac
* Responsible: Pērā bar Bābāw bar 'Amrīḥās (scribe)

#### History

* Object type: codex
* Origin: Iran, Oroomiah
* Date of creation: September 1898

#### Bibliography

* Goshen-Gottstein, M. H. 1979. Syriac Manuscripts in the Harvard College Library. A Catalogue, Harvard Semitic Studies 23, Missoula/MT: Brill, 69.


#### Editor's statements

* 39 pp.
* East Syriac script
* This manuscript was copied from the now lost cod. Urmiensis 270 (= S4)
* Readings from the now-lost cod. Urmiensis 117 (= S5) were transmitted in the margins
* Note on the first page: THE SYRIAC TEXT OF THE LEGEND OF AḤIḲAR FROM A MS. IN THE LIBRARY OF THE AMERICAN MISSION AT OOROMIAH (cod. Urmiensis 270 = S4) with some readings from Cod. Urmiensis 117 (=S5)
* Probably the former owner of the manuscript and co-editor of the Syriac edition of this text, James Rendel Harris, made pencil annotations to the text (not transcribed for this edition).


### Jerusalem, SMMJ 162
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r67c/113r/latest/item.json)


#### Manuscript Identifier

* Identifier: Jerusalem, St. Mark's Monastery (Israel), 162 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/501295))
* Language: Classical Syriac
* Responsible: Gabriel of Beth Sbīrīnā (scribe)

#### History

* Object type: codex
* Origin: Mār Eugen near Nisibis
* Date of creation: 1847-1871

#### Bibliography

* Vööbus, A. 1980. Handschriftliche Überlieferung der Mēmrē-Dichtung des Ja‘qōb von Serūg. Die zerstreuten Mēmrē: Die Handschriften (Corpus Scriptorum Christianorum Orientalium 421, Subsidia 60). Louvain: Secrétariat du CorpusSCO, 62-3.
* Vööbus, A. 1980. Handschriftliche Überlieferung der Mēmrē-Dichtung des Ja‘qōb von Serūg. Die zerstreuten Mēmrē: Die Handschriften (Corpus Scriptorum Christianorum Orientalium 421, Subsidia 60). Louvain: Secrétariat du CorpusSCO, 24–7.


#### Editor's statements

* 3 pp. (fol. 113r-114v)
* West Syriac script


### London, Brit. Libr. Or. 2313
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r67d/172r/latest/item.json)


#### Manuscript Identifier

* Identifier: British Library (Great Britain), Or. 2313 
* Language: Classical Syriac
* Responsible: unknown (scribe)

#### History

* Object type: codex
* Origin: unknown
* Date of creation: unknown; not before 1500, not after 1799

#### Bibliography

* Margoliouth, G. 1899. Descriptive List of Syriac and Karshuni MSS. in the British Museum acquired since 1873, London: British Museum, 8.
* Nöldeke, T. 1913. Untersuchungen zum Achiqar Roman. Abhandlungen der Königlichen Gesellschaft der Wissenschaften zu Göttingen, Philologisch Historische Klasse NF , vol. 14.4, Berlin: Weidmannsche Buchhandlung.


#### Editor's statements

* 18 pp. (fol. 172r-180v)
* East Syriac script
* The manuscript is badly water-damaged, and some pages have been lost.


### Staatsbibliothek zu Berlin, Sachau 336
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r67f/17v/latest/item.json)


#### Manuscript Identifier

* Identifier: Staatsbibliothek zu Berlin (Germany), Sachau 336 ([Digital Source](https://digital.staatsbibliothek-berlin.de/werkansicht?PPN=PPN862954495&PHYSID=PHYS_0038&DMDID=DMDLOG_0001))
* Language: Classical Syriac
* Responsible: Deacon Francis Mīri (scribe)

#### History

* Object type: codex
* Origin: Iraq, Tellkepe
* Date of creation: 1883

#### Bibliography

* Sachau, E. 1899. Verzeichniss der syrischen Handschriften der Königlichen Bibliothek zu Berlin, 2 vols., Berlin: A. Asher & Co., pp. 1:437-42.
* Grünberg, S. 1917. Die weisen Sprüche des Achikar, nach der syrischen Hs Cod. Sachau Nr. 336 der Kgl. Bibliothek in Berlin, Gießen: H. Itzkowski, 1917.
* Guzik, M. H. 1936. Die Achikar-Erzählung nach der syrischen Handschrift Cod. Sachau Nr. 336 der Preussischen Staatsbibliothek in Berlin, Krakau: Renaissance.
* Ebied, R./ Al-Jeloo, N. "Some Further Letters in Syriac, Neo-Aramaic and Arabic Addressed to Eduard Sachau by Jeremiah Shāmīr." Journal of Assyrian Academic Studies 24,1 (2010): 1-45.
* Nöldeke, T. 1913. Untersuchungen zum Achiqar Roman. Abhandlungen der Königlichen Gesellschaft der Wissenschaften zu Göttingen, Philologisch Historische Klasse NF , vol. 14.4, Berlin: Weidmannsche Buchhandlung.


#### Editor's statements

* 76 fols. (fol. 17v-55r)
* East Syriac script
* The version of the Ahiqar tale of this manuscript has been transcribed in: Grünberg 1917.
* The sayings of the Ahiqar tale of this manuscript have been transcribed and translated into German in: Guzik 1937.
* The Metadata (with a link to the digitized images) can be viewed at: [http://orient-digital.staatsbibliothek-berlin.de/receive/SBBMSBook_islamhs_00010373](http://orient-digital.staatsbibliothek-berlin.de/receive/SBBMSBook_islamhs_00010373)


### St. Petersburg, Sado no. 9
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r67g/1/latest/item.json)


#### Manuscript Identifier

* Identifier: Private property (Russia), Sado Collection ms. no. 9 
* Language: Classical Syriac
* Responsible: John of Gawar (scribe)
* Responsible: Anonymous [maybe Josef (son of the copyist John of Gawar)?] (owner (who made further scribal notes))

#### History

* Object type: codex
* Origin: Iran, Urmia
* Date of creation: February 19, 1904

#### Bibliography

* Teule, H. G. B./ Kessel, G. 2012. "The Mikhail Sado Collection of Syriac Manuscripts in St. Petersburg." In: Eastern Christians and their written Heritage. Manuscripts, Scribes and Context, Eastern Christan Studies 14, edited by J. P. Monferrer-Sala, H. G. B. Teule, Sofía Torallas Tovar, Leuven/ Paris/ Walpole, MA: Peeters, 55-6.


#### Editor's statements

* 30 pp.
* Part of the private Collection of the late Mikhail Sado
* East Syriac script
* Paper of Russian provenance.
* The manuscript is slightly damaged.
* The last page states that this booklet was written in 1906 by a deacon who was the son of Deacon John of Gawar. On the front page, the name Deacon John, son of John of Gawar, is mentioned. On the last page, Josef, who is the son of the aforementioned deacon, is mentioned as the owner of the manuscript in 1922.


### Bibliothèque Nationale de France et universitaire de Strasbourg, Ms. 4122
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r67h/1/latest/item.json)


#### Manuscript Identifier

* Identifier: Bibliothèque nationale et universitaire de Strasbourg (France), Manuscrit 4122 ([Digital Source](https://gallica.bnf.fr/ark:/12148/btv1b10229364b/f1.image.r=Syriaque))
* Language: Classical Syriac
* Responsible: Augustine Thomas (scribe)

#### History

* Object type: codex
* Origin: Iran, Khorsabad in Oroomiah (Iran)
* Date of creation: 25.12.1898

#### Bibliography

* Briquel-Chatonnet, F. 1997. Manuscrits syriaque de la Bibliothèque nationale de France (nos 356 - 435, entrés depuis 1911), de la bibliothèque Méjanes d'Aix-en-Provence, de la bibliothèque muncipale de Lyon et de la Bibliothèque nationale et universitaire de Strasbourg, Paris: Bibliothèque nationale de France, 210.


#### Editor's statements

* 26 pp.
* East Syriac script


### Aleppo, SCAA 7/229
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r67j/10r/latest/item.json)


#### Manuscript Identifier

* Identifier: Syriac Catholic Archdiocese of Aleppo (Syria), Ar 7/229 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/512861))
* Language: Classical Syriac
* Responsible: unknown (scribe)

#### History

* Object type: codex
* Origin: unknonw
* Date of creation: unknown [18th cent. (?)]

#### Editor's statements

* 9 pages (fol. 10r-14r)
* West Syriac script
* The manuscript has been described in: See: w3id.org/vhmml/readingRoom/view/512861
* fol. 10v is blank
* In some cases, the copyist explains Syriac words with their Arabic counterparts


### Cambridge, Univ. Cant. Add. 2020
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r84d/66r/latest/item.json)


#### Manuscript Identifier

* Identifier: University of Cambridge - Cambridge University Library (Great Britain), Add. 2020 
* Language: Classical Syriac
* Responsible: Priest Hormizd bar Ḥadbšabbā (scribe)

#### History

* Object type: codex
* Origin: Iraq, Alqosh
* Date of creation: 18.10.1697

#### Bibliography

* F. C. Conybeare, Harris, J. R., Lewis, A. S., Eds., The Story of Aḥiḳar from the Syriac, Arabic, Armenian, Ethiopic, Greek and Slavonic Versions, London: C. J. Clay and Sons, 1898.
* F. C. Conybeare, Harris, J. R., Lewis, A. S., Eds., The Story of Aḥiḳar from the Aramaic, Syriac, Arabic, Armenian, Ethiopic, Old Turkish, Greek and Slavonic Versions, 2nd ed., Cambridge: Cambridge University Press, 1913.
* P. Y. Dolabani, Ed., Aḥīqar sāprā w-ḥakīmā, Mardin: Maṭbaʻtā d-Mardīn, 1962.
* Nöldeke, T. 1913. Untersuchungen zum Achiqar Roman. Abhandlungen der Königlichen Gesellschaft der Wissenschaften zu Göttingen, Philologisch Historische Klasse NF , vol. 14.4, Berlin: Weidmannsche Buchhandlung.
* Wright, W. 1901. A catalogue of the Syriac manuscripts preserved in the library of the University of Cambridge, 2 vols., Cambridge: Cambridge University Press, 2:583-9.


#### Editor's statements

* 24 fols. (fol. 66v-78r)
* East Syriac script
* Copied for the Church Mār Mīlēs at Tell-haš
* The creation of this manuscript has been sponsored by Priest Yaldā and Zīzē of Alqosh


### Birmingham, Mingana Syriac 433
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r86p/1b/latest/item.json)


#### Manuscript Identifier

* Identifier: University of Birmingham (Great Britain), 433 
* Language: Classical Syriac
* Responsible: unknown (scribe)

#### History

* Object type: codex
* Origin: unknown
* Date of creation: ca. 1820

#### Bibliography

* Mingana, A. 1933-9. Catalogue of the Mingana Collection of Manuscripts now in the Possession of the Trustees of the Woodbrooke Settlement, 3. vols., Cambridge: W. Heffer and Sons, 1:767-8.
* Brock, S. "Notes on some Texts in the Mingana Collection", Journal of Semitic Studies 14,2 (1969): 205-226.


#### Editor's statements

* 48 pp. (fol. 1b-25a)
* East Syriac script


### Ms. Graffin
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3r9dx/1/latest/item.json)


#### Manuscript Identifier

* Identifier: unknown (France[?]), unknown 
* Responsible: Abbot Elias (scribe)
* Responsible: Addai Scher [Chaldean Bishop of Seert] (sponsor)

#### History

* Object type: codex
* Origin: Monastery of Rabban Hormizd (near Mosul)
* Date of creation: 13.4.1910

#### Bibliography

* Nau, F. "Documents relatifs à Aḥikar. Textes syriaques édités et traduits", Revue de l’Orient chrétien 21 (1918/1919): 274–307 and 356-400.
* Nau, F. 1920. Documents relatifs à Aḥikar. Textes syriaques édités et traduits. Extrait de la Revue de l’Orient chrétien T. XXI, Paris: Librairie Auguste Picard, 14–71.


#### Editor's statements

* The manuscript is lost.
* According to Nau, it contains 56 pages.
* The manuscript was written on behalf of Bishop Addai Scher by his nephew Elias [abbot of the monastery of Rabban Hormizd (near Mosul)].
* Nau's transcription was published in West Syriac script, although the manuscript was probably written in East Syriac script.


### Paris, Bibliothèque Nationale de France, ms. syr. 422 (= Ms. Pognon)
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3rck1/96r/latest/item.json)


#### Manuscript Identifier

* Identifier: Bibliothèque Nationale de France (France),  syr. 422 
* Language: Classical Syriac
* Responsible: unknown (scribe)

#### History

* Object type: codex
* Origin: unknown
* Date of creation: unknown [19th cent. (?)]

#### Bibliography

* Briquel-Chatonnet, F. 1997. Manuscrits syriaque de la Bibliothèque nationale de France (nos 356 - 435, entrés depuis 1911), de la bibliothèque Méjanes d'Aix-en-Provence, de la bibliothèque muncipale de Lyon et de la Bibliothèque nationale et universitaire de Strasbourg, Paris: Bibliothèque nationale de France, 159-62.
* Nau, F. "Documents relatifs à Aḥikar. Textes syriaques édités et traduits", Revue de l’Orient chrétien 21 (1918/1919): 274–307 and 356–400.
* Nau, F. "Documents relatifs à Aḥikar. Textes syriaques édités et traduits", Revue de l’Orient chrétien 21 (1918/1919): 274–307 and 356-400.
* Nau, F. 1920. Documents relatifs à Aḥikar. Textes syriaques édités et traduits. Extrait de la Revue de l’Orient chrétien T. XXI, Paris: Librairie Auguste Picard, 72–92.


#### Editor's statements

* 80 fols. (fol. 96r-136r)
* East Syriac script
* This manuscript belonged to Henri Pognon and was later to the collection of F. Graffin.
* Fols. 133r and 133v are blank


### Mosul, DFM 430
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3rcnx/1r/latest/item.json)


#### Manuscript Identifier

* Identifier: Dominican Friars of Mosul (Iraq), 430 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/134604))
* Language: Classical Syriac

#### History

* Object type: codex
* Origin: Iraq, Mosul
* Date of creation: 1850-1933

#### Editor's statements

* 54 pages
* East Syriac script
* The manuscript is described in: [https://w3id.org/vhmml/readingRoom/view/134604](https://w3id.org/vhmml/readingRoom/view/134604)
* Fol. 25rv is in the wrong place. This page was originally placed between fol. 9v and 10r.
* The end is missing


### Alqosh, Notre-Dame des Semences, mss. syr. 612 (= Codex 207)
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3vqkf/1r/latest/item.json)


#### Manuscript Identifier

* Identifier: Bibliothèque Syro-Chaldéenne du couvent de Notre-Dame des Semences près d’Alqoš (Iraq) (Iraq), syr. 612 
* Language: Classical Syriac
* Responsible: unknown (scribe)

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1883-1929

#### Bibliography

* Vosté, J.-M. 1929. Catalogue de la Bibliothèque syro-chaldéenne du Couvent de Notre-Dame des Semences près d'Alqosh (Iraq), Rome and Paris: Bureaux of the «angelicum», P. Geuthner, p. 77.


#### Editor's statements

* 71 pp. (fol. 1r-36r)
* East Syriac script


### Alqosh, Notre-Dame des Semences, mss. syr. 611 (= Codex 205)
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/3vqkh/1r/latest/item.json)


#### Manuscript Identifier

* Identifier: Bibliothèque Syro-Chaldéenne du couvent de Notre-Dame des Semences près d’Alqoš (Iraq) (Iraq), syr. 611 
* Language: Classical Syriac
* Responsible: Stefan Raïs (scribe)

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1883

#### Bibliography

* Vosté, J.-M. 1929. Catalogue de la Bibliothèque syro-chaldéenne du Couvent de Notre-Dame des Semences près d'Alqosh (Iraq), Rome and Paris: Bureaux of the «angelicum», P. Geuthner, pp. 76-7.


#### Editor's statements

* 44 pp. (fol. 1r-22v)
* East Syriac script
* First notek (fol. 22a): ܫܿܠܡܬ ܒܐܝܼܕ̈ܝ ܐܣܛܝܦܿܢ ܪܝܣ ܟܢܫܐ ܡܼܢ ܬܪ̈ܬܝܢ ܐܢܨܚ̈ܬܼܐ ܥܬܝܼܩ̈ܬܐ܆ ܏ܗ܆ ܫܢܬ ܐܦܦܓ ܒܕܝܪܐ ܆ ܠܡܪܢ ܒܝܼܪܚ ܢܝܼܣܢ ܂܂܂܂܂܂܂܂܂܂ ܩܫܝܫܐ ܐܪܡܝܐ This [manuscript] was completed by Stefan Raïs; it was compiled from two older versions; in April 1883 AD in the monastery [gap] ... Priest Jeremiah
* Second note (fol. 22b=: ܐܬܢܣܒܼܬ ܡܼܢ ܬܪܝܢ ܢܘܣܟ̈ܐ܆ ܏ܗ܆ ܐܢܨ̈ܚܬܐ ܚܼܕܐ ܟܼܝܪ̈ܬ ܐܝܼܕ̈ܝ ܩܫܝܼܫܵܐ ܗܘܡܘ ܘܐܚܪܬܐ ܘܼܝܹܢ ܡܼܢ ܩܪܝܼܬܐ ܟܪܡܠܝܫ ܐܪܒܲܐܝܼܬܼ ܐܝܼܬܹܝܗܿ ܗ̄ܘܼܵܬܼ ܒܪܡ ܗܿܝ ܕܩܫܝܼܫܐ ܗܘܡܘ ܐܒܼܕܬ ܠܗܿ ܗܫܵܐ܆ ܘܐܚܪܬܐ ܦܝܼܝܫܐ ܒܟܪܡܠܸܝܫ ܩܪܝܼܬܼܵܐ ܘܩܪܝܬܐ ܒܟܪܡܠܝܫ ܐܒܕܬ ܠܗܿ [This copy] is based on two manuscripts: One was the handwriting of the priest Hōmō and the other one was from the village of Karamlīš - it was [written] in Arabic. However, I have lost the one from the priest Hōmō, but the other one remained in Karamlīš; I have lost it in Karamlīš.


### Tellkepe, QACCT 135
view in [edition](https://ahiqar.uni-goettingen.de/syriac/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/syriac/417rk/230r/latest/item.json)


#### Manuscript Identifier

* Identifier: Qalb al-Aqdas Chaldean Church in Tel Keppe (Iraq), 135 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/136599))
* Language: Classical Syriac
* Responsible: Deacon Matīkā bar Yawsep mār Mīkā bar Qūryaqūs bar Ǧerǧīs Ḥadādē beth Ḥaǧī (scribe)
* Responsible: unknown (anonymous)

#### History

* Object type: codex
* Origin: Al-qosh
* Date of creation: 1937

#### Editor's statements

* 31 fols. (fol. 230r-246r)
* East Syriac script
* This manuscript has been moved from Al-Qosh to Tel Keppe.
* Fols. 133r and 133v are blank
## The Arabic and Karshuni witnesses

### Cod. Arab. 236. Transcript
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r177/2a/latest/item.json)


#### Manuscript Identifier

* Identifier: Royal Danish Library (Denemark), Cod. Arab. 236 

#### History

* Object type: codex
* Origin: Syria, Aleppo
* Date of creation: 1670

#### Bibliography

* Codices Orientales Bibliothecae Regiae Havniensis jussu et auspiciis Regis Daniae Augustissimi Christiani VIII. enumerati et descripti: Ps. 2. Codices Hebraici et Arabici Bibliothecae Regiae Hafniensis jussu et auspiciis regiis enumerati et descripti (Hafniae: Schultz, 1851), pp. 139- 140
* Irmeli Perho, Catalogue of Arabic manuscripts, codices arabici et codices arabici additamenta, vol. 2, 3 vol (Copenhagen: NIAS Press, 2007), p. 913.


#### Editor's statements

* scribe: [al-Khuri] Shahedeh (fol.41b)
* Naskh
* /ḍ/ is written /ẓ/ such as: مظى instead of مضى
* /ḍ/ is written /ṭ/ such as: تقطي instead of تقضي
* /ḏ/ is not punctuated: أخد instead of of أخذ
* /ṯ/ is written /t/: تياب instaed of ثياب
* there are many errors in the written text, such as spelling mistakes or incorrect punctuation


### Cod. Arabe. 3637.Transcript
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r178/140/latest/item.json)


#### Manuscript Identifier

* Identifier: Bibliothèque Nationale de France (France), Paris. Arabe 3637 ([Digital Source](https://archivesetmanuscrits.bnf.fr/ark:/12148/cc31518k/))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1772

#### Bibliography

* Notices de manuscrits arabes rédigées par Georges Vajda. Notices des manuscrits Arabe 3630 à 4155, p. 8
* W. M. G. de Slane, Catalogue Des Manuscrits Arabes (Bibliothèque Nationale. Département des Manuscrits. Paris: Impr. Nationale, 1883-1895), p. 621.


#### Editor's statements

* Translated to French by Caussin de Perceval, Les mille et une nuits continuès, VIII, 1806. Transcribed and compared with Paris maunscipt 3656 and the Arabic text fo Mrs. Lewis by L. Leroy in: "Histoire d'Haikar le sage d'après les manuscrits arabes 3637 et 3656 de Paris (1)", ROC 13 (1908): 367-388; ROC 14 (1909): 50- 70, 143-154.
* English translation by Burton in: SUPPLEMENTAL NIGHTS To The Book Of The Thousand And One Nights, vol.6.
* The dots are often omitted: "يعتدر" means "heavy rain", instead of "يعتذر" which means "find excuse"


### Borg. Arab. 201
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r7vw/198b/latest/item.json)


#### Manuscript Identifier

* Identifier: Vatican Library (Vatican), Borg. ar. 201 

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 19th century

#### Bibliography

* Graf, GCAL I, p. 218; Delio V. Proverbio, Turcica Vaticana. Città del Vaticano (Biblioteca Apostolica Vaticana, 2010), p. 199.
* E. Tisserant, Inventaire sommaire des manuscrits arabes du fonds Borgia à la Bibliothèque Vaticane, in Miscellanea Francesco Ehrle. Scritti di storia e paleografia pubblicati sotto gli auspici di S. S. Pio XI in occasione dell'ottantesimo natalizio dell'E.mo cardinale Francesco Ehrle, V: Biblioteca ed Archivio Vaticano. Biblioteche diverse, Roma 1924, pp. 1-34 (20).


#### Editor's statements

* the last two sections of the story including Ahikar return to Assyria and his parables are missing from the MS.
* Nadan is written Nathan
* orthographic mistakes like: كلمن instead of كل من
* orthographic irregularities: فاحذرتك instead of فأحضرتك


### Vat. Arab. 2054
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r7p1/1/latest/item.json)


#### Manuscript Identifier

* Identifier: Vatican Library (Vatican), Vat. ar. 2054 ([Digital Source](https://opac.vatlib.it/mss/detail/272975))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 02.07.1814

#### Bibliography

* Delio V. Proverbio, Turcica Vaticana. Città del Vaticano (Biblioteca Apostolica Vaticana, 2010), p. 199.


#### Editor's statements

* scribe: Anṭūn Ḥamwi
* This fragment has only a small part of the narrative and the rest of the story is missing


### Vat. Arab. 74 ff. 119r-123v
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r7p9/119r/latest/item.json)


#### Manuscript Identifier

* Identifier: Vatican Library (Vatican), Vat. ar. 74 (Scandar 40) ([Digital Source](https://digi.vatlib.it/mss/detail/125564))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1455

#### Bibliography

* Graf, GCAL 1, p. 218; Delio V. Proverbio, Turcica Vaticana. Città del Vaticano (Biblioteca Apostolica Vaticana, 2010), p. 199.


#### Editor's statements

* singular translation from the Syriac by Musa ibn Aṭšah
* this MS represents a different line of tradition than other Arabic and Garsh MSS. Words like (حدايف، زعرور، تقمقم) are not found in other MSS. Sayings are paraphrased differently
* the MS has only the sayings. Narrative and parables are missing
* Ahikar is transcribed Abīḥ-qār


### Sbath 25 ff. 1v-46r
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r7sk/1a/latest/item.json)


#### Manuscript Identifier

* Identifier: Vatican Library (Vatican), Sbath 25 ([Digital Source](https://opac.vatlib.it/mss/detail/221159))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 12th century

#### Bibliography

* Graf, GCAL I, 219; Gregorius Paulos Bahnam, Aḥīqār al-Ḥakīm (Syriac Academy Publications: Bagdad, 1976).
* Paul Sbath, Bibliothèque de Manuscrits Paul Sbath, Prêtre Syrien d'Alep vol. 1, 3 vol (Cairo, 1928), pp. 18-19.
* Delio V. Proverbio, Turcica Vaticana. Città del Vaticano (Biblioteca Apostolica Vaticana, 2010), p. 199.


#### Editor's statements

* the oldest known manuscript up to date for the Arabic version of Ahikar
* scribe: Ibrāhīm bn sʿīd bn ṣālḥ bn msʿūd bn Ibrāhīm bn msʿūd
* some words have been faded and there is a small gap in the sayings
* comparing with other MSS, personal names have been differently transcribed: (Ḥāqār, šfnā, Yābūsm) instaed of (Haiqār, Ishfaghni, Abū Samik)
* Tashkeel errors show diacritic mark "Tanween with kasra: ـٍ" is written on the letter "alif" in certain words, بمجدًا، بخمرًا، بشيئًا
* Sometimes the letter "ض" (ḍ) is written "ظ" (ẓ) such as "ظرب" instead of "ضرب", "ظحك" instead of "ضحك", and "ظيم" instead of "ضيم"
* words of Persian origin: ، (حوشكاشة/خوشكاشة)،(ديوان)، (دستور)
* words of Syriac origin: سنورة ܣܢܘܪܐ ؛ شبَّوقة ܫܒܘܩܐ


### Or. 1292. The Arabic Text in Leiden.
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r7tp/194/latest/item.json)


#### Manuscript Identifier

* Identifier: Leiden University Library (Netherlands), Or. 1292b ([Digital Source](http://hdl.handle.net/1887.1/item:1943626))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: unknown

#### Bibliography

* Jan Just Witkam, Inventory of the Oriental manuscripts in Leiden University Library, vol. 2, 28 vol (Leiden, 2007), p. 102.
* P. Voorhoeve, Handlist of Arabic manuscripts in the Library of the University of Leiden and other collections in the Netherlands (Leiden University Press, 1980), p. 422.


#### Editor's statements

* same version of this MS is found in John Rylands Library, Manchester No.650


### Mingana Arabic Christian 93 (84)
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r7vd/130/latest/item.json)


#### Manuscript Identifier

* Identifier: Cadbury Research Library, University of Birmingham (Great Britain), Mingana ar. christ. 93[84] 

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1780

#### Bibliography

* A. Mingana, Catalogue of the Mingana Collection of Manuscripts, vol. 2, 3 vol (Cambridge: Heffer, 1933), pp. 130-132; Graf, GCAL I, p. 219.


#### Editor's statements

* Egyptian Naskhi
* Sennacherib is written Sankharib
* orthographic mistakes: كرامتن instead of كرامة
* /ẓ/is written /ḍ/: غلض instaed of غلظ


### Cod. Sach. 339. Transcrip
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r179/p.1/latest/item.json)


#### Manuscript Identifier

* Identifier: Staatsbibliothek in Berlin (Germany), Sachau 290 (=Sachau 339) ([Digital Source](http://orient-digital.staatsbibliothek-berlin.de/receive/SBBMSBook_islamhs_00010376))

#### History

* Object type: codex
* Origin: Tur Abdin
* Date of creation: 19th century

#### Bibliography

* E. Sachau, Verzeichniss der syrischen Handschriften der Königlichen Bibliothek zu Berlin (Berlin: Behrend, 1899), p. 815.
* M. Lidzbarski, "Zum weisen Achikar" ZDMG 48/4 (1894): 671- 675
* M. Lidzbarski, Die neu-aramäischen Handschriften der Königlichen Bibliothek zu Berlin I (Weimar: Felber, 1896), 1-77: Arabic and Neo-Syriac. Ibid, II, 1-41, German translation of the Neo-Syriac text.


#### Editor's statements

* the Arabic text is written with a fine naskh calligraphy
* the Arabic text is translated on the opposite page of the manuscript to Neo-Syriac resp. Turoyo (dialect of Tur 'Abdin)".
* the Arabic Text is transcribed in: Die neu-aramäischen Handschriften der Königlichen Bibliothek zu Berlin, Bd. 1. Weimar: Felber, 1896, p. 1- 77; German translation by Lidzbarski in: Die neu-aramäischen Handschriften der Königlichen Bibliothek zu Berlin, Bd. 1. Weimar: Felber, 1896, p. 1- 41.
* the first group of sayings are missing from this manuscript
* It is an Arabic abridgment of the Syriac version(Nau, pp. 86–87)
* the Arabic text offers a much more original recension than that published by Salhani in the Contes arabes (Lidzbarski, Zum weisen Achikar, 671)
* Hamza /ء/ is written /y/ as in عظمايه instead of عظمائه


### Vat. Syriac. 424 ff. 304r-327v
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r7n0/304v/latest/item.json)


#### Manuscript Identifier

* Identifier: Vatican Library (Vatican), Vat. sir. 424 ([Digital Source](https://digi.vatlib.it/view/MSS_Vat.sir.424.pt.2))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1417

#### Bibliography

* S. E. Assemani and A. Assemani, "Codices chaldaici sive syriaci Vaticani Assemaniani", in Scriptorum veterum nova collectio e Vaticanis codicibus edita, vol. 5. 2 (Rome: Vatican apostolic publisher, 1831), pp. 67-68.
* Delio V. Proverbio, Turcica Vaticana. Città del Vaticano (Biblioteca Apostolica Vaticana, 2010), p. 199.


#### Editor's statements

* words with Perisan origin: دستور، بولاد
* Sometimes the letter "ط" (ṭ) is written "ظ" (ẓ) such as "الأظفال" instead of "الأطفال"
* Tashkeel errors show diacritic mark "Tanween with kasra: ـٍ" is written on the letter "alif" in certain words such as بولدًا، بصوتا، بربًا


### Brit_Libr_Or_9321
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r9vn/537/latest/item.json)


#### Manuscript Identifier

* Identifier: British Library (Great Britain), Brit. Libr. Or. 9321 

#### History

* Object type: codex
* Origin: Iraq, Alqosh
* Date of creation: 1896

#### Bibliography

* Handlist of Syriac Manuscripts in the British Museum Acquired since 1899, p. 2.
* Braida, Emanuela, ''The Romance of Aḥiqar the Wise in the Neo-Aramaic MS London Sachau 9321'', Journal of the Canadian Society for Syriac Studies 14 (2014): 50-78.
* Braida, Emanuela, ''The Romance of Aḥiqar the Wise in the Neo-Aramaic MS London Sachau 9321: Part II'', Journal of the Canadian Society for Syriac Studies 15 (2015): 41-50.
* Braida, Emanuela, "Neo-Aramaic Garshuni: Observations Based on Manuscripts." Hugoye: Journal of Syriac Studies 17.1 (2014): 17–31.
* Talia, Shawqi N. "The story of Ahiqar in Syriac and neo-Aramaic: a textual, linguistic and historical comparison", Parole de l’Orient 40 (2015), 429-445.
* Mengozzi, A, "The Neo-Aramaic manuscripts of the British Library: notes on the study of the durikyata as a Neo-Syriac genre," Le Muséon 112 (1999), 459-494.


#### Editor's statements

* bilingual manuscript contains a Sureth version of the Story of Ahiqar together with its translation into Arabic. This text was copied in Baghdad by the priest Gibrayl Quryaquza on Sachau’s request at the end of the 19th century (see Braida, ""Neo-Aramaic Garshuni", 26)
* the copist writes ā with a sequence of madda and hamza: ـــــــــــــَآء instead of calssical ـــــــاء e.g., الحكمآء, الغنآء
* Nādān in written Ndʾān and Nḏʾān
* king of Persian is called king of Persia and Elam
* the Swordsman is called Nābūsmīǧ
* this MS uses speical words not found in other Arabic MSS such as (أخمص قدمي، قلية، مقالي، جذوة، بابوج، قرطب، وكيل، مخدع، أنبار)
* words with Syriac origin: كراس
* words with Persian origin: ابريسم، بابوج
* words with Latin origin:قِلِّيَّة


### Paris. ar. 3656
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r9wf/33a/latest/item.json)


#### Manuscript Identifier

* Identifier: Bibliothèque Nationale de France (France), Paris Arabe 3656 ([Digital Source](https://archivesetmanuscrits.bnf.fr/ark:/12148/cc31537r/))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1689

#### Bibliography

* Notices de manuscrits arabes rédigées par Georges Vajda. Notices des manuscrits Arabe 3630 à 4155, p. 30.
* W. M. G. de Slane, Catalogue Des Manuscrits Arabes (Bibliothèque Nationale. Département des Manuscrits. Paris : Impr. Nationale, 1883-1895), p. 624.
* L. Leroy, "Histoire d'Haikar le sage d'après les manuscrits arabes 3637 et 3656 de Paris," ROC 13 (1908): 367-388; idem, ROC 14 (1909): 50- 70. 143-154.


#### Editor's statements

* the masnuscript has a gap of several leaves


### Cambrigde Add 3497
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3rb3z/1a/latest/item.json)


#### Manuscript Identifier

* Identifier: Cambridge University Library  (Great Britian), Cambrigde Add. 3497 ([Digital Source](https://cudl.lib.cam.ac.uk/view/PR-A-00233-00034-00001/180))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: Bequest of F. Chance (1825-1897) in no date

#### Bibliography

* E. G. Browne, A hand-list of the Muhammadan manuscripts, including all those written in the Arabic character, preserved in the Library of the University of Cambridge (Cambridge, 1900), p. 335; Graf, GCAL I, p. 219.


#### Editor's statements

* the MSS is written in clear naskh


### GCAA 00486
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3rbmc/1a/latest/item.json)


#### Manuscript Identifier

* Identifier: Archdiocese of Aleppo (Melchite) (Syria), GCAA 00486 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/502578))

#### History

* Object type: codex
* Origin: Syria, Aleppo
* Date of creation: 18th century

#### Bibliography

* Francisco del Río Sánchez et al., Catalogue des manuscrits conservés dans la bibliothèque de l’archevêché grec-catholique d’Alep (Syrie) (2003)


#### Editor's statements

* Naskh
* gaps are found in the sayings,the second narrative section, and in the parables
* /ḏ/ is not punctuated
* /ẓ/ is vocalized /ḍ/ as in عضيم instead of عظيم


### Salhani
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3rx14/1/latest/item.json)


#### Manuscript Identifier

* Identifier:  (),  

#### History

* Object type: 
* Origin: unknown
* Date of creation: 

#### Bibliography

* A. Salihani, Ed., Contes arabes: édités d'après un ms. de l'Université S. Joseph. Beirut: Impr. catholique, 1890, p. 1-20.
* M. Lidzbarski, Die neu-aramäischen Handschriften der Königlichen Bibliothek zu Berlin, vol. 2, 2 vol. Weimar: Emil Felber, 1896. Pp. 3-41.
* Anis Fariha, Ahikar al-Hakim- from the ancient Near East (in Arabic) (Beirut, 1962).


#### Editor's statements

* the text has lost much of its original form of narrative, the narration in the first person, in the names, and in the absence of whole pieces (Lidzbarski, Die neu-aramäischen Handschriften 2, p. 4)
* Nadan is vocalized Nathan
* The dots are sometimes omitted: داع


### MS 956
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/40z89/2/latest/item.json)


#### Manuscript Identifier

* Identifier: Ordre Basilien Alepin (Lebanon), MS 956 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/506298))

#### History

* Object type: codex
* Origin: Lebanon, Ṣarbā (Jūniyah)
* Date of creation: 17th century

#### Editor's statements

* Naskh
* The copyist quoted several poetic stanzas
* a number of proverbs are missing from the sayings
* MS is imperfect with loss of text
* 


### MS 619
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/40z8c/39r/latest/item.json)


#### Manuscript Identifier

* Identifier: Université Saint-Joseph. Bibliothèque orientale (Lebanon), MS 619 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/505009))

#### History

* Object type: codex
* Origin: Lebanon, Beirut
* Date of creation: 1858

#### Bibliography

* Louis Cheikho, Catalogue raisonné des manuscrits historiques de la Bibliothèque Orientale de l'Université Saint-Joseph. Arabic mss. 1-792 (1913-1929)


#### Editor's statements

* Naskh
* Scribe: (ḥbīb ibn mrʿī mṭr)حبيب ابن مرعي مطر
* /ẗ/ is written /t/: الكتابت instead of الكتابة


### MS 621
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/410br/58r/latest/item.json)


#### Manuscript Identifier

* Identifier: Université Saint-Joseph. Bibliothèque orientale (Lebanon), MS 621 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/505011))

#### History

* Object type: codex
* Origin: Syria (?), Aleppo (?)
* Date of creation: 17th century

#### Bibliography

* Louis Cheikho, Catalogue raisonné des manuscrits historiques de la Bibliothèque Orientale de l'Université Saint-Joseph. Arabic mss. 1-792 (1913-1929)


#### Editor's statements

* Naskh
* Scribe: ( nʿmẗ ibn al-šmās ʿbd al-lh ibn al-ḥāǧ mīẖāʾīl ibn al-ǧbīr al-ḥlbī) نعمة ابن الشماس عبد الله ابن الحاج ميخائيل ابن الجبير الحلبي
* /ṭ/ is written /ẓ/: احظره instead of احضره
* /t/ is written /ṯ/: وقث instead of وقت
* /ḏ/ is written /d/: هدا instead of هذا
* /ẗ/ is written /t/: الرسالت instead of الرسالة


### MS 626
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/410cr/76r/latest/item.json)


#### Manuscript Identifier

* Identifier: Université Saint-Joseph. Bibliothèque orientale (Lebanon), MS 626 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/505016))

#### History

* Object type: codex
* Origin: 
* Date of creation: 19th century

#### Bibliography

* Louis Cheikho, Catalogue raisonné des manuscrits historiques de la Bibliothèque Orientale de l'Université Saint-Joseph. Arabic mss. 1-792 (1913-1929


#### Editor's statements

* Naskh
* the end of this MS is missing
* the text is full of orthographic errors, most of which are due to carelessness on the part of the copist
* the copyist quotes numerous poetic verses
* the MS contains words that do not appear in other MSS: غلوة، غصَّ، بدرق، خاوز، حطر
* /ẗ/ is written /t/: التربيت instead of التربية


### Brit.Mus. cod. Add. 7209 Transcrip.karsh
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r176/182b/latest/item.json)


#### Manuscript Identifier

* Identifier: British Library (Great Britain), Brit. Mus. Add. 7209 

#### History

* Object type: codex
* Origin: unknown, unknown
* Date of creation: 1560

#### Bibliography

* Van Dyke Arbic Bible, 1865
* Van Dyke Arbic Bible, 1865
* J. Forshall and Rosen, F. A., Catalogus codicum manuscriptorum orientalium qui in Museo Britannico asservantur. Pars prima: codices syriacos et carshunicos amplectens. London: British Museum, 1838, p. 111 (No.14).
* W. Wright, Catalogue of Syriac Manuscripts in the British Museum acquired since the year 1838, vol. 3, 3 vol. London: British Library, 1872, p. 1238.


#### Editor's statements

* This manuscript was used in the edition of Conybeare (The story of Ahikar, 1898)to supplement in some places
* The English translation provided here shares many expressions with the text of Mrs. Lewis in Conybear's edition.
* the letter "و" (waw) is used as a diacritic to indicate the short vowel sound (ḍammah) instead of using the actual diacritic symbol of ḍammah (ٌ):خدمتوك، قتلتوك، تركتوك
* the Hamzah (ء) is dropped altogether, and sometimes substituted by the letter "ي" (ya): سرايري، طرايقهم
* words with Syriac origin:ܫܝܩܐ ;جومة ܓܘܡܬܐ ;ܟܪܙܐ (كَرَّاز)؛؛؛؟; ܩܘܩܝܐ
* words with Persian origin: جوسق


### Mingana Syriac 133
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r17b/82a/latest/item.json)


#### Manuscript Identifier

* Identifier: Cadbury Research Library, University of Birmingham (Great Britain), Mingana syr. 133 

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1809

#### Bibliography

* A. Mingana, Catalogue of the Mingana Collection of Manuscripts now in the Possession of the Trustees of the Woodbrooke Settlement [1], vol. 1, 3 vol. Cambridge: W. Heffer and Sons, 1933, pp. 308-309.


#### Editor's statements

* scribe:Joseph, son of Isaac an-Najjar, son of 'Abd al-Karim
* script: West Syrian


### Mingana Syriac 258.Transcrip. karsh
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r17c/147a/latest/item.json)


#### Manuscript Identifier

* Identifier: Cadbury Research Library, University of Birmingham (Great Britain), Mingana Syr. 258 

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1570

#### Bibliography

* A. Mingana, Catalogue of the Mingana Collection of Manuscripts now in the Possession of the Trustees of the Woodbrooke Settlement [1], vol. 1, 3 vol. Cambridge: W. Heffer and Sons, 1933 ,pp, 514-515.


#### Editor's statements

* West Syrian
* the parables are incomplete
* The negligent hand of the copyist has caused many orthographic errors: كنتوا، وصلتوا، اخطيتوا،فعلتوا
* the name which Ahikar has given to himself in Egypt is Abīšām
* words with Persian origin: زنجير، تخت
* the scribe uses the letter "waw" (و) instead of a short vowel (damma): فعلتوا، بقيتوا، اطعمتوك


### MS orient 2652.Transcrip. karsh
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r17d/47b/latest/item.json)


#### Manuscript Identifier

* Identifier: Forschungsbibliothek Gotha (Germany), Ms. orient. A 2652 ([Digital Source](https://archive.thulb.uni-jena.de/ufb/receive/ufb_cbu_00004733))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: unknown

#### Bibliography

* W. Pertsch, Die orientalischen Handschriften der Herzoglichen Bibliothek zu Gotha, vol. 4 (Gotha, 1883 ), p, 405 (No.5).
* C. H. Cornill, Das Buch der weisen Philisophen (Leipzig, 1875), p. 32.


#### Editor's statements

* The codex is apparently transcribed from a purely Arabic, but strongly mixed with Syriac (Cornill, 40)
* Cornill translated 15 sayings from this ocdex into his Ethiopian edition of Ahikar


### Vat. Syriac. 199, ff.261v-280v
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r7nv/264v/latest/item.json)


#### Manuscript Identifier

* Identifier: Vatican Library (Vatican), Vat. sir. 199 ([Digital Source](https://digi.vatlib.it/view/MSS_Vat.sir.199))

#### History

* Object type: codex
* Origin: Syria, An Nabk
* Date of creation: 1545

#### Bibliography

* J. S. Assemani and Assemani, S. E., Bibliothecae Apostolicae Vaticanae codicum manuscriptorum catalogus, in tres partes distributus, in quarum prima Orientales, in altera Graeci, in tertia Latini, Italici aliorumque Europaeorum idiomatum codices, vol. 3, 3 vol. Paris: Maisonneuve, 1759, p. 447; Graf, GCAL I, p. 218.
* Delio V. Proverbio, Turcica Vaticana. Città del Vaticano (Biblioteca Apostolica Vaticana, 2010), p. 199.


#### Editor's statements

* scribe: Bešārh ibn ʿbd al-ʿzīz (fol. 417)
* words with Perisan origin:دِردار
* words with Syriac origin: حيل (ܚܝܠ)
* incorrect usage of the diacritic mark Tanween with a kasra (ـٍ) is written on the letter "alif" such as: بولداً، بصوتًا


### Vat. sir. 159. ff.293rb-299va. Transcrip.Karsh
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r17g/293r/latest/item.json)


#### Manuscript Identifier

* Identifier: Vatican Library (Vatican), Vat. sir. 159 ([Digital Source](https://digi.vatlib.it/view/MSS_Vat.sir.159))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1625

#### Bibliography

* J. S. Assemani and Assemani, S. E., Bibliothecae Apostolicae Vaticanae codicum manuscriptorum catalogus, in tres partes distributus, in quarum prima Orientales, in altera Graeci, in tertia Latini, Italici aliorumque Europaeorum idiomatum codices, vol. 3, 3 vol. Paris: Maisonneuve, 1759, p. 315.
* Delio V. Proverbio, Turcica Vaticana. Città del Vaticano (Biblioteca Apostolica Vaticana, 2010), p. 199.
* E. Kuhn, “Zum weisen Akyrios”, Byzantinische Zeitschrift , vol. 1, pp. 127-130, 1892


#### Editor's statements

* Scribe: Ephraemi Gargarenfis (fol.300v)
* the careless hand of scribe has caused many orthographic irregularities and mistakes: قلتوا، اخذتوا should be قلتُ، أخذتُ
* Ahiqar is written with the Arabic definite article-al- الحيقار al-ḥīqār (resemlbes to the archaic form mentioned by ʿdī bn zīd (6th century)
* Abikam is written Abīšām
* words with Perisan origin: بس
* there are many examples of word fusion, where the two words are written together without spaces: كلمن، كلشي، منكل


### Cod. Add. 2886. Transcript. karsh
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r17h/81a/latest/item.json)


#### Manuscript Identifier

* Identifier: University Library, Cambridge University (Great Britain), Camb. Add. 2886 ([Digital Source](https://www.fihrist.org.uk/catalog/manuscript_3135))
* Language: Jacobite Karshuni

#### History

* Object type: codex
* Origin: Syria, An Nabk
* Date of creation: 1783

#### Bibliography

* W. Wright, A Catalogue of the Syriac Manuscripts Preserved in the Library of the University of Cambridge, vol. 2, 2 vol. Cambridge: Cambridge University Press, 1901, pp. 732-739.


#### Editor's statements

* Scribe: rabban 'Abdallāh of ṣadad, son of the deacon Nāmūsī, son of Tilījān, of the family of Abū Thābit (f. 200v)
* the text is not vocalized and is characterized by an uncertain syntax, morphology and spelling (especially as regards the notation of the hamzah)
* Transliterated and translated to English in: F. C. Conybeare, J. Rendel Harris, and Agnes Smith Lewis (eds.,) The Story of Aḥikar: from the Aramaic, Syriac, Arabic, Armenian, Ethiopic, Old Turkish, Greek and Slavonic versions (Cambridge: Cambridge University Press, 1913), 1-33 (arab. Pag.), Eng. trans. 130-161, APOT II, 729a-776b
* the editor (Mrs. Lewis) added to the text of this manuscript ten passages, three of which are borrowed from the edition of Salhani (Contes arabes, 1890), p. 3, 4, 8, and seven from Brit. Mus. 7209, p. 4, 5, 7, 8, 20, 28.


### Mingana Syriac 237
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r7tt/1r/latest/item.json)


#### Manuscript Identifier

* Identifier: Cadbury Research Library, University of Birmingham (Great Britain), Mingana syr. 237 

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 1750

#### Bibliography

* Mingana, Alphonse, Catalogue of the Mingana Collection of Manuscripts now in the Possession of the Trustees of the Woodbrooke Settlement, vol. 1, Cambridge 1933, 483-484; Graf, GCAL I, p. 219.
* Brock, Sebastian P., "A Piece of Wisdom Literature in Syriac", Journal of Semitic Studies 13:2 (1968): 212-217


#### Editor's statements

* West Syrian
* This fragment is a conversation between Pharaoh and Ahikar


### MS 882
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/415sp/47/latest/item.json)


#### Manuscript Identifier

* Identifier: Church of the Forty Martyrs (Turkey Mardin), MS 882 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/504021))

#### History

* Object type: codex
* Origin: unknown
* Date of creation: 15th-16th century

#### Editor's statements

* Serto
* Parables and the third narrative section or the fate of Nadan are missing
* sometimes the /t/ is replaced with a /ẗ/) like (كانة) instead of (كانت) and vice verse /ẗ/ is replaced with /t/ like (شيخوخت) instead of (شيخوخة)
* words not found in other MSS: غول ، دمث ، خلم، تريم
* orthographic mistakes like:كليوم ، كلشي
* words with Syriac origin: إِرْدخَل (ܐܪܕܟܠܐ)


### Syr 17
view in [edition](https://ahiqar.uni-goettingen.de/arabic-karshuni/#/?source=external&redirectUrl=https://ahiqar.uni-goettingen.de/website/&itemurl=https://ahiqar.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/415tw/1r/latest/item.json)


#### Manuscript Identifier

* Identifier: Fondation Georges et Mathilde Salem (Syria Allepo), Syr 17 ([Digital Source](https://w3id.org/vhmml/readingRoom/view/501794))

#### History

* Object type: codex
* Origin: Lebanon
* Date of creation: 19th century

#### Bibliography

* Paul Sbath, Bibliothèque de manuscrits Paul Sbath, prêtre syrien d’Alep: Catalogue, 3 vols. (1928-1934); Francisco del Río Sánchez, Catalogue des manuscrits de la fondation Georges et Mathilde Salem (Alep, Syrie) (2008).


#### Editor's statements

* Serto
* the MS was formerly known as Sbath MS 1250
* scribe: al-ẖūrī īūsf īūns
* dots are fallen, no distinction is made between ṭ/ẓ, d/ḏ and t/ṯ
* Ahikar's wife is not mentioned by name
* orthographic mistakes: انكان، كليوم
