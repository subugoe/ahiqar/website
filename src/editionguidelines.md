---
home: false
title: Edition Guidelines
layout: Layout
---

# {{ $frontmatter.title }}

All texts in this archive are licensed under a Creative Commons licence (CC-BY-SA). These texts have been created as XML files with their own schema, following the [Guidelines for Electronic Text Encoding and Interchange (in version P5) of the Text Encoding Initiative (TEI)](http://www.tei-c.org/). However, some of the images have also been made available under a Creative Commons licence (CC-BY-SA), while others can only be used with a VPN connection to the University of Göttingen.


[[toc]]

## Format

### Translations

Translations are provided in a separate `tei:text` element that has the attribute `type="translation"`. The language of the translation is stated in the `xml:lang` attribute.

```xml
<text type="translation" xml:lang="eng">
  <body>
    <ab>A line of translated text.</ab>
    <ab>A second line of translated text.</ab>
  </body>
</text>
```

### Page beginning

After the conversion and upload to TextGrid, every `tei:pb` element should be checked for its linked facsimiles as the automatic conversion is not able to validate the output. To do so, please look at the document in the Lab and check if the links are correct.

```xml
<pb n="PAGE" facs="TEXTGRID-URI"/>
```

The n attribute hosts the page number, e.g. 2r and should always start with a digit, followed by &quot;r&quot; (recto&quot;) or &quot;v&quot; (verso). When there is a page number available at the source, we copy the value and do not have to add &quot;r&quot; or &quot;v&quot;.

Please omit the `@facs` in the transliteration.

#### Empty pages

Empty pages will be encoded by a sequence of `tei:pb` elements. If the foliation number omits these pages, we add &quot;+1&quot; to the last number of the original (or archival) foliation.

```xml
<pb n="PAGE" facs="TEXTGRID-URI1"/> <pb n="PAGE" facs="TEXTGRID-URI2"/> <pb n="PAGE" facs="TEXTGRID-URI3"/>
```

#### Special case: Ms. Graffin

In case of Ms. Graffin the manuscript is lost, but we have an edition by Francois Nau in which the page beginnings of the manuscript have been noted. While the page beginnings of the edition are encoded as usual page beginnings, the ones of the lost manuscript are encoded as follows:

```xml
<note hand="#Nau" type="manuscript_page_beginning">(p. 2)</note>
```

Nau as the one responsible for the addition &quot;(p. 2)&quot; is listed in the `tei:teiHeader` under `tei:editionStmt`  as follows

```xml
<editionStmt>
  <respStmt xml:id="Nau">
    <resp when="1920">Former editor</resp>
    <persName>François Nau</persName>
  </respStmt>
</editionStmt>
```

### Column beginning

```xml
<cb/>
```

Every column is marked up at the beginning.

### Line

Every line of text should be represented via

```xml
<ab>line of text</ab>
```

To encode a blank line (it has to be an intentionally left space between to lines!) the correct encoding is `<ab/>`.

### Verse line

A verse line that interrupts the usual prose style. We use `tei:lg` for the stanza and `tei:l` for all lines.

```xml
<lg>
    <l>line1</l>
    <l>line2</l> 
</lg>
```

#### Multi-line verses as part of the running text

In some cases verses are not set apart visually from the main text but embedded in them. For these cases we use `tei:seg[@type = "verse"]`.

```xml
<ab>some running text <seg type="verse" next="#verse_cont_1">inde per immensum</seg></ab>
<ab><seg xml:id="verse_cont_1" next="#verse_cont_2">croceo velatus amictu / aethera digreditur Ciconumque /</seg></ab>
<ab><seg xml:id="verse_cont_2" type="verse">Hymenaeus ad oras</seg> this is running text again</ab>
```

The different parts of the running text are connected with a `next` attribute that links to the following `tei:seg`&#39;s `xml:id`.

### Heading-like structures

When there is a division into different sections, may be not applied to the complete document or not applied homogeneously, we indicate this by NOT using `tei:head` (what causes other structures to be used in the XML file) and encode this with

```xml
<ab type="head">section 123</ab>
```

### Lost or damaged areas

If a part of a document is lost or damaged we can use

```xml
<damage extent="half page">
```

The extent attribute lists the quantity of the lost part like the following:

*   one page
*   half page
*   one line
*   two words

When the lost content can be reproduced, [supplied](https://wiki.de.dariah.eu/display/AHK/Encoding+Guidelines#EncodingGuidelines-Additionsbytheeditor) can be used.

### Colophon

```xml
<ab type="colophon">end of the text</ab>
```

There is a corresponding part in the `tei:teiHeader`, pointing to these lines with a `tei:colophon`.

#### Inline Colophon

```xml
<ab>and she died. <seg type="colophon">end of the text</seg></ab>
```

#### Poetic Colophons

```xml
<lg type="colophon">
  <l>line 1<l>
  <l>line 2</l>
</lg>
```

### Line breaks within markup

Since we have to maintain the hierarchical structure of XML, we have to use use two linked elements for encoding phenomena on different lines like the following example:

```xml
<ab><persName next="#mss1234_2r_1">-ihA</persName>more text</ab>
<ab><persName xml:id="mss1234_2r_1">rak</persName></ab>
```

Both `persName`s are links so it marks up the same entity and not two different, or two times the same entity. Please notice the &quot;#&quot; to point to an `xml:id`. `xml:id` MUST NOT start with a digit. The structure of the `xml:id` value is RECOMMENDED as: SIGNATURE-OR-MS-ID\_PAGE\_NUMBER. oXygen supports the linking mechanism. When a &quot;#&quot; is typed in, a list with all currently used `xml:id` s shows selectable items.

Word break together with line break

```xml
<ab>this is my sen</ab> <ab><lb break="no"/>tence.</ab>
```

## Language

`@xml:lang`  will be used to set a language for all descendant XML nodes. We are using a set of 7 values:

ara (Arabic)

syc (Classical Syriac)

*   eng (English)
*   lat (Latin)

karshuni (Karshuni)

*   syc-syrj (Western Syriac)
*   syc-syrn (Eastern Syriac)

These values have to be declared in the TEI header as described [here](https://wiki.de.dariah.eu/display/AHK/TEI+Header). They match the `ident` attribute of a `tei:language` declared in the TEI header.

## Textual Content

### Headlines

```xml
<head>Headline</head>
```

`head` is to be used without `tei:ab`.

### Proverbs

```xml
<quote>It was, so it is not.</quote>
```

Any proverb will be encoded as quotation.

In a longer segment, we have to link the lines with `@next` attributes.

```xml
<ab>some other text<quote next="#prov1">It was,</quote></ab>
<ab><quote xml:id="prov1">so it is not.</quote> an more other text</ab>
```

### Highlighted parts

Textual content that is highlighted by using a different color, and underline or other text decoration.

```xml
<hi rend="underline">highlighted text</hi>
```

The rend attribute MAY contains one or more (separated by whites pace) of the following:

*   underline
*   color(red) - any color name from [this list](https://www.w3schools.com/colors/colors_names.asp) is applicable, always in lower-case
*   italic

### Empty lines

When a line is left intentionally empty (may be to include text later on) we encode an empty element

```xml
<ab/>
```

If the reason is unclear we continue the encoding and set a marker for a pace between two lines

```xml
<ab>first line</ab>
<ab rend="margin-top">second line with a space to the next line</ab>
```

### Additions by the editor

Text added by the editor has to be marked up with

```xml
<supplied>added text</supplied>
```

### Additions by the author

#### Surplus

Text added by the author (e.g. to maintain a justified style) we are using

```xml
<surplus>additional character or text, may be repeated in the next line</surplus>
```

This element is used for additional text that is in the document for unknown reason.

#### interlinear or marginal additions

Additions to be read within the text placed around the main part (interlinear, marginal, footer)

```xml
<add place="margin">addition</add>
```

These parts are encoded at the place where the addition belongs to, to maintain a readable text.

Allowed values in the place attribute:

*   margin
*   interlinear
*   footer
*   header
*   above
*   below

### Deletions

A word, phrase or other part deleted by the author.

```xml
<del rend="strikethrough">this part is deleted</del>
```

The rend attribute is optional, allowed values are:

*   strikethrough

### Abbreviations

### Widows and orphans

[https://en.wikipedia.org/wiki/Widows\_and\_orphans](https://en.wikipedia.org/wiki/Widows_and_orphans)

```xml
<catchwords>phrase to ensure correct order</catchwords>
```

### Typos

Errors in writing are encoded in a diplomatic way together with a corrected form. The following encoding indicates that the correction has been made by a scribe:

```xml
<choice><sic>errro</sic><corr>error</corr></choice>
```

If a correction is made by an editor a `resp`  attribute has to be added to the `tei:corr` element pointing to the editors `xml:id` as named in the `tei:teiHeader` (`tei:editor`).

Entry in the `teiHeader`:

```xml
<titleStmt>
  <title type="main">The Story and Proverbs of Ahiqar the Wise</title>
  <editor xml:id="sb">Simon Birol</editor>
  <editor xml:id="ae">Aly Elrefaei</editor>
</titleStmt>
```

Encoding of the correction

```xml
<choice>
  <sic>errro</sic>
  <corr resp="#sb">error</corr>
</choice>
<choice>
  <sic>errro</sic>
  <corr resp="#ae">error</corr>
</choice>
```

Please keep in mind that `tei:choice`  needs two child elements.

### Unclear reading

Transcriptions made with a somewhat higher level of uncertainty are marked up with

```xml
<unclear reason="illegible">not sure if this transcription is correct</unclear>
```

### Glyphs and other characters

For quotation marks the `tei:g` element is used with a current character that represents the original one.

```xml
<g>»</g>a quote<g>«</g>
```

### Multilingualism

When words or lines of a text written in a language different from the one specified in a superior `tei:text`  element, the language should be marked up.

```xml
<ab>a sentence <seg xml:lang="ger">mit mehreren</seg> words in German.</ab>
<ab xml:lang="ger">Wenn ganze Zeilen in einer anderen Sprache stehen.</ab>
```

Any language code used here MUST BE one of the codes mentioned in the section [Language](https://wiki.de.dariah.eu/pages/viewpage.action?pageId=95656123#EncodingGuidelines-language).

### Annotations (special case)

Harvard 80 contains several witnesses inside a single document. The copyists notes are separated by `tei:note/@resp`  if they can be distinguished. Anonymous writers are marked as &quot;anonym&quot;.

```xml
<ab>
  <note resp="#anonym">another manuscript has added <cit type="verbatim">          <quote><persName>ܣܢܚܪܝܒܼ</persName></quote>          <bibl type="provided-by-editor" source="#S5">S5</bibl>
    </cit>
  </note>
</ab>
```

While the text of the note is a direct child of `tei:note` , the word or phrase &quot;quoted&quot; from the other manuscript(s) are marked with `tei:cit` as described in [Quotes and paraphrases of other works](https://wiki.de.dariah.eu/display/AHK/Encoding+Guidelines#EncodingGuidelines-Quotesandparaphrasesofotherworks).

If the hand has been identified successfully, the `xml:id` given in `@tei:resp` has to be resolved in the TEI header within the element `tei:ms:item` using `tei:respStmt`:

```xml
<msContents>
  <msItem>
    <textLang>Classical Syriac</textLang>
    <respStmt xml:id="anonym">
      <resp notBefore="1856" notAfter="1913">Scribe</resp>
      <persName>unknown</name>
    </respStmt>
  </msItem>
</msContents>
```

#### Content of other manuscripts mentioned in annotations

In some cases known or unknown annotators add philological information by stating that a certain word or phrase is different in another manuscript. These &quot;quotes&quot; are encoded as `tei:cit` just as described in [Quotes and paraphrases of other works](https://wiki.de.dariah.eu/display/AHK/Encoding+Guidelines#EncodingGuidelines-Quotesandparaphrasesofotherworks):

```xml
<note>    [… previous text …]     
  <cit type="verbatim">
    <quote><persName>ܣܢܚܪܝܒܼ</persName></quote>
    <bibl type="provided-by-editor" source="#S5">S5</bibl>
  </cit>
</note>
```

### Enumeration labels

In some manuscripts the proverbs are enumerated. Their labels are encoded with `tei:seg[@type = "label"]`.

```xml
<ab><seg type="label">1.</seg> Text of the proverb/saying.</ab>
```

### Quotes and paraphrases of other works

In some cases the text of Ahiqar directly quotes or paraphrases other works like the Bible. These occurrences are encoded with `tei:cit`  as follows:

#### Quotation

```xml
<cit type="verbatim">
  <quote>In the beginning God created the heavens and the earth.</quote>
  <bibl type="provided-by-editor" source="#item-in-listBibl">Gen 1,1.</bibl>
</cit>
```

The source given in `@source` has to be resolved in the TEI header within `listBibl` .

```xml
<listBibl>
  <bibl xml:id="item-in-listBibl">Some reference.</bibl>
</listBibl>
```

#### Paraphrase

```xml
<cit type="paraphrase">
  <quote>The fifth dawn was breaking.</quote>
  <note type="parallel-text">
    <note type="original-phrase">And evening passed and morning came, marking the fifth day.</note>
    <bibl type="provided-by-editor" source="#item-in-listBibl">Gen 1,23.</bibl>
  </note>
</cit>
```

Again, the source given in `@source` has to be resolved in the TEI header within `listBibl` .

```xml
<listBibl>
  <bibl xml:id="item-in-listBibl">Some reference.</bibl>
</listBibl>
```

##### If a paraphrase refers to more than one other text

In some cases, a paraphrase alludes more than one other text. In this case we have to make the connection between `tei:note`  and `tei:bibl` clear by using the `@corresp` and `@xml:id` attributes.

The following example is taken from SMMJ 162:

```xml
<cit type="paraphrase">
    <quote xml:id="prov5.1.spr66">ܢܟ̈ܣܐ܂ ܥܕܡܐ ܕܡܹܬܡܲܠܝܐ ܥܦܪܐ܂</quote>
    <note type="parallel-text">
        <note corresp="#Peshitta_Prov_Eccl_etc_Prov_27_20_bibl" type="original-phrase">ܫܝܘܠ ܘܐܒܕܢܐ ܠܐ
        ܣܒܥܝܢ܂ ܗܟܢܐ ܐܦ ܥܝܢܗܘܢ ܕܒܢ̈ܝ ܐܢܫܐ ܠܐ ܣܒܥܐ</note>
        <bibl xml:id="Peshitta_Prov_Eccl_etc_Prov_27_20_bibl" type="provided-by-editor"
          source="#Peshitta_Prov_Eccl_etc">Prov 27,20.</bibl>
    </note>
    <note type="parallel-text">
        <note corresp="#Peshitta_Prov_Eccl_etc_Eccl_1_8_bibl" type="original-phrase">ܟܠܗܘܢ ܦܬ̈ܓܡܐ ܠܐܝܢ܂
         ܠܐ ܢܣܒܥ ܓܒܪܐ ܠܡܡܠܠܘ܂ ܘܠܐ ܣܒܥܐ ܥܝܢܐ ܠܡܚܙܐ܂ ܘܠܐ ܡܠܝܐ ܐܕܢܐ ܠܡܫܡܥ܂</note>
        <bibl xml:id="Peshitta_Prov_Eccl_etc_Eccl_1_8_bibl" type="provided-by-editor"
          source="#Peshitta_Prov_Eccl_etc">Eccl 1,8.</bibl>
     </note>
</cit>
```

#### Quotes and paraphrases that span more than one line

Quotes and paraphrases aren&#39;t restricted to one line but can encompass several ones. The basic mechanism is the same as connecting proverbs with each other via `@next` and `@xml:id` . It is encoded as follows (with a simplified example from SMMJ 162):

```xml
<ab>
  <cit type="paraphrase" next="#prov5.spr66.2">
    <quote next="#prov5.1.spr66">ܕܒܪܐܢܫܐ ܡܒܘܼܥܐ ܗܝܿ܂ ܘܠܐ ܣܿܒܥܐ</quote>
  </cit>
</ab>
<ab>
  <cit type="paraphrase" xml:id="prov5.spr66.2">
    <quote xml:id="prov5.1.spr66">ܢܟ̈ܣܐ܂ ܥܕܡܐ ܕܡܹܬܡܲܠܝܐ ܥܦܪܐ܂</quote>
    <note type="parallel-text">
      <note type="original-phrase">ܫܝܘܠ ܘܐܒܕܢܐ ܠܐ ܣܒܥܝܢ܂ ܗܟܢܐ ܐܦ ܥܝܢܗܘܢ ܕܒܢ̈ܝ ܐܢܫܐ ܠܐ
        ܣܒܥܐ</note>
      <bibl type="provided-by-editor" source="#Peshitta_Prov_Eccl_etc">Prov 27,20.</bibl>
    </note>
  </cit>
</ab>
```

As is documented in the example, both `tei:cit`  and `tei:quote` are connected with their respective counterpart via `@next` and `@xml:id`. Since the whole quote or paraphrase refers to the same referenced work, `tei:note`  and `tei:bibl` only have to be set once in the last `tei:cit` element of the quote/paraphrase.

## Entities

### Persons and person-like entities

```xml
<persName>Ahikar</persName>
```

We are using `persName` for human beings and other impersonated entities like gods.

### Places

```xml
<placeName>Ashur</placeName>
```

### Unsolved technical issues

#### Displaying of the biblical quotations

A complete text passage in the text panel with a biblical reference cannot be marked when it contains more than a line. In such cases, its last line is marked only.

#### Special case: The letter 'resh' with plural-marker

In some cases, when a 'resh' is combined with a plural-marker and an additional sign (e.g. a vowel-point), it is not illustrated as 'ܪ̈', but as a 'ܪ' with a pair of points (i.e. plural-marker) above it.

### How to Cite This Page

Birol, Simon and Aly Elrefaei. „Edition Guidelines“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/editionguidelines.html.
