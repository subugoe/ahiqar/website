---
title: Collation
layout: Layout
hide_sidebar: true
---

# {{ $frontmatter.title }}

## Introductory remarks

On this page, we present the results of collating the textual witnesses using CollateX (https://collatex.net/). We improved the results by removing vocalizations and punctuation, and by dividing the textual witnesses into several branches based on the stemmata. The only difference is that the West Syriac branch and the Urmia tradition have been combined into a single file (cf. [Stemmata](stemmata.html)).


### How to Cite This Page

Birol, Simon and Aly Elrefaei. „Collation“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/collation.html.

## Stemmata

<!-- DO NOT TOUCH FOLLOWING LINES. TO UPDATE SEE ../readme.md -->
<!-- HTML -->

<div xmlns:xhtml="http://www.w3.org/1999/xhtml">
    <h2>Arabic and Karshuni</h2>
    <div>
        <h3>Stemma 1</h3>
        <div>items included: Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321</div>
        <ol>
            <li>First Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_first_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_first_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_first_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Second Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_second_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_second_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_second_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Third Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_third_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_third_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_third_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Sayings<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_sayings_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_sayings_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_sayings_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Parables<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_parables_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_parables_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Brit_Mus_Add_7209_Vat_sir_159_Mingana_Syr_258_Cod_Arab_236_DFM_00614_Sachau_290_Sachau_339_Brit_Libr_Or_9321_parables_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
        </ol>
    </div>
    <div>
        <h3>Stemma 2</h3>
        <div>items included: Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497</div>
        <ol>
            <li>First Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_first_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_first_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_first_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Second Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_second_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_second_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_second_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Third Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_third_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_third_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_third_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Sayings<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_sayings_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_sayings_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_sayings_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Parables<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_parables_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_parables_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Paris_Arabe_3637_Paris_Arabe_3656_Camb_Add_2886_Mingana_ar_christ_93_84_Mingana_syr_133_Vat_ar_2054_GCAA_00486_Salhani_Borg_ar_201_Or_1292b_Ms_orient_A_2652_Cambrigde_Add_3497_parables_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
        </ol>
    </div>
    <div>
        <h3>Stemma 3</h3>
        <div>items included: Sbath_25_Vat_sir_424_Vat_sir_199</div>
        <ol>
            <li>First Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_first_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_first_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_first_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Second Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_second_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_second_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_second_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Third Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_third_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_third_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_third_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Sayings<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_sayings_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_sayings_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_sayings_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Parables<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_parables_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_parables_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_parables_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
        </ol>
    </div>
    <h2>Syriac</h2>
    <div>
        <h3>Stemma 1</h3>
        <div>items included: Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80</div>
        <ol>
            <li>First Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_first_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_first_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_first_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Second Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_second_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_second_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_second_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Third Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_third_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_third_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_third_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Sayings<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_sayings_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_sayings_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_sayings_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Parables<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_parables_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_parables_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Ar_7229_Sachau_162_162_Or_2313_Add_7200_Add_2020_Sado_no_9_Manuscrit_4122_Syr_80_parables_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
        </ol>
    </div>
    <div>
        <h3>Stemma 2</h3>
        <div>items included: Sachau_336_433</div>
        <ol>
            <li>First Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_first_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_first_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_first_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Second Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_second_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_second_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_second_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Third Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_third_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_third_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_third_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Sayings<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_sayings_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_sayings_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_sayings_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Parables<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_parables_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_parables_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_Sachau_336_433_parables_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
        </ol>
    </div>
    <div>
        <h3>Stemma 3</h3>
        <div>items included: syr_434_syr_422_430_syr_612_syr_611_Unknown</div>
        <ol>
            <li>First Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_first_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_first_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_first_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Second Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_second_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_second_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_second_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Third Narrative Section<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_third_narrative_section_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_third_narrative_section_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_third_narrative_section_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Sayings<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_sayings_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_sayings_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_sayings_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
            <li>Parables<ul>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_parables_result.csv">CSV</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_parables_result.json">JSON</a>
                    </li>
                    <li>
                        <a href="{{ VITE_APP_BASE_URL_API }}/rest/collation-results/syc_syr_434_syr_422_430_syr_612_syr_611_Unknown_parables_result.svg">SVG</a>
                    </li>
                </ul>
            </li>
        </ol>
    </div>
</div>
