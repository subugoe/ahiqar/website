---
home: false
title: Textual Criticism in the "Ahiqar" Project
layout: Layout
---

# {{ $frontmatter.title }}

In the Ahiqar project, the collation is done automatically by [CollateX](https://collatex.net/), a collation tool. To prepare the collation, the we take the following steps:

1. Each manuscript has markers in it which denote semantic units within the text. These units are
    * first narrative section
    * second narrative section
    * third narratice section
    * parables
    * sayings
    The manuscript is divided into these units.
2. For each unit we extract the words without punctuation. In case of the Syriac texts, certain diacritica are excluded to simplify the collation.
3. We create a JSON-based input for CollateX.

The collation takes place on demand on our [collation repository](https://gitlab.gwdg.de/subugoe/ahiqar/collatex). The results are then fed back into our data base.

## Peculiarites

The textual criticism in the online version of "Ahiqar" does not predefine any base text; we rather regard each manuscript that a user views as a kind of base text from which variants are generated. Variants are only generated within one language and, what is important to note, within one line of transmission. These lines of transmission can be comprehended at the [Stemmata section]({{ VITE_APP_BASE_URL_API }}/stemmata.html#ii-textual-witnesses-in-classical-syriac) of this website.

The variants apparatus in "Ahiqar" is positive, i.e. we display the full text of the other manuscripts as a "variant". We currently determine possibility to switch to a negative apparatus as well.

Furthermore, it should be noted that the online version of the variant apparatus contains no abbreviations, corrections or marginal notes. Such instances are recorded separately in the annotation panel. 

In addition to the typical variations, the apparatus also encompasses disparate spellings of a given word. This is done in order to highlight existing relationships between individual textual witnesses and to avoid the unnecessary inflation of the apparatus that would result from including a large number of such spellings. It should be noted, however, that this only concerns different orthographies of the consonants of individual words, not of the vowels. With regard to the latter, it is evident that copyists often proceeded arbitrarily.

The apparatus includes only the manuscripts of the respective linguistic tradition. Consequently, the variant apparatus of an Arabic text contains only the deviations from the Arabic texts. The texts in Karshuni are included in their transliterated form in the apparatuses of these Arabic texts.
