---
home: true
heroText: Ahiqar
tagline: The Story of Ahiqar in its Syriac and Arabic Tradition
---

<div class="container">
<div class="row">
  <div class="col-md-6">
    <h2>Welcome</h2>
    <Cta/>
    <p class="body-1"><I>The Story of Ahiqar in its Syriac and Arabic Tradition</I> is a project funded by the German Research Foundation (DFG) and hosted by both the Faculty of Theology at the University of Göttingen and the Göttingen State and University Library. It aims to index and make accessible the Ahiqar story in its Syriac and Arabic traditions.</p>
    <p class="body-1"><i>The image on the right is taken from fol. 3v of Ms Hunter Or. 40 (1532). The identities of the four main figures are given next to or above them. From right to left: Aristotle, Galen, Plato and Ahiqar. On the left is the inscription: 'These are the wise who know God: He is the wise, the healer.'</i></p>
  </div>
  <div class="col-md-6 image">
    <a href="./assets/images/banner.jpg" target="_blank">
      <Banner/>
    </a>
  </div>
</div>

## Plot of the Ahiqar Story

The story of the sage Ahiqar is one of the most frequently edited and reworked stories in ancient Near Eastern literature. Ahiqar also appears in the apocryphal Book of Tobit. It describes how the childless Ahiqar, counsellor at the court of the Assyrian kings Sennacherib (reigned 705-681 BC) and Esarhaddon (reigned 681-669 BC), adopts and trains his nephew Nadan to succeed Ahiqar. However, it is Nadan himself who conspires against his uncle and attempts to kill him. Ahiqar escapes execution by trickery and, hidden away, appears in time to solve a riddle that the Assyrian king received from the Egyptian Pharaoh. As a result, his nephew is condemned to death; but Ahiqar, who is reinstated, has the opportunity to lecture Nadan on morality before his nephew dies.



## The peculiarities of the Syriac and Arabic versions

The story of Ahiqar spread beyond the Middle East. The oldest source is an Aramaic papyrus from the 5th century BC, in which it seems clear that several earlier materials (stories and proverbs) are interwoven. There are many other versions of Ahiqar’s story, not only in Aramaic, but also in Syriac, Arabic, and many other ancient and modern languages.

The extensive Syriac and Arabic versions played a central role in the transmission of the (originally Aramaic) material into other languages and literary traditions. The last attempt so far to compile a reasonably comprehensive account of the tradition of the Ahiqar’s stories was made more than a hundred years ago by F. C. Conybeare et al. in 1913, but the study does not include all known textual witnesses. As for the Aramaic version, several publications have appeared since then, making it easily accessible. Despite the fundamental importance of the Syriac and Arabic versions, reliable textual editions are still lacking. The aim of the project is to fill this gap by providing a textual and literary analysis of the Syriac and Arabic versions of the Ahiqar tradition in digital form. in order to establish the material and technical basis for further research on the other versions.

To this end, the main textual witnesses of the Syriac and Arabic versions have been identified, transcribed, and translated. The names of persons, places, motifs, and biblical references have also been marked. A brief description of the manuscripts has been added, together with a list of relevant publications. Several of these manuscripts exhibit characteristics of the scribe's own dialect of Syriac (especially regarding vocalization and spirantization). Only those phenomena that can be described as gross errors have been corrected (in terms of orthography and content).

All texts in this archive are licensed under a Creative Commons licence (CC-BY-SA). These texts have been created as XML files with their own schema, following the Guidelines for Electronic Text Encoding and Interchange (in version P5) of the Text Encoding Initiative (TEI). However, some of the images have also been made available under a Creative Commons licence (CC-BY-SA), too, while others can only be used with a VPN connection to the University of Göttingen.
## Contact
<Contact/>
</div>



