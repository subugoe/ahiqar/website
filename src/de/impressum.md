---
home: false
title: Impressum
layout: Layout
---

# {{ $frontmatter.title }}

Bitte lesen Sie das [Impressum der Niedersächsischen Staats- und Universitätsbibliothek Göttingen](https://www.sub.uni-goettingen.de/impressum/).
