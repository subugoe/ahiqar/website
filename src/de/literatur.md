---
home: false
title: Literatur
layout: Layout
---

# {{ $frontmatter.title }}

## Syrischer Überlieferungszweig

### Textausgaben bzw. Übersetzungen

* Braida, Emanuela (2014): The Romance of Ahiqar the Wise in the Neo-Aramaic MS London Sachau 9321, in: Journal of the Canadian Society for Syriac Studies 14, pp. 17-31.
* Conybeare, Frederick Cornwallis/Harris, James Rendel/Lewis, Agnes Smith (eds.) (1898). The Story of Aḥiḳar from the Syriac, Arabic, Armenian, Ethiopic, Greek and Slavonic Versions. London (C. J. Clay and Sons).
* Conybeare, Frederick Cornwallis/Harris, James Rendel/Lewis, Agnes Smith, (eds.) (1913). The Story of Aḥiḳar from the Aramaic, Syriac, Arabic, Armenian, Ethiopic, Old Turkish, Greek and Slavonic Versions . 2nd ed., Cambridge (Cambridge University Press).
* Dolabani, Philoxenus Yuhanna, (ed.) (1962), ed. Aḥīqar sāprā w-ḥakīmā. Mardin (Maṭbaʻtā d-Mardīn).
* Ferrer, Joan and Monferrer-Sala, Juan Pedro (2006): Historia y enseñanzas de Ahíqar o La antigua sabiduría oriental: edición, traducción y estudio. Studia Semitica, Series Minor 2. Cordoba (Universidad de Córdoba).
* Goshen-Gottstein, Moshe H. (ed.) (1965): Wisdom of Aḥiqar, Syriac and Aramaic. Jerusalem (The Hebrew University of Jerusalem).
* Grünberg, Smil (1917). Die weisen Sprüche des Achikar [Ahiqar] nach der syrischen Hs. Cod. Sachau Nr. 336 der Kgl. Bibliothek in Berlin herausgegeben und bearbeitet . Berlin (H. Itzkowski).
* Guzik, Markus Hirsch (ed.) (1936). Die Achikar-Erzählung nach der syrischen Handschrift Cod. Sachau Nr. 336 der Preussischen Staatsbibliothek in Berlin, Krakau (Renaissance).
* Lidzbarski, Mark (1896). Geschichten und Lieder aus den neu-aramäischen Handschriften der Königlichen Bibliothek zu Berlin. Beiträge zur Volks- und Völkerkunde 4. Weimar (E. Felber).
* Mūḥtas, Rūbīl, (ed.) (1941). Kūnāšā d-tašʿīṯā u-maṯlē ḏ-Aḥīqar ḥakkīmā. ‘am mēmrē ḏ-pelōṭarkōs u-medem medem min pardaysā ḏ-aṿāhāṯā. Trichur (Mar Narsai Press) (Reprint 1961).
* Nau, François (1909). Histoire et Sagesse d Ahikar l Assyrien (fils d Anaël, neveu de Tobie). Traduction des versions syriaques avec les principales différences des versions arabes, arménienne, grecque, néo syriaque, slave et roumaine. Documents pour l étude de la Bible. Paris (Letouzey et Ané).
* Nau, François (1920): Documents relatifs à Ahikar. Édition et traduction d un manuscrit de Mgr Graffin (C), avec les principales variantes d un manuscrit de M.H. Pognon (P). Édition de la partie récente du manuscrit de M.H. Pognon, Paris (Libraire Auguste Picard) (Nachdruck der gleichnamigen Beiträge von: Revue de l Orient chrétien III, 1 (21) (1918/1919 ), pp. 148-160).
* Talay, Shabo (2002): Die Geschichte und die Sprüche des Aḥiqar im neuaramäischen Dialekt von Mlaḥsô, in: Werner Arnold, Hartmut Bobzin (eds.): "Sprich doch mit deinen Knechten aramäisch, wir verstehen es!” Beiträge zur Semitistik. Festschrift für Otto Jastrow zum 60 . Geburtstag . Wiesbaden (Harrassowitz), pp. 650-666.
* Talon, Philippe (2013): Histoires syriaques. La Sagesse d'Ahiqar et autres œuvres syriaques. Nouvelles Études Orientales. Fernelmont (E.M.E. Editions).
* Tronina, Antoni/ Starowieyski, Marek (2011): Apokryfy syryjskie: Historia i przysłowia Achikara, Grota Skarbów, Apokalipsa Pseudo-Metodego. Pisma apokryficzne 6. Krakow (Wydawnictwo WAM).

### Einträge in Handschriftenkataloge

* Briquel-Chatonnet, Françoise (1997): Manuscrits syriaque. de la Bibliothèque nationale de France (nos 356 - 435, entrés depuis 1911), de la bibliothèque Méjanes d'Aix-en-Provence, de la bibliothèque muncipale de Lyon et de la Bibliothèque nationale et universitaire de Strasbourg. Paris (Bibliothèque nationale de France).
* Forshall, J./ Rosen, F.A. (1838): Catalogus codicum manuscriptorum orientalium qui in Museo Britannico asservantur. Pars prima: codices syriacos et carshunicos amplectens, London (Impensis curatorum Musei Britannici).
* Goshen-Gottstein, Moshe H. (1979): Syriac manuscripts in the Harvard College Library. A catalogue, Harvard Semitic Studies 23, Ann Arbor (Scholars Press).
* Lidzbarski, Mark (1896): Die neu-aramäischen Handschriften der Königlichen Bibliothek zu Berlin. Ergänzungshefte zur Zeitschrift für Assyriologie, Semitistische Studien 4/9,1-2. Weimar (E. Felber).
* Mingana, Alphonse 1933-1939): Catalogue of the Mingana Collection of Manuscripts now in the Possession of the Trustees of the Woodbrooke Settlement, 3 vols., Cambridge: W. Heffer & Sons.
* Sachau, Eduard (1898): Verzeichniss der syrischen Handschriften der Königlichen Bibliothek zu Berlin, 2 vols., Berlin (A. Asher & Co).
* Sarau, Oshana/ Shedd, William A. (1898): Catalogue of the Syriac manuscripts in the Library of the Museum Association of Oroomiah College, Urmia: (N. N.).
* Scher, Addai (1906): Notice sur les manuscrits syriaques conservés dans la bibliothèque du couvent des Chaldéens de Notre-Dame-des-Semences, Paris (Imprimerie Nationale).
* Vosté, Jean-Marie (1929): Catalogue de la Bibliothèque syro-chaldéenne du Couvent de Notre-Dame des Semences près d'Alqosh (Iraq). Rome/ Paris (Bureaux of the «angelicum»/ P. Geuthner).
* Wright, William (1870-2): Catalogue of Syriac Manuscripts in the British Museum acquired since the year 1838, 3 vols. London (Longmann & Co. and Asher & Co).

### Weitere Literatur

* al-Baraṭlī, Quryāqūs Ḥanna (2007): Taš‘ita d-Aḥiqar. Qiṣṣat Ahīqār. Dohuk (Dar al-Mašriq al-Thaqāfīyah).
* Arzhanov, Yury N. (2019): Syriac Sayings of Greek Philosophers: A Study in Syriac Gnomologia with Edition and Translation. Corpus scriptorium Christianorum Orientalium 669, Subsidia 138. Leuven (Peeters).
* Ayali-Darshan, Noga (2018): The Sequence of Sir 4:26–27 in Light of Akkadian and Aramaic Texts from the Levant and Later Writings, in: Zeitschrift für die alttestamentliche Wissenschaft 130,3, pp. 436-449.
* Barton, George Aaron (1900): The Story of Aḥiḳar and the Book of Daniel. In: The American Journal of Semitic Languages and Literatures 16,4, pp. 242–247.
* Baumstark, Anton (1922). Geschichte der syrischen Literatur mit Ausschluß der christlich-palästinensischen Texte, Bonn: A. Marcus und E. Webers Verlag Dr. jur. Albert Ahn.
* Bledsoe, Seth A. (2013): Can Ahiqar Tell Us Anything about Personified Wisdom? Journal of Biblical Literature 132,1, pp. 119–136.
* Bledsoe, Seth A. (2014): The Relationship between the Elephantine Ahiqar Sayings and Later Recensions: A Preliminary Analysis of the Development and Diffusion of the Ahiqar tradition, in: Marie-Christine Bornes-Varol, Marie-Sol Ortola: Enoncés sapientiels: traductions, traducteurs et contextes culturels . Aliento Échanges sapientiels en Méditerranée 4. Nancy (Presses de Nancy), pp. 223- 50.
* Bousset, Wilhelm (1905): Beiträge zur Achikarlegende. In: Zeitschrift für die Neutestamentliche Wissenschaft 6,1, pp. 180–193.
* Braida, Emanuela (2012): Garshuni Manuscripts and Garshuni Notes in Syriac Manuscripts, in: Parole de l’Orient 37, pp. 181–198.
* Braida, Emanuela (2015): The Romance of Ahiqar the Wise in the Neo-Aramaic MS London Sachau 9321: Part II, in: Journal of the Canadian Society for Syriac Studies 15 (2015), pp. 41-50.
* Braida, Emanuela / Destefanis, Simona (2008), “I fratelli hanno bisogno dei fratelli e i re dei re”. Ipotesi sull’origine di un episodo delle versioni siriache e araba de Romanzo di Ahiqar, in: Alessandro Mnti et al. (ed.): Essays in Honour of Fabrizio Pennacchietti. Alessandria (Edizioni dell’Orso), pp. 35–41.
* Braida, Emanuela (2014): Neo-Aramaic Garshuni: Observations Based on Manuscripts, in: Hugoye: Journal of Syriac Studies 17,1, pp. 17-31.
* Braida, Emanuela (2010): Il Romanzo del saggio Ahiqar: una proposta stemmatica, in: Frederick Mario Fales/ Fausta Grassi (eds.): Camsemud 2007. Proceedings of the 13th Italian Meeting of Afro-Asiatic Linguistics, Held in Udine, May 21st-24th, 2007. History of the Ancient Near East, Monographs 10. Padova (S.A.R.G.O.N.), 2010, pp. 49-64.
* Briquel-Chatonnet, Françoise (2005): L’histoire et la sagesse d’Aḥiqar: fortune littéraire de la vie d’un dignitaire araméen à la cour assyrienne. In: D’un Orient l’autre. Actes des 3e Journées
* de l’Orient, Bordeaux, 2–4 octobre 2002, éd. J.-L. Bacqué-Grammont, A. Pino et S. Khoury (Cahiers de la société asiatique, nouvelle série IV). Paris, Louvain (Peeters), pp. 17–40.
* Briquel-Chatonnet, Françoise (2007): De l’Aḥiqar araméen à l’Aḥiqar syriaque: les voies de transmission d’un roman, in: Sophia G. Vashalomidze/ Lutz Greisiger (eds.): Der Christliche Orient und seine Umwelt. Gesammelte Studien zu Ehren Jürgen Tubachs anläßlich seines 60. Geburtstags. Wiesbaden (Harrassowitz), pp. 51–57.
* Briquel-Chatonnet, Françoise (2006): “Construis-moi un château dans le ciel”. Remarques sur un motif de conte, d’Ahiqar à Thomas, in: The Harp 20, pp. 55-64.
* Brock, Sebastian (1968): A piece of wisdom literature in Syriac". Pages 212-217. In: Journal of Semitic Studies 13,2, pp. 212-217.
* Brock, Sebastian (1969): Notes on some Texts in the Mingana Collection, in: Journal of Semitic Studies 14,2, pp. 205-226.
* Contini, Riccardo/ Grottanelli, Cristiano (eds.) (2005): Il saggio Ahiqar. Studi Biblici. Brescia (Paideia Editrice).
* Degen, Rainer (1975): Achikar, in: Kurt Ranke (ed.): Enzyklopädie des Märchens. Berlin (de Gruyter), pp. 53-59.
* Denis, Albert-Marie (2000): Introduction à la littérature religieuse judéo-hellénistique. Pseudepigraphes de l'Ancien Testament, 2 vols., Turnhout (Brepols).
* Destefanis, Simona / Braida, Emanuela (2007): An Outline of the Romance and Proverbs of the Wise Ahiqar and its Modern Neo-Aramaic Version, in: Alessandro Monti (ed.): Roads To Knowledge: Hermeneutical and Lexical Probes. DOST Critical Studies 1. Alessandria (Edizioni dell’Orso), pp. 19–26.
* Destefanis, Simona / Braida, Emanuela (2010): ''I Proverbi di Ahiqar nella versione neoaramaica di Rubeyl Muhattas. Un’analisi comparativa delle sue fonti, in: Frederick Mario Fales/ Fausta Grassi (eds.): Camsemud 2007. Proceedings of the 13th Italian Meeting of Afro-Asiatic Linguistics, Held in Udine, May 21st-24th, 2007. History of the Ancient Near East, Monographs 10. Padova (S.A.R.G.O.N.), 2010, pp. 221-228.
* Dillon, Emile Joseph (1898): Ahikar the Wise: An Ancient Hebrew Folk Story, in: Contemporary Review 73, pp. 362-386.
* Gaster, Moses (1900): Contributions to the History of Aḥiḳar and Nadan, in: Journal of the Royal Asiatic Society 32,2, pp. 301–319 (Art. XIII).
* Holm, Tawny L. (2014): Memories of Sennacherib in Aramaic Tradition, in: Isaac Kalimi/Seth Richardson (eds.): Sennacherib at the Gates of Jerusalem: Story, History and Historiography. Culture and History of the Ancient Near East 71. Leiden/Boston (Brill), pp. 295-323.
* Karouby, Laurent (2013): “Histoire et Sagesse d’Aḥiqar l’Assyrien” ou l’Ummānu sans descendance: invariance et variations, de l’Antiquité au XVIIIe siècle'' (unpublished dissertation, Aix-Marseille Université).
* Khan, Geoffrey(2016): The Neo-Aramaic Dialect of the Assyrian Christians of Urmi. Studies in Semitic Languages and Linguistics 86. Leiden/Boston (Brill).
* Kottsieper, Ingo (1990): Die Sprache der Aḥiqarsprüche. Beihefte zur Zeitschrift für die alttestamentliche Wissenschaft 194, Berlin (De Gruyter).
* Kottsieper, Ingo (1991): Die Geschichte und die Sprüche des weisen Achiqar. In: Texte aus der Umwelt des Alten Testaments 3,2, Gütersloh (Mohn).
* Küchler, Max (1979): Frühjüdische Weisheitstraditionen. Zum Fortgang weisheitlichen Denkens im Bereich des frühjüdischen Jahweglaubens. Orbis Biblicus et Orientalis 26, Fribourg (Universitätsverlag) and Göttingen (Vandenhoeck und Ruprecht).
* Kuhn, Ernst (1892): Zum weisen Akyrios, in: Byzantinische Zeitschrift 1,1, pp. 127–130.
* Lidzbarski, Mark (1894): Zum weisen Achikâr, in: Zeitschrift der Deutschen Morgenländischen Gesellschaft 48, pp. 671-675.
* Lindenberger, James M. (1985): Ahiqar: A New Translation and Introduction, in: J. H. Charlesworth: Expansions of the “Old Testament” and Legends, Wisdom and Philosophical Literature,
* Prayers, Psalms and Odes, Fragments of Lost Judeo-Hellenistic Works. Vol. 2: The Old Testament Pseudepigrapha. London: Darton, Longman & Todd, pp. 479-507.
* Lourié, Basil (2013): The Syriac Aḥiqar, Its Slavonic Version, and the Relics of the Three Youths in Babylon, in: Slověne 2,2 , pp. 64-117.
* Lourié, Basil (2015): Direct Translations into Slavonic from Syriac: A Preliminary List, in: Cristiano Diddi (ed.): ΠΟΛΥΙΣΤΩΡ. Scripta slavica Mario Capaldo dicata. Moscow/Rome (Индрик), pp. 161-168.
* Meißner, Bruno (1894): Quellenuntersuchungen zur Haikargeschichte, in: Zeitschrift der Deutschen Morgenländischen Gesellschaft 48, pp. 171-97.
* Meißner, Bruno (1917): Das Märchen vom weisen Achiqar. Der Alte Orient 16:2, Leipzig (Hinrichs).
* Moore, James (2017): “’I Am Unable to Do My Job’: Literary Depictions of the Scribal Profession in the Story of Ahiqar and Jeremiah 36” (unpublished dissertation, Brandeis University).
* Nau, François (1907): Le mot ’arhe’ dans Ahikar et Bar Bahlul, in: Journal asiatique 10,9, pp. 149-159.
* Nau, François (1906): Note sur trois anciennes traductions de la légende d’Ahikar et de la Chronique d’Édesse, in: Actes du XIVe Congrès international des Orientalistes, Alger 1897. Première partie: Procès-verbaux. Paris (E. Leroux), p. 69.
* Nau, Francois (1914): Préceptes anonyms et histoire d’Ahiqar, in: Revue de l’Orient chrétien 19, pp. 209-214.
* Niditch, Susan (1976): A Test Case for Formal Variants in Proverbs, in: Journal of Jewish Studies 27,2, pp. 192-194.
* Niehr, Herbert (2007): Aramäischer Aḥiqar. Jüdische Schriften aus hellenistisch-römischer Zeit, Neue Folge 2,2. Gütersloh (Gütersloher Verlagshaus).
* Nöldeke, Theodor (1913). Untersuchungen zum Achiqar Roman . Abhandlungen der Königlichen Gesellschaft der Wissenschaften zu Göttingen, Philologisch Historische Klasse NF 14.4 . Berlin (Weidmannsche Buchhandlung).
* Smend, Rudolf (1908): Alter und Herkunft des Achikar-Romans und sein Verhältnis zu Aesop. Beihefte zur Zeitschrift für die alttestamentliche Wissenschaft 13, Berlin (De Gruyter), pp. 57–125.
* Talia, Shawqi (2015): The Story of Ahiqar in Syriac and Neo Aramaic: A Textual, Linguistic and Historical Comparison, in: Parole de l Orient 40, 13-27.
* Weigl, Michael (2010): Die aramäischen Achikar-Sprüche aus Elephantine und die alttestamentliche Weisheitsliteratur. Beihefte zur Zeitschrift für die alttestamentliche Wissenschaft 399, Berlin (De Gruyter).
* Yellin, Avinoam (1924): Notes on the Syriac Version of the Story of Ahikar as Edited by J. Rendel Harris, in: Jewish Quarterly Review NS 15,1, pp. 119-121.
