---
home: false
title: Stemmata
layout: Layout
---

# {{ $frontmatter.title }}

## Der syrische Überlieferungszweig

### I. Neuaramäische Textzeugen

Überliefert ist die Ahiqar-Erzählung sowohl im klassischen Syrischen als auch in den neusyrischen Dialekten der Ost- und Westsyrer (auch bekannt als 'Aramäer', 'Assyrer' oder 'Chaldäer'; Selbstbezeichnung: Suryoye bzw. Suraye). Die neusyrischen Versionen dürften allesamt aus dem Arabischen übersetzt worden sein und sind daher für die syrische Überlieferung außen vorgelassen. Es handelt sich um folgende Manuskripte (sortiert nach Erstellungszeitraum):

#### A. Staatsbibliothek zu Berlin, Sachau 339 (gelistet bei Sachau unter der Laufnummer 290)

Seiten: 1r-59r

Abfassungszeit: Zwischen 1881-1889

Kopist: 'Ešaʻyā aus Qilith

Herkunft: westsyrisch (Tur Abdin)

Katalogeintrag: Sachau 1889, Bd. 2, 815, Link: [https://archive.org/details/bub_gb_v_g3AQAAMAAJ/page/n375/mode/2up](https://archive.org/details/bub_gb_v_g3AQAAMAAJ/page/n375/mode/2up)

Link zum Digitalisat: [https://digital.staatsbibliothek-berlin.de/werkansicht?PPN=PPN862951585&PHYSID=PHYS_0005](https://digital.staatsbibliothek-berlin.de/werkansicht?PPN=PPN862951585&PHYSID=PHYS_0005)

Transkription & Übersetzung: Lidzbarski 1896, 2 Bde. (Link: [https://archive.org/details/bub_gb_HpZwlQ3FTLAC/page/n9/mode/2up](https://archive.org/details/bub_gb_HpZwlQ3FTLAC/page/n9/mode/2up))

#### B. British Library (London), Brit. Libr. Add. 9321 (auch bekannt als "MS London-Sachau 9321")

Seiten: fol. 536b-620b

Abfassungszeit: um 1897

Kopist: Ǧibra'īl Qūryaqūzā

Herkunft: ostsyrisch (Dialekt von 'Alqōš (Irak))

Katalogeintrag: nicht vorhanden

Link zum Digitilisat: nicht vorhanden

Transkription & Übersetzung: Braida 2014

#### C. Tellkepe (Iraq), QACCT 135

Seiten: fol. 230a-246a

Abfassungszeit: 1937

Kopist: Diakon Matīkā bar Yawsep mār Mīkā bar Qūryaqūs bar Ǧerǧīs Ḥadādē beth Ḥaǧī

Herkunft: ostsyrisch ('Alqōš (Irak))

Katalogeintrag: [https://w3id.org/vhmml/readingRoom/view/136599](https://w3id.org/vhmml/readingRoom/view/136599)

Link zum Digitilisat: [https://w3id.org/vhmml/readingRoom/view/136599](https://w3id.org/vhmml/readingRoom/view/136599)

Transkription & Übersetzung: nicht vorhanden

Hinzu kommt die mündlich tradierte Erzählung aus Mlaḥsō (Südosttürkei), die ebenso aus dem Arabischen entlehnt und daher auch ausgeklammert ist (s. hierzu: Talay 2002).

### II. Textzeugen im Klassischen Syrisch

Bei den Handschriften im klassischen Syrischen ist der Sachverhalt ein anderer: Insgesamt wurden 21 Textzeugen identifiziert, die wiederum in einer ost- und westsyrischen Tradition unterteilt werden können. Vier Manuskripte gelten als verloren und konnten nicht aufgespürt werden. In einem Fall sind zumindest  Lesarten einer der verlorengegangenen Handschriften als Randnotizen von Harvard syr. 80 (= Ms. Urmia 270) überliefert. Zudem liegt in einem anderen Fall eine von Nau erstellte Transkription einer weiteren verlorengegangenen Handschrift (= Ms. Graffin) vor.

Bei einer Handschrift aus dem Tur Abdin handelt es sich um eine Abschrift einer der genannten Druckausgaben – sie bleibt für das Projekt unberücksichtigt.

Among those manuscripts form Tur Abdin, there is a copy of one of the published version. Therefore, this manuscript will be neglected for this project.

#### II.I Verlorene Textzeugen

##### A. College of Urmia (Iran), Ms. Urmia 115

Seiten: unbekannt

Abfassungszeit: 1868/9

Kopist: Anonym

Herkunft: ostsyrisch

Katalogeintrag: Sarau/Shedd 1898, 21

Link zum Digitilisat: nicht vorhanden

Transkription & Übersetzung: nicht vorhanden

##### B. College of Urmia (Iran), Ms. Urmia 117

Seiten: unbekannt

Abfassungszeit: 1887

Kopist: Šmū'īl Tḥūmānāyā d-Mazrʻā

Herkunft: ostsyrisch

Katalogeintrag: Sarau/Shedd 1898, 21

Link zum Digitilisat: nicht vorhanden

Transkription & Übersetzung: Lesarten in Harvard syr. 80;

Weiteres: keine komplette Abschrift seiner ca. 800 Jahre alten verlorengegangenen Vorlage

##### C. College of Urmia (Iran), Ms. Urmia 230

Seiten: unbekannt

Abfassungszeit: 1894

Kopist: Yōḥanān bar Ṭalyā da-Tḥūmā

Herkunft: ostsyrisch

Katalogeintrag: Sarau/Shedd 1898, 37

Link zum Digitilisat: nicht vorhanden

Transkription & Übersetzung: nicht vorhanden

##### D. Ms. Graffin

Seiten: 1-56

Abfassungszeit: 1908

Kopist: Priester Elias, Abt des Klosters Rabban Hormizd (Neffe des Bischofs Addai Scher)

Herkunft: ostsyrisch ('Alqōš (Irak))

Katalogeintrag: nicht vorhanden (s. Transkription & Übersetzung)

Link zum Digitilisat: nicht vorhanden

Transkription & Übersetzung: Nau 1918-1919, 274-307 und 356-380

Weiteres: Auftragswerk für Bischof Addai Scher

#### II.II Further text witnesses

##### A. Midyat (Tur Abdin), MGMT 192

Seiten: 3-42

Abfassungszeit: 1964/5

Kopist: Ḥanna Qermez

Herkunft: westsyrisch (Tur Abdin)

Katalogeintrag: [https://w3id.org/vhmml/readingRoom/view/123101](https://w3id.org/vhmml/readingRoom/view/123101)

Link zum Digitilisat: [https://w3id.org/vhmml/readingRoom/view/123101](https://w3id.org/vhmml/readingRoom/view/123101)

Transkription & Übersetzung: Abschrift der Ausgabe Dolabanis (= Abschrift der ersten Ausgabe Conybeares et al. 1898 mit Korrekturen)

Transcription & translation: Copy of the edition of Dolabanis (i. e. copy of the first edition of Conybeare et al. 1898 with additional corrections)

Die restlichen 16 Textzeugen (zusammen mit der Abschrift von Ms. Graffin) wurden wie folgt aufgeteilt: Im linken Bildrand befindet sich die westsyrische Überlieferung, deren ältester Textzeuge Sachau 162 aus dem 15. Jahrhundert ist. Diese Urkunde ist stark beschädigt und reißt inmitten der Sprüche ab. Die anderen beiden Handschriften weisen nur die Sprüche auf, wobei Aleppo SCAA 7229 auch die Parabeln überliefert. Dieses überschaubare Ergebnis für die westsyrische Tradition erklärt gleichzeitig, warum bei der westsyrischen Druckausgabe aus dem Jahr 1929 des Bischofs Dolabani, der einer der besten Handschriftenkenner dieser Tradition war, die Erstausgabe von Conybeare et al. abgeschrieben, korrigiert und publiziert hatte.

<img src="./../img/image2021-5-1_16-37-23.png" width="500">

Die übrigen Textzeugen zählen zu der ostsyrischen Tradition, wobei abgesehen von den beiden älteren Fragmenten Brit. Libr. Add. 7200 und Brit. Libr. Or. 2313 die restlichen Schriften sicher räumlich verortet werden können: Ein Traditionsstrang stammt aus Urmia (Iran) und blieb dort bis zum Anbeginn des Ersten Weltkriegs präsent. Als wichtigster Textzeuge gilt Cambridge Add. 2020, das auch für die Ausgaben von Conybeare et al. die Basis bildete. Diese Handschrift wurde zwar im Nordirak erstellt, jedoch unterscheidet sich einerseits von den späteren Textzeugen dieser Region und weist andererseits vielmehr Übereinstimmungen zu den weiteren drei Urkunden aus Urmia auf. Diese drei Urkunden stehen dabei in einem nahen Verwandtschaftsverhältnis zueinander, weisen jedoch diverse Sonder- und Trennfehler auf, die eine direkte Abhängigkeit unmöglich machen. Auch stimmt die Anordnung der Sprüche, trotz diverser Einfügungen, überein.

Eine zweite Tradition ist im Nordirak beheimatet, wobei diese Überlieferung diverse Zusätze und Erläuterungen, die in der Tradition von Urmia fehlen, aufweist. Die Überlieferung aus dem Nordirak ist in zwei Strängen aufzuzeichnen: Bei der am äußersten Rand abgebildeten Tradition handelt es sich um zwei Manuskripte, von denen große Teile aus dem Arabischen rückübersetzt wurden. Die ältere Handschrift Birmingham Mingana syr. 433 ist gemäß der Satzstellung und dem Wortlaut näher an den übrigen Urkunden als Berlin Sachau 336. Nur in dieser Handschrift taucht anstelle des Namens „Nadan“ der Name „Nadab“ – so wie in Tobit 14,10 – auf. Selbst in der arabischen Überlieferung ist diese Schreibweise nicht vorhanden, sodass eine Verwechslung der Buchstaben ausgeschlossen werden kann. Die Nähe zu Berlin Sachau 336 ist offensichtlich, jedoch weist Sachau 336 größere Abweichungen und mehrere Hinzufügungen, darunter die meisten Sprüche und Parabeln, zu allen anderen Textzeugen auf. Was das im Einzelnen bedeutet, hält Nöldeke (1913, 54) fest:

B (= Sachau 336; Anm. SB) ist also zusammengesetzt aus ziemlich arg entstellten Originalstücken und aus solchen, die aus dem Arabischen nicht sehr geschickt retrovertiert sind. Das wird so zu erklären sein: ein Abschreiber kopierte eine von vorn und gegen das Ende defekte syrische Handschrift und ergänzte sie durch Rückübersetzung aus einer arabischen. Dieser buntscheckige Text mag weiter durch verschiedene Schreiberhände gegangen sein und noch allerlei Ungemach erlitten haben, bis er die Gestalt gewann, in der er uns in der Sachauschen Handschrift vorliegt. Will jemand diese Gestalt ein Monstrum horrendum informe nennen, so kann ich ihm nicht widersprechen. Aber trotzdem kann die Wissenschaft auch aus diesem Achikar-Text Nutzen ziehen.
