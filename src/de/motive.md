---
home: false
title: Motive
layout: Layout
---

# {{ $frontmatter.title }}

Bereits 1979 begann Max Küchler in seiner Studie "Frühjüdische Weisheitstraditionen" (Fribourg/Göttingen 1979) seine Ausführungen zu Ahiqar mit einer stichwortartigen Zusammenstellung an Themen, die in der Ahiqar-Erzählung behandelt werden. In Anknüpfung daran sind im Folgenden die zentralen Motive und Themen, die in den verschiedenen Transkriptionen Erwähnung finden, zusammengestellt und ausgezeichnet worden. Mit diesem Schritt ist eine motivgeschichtliche Analyse der syrischen und arabischen Aḥiqar-Traditionen erarbeitet, mit deren Hilfe das bisherige Bild von der Überlieferungsgeschichte korrigiert, präzisiert und erheblich erweitert wird. Eine kurze Beschreibung der diversen stichwortartigen Oberbegriffe folgt:

<!-- markdownlint-disable MD033 -->
<template>
  <v-simple-table>
    <template v-slot:default>
      <thead>
        <tr>
          <th class="text-left">
            Buchstabe
          </th>
          <th class="text-left">
            Kennwort
          </th>
          <th class="text-left">
            Kategorie
          </th>
          <th class="text-left">
            Beschreibung
          </th>
        </tr>
      </thead>
      <tbody>
        <tr
          v-for="item in motifs"
          :key="item.letter"
        >
          <td>{{ item.letter }}</td>
          <td>{{ item.keyword }}</td>
          <td>{{ item.category }}</td>
          <td>{{ item.description }}</td>
        </tr>
      </tbody>
    </template>
  </v-simple-table>
</template>

<script>
  export default {
    data () {
      return {
        motifs: [
          {
            letter: 'B',
            keyword: 'Belehrung der Söhne',
            category: 'Soziale Beziehung, Familie',
            description: 'Unterweisung und Erziehung der eigenen Kinder',
          },
          {
            letter: 'B',
            keyword: 'Bewahren von Geheimnissen',
            category: 'Soziale Beziehung, Loyalität',
            description: 'Umgang mit Geheimnissen',
          },
          {
            letter: 'B',
            keyword: 'Bürde/Schuld',
            category: 'Verpflichtung',
            description: 'Stand und Situation einer schuldigen oder mit Verpflichtung beladenen Person',
          },
          {
            letter: 'D',
            keyword: 'Darstellung des Königs',
            category: 'Politik, König',
            description: 'Bild des assyrischen Königs',
          },
          {
            letter: 'D',
            keyword: 'Demut und Respekt zeigen',
            category: 'Soziale Beziehung',
            description: 'demütiges und respektvolles Handeln',
          },
          {
            letter: 'E',
            keyword: 'Einsicht, Urteilsvermögen',
            category: 'Reflexion, Urteilsbildung',
            description: 'Einschätzung und Urteilsbildung in einer Situation',
          },
          {
            letter: 'E',
            keyword: 'Erfolgreicher Hofbeamter',
            category: 'Politik',
            description: 'Rehabilitation von Ahikar',
          },
          {
            letter: 'F',
            keyword: 'Feinde',
            category: 'Soziale Beziehung',
            description: 'Beziehung und Haltung zu feindlichen Personen',
          },
          {
            letter: 'F',
            keyword: 'Festhalten an Gott bzw. Götter',
            category: 'Gott, Loyalität',
            description: 'Vertrauen in und Beziehung zu Gott bzw. Göttern',
          },
          {
            letter: 'G',
            keyword: 'Gehorsam gegenüber den Eltern',
            category: 'Soziale Beziehung, Loyalität',
            description: 'Beziehung zu den Eltern',
          },
          {
            letter: 'G',
            keyword: 'Guter Name',
            category: 'Sozialer Rang, Ehre',
            description: 'Ertrag von weisen und gerechten Taten',
          },
          {
            letter: 'H',
            keyword: 'Herausforderungen an den König',
            category: 'Politik',
            description: 'Politische Aufgaben des Königs',
          },
          {
            letter: 'I',
            keyword: 'Intrige',
            category: 'Politik',
            description: 'Die Reaktion von Nadan gegen Ahikar',
          },
          {
            letter: 'L',
            keyword: 'Luftschloss',
            category: 'Rätsel',
            description: 'Das Rätsel von dem Luftschloss',
          },
          {
            letter: 'P',
            keyword: 'Parabel Tiere',
            category: 'Parabel, Tiere',
            description: 'Parabeln mit Bezug zu Tieren',
          },
          {
            letter: 'P',
            keyword: 'Parabel Pflanzen',
            category: 'Parabel, Pflanzen',
            description: 'Parabeln mit Bezug zur Pflanzenwelt',
          },
          {
            letter: 'R',
            keyword: 'Reichtum Armut',
            category: 'Sozialer Stand',
            description: 'Bedeutung von Reichtum bzw. Armut',
          },
          {
            letter: 'S',
            keyword: 'Schönheit/Vergänglichkeit',
            category: 'Frau, Vergänglichkeit, soziale Beziehung',
            description: 'Beziehung zu einer Frau und die Vergänglichkeit ihrer Schönheit',
          },
          {
            letter: 'S',
            keyword: 'Selbstbetrachtung, Selbstreflexion',
            category: 'Selbstbetrachtung',
            description: 'Reflexion des eigenen Verhaltens',
          },
          {
            letter: 'S',
            keyword: 'Soziale Kontakte außerhalb der Familie',
            category: 'Soziale Beziehung',
            description: 'Soziale Kontakte außerhalb der Familie',
          },
          {
            letter: 'S',
            keyword: 'Soziale Kontakte zu Familienmitglieder',
            category: 'Soziale Beziehung',
            description: 'Soziale Kontakte innerhalb der Familie',
          },
          {
            letter: 'S',
            keyword: 'Streit',
            category: 'Soziale Beziehung, soziales Verhalten',
            description: 'Umgang bei Konflikten und Streitereien',
          },
          {
            letter: 'T',
            keyword: 'Tod/Alter',
            category: 'Vergänglichkeit,, sozialer Status, Tod, Ehre',
            description: 'Beziehung zu älteren Menschen und Vergänglichkeit des Menschen',
          },
          {
            letter: 'T',
            keyword: 'Treue zur Weisheit',
            category: 'Weisheit, Wissen',
            description: 'Notwendigkeit und Priorität der Weisheit',
          },
          {
            letter: 'U',
            keyword: 'Umgang mit Sklaven',
            category: 'Soziale Beziehung',
            description: 'Beziehung und Behandlung von Sklaven und Mägden',
          },
          {
            letter: 'V',
            keyword: 'verführerische Frauen',
            category: 'Soziale Beziehung, Frauen',
            description: 'Beziehung zu verführerischen Frauen',
          },
          {
            letter: 'V',
            keyword: 'Vorsicht gegenüber dem Stärkeren',
            category: 'Soziale Beziehung',
            description: 'Beziehung zu stärkeren Personen oder Mächten',
          },
          {
            letter: 'W',
            keyword: 'Wahrheit Lüge',
            category: 'Soziales Verhalten',
            description: 'Haltung gegenüber Wahrheit und Lüge',
          },
          {
            letter: 'W',
            keyword: 'Weise, dumme und sündige Haltungen bzw. Personen',
            category: 'Weisheit, soziales Verhalten, soziale Beziehung',
            description: 'Beziehung zu weisen, dummen und sündigen Menschen und ihr Verhalten',
          },
        ],
      }
    },
  }
</script>
<!-- markdownlint-enable MD033 -->
