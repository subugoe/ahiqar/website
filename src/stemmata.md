---
title: Stemmata
---

# {{ $frontmatter.title }}

## The Syriac Tradition

Author: Simon Birol

### I. Neo-Aramaic Textual Evidence

The story of Ahiqar is handed down in both Classical Syriac and the Neo-Aramaic dialects of the West and East Syriacs (also known as 'Assyrians', 'Chaldeans' or 'Arameans'). The Neo-Aramaic versions appear to have been translated from Arabic, and as a result, they have been preserved within the Syriac branch. These are the textual witnesses, in order of composition:

#### A. Staatsbibliothek zu Berlin, Sachau 339 (listed in Sachau's catalogue as limit identifier 290)

* Pages: 117 (fol. 1r-59r)
* Date of creation: 1881-1889
* Copyist: 'Ešaʻyā of Qilith
* Provenance: West Syriac (dialect of Tur 'Abdin)
* Catalogue entry: Sachau 1889, 2:815
* Link to the digitized original: [https://digital.staatsbibliothek-berlin.de/werkansicht?PPN=PPN862951585&PHYSID=PHYS_0005](https://digital.staatsbibliothek-berlin.de/werkansicht?PPN=PPN862951585&PHYSID=PHYS_0005)
* Transcription & translation: Lidzbarski 1896

#### B. British Library (London), Brit. Libr. Add. 9321 (also known as "MS London-Sachau 9321")

* Pages: 167 (fol. 536b-620b)
* Date of creation: ca. 1897
* Copyist: Ǧibra'īl Qūryaqūzā
* Provenance: East Syriac (dialect of Alqosh (Iraq))
* Catalogue entry: not available
* Link to the digitized original: not available
* Transcription & translation: Braida 2014


There is also the oral tradition from Mlaḥsō (south-eastern Turkey), which was taken from an Arabic version (cf. Talay 2002).

### II. Textual witnesses in Classical Syriac

The situation is quite different for the textual witnesses in Classical Syriac: A total of 22 manuscripts have been identified which can be divided into an East Syriac and a West Syriac branch. Five of these manuscripts are lost and could not be found. In one case, readings of a lost manuscript are available as marginal notes of Harvard syr. 80 (= Ms. Urmia 270), while its submission (= Ms. Urmia 117) was also a lost textual witness. Additionally, another lost manuscript (= Ms. Graffin) had been transcribed by Nau before it was lost.

Among the Tur Abdin manuscripts is a copy of a published version. Therefore, this manuscript (= Midyat Mar Gabriel MGMT 192) will be excluded from this project.

#### II.I Lost text witnesses

##### A. College of Urmia (Iran), Ms. Urmia 115

* Pages: unknown
* Date of creation: 1868/9
* Copyist: Anonymous
* Provenance: East Syriac
* Catalogue entry: Sarau/Shedd 1898, 21
* Link to the digitized copy: not available
* Transcription & translation: not available

##### B. College of Urmia (Iran), Ms. Urmia 117

* Pages: unknown
* Date of creation: 1887
* Copyist: Šmū'īl Tḥūmānāyā d-Mazrʻā
* Provenance: East Syriac
* Catalogue entry: Sarau/Shedd 1898, 21
* Link to the digitized copy: not not available
* Transcription & translation: Readings are available in Harvard syr. 80;
* Further remarks: it is an uncompleted copy of a nearly 800 years old lost transmission

##### C. College of Urmia (Iran), Ms. Urmia 230

* Pages: unknown
* Date of creation: 1894
* Copyist: Yōḥanān bar Ṭalyā da-Tḥūmā
* Provenance: East Syriac
* Catalogue entry: Sarau/Shedd 1898, 37
* Link to the digitized copy: not available
* Transcription & translation: not available

##### D. College of Urmia (Iran), Ms. Urmia 270

* Pages: unknown
* Date of creation: probably 1898
* Copyist: unknown
* Provenance: East Syriac
* Catalogue entry: not  available
* Link to the digitized copy: not available
* Transcription & translation: not available

##### E. Ms. Graffin

* Pages: 56
* Date of creation: 1908
* Copyist: Priest Elias (abbot of the monastery Rabban Hormizd and nephew of Bishop Addai Scher)
* Provenance: East Syriac ('Alqōš (Iraq))
* Catalogue entry: not available (cf. transcription & translation)
* Link to the digitized copy: not available
* Transcription & translation: Nau 1918-1919, 274-307 and 356-380
* Further remarks: Commissioned work for Bishop Addai Scher

#### II.II Copy of Dolabani's edition

##### Midyat (Tur Abdin), MGMT 192

* Pages: 39 (p. 3-42)
* Date of creation: 1964/5
* Copyist: Ḥanna Qermez
* Provenance: West Syriac (Tur Abdin)
* Catalogue entry: [https://w3id.org/vhmml/readingRoom/view/123101](https://w3id.org/vhmml/readingRoom/view/123101)

#### II.III Other textual witnesses

The remaining 16 textual witnesses (plus Graffin's copy) are represented as follows: On the left is the Western Syriac branch. Its oldest textual witness dates from the 15th century. This manuscript is badly damaged and torn within the sayings. The other witnesses give only the sayings. However, Aleppo SCAA 7/229 also includes the parables. This manageable quantity gives an indication of why Bishop Dolabani (1885-1969) - one of the foremost experts on Syriac manuscripts of his time - published and corrected the first edition of Conybeare et al. (1913): There were no textual witnesses in Western Syriac circles.

Another point is evident: in addition to the manuscripts, which were exclusively retranslated from an Arabic source (cf. [Sayings](sayings.html)), several sayings from Aleppo SCAA 7/229 and Sachau 162 are also present. It is therefore possible that these West Syriac manuscripts originate from a contaminated source containing parts of the Arabic retranslated sayings.

The remaining textual witnesses belong to the East Syriac tradition. Apart from the two oldest fragments in Brit. Libr. Add. 7200 and Brit. Libr. Or. 2313, the other manuscripts can be precisely located: One branch originates from Urmia (Iran) and existed until the beginning of the First World War. Its most important textual witness is Cambridge Add. 2020, which was used as the basis for the editions of Conybeare et al. (1913). In addition, the submission ('Vorlage') of this manuscript has been identified as BL Or. 2313, because of its almost complete agreement with it (cf. [Proverbs](proverbs.html)).

Although Cambridge Add. 2020 was written in northern Iraq, it is quite different from other manuscripts of this type (all of which were written later). Moreover, it has explicit affinities with the later Urmia manuscripts, see for example the beginning of the text (identical words are placed in the same line (unless they are marked in red):

<a href="./assets/images/stemmata1.png" target="_blank">
    <img src="./img/stemmata1_small.png" />
</a>

The three manuscripts are closely related. At best, this can be seen in a lost text passage in all three manuscripts, while Harvard 80 compensates for the lost text by using another submission (Ms. Urmia 117). However, the arrangement of the sayings and proverbs shows that the copyist of Harvard 80 follows the same pattern (= Ms. Urmia 270) as Strasbourg S4122. Interestingly, Sado 9 can be described as a contaminated manuscript that mostly follows the model of Ms. Urmia 270 and partly uses another text. This can be seen from the arrangement of the sayings: The first 44 sayings are identical with Cambridge Add. 2020, so his other submission must be closely related to this manuscript.

In 1900, Isaac Adams published a translation of the Ahiqar story from a Syriac manuscript in his book ‘Persia by a Persian’ (link: https://archive.org/details/persiabypersianp00adamuoft). All he writes about his original is that it is an old manuscript. A text comparison shows that the manuscript clearly belongs to the Urmia tradition, which is also suggested by Adams' origins in the Urmia region. However, none of the surviving manuscripts could have been the original, as the two oldest manuscripts date to 1898, the year in which Adams published his book.

A second East Syriac branch originates from northern Iraq. This branch has additions and annotations that are missing from the Urmia tradition. Furthermore, this tradition can be divided into two branches: The red branch consists of two manuscripts, most of which were reconstructed from defective manuscripts (some parts were even re-translated from an Arabic source). The older manuscript Birmingham Mingana syr. 433 is closer in wording and word order to the other manuscripts than Berlin Sachau 336. Only in Mingana syr. 433 transcribes the name of Ahiqar's son as „Nadab“ (in accordance with Tobit 14:10) and not as „Nadan". Even in the Arabic tradition, this spelling is not attested, and a possible confusion of letters or dots can be ruled out. The proximity to Berlin Sachau 336 is evident, although Berlin Sachau 336 contains several deviations and additions (including the highest number of sayings and parables). Thus, Nöldeke (1913, 54) is correct when he writes:

> B (= Sachau 336; Anm. SB) ist also zusammengesetzt aus ziemlich arg entstellten Originalstücken und aus solchen, die aus dem Arabischen nicht sehr geschickt retrovertiert sind. Das wird so zu erklären sein: ein Abschreiber kopierte eine von vorn und gegen das Ende defekte syrische Handschrift und ergänzte sie durch Rückübersetzung aus einer arabischen. Dieser buntscheckige Text mag weiter durch verschiedene Schreiberhände gegangen sein und noch allerlei Ungemach erlitten haben, bis er die Gestalt gewann, in der er uns in der Sachauschen Handschrift vorliegt. Will jemand diese Gestalt ein Monstrum horrendum informe nennen, so kann ich ihm nicht widersprechen. Aber trotzdem kann die Wissenschaft auch aus diesem Achikar-Text Nutzen ziehen.

Other differences from other textual witnesses relate to the intervention of the copyist, see for example:

<a href="./assets/images/stemmata2.png" target="_blank">
    <img src="./img/stemmata2_small.png" />
</a>

Mosul DFM 430 shows clear links between the two textual traditions of northern Iraq. In addition, it contains several paraphrases and additions, some of which are found only in the Arabic tradition (e.g. the sayings D3 and D4 correspond to the sayings 89 and 90 of the Karshuni manuscript BL Add. 7209 cf. [Sayings](sayings.html) and [Arabic Translation](arabictranslation.html)). Therefore, it cannot be ruled out that the copyist used another Arabic textual witness in addition to his retranslated transmission to make these additions. Besides, Tellkepe QACCT 135 is a copy of Mosul DFM 430. The other four manuscripts from northern Iraq are closely related. Firstly, the manuscripts of Notre-Dame des Semences, ms. syr. 611 and 612 (= NDS syr. 611 and 612), which transmitted readings from another manuscript as marginal notes. This latter manuscript is a copy of NDS 611. Ms. Graffin does not mention the marginal notes, but the order of the words, sayings and proverbs suggests that this manuscript and NDS 611 come from the same manuscript. The differences between this manuscript and Paris BnF syr. 422 are minor:

<a href="./assets/images/vgl-tab1.jpeg" target="_blank">
    <img src="./img/vgl-tab1_small.jpeg" />
</a>

In addition, a commentary by a scribe at the end of NDS 611 is of particular importance: One of the inputs of this manuscript was an Arabic text of Ahiqar (see [Manuscripts](manuscripts.html)). Thus, the influence of the Arabic version on this textual witness is clearly established. With regard to its close affinities with Ms. Graffin and Paris BnF syr. 422, the entire northern Iraq branch of the Syriac version of the story of Ahiqar bears noticeable traces of its Arabic versions (especially of the Arabic tradition C). All in all, the stemma can be constructed as follows:

<a href="./assets/images/Syriac_Stemma_new.png" target="_blank">
    <img src="./img/Syriac_Stemma_new.png" alt="A stemma illustrating the relationship between the Syriac manuscripts" />
</a>

### How to Cite This Section

Birol, Simon. „Stemmata. The Syriac Tradition“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/stemmata.html.


## The Arabic Tradition

Author: Dr. Aly Elrefaei

The Arabic and Karshuni manuscripts of the Ahiqar story can be categorized into four distinct traditions, designated as A, B, C, and D. This classification is based on critical textual phenomena, including substantial errors, additions, omissions, parallels, and divergences in the transmitted texts.

1. **Tradition A**, representing the earliest and most foundational stage of transmission, is attested by three manuscripts: two Arabic and one Karshuni. These witnesses span the period from the 12th to the 14th century, making this tradition pivotal for understanding the earliest textual forms and their geographical or cultural contexts.
*Textual witnesses*: 
    * Sbath 25
    * Vat. syr. 424
    * Vat. syr. 199
2. **Tradition B**, by contrast, is represented by only a single manuscript in Arabic, dating to the 15th century. This solitary witness highlights the limited textual preservation within this tradition, though its unique features may reflect a distinct phase in the textual evolution.
*Textual witnesses*: 
    * Vat. ar. 74
3. **Tradition C** is significantly broader, comprising nine manuscripts—five in Karshuni and four in Arabic. These witnesses collectively date from the 16th to the 18th century, representing a phase of expanded textual transmission and broader geographic or linguistic diffusion. The manuscripts in this tradition exhibit substantial textual development, with variations that reflect regional or scriptoral influences.
*Textual witnesses*: 
    * MS 882
    * Brit. Add. 7209
    * Ming. syr. 258
    * Vat. syr. 159
    * Cod. Arab. 236
    * DFM 614
    * Sach. 339
    * Brit. Or. 9321
    * CH 17
4. **Tradition D**, the most extensive and diverse, is represented by seventeen manuscripts, including four in Karshuni and thirteen in Arabic. These manuscripts, dating from the 18th and 19th centuries, constitute the largest corpus within the textual tradition. Their widespread distribution and the variety of textual features they preserve make this tradition crucial for understanding the later phases of the story’s transmission and adaptation.
*Textual witnesses*: 
    * MS 956
    * MS 621
    * Paris. ar. 3656
    * GCAA 486
    * Paris. ar. 3637
    * Ming. ar. 93
    * Cam. Add. 2886
    * Ming. syr. 133
    * Vat. ar. 2054
    * MS 619
    * Borg. ar. 201
    * MS 626
    * Syr 17
    * Salhani
    * Leiden Or. 1292
    * Gotha 2652
    * Cam. Add. 3497

**This table lists the manuscripts with their corresponding tradition, date, origin, and language.**
| Manuscript ID | Tradition | Date | origin | Language |
| --------------|-----------|------|--------|----------|
| Sbath 25      | A         | 12th century | Unknown | Arabic |
| Vat. syr. 424 | A         | 1417 | Unknown |Arabic |
| Vat. syr. 199 | A | 1545 | Syria, An Nabk | Karshuni |
| Vat. ar. 74 |  B | 1455 | Unknown | Arabic |
| MS 882 | C | 15th-16th century | Unknown | Karshuni |
| Brit. Add. 7209 | C | 1560 | Unknown | Karshuni |
| Ming. syr. 258 | C | 1570 | Unknown | Karshuni |
| Vat. syr. 159 | C | 1625 | Unknown | Karshuni |
| Cod. Arab. 236 | C | 1670 | Syria, Aleppo | Arabic |
| DFM 614 | C | 18th century | Iraq, Mosul | Arabic |
| Sach. 339 | C | 19th century | Tur Abdin | Arabic |
| Brit. Or. 9321 | C | 1896 | Iraq, Alqosh | Arabic |
| CH 17 | C | 18th century | Lebanon, Zūq Muṣbiḥ | Arabic |
| MS 956 | D | 17th century | Lebanon, Ṣarbā (Jūniyah) | Arabic |
| MS 621 | D | 17th century | Syria (?), Aleppo (?) | Arabic |
| Paris. ar. 3656 | D | 1689 | Unknown | Arabic |
| GCAA 486 | D | 18th century | Syria, Aleppo | Arabic |
| Paris. ar. 3637 | D | 1772 | Unknown | Arabic |
| Ming. ar. 93 | D | 1780 | Unknown | Arabic |
| Cam. Add. 2886 | D | 1783 | Syria, An Nabk | Karshuni |
| Ming. syr. 133 | D | 1809 | Unknown | Karshuni |
| Vat. ar. 2054 | D | 1814 | Unknown | Arabic |
| MS 619 | D | 1858 | Lebanon, Beirut | Arabic |
| Borg. ar. 201 | D | 19th century | Unknown | Arabic |
| MS 626 | D | 19th century | Unknown | Arabic |
| Syr 17 | D | 19th century | Lebanon | Karshuni |
| Salhani | D | Unknown date | Unknown | Arabic |
| Leiden Or. 1292 | D | Unknown date | Unknown | Arabic |
| Gotha 2652 | D | Unknown date | Unknown | Karshuni |
| Cam. Add. 3497 | D | Unknown date | Unknown | Arabic |

### How to Cite This Section

Elrefaei, Aly. „Stemmata. The Arabic Tradition“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/stemmata.html.
