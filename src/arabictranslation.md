---
title: Translation of the Arabic Version
---

# {{ $frontmatter.title }}

## Introductory remarks

This is an English Translation for the Karshuni manuscript (Brit. Mus. Add. 7209) from the 16th century.  The English translation provided here shares many expressions with the Karshuni text (Camb. Add. 2886) edited by Mrs. Lewis in Conybear's book (The Story of Aḥiḳar from the Aramaic, Syriac, Arabic, Armenian, Ethiopic, Old Turkish, Greek and Slavonic Versions, 2nd edition, Cambridge: Cambridge University Press, 1913).

(f. 182b)

### And also with the help of God, we write the story of Haiqar the wise

In the days of Sennacherib son of Sarhadum king of Assyria and Nineveh, I Haiqar was his vizier and secretary. And when I was a young man the enchanters, the astrologers and the knowing told me: “You will not fortunate having a child.” And I had a fine fortune and had acquired a great property, andMeta Edition/ Arabic Translation I had married sixty women and had built sixty magnificent, beautiful, exquisite castles and gigantic houses for them. I was aged sixty years old and had not fortunate in having a child. Then I, Haiqar, presented offerings and sacrifices to the Gods and burned incenses before them Frankincense, Cinnamon and Cancamum  and said: “Oh Gods, give me a boy that I may be rejoiced in him, and that he may inherit me when I die, and that he may close my eyes, and that he may bury me. If from the day I die, until the day he dies, a hundredweight of gold should be expended and wasted, my wealth (f. 183a) would not be exhausted and my possessions would not be diminished. But the Gods did not answer him. Then he turned away from them and was sorrowful, and suffered, and he returned to God with speech and supplication, beseeching and saying: “O God of Heaven and Earth, O Creator of created things! I beseech You to give me a son, that he may inherit me, and at the time of my death he may close my eyes, and that he may bury me.” Then there came to him a voice saying, “Inasmuch as you relied on gods and made your hope on them not me, and offered sacrifices to them, for this reason, you shall remain without sons and daughters. But there is your sister’s son Nadan, make him your child and teach him your learning, and he may inherit you.”

Then I took to myself Nadan my sister’s son, and because he was still a boy child, I handed him over to eight wet nurses, that they might suckle him and bring him up. And I brought him up  with fat, honey, and butter, and dressed him silk and purple, and seated him upon soft couches and thickets. And Nadan thrived and grew big, shooting up like a laudable (august) cedar. And I taught him writing, wisdom, and philosophy.

When King Sennacherib returned from his journey and march, one day he called me, I, his secretary and vizier Haiqar, and said to me: (f. 183b) “O my highly respected friend, the honored beloved (comrade), the skillful wise one, my secretary and knowing (diviner of my secrets) of my secrets; you have advanced in years and grown older, and your death must be near.  Tell me who shall serve me after your death. Tell me who shall serve me after your death and demise.”  Then I said to him: “O my lord the king, may you live forever and eons. Behold there is my sister’s son, who is like my child (And I taught him my knowledge, and he is a learned wise.” Then my lord the king said to me: “Go and bring him, that I may see him. And if he is suitable to serve me, he may stand before me, and shall serve me; and you shall go your way, and I relieve your onus , so that you may manage your old age in dignity.” Then I, Haiqar, took my sister’s son Nadan and brought him into the presence of King Sennacherib. When the king looked at him, he rejoiced in him and said: “May god preserve this your son. And as you served me and my father Sarhadum and carried out our undertakings, so may Nadan your son serve me and carry out my undertakings, so that I may make him powerful, (f. 184a) and I may honor him for your sake and bestow grace upon him.” And I did obeisance to the king and said, “May my lord the king live forever and eons! I desire of you my lord that you be patient with my sister’s son Nadan and forgive his mistakes, so that he may serve you as I served you and your father before.” Then Sennacherib gave him his word  and swore to him that he should be with him in all honor and nobility and that he would do him all good. And I, Haiqar, kissed the hand of the king, taking Nadan with me and I taught him night and day till I had filled him more with science, wisdom, and knowledge than with bread and water. And I taught him and spoke to him proverbs thus:

### Knowledge of Haiqar the wise and his fine proverbs

1- O my son Nadan! Hear my speech and follow my advice and recall this my saying, as if it were God’s saying.

2- O my son! If you heard a word, ]let it[ die in your heart, and reveal it not to another (I mean disclose it) lest it become a coal of fire  and burn your tongue and cause pain in your body, and you be reproached, and you be ashamed before God and man.

(f. 184b)

3- O my son! if you have heard something, relate it not.

4- O my son! loose not a tied knot (I mean untie) and tie not a loosened knot.

5- O my son! make your discourse easy, be hasty to hear, and be not hasty to return an answer.

6- O my son! covet not outward beauty, for beauty wanes and disappears , but an honorable remembrance and a good name endures and lasts forever.

7- O my son! let not a fickle woman deceive you with her speech, for bitterness, and poison of death flow out of her speech, and entangle you in the net and ensnare you into the trap.

8- O my son! if you see a woman costumed with dress and anointed with ointments and fragrance, who is despicable and ignorant in her soul, desire her not in your heart. For if you hand over to her everything you have, you will not find usefulness in her, and you gain a sin, and God will be wroth with you and be angry with you.

9- Oh my son! be not hasty and despicable like the almond-tree, for it germinates and brings forth leaves before all the trees, and edible fruit after them all, but be quiet, meek, and tasty  like a mulberry tree, which leafs after them all, and brings forth edible fruit before them all.

(f. 185a)

10- O my son! incline your head and look down, and soften your voice, and be courteous, and do not be foolish. And raise not your voice with bragging and showiness. For if it was by a loud strong voice that a house was built, the ass would build two houses in a day; and if it was by dint of strength that the plow was driven, the plow would never be removed from under the shoulders of camels.

11- O my son! it is better to remove stones with a wise man than to drink wine with a base man.

12- O my son! pour out your wine on the tombs of the righteous and of the just, and drink it not with the foolish and sinful.

13- O my son! keep your secret not with the ignorant, for they will not be able to conceal it.

14- O my son! cleave to wise men who fear God and be like them, and cleave not to the ignorant, lest you become like them, and learn their ways.

15- O my son! before you consider someone dear and a friend, try him first and only then take him as a friend; and do not indiscriminately praise a person; try him ]first[ and then take him.

(f. 185b)

16- O my son! do not spoil your speech with one who lacks wisdom; and do not strive for a good thing with the ignorant and the depraved.

17- O my son! cleave to a wise man who fears God and be like him; and cleave not to the ignorant and the foolish and the wicked man, lest you become like him and learn his ways, and God and people be wroth with you.

18- O my son! While a shoe stays on your foot, walk with it on the thorns and elm, and make a road for yourself, your sons, and your sons’ sons.

19- O my son! before the breeze blows and while the waves of the sea are silent, make your ship and boat taut at the port; and before the sea moves and billows, and its waves and storms multiply, and the boat sinks, you should think about your journey.

20- O my son! if the rich man swallows a snake, people say “For healing and his interest,” and if a poor man eats it, the people say “From his hunger,” for it is by a great effort that a pious and righteous man could be found.

21- O my son! eat only your share, and covet not your comrade’s thing.

22- O my son! be not neighbor to the fool, and eat not bread with a person who does not feel ashamed, (f. 186a) and do not keep with him a secret.

23- O my son! when good fortune happens to the one who hates you, be not angry nor distressed; and rejoice not in his calamities.

24- O my son! do not approach the inimical and shrill woman, and do no admire the prettiness of a despicable and fickle woman, for the beauty of the woman is her modesty and not the ornaments of her dress and her outward beauty, which are used to deceive and mislead  you.

25- O my child! like a lead in the ears of beasts that are not useful for them, so a woman with a good appearance while she is bad in her action and speech, with little knowledge, fickle, and wicked.

26- O my child! if your enemy hastens to do evil to you, hasten to do good to him and deal with him thoughtfully.

27- O my son! the wise man falls and gets up, but the fool stumbles and falls every day and for him there is no getting up. If the wise man falls, he gets up, and the righteous man on his seat is not shaken.

28- O my son! if the wise man is sick, the physician can heal and cure him. But as for the stupid man, there is no drug for his pains and wounds.  

29- O my son! if (a man) approach you who is low and inferior to yourself, go forward to meet him (f. 186b) and remain standing, and if he cannot reward you, God will reward you.

30- O my son! do not refrain from beating your son, for the drubbing of a boy is like manure to the garden, and like tying of a purse, and like the tethering of beasts, and like the bolting of the door.

31- O my child! restrain your son from wickedness so that he may give you rest, and teach him manners and beat him while he is still young, and make him obey your commands, lest he grow older and after a while rebel against you and degrade you amongst your fellows, and he make you hang your head in the streets and the assemblies, and you be ashamed for his immorality and be despised for his bad deeds.  

32- O my son! do not take a runaway slave, nor a thievish handmaid, for everything which you commit to them they will ruin.

33- O my son! the speech of lying (and) ignorant people is like birds who fly in the air and they are plump, and those who do not have a heart devour them, and those who do not have acumen listen to them.

34- O my son! do not impoverish your parents, lest they curse you and God answer them; for it has been said, “He who despises his father and his mother, let him die the death (I mean the death of sin), he makes God angry, and he who honor his father (f. 187a)  and his mother shall prolong his life on earth and shall meet all that is good.

35- O my son! go not on the road without weapons (I mean remembrance of God and the sign of the Cross), for you know not where your foe may meet you. Be careful on your road, for many enemies (I mean devils and sons of the wicked humans) wait to waylay you.

36- O my son! like a beautiful tree with its fruits, its boughs, and its leaves, thus is a man who has a good wife and children and brothers; and the man who has no wife, no children, and no brothers is disgraced in the world by his enemies and is abhorred by them, and he is like a tree in the middle of the road and whoever crosses the road eats from its fruits, and the beasts of the desert scatter and cut down its leaves.

37- O my son! say not “My lord is a fool and I am wise,” but he must be honored by you even if he is deficient so that you may be beloved; and count not yourself among wise men, if you are not considered as such among others.

38- O my son! do not convey the speech of ignorance and folly in the presence of your lord, lest you be despised and despicable in his eyes.

39- O my child! be not one of those to whom their lord says, (f. 187b) “Go away from me,” but be one of those to whom it is said, “Approach and come near to me.”

40- O my child! in the day in which sorrows, calamities and distress come upon you, invent not a lie against your God nor insult Him, lest He listens to your speech and be wroth with you.

41- O my son! praise not your servant in the presence of his companion, for you know not which of them you shall choose in the end.

42- O my child! a slave who leaves his first masters and goes to others, his condition will not be improved.

43- O my son! a goat that moves running   in circles is eaten by wolves.

44- O my son! make your rule fair and just, so that you may attain a venerable age and you may rest.

45- O my son! sweeten your tongue and make the speech of your mouth fair and address the people well, for the tail of the dog gives him bread and his mouth gives him beating and stones.

46- O my son! permit not your companion to tread on your foot, lest he tread also on your chest (I mean let him not commit against you a small sin, lest he commit also a big one).

47- O my son! if you strike a wise man with words of wisdom, it will lurk in his breast like a sublime (subtle urge) sense of zeal ; but if you drub the ignorant with many sticks he will neither know nor understand.

(f. 188a)

48- O my son! if you send a wise man to undertake your businesses, you need not give him many orders nor warn him too much, for he will do your business as your heart desires; but if you would send a foolish man do not let others see you talking to him, but go yourself and do not send him, for he will not do your business as you desire even if you advise him again and again.

49- O my son! if they send you to a place, do not compel them to send another after you; do no contend with a strong man, for he will meet you with evil where you least expect it.

50- O my son! make trial of your son, and of your servant with bread and water, and only afterward hand to them your fortune and possessions.

51- O my son! be the first who leaves wedding feasts and banquets, and do not wait to be adorned with ointments and fragrance, lest your head get fractured and you be wounded.

52- O my son! he whose hands are full is called a revered wise man, and he whose hands are empty they call him a needy poor man, and even the most despicable will not dignify him.

53- O my son! I have eaten a colocynth, and swallowed aloes, and I have found nothing more bitter than poverty and scarcity.

54- O my son! I have carried iron and lead, and I have found nothing more bitter than debt.

55- O my son! I have carried salt and big stones, (f. 188b) and they were not as burdensome for me as he who sits, laughs, and mocks in the house of his father-in-law and of others.

56- O my son! teach your son hunger and thirst, that he may do well in the management of his household.

57- O my child! teach not to fools words of wisdom and knowledge, for words of wisdom to them are like he who cements ceramic to his body to appear fat.

58- O my son! if you are in need and suffering losses, do not display your condition to your friend, lest you be despised by him.

59- O my son! the blindness of the eye is better than the blindness of the heart, for those whose eyes are blind may be guided little by little to the path, but the blind of heart leaves the straight path and goes in a crooked way.

60- O my son! it is better for a man to misstep than to misspeak, for a misstep will not cause him to die.

61- O my son! a friend who is near is better than a brother who is far away; and a good name is better than fine suet, for the suet runs out, deteriorates, and spoils, but a good name endures and persists forever; beauty becomes faded and decrepit, and the world becomes vain and wanes , but a good name neither becomes vain nor wanes.

(f. 189a)

62- O my son! the man who has no rest, his death is better than his life (I mean a repentance from his sins); and the sound of weeping and lament in the ears of a man who fears God is better than the sound of singing and rejoicing.

63- O my child!  a frog leg  in your hand is better than a goose in the pot of your neighbor, a sheep near to you is better than an ox far away; and a sparrow in your hand is better than a thousand in flight; gathering in poverty is better than scattering in abundance; and a living fox is better than a dead lion (I mean a living man with a pedigree is better than a sinful man…); and a pound of wool is better than a pound of riches, I mean of gold and silver; for gold and silver are hidden and covered up in the earth, and are not seen, but wool stays in the markets and it is visible, and it is a beauty to him who wears it.

64- O my son! bury an utterance in your heart and it will be good for you, and reveal not the secret of your friend, for if you reveal his secret, you alter and destroy him.

65- O my child! let not a word issue from your mouth till you have taken counsel with your heart, for it is better for you to misstep than to misspeak.

66- O my child! If you hear an utterance from someone, bury it in your heart as in the earth and do not reveal it, for whenever you tread on it, (f. 189b) you bury it and suppress it further.

67- O my child! stand not between persons quarreling, because from laughing there comes a bad word, and from a bad word there comes quarrel, and from quarrel there comes war, and from war there comes fighting, and you will be either killed or be called to bear witness, but run from there and save yourself.

68- O my son! do not stand up in a quarrel against he who is stronger than you, and ignore a word; and disarm the evil, and overcome the evil by generosity.

69- O my child! attain a humble heart and a patient spirit and endurance and upright conduct, for there is nothing in the world more excellent than that, and you will have a good and enjoyable life.

70- O my child! keep not from your first friend, lest your second friend not last.

71- O my son! visit your friend and speak good of him in the Sultan’s presence, and strive  to save him from the mouth of the lion.

72- O my son! rejoice not in the death of your enemy, but chant in lament that after a while  you shall be his neighbor in the grave.

73- O my child! revere and honor everyone who is older than you, and rise for him (f. 190a) and hasten to greet him , and if he cannot repay you, God will repay you.

74- O my child! show respect to the good man who fears God and listen to him, and receive his words, that he may pray for you and bless you.

75- O my son! if you want to be one of God’s pious, you should purify yourself in his presence, and serve him with pureness and sanctity, and do not withdraw from his presence but fear him.

76- O my son! if water would stand still in the waterway, and the birds fly in the heaven , and if a black crow become white, and myrrh become sweet as honey, then the foolish and ignorant man might be courteous and understanding.

77- O my son! if you desire to be wise, keep your tongue from lying, and your hands from stealing, and your eyes from beholding rudeness: then you will be called wise.

78- O my son! be lowly and humble in your youth, and be tranquil, calm and docile, so that in your old age you shall be honored and respected, and will be loved by everyone.

80- O my son! do not stand up against a man in the days of his power, nor a river in the days of its torrent.

81- O my son! the eyes of man are not satisfied (f. 190b) by possessions   and money until they are satisfied by dust.

82- O my son! interfere not in the affairs of a wedding, for even if it turns out well, they will not praise you, and if it turns out bad, they will insult you and curse you.

83- O my son! whosoever is elegant in dress, so is he in speech; and he who has a mean appearance in dress, so also is he in speech.

84- O my son! if you have committed a theft and it has become known to the Sultan, give him a share of it as his portion, so that you may be delivered, for otherwise you will taste every bitterness and you will suffer all adversities.

85- O my son! make friends with a hand which was once satisfied and thereafter became hungry, and choose not for yourself a hand which was once hungry and thereafter became satisfied.  

86- O my son! a dog which leaves his first owners and follows after you, beat him with every stone, for he will not stand by you.

87- O my son! let the wise man beat you with many rods, but let not a foolish and ignorant man anoint you with delectable and aromatic oil.

88- O my son! he who remains faithful to friendship, will attain the glory of this world and the bliss of the hereafter as well.

89- O my son! there are four things from which a king cannot be secure: oppression by the vizier, mismanagement, perversion of his will, and tyranny over his flock.

(f. 191a)

90- O my son! there are four which cannot be hidden: the prudent, the foolish, the rich, and the poor.

**The proverbs have come to an end.** And I, Haiqar, thought that Nadan my sister’s son would keep everything which I taught him and hold it in his heart and make much use of it, and that he would succeed me before Sennacherib the king and serve him. And I was unaware that he did not keep all that I taught him, and did not make use of it, and that he neither listened to my speech nor kept my orders. Instead, he began to mock me and say, “Haiqar grows old, and is advanced in years, and his mind is lost, and he is absent-minded, and is inattentive, and he knows nothing.”  And Nadan began to disperse Haiqar’s fortune and wealth, and to beat the slaves and handmaidens, and to sell the horses and mules, and be a spendthrift with property  , belongings , and everything that Haiqar had possessed.

And when I, Haiqar, saw that he had no compassion for my wealth nor for my household, I said to him: “keep away from everything I own, and do not impede my household and my slaves as long as I am alive.” And I informed king Sennacherib of all that Nadan did and what I heard from him.  And the king said to him: “As long as Haiqar (f. 191b) is alive and well , no one else shall have authority over his possessions and wealth.”

And I, Haiqar, took Nabûzardân Nadan’s brother to my house to teach him so that he may take the place of his brother. And when Nadan saw him in my house, he envied him and was jealous of him, and was furious and very angry. And he began to say that: “Haiqar is unstable and is absent-minded, his wisdom is ineffectual and his knowledge is lost; he has handed over his house and his wealth to my brother Nabûzardân, a young boy with neither learning nor knowledge, and he has driven me from his house.” And when I, Haiqar, heard that from him, I said: “how sad for my wisdom! how my son Nadan did despise and abhor it.”  And Nadan went with anger to the house of the king my lord and he sat down to write and complain about Haiqar, and to fabricate against him falsehood and injustice.  And he wrote two letters to the kings, the enemies of Sennacherib, who hate him. The first letter was to Achish the king son of Samhalîm king of Persia and the barbarians , writing thus:

“From Sennacherib the king, and from Haiqar his secretary and his vizier, all peace and (f.192a) greetings and honor and kissing of hands and feet, let there be peace between you and me, O great king!  When this letter reaches you, you will arise quickly and be not slow and come to me to Assyria, and I will deliver the kingdom up to you without arraying for battle and without labour.”

And he wrote another letter in the name of Haiqar to Pharaoh King of Egypt, writing thus: “Let there be peace between you and me O king!  When this letter reaches you, you shall come to  me to the plain of Nisrîn on the twenty-fifth of the month of Av, and I will bring you in to Assyria, to Nineveh, and I will deliver the kingdom up to you without war, without arraying for battle, and without labour.”

And he emulated the writing of Haiqar, and he sealed the letters with Haiqar’s signature.  And he deposited them in the king’s house so that the king’s servants might find them and show them to the king. And he wrote another letter in the name of Sennacherib the king my lord, and sent it to me, Haiqar, saying thus: “Peace from Sennacherib the king to Haiqar the honored, my Vizier, my secretary, and my chancellor . When this letter reaches you, assemble the soldiers (f. 192b) who are with you, and go out to meet me in the mountain of Sahû and go before me to Nisrîn on the twenty-fifth of the month of Av. When you see me coming near to you, array the soldiers before me as a man who would fight against me. Draw up battle lines against me, for I have with me the messengers of Pharaoh king of Egypt so that they may see and understand and come to know the strength of your army and may fear you, for they are our enemies and they hate us.

And he sent the letter by one of the king’s servants. And Nadan picked up one of the letters, which he had written and deposited into the king’s palace, as if he had just found it at this moment. And Nadan read it to king Sennacherib. And when the king heard what was in the letter, he was greatly perplexed, was saddened and dejected, and said, “O my God! what wrong have I done to Haiqar that he has written these things to Pharaoh king of Egypt my enemy and hater, and that he has paid me back in this way?” And Nadan said to king Sennacherib, “be not angry, and be not sorrowful, and be not aggrieved, instead arise and let us go to the plain of Nisrîn (f. 193a) on that day which he mentioned in the letter and let us know the truth of the tale whether it is true or not.”  And Sennacherib the king and Nadan my child went and they came to me at the plain of Nisrîn. They found me with the soldiers assembled around me. And when I, Haiqar, saw that they had come near and had arrived, I did as was written and prescribed in the letter, and I incited the soldiers to draw up in battle formation against them. And when my lord the king Sennacherib saw what I had done, he was afraid of me and thought that I was rebelling against him and held talks with his enemies and those who hate him.  And I did not sense nor did I know the intrigue and deception which Nadan had carried out against me. And Nadan said to the king, “Behold, you have come to know the truth of the tale! Be not sad, instead go back to your house and your kingdom and be not afraid, and I will bring Haiqar to you bound and fettered with chains, for he has rebelled against you and has committed a transgression (failed to recognize your).

And the king returned to his house, being dejected and aggrieved. And Nadan my child came to me and said, “The king Sennacherib rejoices greatly in you (f. 193b) and praises you for having done what he commanded you in his letter. And now he has sent me back to you so that you and I alone may come before him, and that you may dismiss the soldiers each one in peace to his own house.” And I dismissed the soldiers, and I and Nadan my child came to the king Sennacherib my lord and I greeted him. And when the king saw me he said to me, “Have you come, O Haiqar! my Secretary, the ruler of my city and my country and all my kingdom, whom I have loved and honored, and released from service.  But now you have rebelled against me, and your love has turned to hate, and you have transgressed against me and become one of my enemies.” And he brought out the letters which Nadan had written in my name  and gave them to me, which resembled my handwriting, and were sealed with my signature. And when I read them, I was shocked and my limbs trembled, and I shivered and was frightened, and my tongue was tied, and I wanted to speak a word from my words of wisdom and knowledge but I could not.  And Nadan rebuked me and said, “Turn away from the presence of the king, O foolish sheikh, O evil sheikh! (f. 194a) allow your hands to be shackled and your legs to be chained. And he chained me with chains and fetters. And Sennacherib the king turned his face from me and was angry with me and said to the swordsman, whose name was Jebûs Mekmiskînkinte, “Arise, take Haiqar and go slay him and cast his head a hundred cubits from his body.”  Then I, Haiqar, prostrated before the king, and said to him, “May you live forever O king! if you choose to kill me, let your wish be fulfilled and may you live; though I know that I have neither done wrong nor have I sinned. Nevertheless, I beg of you my lord the king to command that they kill me at the door of my house and give my body to my slaves and my household that they may bury me, and let my blood alone be your sacrifice.”

And the king said to Jebûsamikmiskînkinte  the swordsman, “Go, slay Haiqar at the door of his house, and give his body that they might bury him.” And I, Haiqar, when I came out of from the presence of the king, I sent to Ishfaghnî my wife and said to her, “Come out and meet me and bring out with you from my house a thousand young virgins, (f. 194b) and dress them in gowns of silk, purple, and scarlet that they may weep and wail over me, and that they may lament me before I die. And you my wife! return to my house and prepare a bread table for the swordsman and the Persians and the Assyrians who they are with him. And come out to meet them and receive them with rejoicing and pleasure, and let them come into my house and prepare before them delectable food and drink, and mix wine and give them to drink, and you shall serve them.”

And Ishfaghnî my wife was a discerning woman with great knowledge, and she did all that I commanded her. And she prepared a table for them, and mixed wine for them, and they ate and drank while she was serving them, and they became drunk and slept where they sat.

Then I, Haiqar, said to Jabûskmîsî the swordsman, “Raise your sight to the heavens, to God, and remember the bread and the salt that we all ate together; and know that I am not guilty nor have I committed any sin; Nadan my child has deceived and cheated me, do not take part in my sin by killing me, for I am wronged. Remember also and recall  that day when (f. 195a) the king Sarhadum, the father of this Sennacherib, was angry with you and commanded me to slay you, and since I knew that you were not guilty, I hid you and did not kill you, and I let you live till the king’s anger subsided and he   was pleased with you. And he gave the command and I brought you before him, and he blessed you with many endowments. And now you too should reward me with goodness and generosity as I did for you, and hide me in some place. And there is a guilty slave of mine in the prison, whose name is Mdîfar, and he deserves to be killed, for he has committed many sins. Bring him out and dress him in my clothes, and command the Assyrians who are with you to go out to slay him while they are still drunk. Thus, they will not know who they have killed. And cast his head a hundred cubits away from his body, and give his body to them that they may bury it. And the story will be reported and spread in Assyria and Nineveh and in all countries that Haiqar has been killed. Then Jabûsamikmiskînkinte and my wife with him set out and made a hidden hole for me under the ground, and they dug it out for me like a cellar its length fourteen cubits and its width seven cubits and its height five cubits, and they made it (f. 195b) under the doorstep of my house and they brought me into it and hid me. And they provided me with bread and plenty of water. And they left me hidden in that hole, and set about informing Sennacherib the king that Haiqar was killed according to your edict. And when the story of my execution spread, and the people of the city heard, they all wept for me with a great weeping, (and) the women of the city scratched and scraped at their faces and wailed and said: “Alas for you, O Haiqar the secretary, the skillful, knower of secrets, explainer of words, problems and mysteries. Woe to us on account of you! Where can we find another like you? Where can there be a man so discerning, so learned, and so wise as to resemble you that he may fill your place?”

And Sennacherib the king called for Nadan my child and said to him, “Go and mourn and express grief for Haiqar your father and tutor, and wail and be sorrowful for him.” And when my child Nadan, the foolish, heartless, and wicked came, he neither wept nor expressed sorrow nor let my name come forth from his mouth, but instead he assembled to him wanton, depraved and greedy people and set about eating, drinking, dancing, rejoicing, singing and getting drunk. (f. 196a) And Nadan began to seize my slaves and maid-servants, and denuded them and whipped them and drubbed them and tortured them and made them taste every evil and affliction. he was neither ashamed nor embarrassed for my wife, who had brought him up like her own child, but that sinful man  wanted to fall into further sin with her and rape her. And I Haiqar, the wronged, had been cast into that hiding-place in the dark, black hole and I heard the whipping of my slaves and their weeping and the punishments and all the hardship which they underwent from Nadan. And I heard and endured and wept and was sorrowful for them and for myself, and for what happened  to me and had come upon me. Then I returned and prayed to God the Merciful. A few days later came Jebûsamikmiskînkinte to me and entered to me and comforted me and stilled my heart and consoled me. Also, he brought me bread and water. And when he wanted to leave, I said to him, “Present a prayer and supplication in my behalf to the God  and say, “O the Generous God, the Most High, O the Merciful God, O the Righteous One, O the Rightful One, O the Overflow of Grace upon His creation, remember (f. 196b) Haiqar who is reliant on you and seeks help from you and has recourse only to you, look to him with Your Mercy , save him and deliver him; for he has put his hope upon you and he is wronged, listen to his prayer and accept his supplication, help him and answer him; for he cries out to you from the severity of his distress and from the multitude of his afflictions and pains.”

And when Pharaoh King of Egypt heard that Haiqar the skillful, the subtle in sciences, the solver of problems and the explainer of questions was killed, he rejoiced greatly and took a pleasure and wrote a letter and sent saying, “From Pharaoh king of Egypt to Sennacherib king of Assyria and Nineveh! Peace and greetings to you. know O king that I have been desiring to build a castle  between the heavens and the earth, and I want you to send me an architect,  a builder and a hewer, that he should know to answer me each question I ask him; and if you send one who could build the castle and do what we say to him, we will send him back to you with the taxes of Egypt and all of its lands and surroundings, the taxes of the fertile valley for three years. Otherwise, you should send back to us the messenger who I have sent to you along with the taxes of (f. 197a) Assyria and Nineveh for three years.” And when the letter arrived, they read it before the king Sennacherib. The king called and assembled all free men and the wise men and the philosophers and the magicians and the learned men who they are in his kingdom, and read before them the letter and said to them, “Who among you can go to Egypt and answer to Pharaoh the King?” They answered him and said to him, “Our lord the king knows regarding such questions and problems, not only in your time and days but also in the days of your father, only Haiqar the wise understood, solved, and explained such questions and problems. As for us, we had not understood as he did, nor is any of us a successor to his learning and knowledge, unless it be Nadan, his sister’s son, for Haiqar taught him his learning and wisdom and knowledge. Call him and ask him and he shall solve this question, for we do not know its explanation.”  Then he called him, and when Nadan came before the king and read the letter, he answered and said to the king, “O my lord! Let those people brag and rave, for who is able to (f. 197b) construct a building between the heavens and the earth? Even the gods cannot do this, the letter is raving mad.”

And when Sennacherib heard the speech of Nadan he was overcome with great sorrow and wept heavily, and stepped down from his throne and sat on sackcloth and ashes, and wept and said, “Alas for you! O teacher of my country and ruler of my kingdom, where shall I find your like? and where should I search for you? woe is me for you! how did I come to destroy and execute you by the speech of an ignorant and foolish boy, without knowledge, without learning, without religion, and without manliness?  I will grieve and weep over you till I die! who can bring you to me at this moment, or bring me good tidings that Haiqar is alive? I would give him half of my kingdom.”  When I, Jebûsamîk Meskîn kentê the swordsman, heard that and saw the sorrow of the king and his weeping over Haiqar, I approached and bowed down before the king and said to him, “O my Lord! command your servants to crucify me for I your servant am the guilty offender. I have acted contrary to your command, and every slave who acts contrary to the decree of his master deserves to be crucified. you gave the order  in anger (f. 198a) to kill Haiqar and had not examined his guilt, and I knew that you would regret  killing him, I knew that he had been wronged and had not sinned, and God shall answer those who are oppressed. Behold Haiqar lives, he is hidden underground, buried in a hole like a grave. Nevertheless, I did wrong when I acted contrary to your decree, please either crucify me or pardon me and forgive my sin.” And when the king heard my speech, he rejoiced, took pleasure and exulted and said, “O faithful servant! if your speech is true, I wish to make you wealthy. If you show me Haiqar alive, I would give you half of my kingdom and a hundred quintals of gold and fifty quintals of purple and silk garments.” Then Jebûsamîk said to the king, “O my lord! swear to me by the Living God that you will not hold this guilt against me and that you will not let any bad befall me for this sin.” And the king swore to him and gave him his word that he would not harm him nor do any wrong to him. And at once rode Jebûsamîk upon the litter, and he came to me like a rushing wind and opened the hole, (f. 198b) and I Haiqar climbed up from the hiding place. And because I sought help from God, I was not disgraced.  And Jebûsamîk Mes took me and brought me to the king. And when I came before the king, I fell down to the ground and bowed down before the king. And the hair of my head had grown long and come down to my shoulders, and my chin had become like the claws of an eagle, and my body was covered with dust and had become disheveled, and the color of my face had changed and faded like the color of ashes, and my appearance and form was only semi-human. And when the king looked long at me and saw that my beauty had decayed and I become disarrayed, he felt sorrow over me and wept and felt ashamed, and could not talk to me, and he wept greatly and said to me, “O Haiqar! I did not offend you, but Nadan whom you had raised up as your child, it is he who offended you.”

Then I said to the king, “May you my lord live forever! Since God has shown me your face, nothing has hindered me and nothing has harmed me.” Then the king answered and said, “Blessed be God the Merciful, who looked upon you and knew that you had been wronged, and has delivered you from being killed.” (f. 199a) And the king said to me, “Go O Haiqar and swim in the bath, and shave the hair of your head, and cut your nails, and eat and drink, and amuse yourself for forty days, that you may revive your soul, and your condition and your color and your body may be restored and improved, and after that come to me.” Then I set out and went to my house, and I did as the king commanded. but I shortened it by twenty days, for the affairs and the work of the king was urgent and pressing. And when I came before the king  and greeted him, he brought out the letter which the Egyptians had sent to him and said, “Haiqar, take this and see what the Egyptians sent to us after you were killed. They have provoked us and saddened us, and all the people of our country have left and fled to their country, because of the tribute and heavy taxes that they demand from us.” And when Haiqar read the letter he understood its contents. He answered and said to the king Sennacherib, “Be not sorrowful, O my lord and be not wroth! I will go to Egypt and give answer to the king, and I will provide an explanation to what he asks, and I will bring to you the taxes and the tribute from Egypt, and I will send back all those who have run away to their country; and I will put your enemies and those who hate you to shame (f. 199b) with the help of God, your country and your happiness.” And when Sennacherib heard me say this, he rejoiced greatly and delighted and was pleased. And he made a day of rejoicing and lavished gifts upon me, the kings, the princes and the notables. And he seated Jabûsamîkmiskînkinte in a high place and a prestigious position and gave him many gifts.  After a day, I Haiqar the poor , wrote a letter and sent to my wife Ishfaghnî and said to her, “When this letter reaches you, you will order the huntsmen to capture for us two young eaglets, and will command the cotton makers to weave for us two ropes of cotton and they should be the thickness of a finger and two thousand cubits long, and will command the carpenters to make for us great boxes, and you will give Nabûhail and Tabshâlîm the two little boys who belong to us to seven wet-nurses to suckle them and raise them up, and you will slaughter every day a sheep and feed the eagles to grow up and to get fat, and you will have the two boys ride every day on the backs of the eagles as long as they are young without any extra load, and you will knit the ropes to the feet of the eagles, and you will let the eagles fly (f. 200a)  into the air, and the two boys being mounted on their backs little by little till they become accustomed to carrying them. And when the eagles  go up to fly in the sky, you will teach the two boys to shout and say while flying on the backs of the eagles, “Deliver to us grout, lime, clay, bricks, and stones, for the architects and the builders and the workmen stand idle, and they want to build a castle in the heaven. And then you will draw in and pull down the eagles  and the two boys. Do this till I return.” And Ishfaghnî my wife was a skilled woman, there is none among the women of our country who understands or is more clever than her, and she did and worked all that I commanded.

A few days later, I Haiqar said to Sennacherib the king, “Permit me, O my lord, to go to Egypt, for the time to go has come. And when he gave me the command, I took with me a lot of soldiers. And I traveled for a day  and commanded the soldiers to stop in a wide, magnificent and beautiful plain, and I brought out the eagles from the boxes, and I tied the ropes to their feet, and let the two boys ride on their backs, and they flew to the sky and went up to a great height (f. 200b) till they became invisible to the people. And while they ascended aloft, the two boys and the eagles screamed and shouted, saying, “Deliver to us grout, lime, clay, bricks, and stones, for the architects and the builders and the workmen stand idle, and they want to build a castle in the heaven.” And I dragged them and pulled them down to me, and I looked at them as my heart would desire. And I praised Ishfaghnî my wife who fixed and brought to perfection everything which I had ordered and performed every command which I had commanded. And when the Assyrians and the people of Nineveh, who had fled to Egypt, heard all of what I did, they came back to their country and their places.

### A mention of Haiqar’s entry to Egypt to Pharaoh the king

And when I, Haiqar, had arrived with my soldiers to Egypt, I went to the door of Pharaoh the king. And his nobles made me known to him and said to him, “Sennacherib the king has sent to you a man as you demanded, what would be your decree?” And Pharaoh the king commanded to give us a dwelling place for me and for all the soldiers who are with me. Then Pharoah gave the command and (f. 201a) they brought me in to him, and when I came before him, I did obeisance to him and greeted him. Then he said to me, “What is your name, O man!” And I said to him, “Your servant is Abiqâm, and I am one of Sennacherib the king’s littlest ants.”  And when Pharaoh heard this, he became upset and said, “Am I so despised by your lord that he has sent me a little ant to reply to me?” He also said to me, “Go O Abiqâm today to your house and dwelling, and come back to me tomorrow.” Then I went, and Pharoah commanded all of his notables and magnates, “all of you are to wear purple and red dress tomorrow, and come back to me.” When it was morning, the king clothed himself in purple dress and sat on his throne, and all of his magnates were standing before and around him. And he commanded and they brought me into him, and Pharoah said to me, “O Abiqâm, who am I like? and my notables, to whom are they like?” I said to him, “O my lord! you are like the idol Bel, and your notables are as his servants.” And he said to me, “Go O Abiqâm to your dwelling, and come back to me tomorrow.” And the king commanded his magnates, “all of you are to wear white clothes from linen tomorrow and come back to me.” And the king (f. 201b) was also clothed white, silken dress and sat on his throne, and his magnates were standing before him. And he commanded and I entered to him, and he said to me, “O Abiqâm, who am I like? and my notables, to whom are they like?” I said to him, “You are like the sun, and your notables are like the sunbeams.” Then he said to me, “Go today to your house and come back to me tomorrow.” And the king decreed his notables, “Wear tomorrow black clothes and the veils of throne will be black and silken, and the king was clothed scarlet dress and sat on his throne, and he commanded and they brought me in to him and he said to me, “Who am I like? and my magnates, to whom are they like? I said to him, “You are like the moon and your magnates are like the planets.” He said to me, “Go to your house and come back to me tomorrow.” And the king decreed to his notables, “Wear tomorrow colorful clothes of every color and kind and the veils of throne will be red, and the king was clothed velvet  dress and sat on his throne, and he commanded and they let me pass before him and he said to me, “Who am I like? (f. 202a) and my magnates, to whom are they like? I said to him, “You are like the month of April and your magnates are like the chamomile and its blossoms.” When he heard this he rejoiced with great joy, and the king said to me, “The first time you compared me to the idol Bel, and my notables to his servants. The second time you compared me to the sun and my notables to its sunbeams. The third time you compared me to the moon and my notables to its planets. The fourth time you compared me to April and my notables to the chamomile and its blossoms. But tell me, your lord Sennacherib whom is he like? and his magnates, to whom are they like? and I shouted with a great voice and said, “Be it far from me to make mention of my lord the king and you seated on your throne. But get up on your feet that I may tell you whom my lord is like.”

And the king arose from his throne and I said to him, “My lord Sennacherib is like the God of heaven and his magnates as the lightning bolts; who when he wills, the winds blow and the rains falls. And he models the clay and brings blessings to his kingdom. And he also commands the thunders and they thunder and the lightning bolts and they flash. And he holds the sun, (f. 202b) and it does not shine, and its beams, and they do not appear. And he blocks the idol Bel and its magnates that they may not pass through the streets, and he blocks the moon and the stars that they may not shin. And if he chooses, he commands the north and the winds and the storms blow and strike, and the rains fall along with the cold, and they trample on April and disperse its blossoms and chamomile.

And when Pharoah heard this from me, he was greatly perplexed and was sorrowed and wroth, and said to me, “Let me know the facts, and tell me the truth O man, what is your name”? Then I said to him, “I am Haiqar the scribe, the private councilor of King Sennacherib.” Pharaoh said to me, “We have heard that Haiqar was killed, yet you are now alive.” I said to him, “Praise and thanks be to God the Merciful, the Honorable who hears the prayer of the broken hearted and saves the oppressed, and rebukes the oppressors and dishonors them and destroys them. There was a deception against me-- plot, deceit, and injustice from wicked people-- and they lied against me before my lord the king, and he commanded me to be killed, but my God saved me from murder, (f. 203a) and blessed are those who trust in Him and seek Him and ask Him for help, for he shall redeem and deliver each one.”

The king Pharaoh responded and said to me, “Go O Haiqar to your house and tomorrow come back to me, and tell me something that neither I nor any of my notables have heard, and that has never been heard in my country.” Then I went to my house and whispered to myself, “what word shall I say to them which they have never heard?” And I Haiqar sat and wrote a letter writing thus, “From Pharoah the king of Egypt to Sennacherib the king of Assyria and Nineveh. Peace, you know O my brother! that brothers have need of their brothers, and kings of kings, and now in this time I am in need of outlay, for my treasuries have been emptied and my supplies have been decreased. And I need you to send to me from your friendship and charity and to lend me nine hundred quintals of gold, and after a little time I will send it back to you and return your gold.” And I folded the letter and came before  the king Pharoah on the morrow. Then he said to me, “Tell me something that neither I nor any of my notables have heard, (f. 203b) and that has never been heard in my country.” Then I brought out the letter and handed it to him, and when they read it they were astonished and amazed, and said, “Truly and verily we have never heard this word, nor was it heard in our country.” Then I said to them, “Rightly there is a debt which Egypt owes to Assyria and Nineveh until it is fulfilled.” And when they heard this, they were astonished and their minds were perplexed. This was fixed as a payable debt,  and they accepted the terms. But Pharaoh the king answered and said to me, “I desire from you O Haiqar! to build me one marvel and luxurious castle that I may dwell in, and it should be between heaven and earth, and its height from earth two thousand cubits.”

Then I said to him, “To hear is to obey your command O my lord! I will build you a castle according to what you want and desire; but, O my lord! the builders and the architects must come from me, and lime and grout and clay and stones and the rest of what you need must be provided by you.” Pharaoh said, “so it is.”

And immediately I let the eagles out of the boxes and tied the ropes to their feet, and I let the young boys ride on their backs (f. 204a) and tied them well, and the eagles and boys flew. And they soared high, till they became unseen to all. And the boys and eagles began to shout, saying, “Bring to us grout and lime and clay and bricks and stone, for the king’s masons are standing idle, and the builders and workers want to build a castle for the king Pharoah into the heavens. They also said O servants! mingle for us a drink to drink.”  And when Pharoah and his notables saw this, their minds were stunned and they stammered, and they wondered and were humbled. And I Haiqar took a rod and began to beat Pharaohs’ notables so that they would deliver stones and anything needed to the craftsmen and the architects, who are idle. And I shouted for the troops, who they were with me, to beat Pharaohs’ notables with rods and whips so that they would bring stones and lime to the builders, and we beaten them severely, till they were subdued and defeated by us, and they fled and left us alone, and they entered their houses.  And Pharaoh was upset and said, “You are quite insane; who can f. 204b lift and bring anything up to this height and elevation, which they have demanded?” I said to him, “You are the madman and deceitful and stupid; if my lord the king Sennacherib were here, he would have built two castles in a single day.” Pharaoh answered and said to me, “Give up  now building the castle, for we do not need it. It has been proven to us that you can build many castles. But go today back to your house and tomorrow come to me.”

And I went, and on the morrow,  I came and entered to him, and he said to me, “O Haiqar, tell me how such a thing is possible? that the stallion of your lord Sennacherib neighs there in Assyria and Nineveh, and our mares hear his voice, and they cast their young .” And I left him and got out, and commanded my servants to catch me a single cat from the cats of the king’s house, and commanded the servants to flog her with a violent flogging till she screamed and squalled with extremely loud cries  till the Egyptians heard her voice and they went  to inform the king.  (f. 205a) And Pharaoh called me and said to me, “why are you flogging this cat?” I said to him, “O my lord! this wicked thing  has caused great trouble and has harmed me greatly, for I had a wonderful cock with a nice and beautiful voice and astounding beauty, which my lord the king had given to me, and it used to inform me with its incredible voice the hours of the night and the day.  And at night this damned cat went to Assyria and cut off the head of the cock and killed it and came back.” Pharaoh replied and said to me, “O Haiqar, I see that you are growing old in senescence and your wisdom is diminished and your knowledge is lessened, for between Egypt and Assyria and Nineveh there are more than three hundred and sixty parasangs (I mean days) , and how could this cat go all of this distance in one night and cut off the head of the cock and then come back to here?”

I said to him, “If there were such a distance between Egypt and Assyria, how could my lord’s horse neigh there and your mares cast their young here?” And when he heard this, he was astounded and ashamed and knew that I had explained to him his question.

(f. 205b)

And he said to me, “Explain this riddle: a mason has built a pillar (I mean hermitage), and he has built it from eight thousand seven hundred and sixty-three bricks, and has bound it with three hundred and sixty-five bricks, and has planted on it twelve cedars, and has made in each cedar thirty boughs (I mean rods), and in each rod there have been two bunches of fruits one is white and one is black.”  I said to the king, “This riddle, the herdsmen of Assyria and Nineveh know it. The mason is God Glorified and Exalted Be who created the year which is the pillar, and made the hours of the year eight thousand seven hundred and sixty-three hours,  and made the days of the year three hundred and sixty-five days, and the twelve cedars are the twelve months, and in each cedar there are thirty rods (I mean thirty days, the days of the month), and in each rod there are two bunches one is white and one is black, which are the night and the day.”

And the king said to me, “O Haiqar, twine to me two ropes of the sea-sand and its dust, and their thickness (f. 206a) should be in thickness of finger.” I said to the king, “Order them to bring me a rope out of the treasury that I may make one like it.” And he said to me, “Give up! If you do not make these ropes, I will not give you the tribute of Egypt.” Then I thought in my heart, and went to the back of the king’s house, and bored two holes in the wall and the sun penetrated into the holes, and I went and filled my hands with sea-sand, and scattered it through the holes till the dust became as if woven in the sunbeam and entwined like a rope. And I said to the king, “Command your servants to take these ropes, and whenever you desire, I will weave you many ropes.” And the king and his notables were also astonished. And the king said to me, “O Haiqar, we have a millstone here and it is a marvelous thing but it has been broken, and I want you to sew it up for us. And he ordered his servants to carry the broken stone and put it before me. And I looked and there is also another stone near them and it is also broken (f. 206b) like that millstone, and it is also broken like it, and I went and carried it and threw it down before the king. And I said, “I am a foreigner here, and I have not brought with me tools for sewing. You command  the shoemakers whom you have in the country, and they shall cut awls for me from this stone which I carried, that I may sew the stone which you have brought to me.” And the king and all of his notables laughed and said, “Blessed be God, who gave you O Haiqar this learning and wit and wisdom and great knowledge.”

A notice of Haiqar’s departure from Egypt and his return to Sennacherib king of Assyria and Nineveh

And when the king and his notables saw that they were overcome and defeated and that I Haiqar explained for them all their riddles and solved all their problems, and that no argument remained for them against me, Pharaoh gave me the money and the taxes of Egypt and of its lands for three years. And he gave me the nine hundred quintals of gold that I wrote in my letter, which they wanted to borrow (f. 207a) from my lord, and they gave me and my lord many gifts. And they bestowed  gifts upon me and upon all the soldiers who were with me. And he permitted me to go back and return to my lord. And I kissed his hand and returned on my way.

And when I reached my lord Sennacherib and he heard the news of my return and arrival, he went out to meet me and rejoiced with great joy, and sat me to his right on his throne and said to me, “Ask me O Haiqar! everything you would like to have and desire that I may make you rich.”  And I said to him, “I desire you to live and last forever and eons, and everything you want to bless me with give it to Jebûsamîk Meskîn kentê, for after God it is he who granted to me this life.” And he bestowed upon him much.

And the king began to ask me about everything I had done before Pharoah and his notables, and I narrated the events to him one by one and he listened and was amazed. And I brought out the money, the taxes, the endowments, the gifts , and the gold that I brought to him.  And the king rejoiced with a great joy beyond description and said to me, (f. 207b) “Tell me what share should I give you from it.” And I said to him, “I desire your safety, I have no need of this. But command and give me Nadan, my sister’s son, that I may judge him according to your order, and do not ask for his blood from me.”

And he commanded and they gave me Nadan, my sister’s son, and I took him and walked to my house, and I tied him with a chain of iron, and fettered his hands and feet with iron, and a fetter of iron on his neck, and I began to whip him with a hard and bitter whipping, a severe whipping, and I beat him, a thousand rods on his shoulders and a thousand on his breast, and cast him into the latrine  (I mean, I wanted him to sniff the stench). And his foot was damaged  on the house door, and I gave him bread and water in very small amounts, and I delivered him to the hands of Nebuhal, my lad, to guard him, and I said to my lad, “Write down all words that I want to say to Nadan the fool, who I had honored and revered, but who did not understand, and betrayed me and desired to kill me and to end my life. And whenever I go in or out and scold him, you write the speech down.” And then I Haiqar (f. 208a) began to say to him,

1- “O my son! it is said in the proverbs: He who listens not with his ear, they will make him listen with the scruff of his neck.”

And he said to me, “Why are you wroth with me?” Then I said to him, “Because I revered you and taught you and brought you up, and desired to seat you on the throne of the kingdom, and you pushed me out of my position and wanted to kill me. But God saved me and redeemed me because I was wronged, for God answers the wronged and supports the broken hearted, and He pushes down the haughty and those who glory in their spirits.

2- “O my son! you have been to me like the scorpion which stings  the rock but the rock and it does not feel it. And it also stings  the needle but the needle says to it, ‘Behold, you have struck a dung that is harder than your dung.’ And it also stings  the camel in its hooves and foot, and (the camel) raised its head to heaven and said to it, may it be for you as it is for me.”

3- “O my son! you have been to me like a goat (I mean a sheep) which stood upon grass and roots of the madder and ate from them, and the madder said to the goat, ‘Why do you eat me? (f. 208b)  for they tan your hide in me,’ and the goat said, ‘I eat from you in my life, and in my death they take out your roots and tan my hide in you’.”

4- “O my son! you have been to me like a man who took a stone and threw it up to heaven to stone God. but the stone did not reach Him, and yet he brought sin upon himself from God.”

5- “O my son! you have been to me like a man who saw another man, his comrade, who was shivering from cold, and he took a bucket of water and poured it upon him.”

6- “Would that, O my son! you had killed me, you would have been able to take my place and be my heir and successor.”

7- “know, O my son! if the tail of the ram and the pig were seven cubits long, it would not approach the worth of the horse and would not perform its work; and even if its hair were soft and fine like linen, free men would not wear it.”

8- “O my son! I used to say that you would have been a substitute for me and take my place and rule my house and inherit my property and gain my belongings, my learning, my knowledge, and my wisdom. But you did not accept my teaching nor (f. 209a) did you hear my speech, and you did not pray to God nor did He hear your voice.”

9- “O my son! you have been to me as a lion who came across and met an ass in the morning, and the lion said to the ass, ‘Come in peace O my beloved brother. Said the ass, ‘May your peace come and meet the man who tied me in the evening and failed to bind the bond firmly enough, perhaps then I would not have seen your face’.”

10- “O my son! you have been to me like a trap which was set up on the dunghill, and there came a sparrow and found the trap set up. And the sparrow said to the trap, ‘What are you doing here?’ Said the trap, ‘I am praying here to God’. And the lark asked it also, ‘What is this stick  that you hold?’ Said the trap, ‘That is a rod  on which I lean at the time of prayer.’ Said the lark: ‘And what is that thing in your mouth?’ Said the trap: ‘That is bread and victuals which I carry for all the hungry and the poor who come near to me.’ Said the lark: ‘Now then may I come forward and eat, for I am hungry?’ and the trap said to him, ‘Come forward.’ And the lark approached (f. 209b) that it might eat. But the trap sprang up and seized the lark by its neck. And the lark answered and said to the trap, ‘If that is your bread for the hungry God will not accept your alms and your good deeds. And if that is your fasting and your prayers, God accepts from you neither your fast, nor your prayer, and God will not perfect what is good concerning you’.”

11- “O my son! you have been to me like a weevil in the wheat, for it does no good to anything and spoils the wheat.”

12- “O my son! you have been to me like a man who sowed ten measures of wheat in a field, and when he reaped it he found in it ten measures, and he said to the field, ‘if you were not to produce more than I sowed and gathered, you would not have come to be nor grown’.”

13- “O my son! you have been to me like the trapped partridge that could not save herself from the hunter, but she called out with her sweet and fine voice to many partridges, that they might be caught like her.”

14- “O my son! you have been to me like the dog that was cold and went into the potter’s house (I mean the potter) to get warm and stay there. (f. 210a) And when it had gotten warm and stayed there, it began to bark at them, and they chased it but it did not run out. And they beat it and killed it, that it might not bite them and kill them.”

15- “O my son! you have been to me like the ram, the pig, who went into the hot bath with the nobles and free men to swim, and when it swam and came out of the hot bath, it saw a filthy geyser and it went down and wallowed in it.”

16- “O my son! you have been to me like the ape, in whose ears they had put gold swivels, but its (…) was not cleaned of ashes and soot.”

17-“O my son! you have been to me like the ram which enjoined its ram and goats comrades to the butchers, yet it was unable to save itself from slaughter.”

18- “O my son! the dog which is not fed from its hunting becomes food for wolves.”

19- “O my son! the hand which does not plow and is not diligent and clever shall be cut off and detached from the arm.”

 20- “O my son! the eye in which no light is seen, the ravens shall pick at it and pluck it out.”

“O my son! I showed you the face of the king and brought you up, (f. 210b) and you have rewarded the one who does good to you with evil. How would you repay and requite the one who does bad to you?”

22- “O my son! you have been to me like the tree which said to those who wanted to cut it down, ‘were it not for my own strength in your hands, you would not be able to do it’.”

23- “O my son! you have been to me like the cat to whom they said, ‘leave off thieving and the king will make for you gold swivels and make for you chains of silver and pearls, and feed and drink you (till) fullness.’ And she said, ‘I am not ever giving up of the craft that my father and mother have taught me’.”

24- “O my son! you have been to me like the serpent which was riding on a boxthorn when he was in the river, and a wolf saw them and said, ‘Wicked upon wicked, and let him who is more mischievous than they direct both of them.’ And the serpent said to the wolf, ‘The lambs and the goats and the sheep which you have eaten all your life, will you return them to their fathers and to their parents or not?’ Said the wolf, ‘No.’ And the serpent said to him, ‘Therefore after me you are the worst of us’.”

(f. 211a)

25- “O my son! I fed you with all good and delicious food but you did not even sate me with bread, (but you left me) in the dust and pit and hidden-place, and sought to kill me.”

26- “O my son! I brought up your stature like a beautiful cedar, and you have bent and twisted me in my life and brought me down in the hidden-place by the numbers of your evil deeds and sins.”

27- “O my son! I thought that I had built a fortified castle and a refuge for myself that I might be concealed from my foes, but you threw me down and pushed me before my enemies, but God the Merciful saved me.”

28- “O my son! I wished you all good, and you rewarded me with evils. I would like to root out  your eyes and cut out your tongue, and take off your head with the sword, and requite all your bad deeds, and recompense all your evils.”

And Nadan my boy replied and said to me, “Far be it from you! that you do evil or that there be a misdeed on your hands. But do with me according to your generosity and goodness and kindness, and forgive the sins which I committed against you, for God forgives the sins of the sinners and the faults of the guilty, and (He) accepts the repentant. Accept me into the service of your horses, [f. 211b] and I will herd your house’s hogs, and sweep up your dung, and I will be called a wicked man and you will be called a righteous man.

29- Then I said to him, “O my son! you are like the palm tree which was planted by the shore of the river, and it used to cast all of its dates into this river, and its owner came to cut it down, for he had not profited at all from its dates. And the palm tree said to him, ‘Remove me to that clime and then I will provide you with my dates.’ And he said to it, ‘Being on your own spot and property you have not borne fruits from your stems and roots and branches, how then shall you provide me with anything otherwise.

30- “O my son! the old age of the eagle is better than the youth of the hatchling  .”

31- “O my son! they said to the wolf, ‘Keep away from coming near to the sheep lest their dust should come upon you’. And he said, ‘I will not keep away, for their dust is good for my eyes’.”

32- “O my son! they brought the wolf to the school that he might learn, and the teacher said to him, ‘Say A, B.’ He answered and said, ‘Lamb, and goat,’ as his nature.

33- “O my son! have you not thought upon what I taught you, God is a just ruler and a fair Judge and those who do good he will reward with good and bliss; and those who do evil, he will repay with torture and hellfire and hell. And now God is between me and you and He will repay you according to your deed.

34- “O my son! they set the head of the ass at the table and it rolled and fell on the ground, and they said, ‘He withheld his spirit not to accept dignity and reverence.”

35- “O my son! you have confirmed the saying which runs, ‘If you beget a boy, call him your son, and if you rear a boy, call him your slave’.”

36- “O my son! Verily, better than all of these sayings is, ‘Take your sister’s son and throw  him on the ground, and bash him against the wall, namely you O my son’.

37- And God knows what is hidden, and He will requite every man according to his work and will judge between you and me. And I will say nothing to you, (for) God will repay you as much as you deserve’.”

And when Nadan the ignorant heard that speech, he swelled up immediately and became like a distended bottle. (f. 212b) and his bones swelled. And because of his cheating and misconduct, he was rent and burst, and perished, and died. And his latter end was destruction, and he went to hell. And it has been said in the book of Proverbs, “He who does good shall meet and find good; and he who does evil shall meet and find evil. And he who digs a pit for his comrade he himself shall fall down into it; and he who builds (namely sets up) a trap for his comrade he himself shall be caught in it.”

Now the story of Haiqar the wise Persian, the skillful philosopher, has come to a conclusion, he who first worshiped idols but at the end he believed in God and acknowledged His great name. And his teaching to his sister’s son is finished.

And you O blessed listeners! may God forgive your sins and iniquities, and may He make his mercy flow upon you, and may He save you from all evils and sorrows, and all misfortunes and calamities, and all plagues and afflictions, and may He fill your hearts with all understanding and all spiritual knowledge, that you may protect yourselves and keep away (f. 213a)  from all hate and envy and jealousy, and from each sin, by the prayer of all the righteous and the rightly guided. And praise be to God, Lord of the Worlds, Amen. The mercy of God be upon all of us, Amen. **The story of Haiqar the wise is completed.**

## How to Cite This Page

Elrefaei, Aly. „Translation of the Arabic Version“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/arabictranslation.html.