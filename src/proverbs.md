---
title: Proverbs
layout: Layout
hide_sidebar: true
---

# {{ $frontmatter.title }}

## In the Syriac Version of the Story of Ahiqar

Author: Simon Birol

## Introductory remarks

The following tables give a comparative overview of the order and existence of the proverbs in the story of Ahiqar. The proverbs have been numbered (see Tab. 1) and the various correspondences (cf. Tab. 2) have been made on the basis of content rather than exact verbal correspondence. Biblical parallels have been included in Tab. 1 as well. Where the biblical parallels are in brackets, the parallel is not to be found in the transcribed text, but in a version of the proverb in another textual witness. A synopsis of the proverbs has not yet been implemented and will be introduced at a later date in conjunction with the tables for the Arabic versions at a later date.

### How to Cite This Page

Birol, Simon. „Proverbs. In the Syriac Version of the Story of Ahiqar“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/proverbs.html.

### Tab. 1: A comparative order of the proverbs of Ahiqar in the Syriac manuscript tradition

These proverbs are not recorded in Jerusalem SMMJ 162 and Sachau 162. Nor is this part transmitted in the fragment of BL Add. 7200. The proverbs have been numbered and transcribed as follows:

* Nos. 1-39 are transcribed from Cambridge Add. 2020 (C)
* No. X1 is transcribed from Sachau 336 (U)
* Nos. X2-X3 are transcribed from Harvard syr. 80 (H)
* Other sigla: Oxford BL Or. 2313 (O), BnF syr. 434 (B), Aleppo SCAA 7/229 (A), Notre-Dame des Semences, mss. 611 (K), Notre-Dame des Semences, mss. 612 (I), Ms. Graffin (G), BnF syr. 422 (N), Mingana syr. 433 (M), Strasbourg S4122 (T), St. Petersburg, Sado 9 (P), Mosul DFM 430 (D), Tellkepe, QACCT 135 (Q)

<ModalTrigger modal-element="#table1" trigger-element="#table1-trigger" />
<div class="modal fade" id="table1" tabindex="-1" aria-hidden="true" role="dialog">
<div class="modal-dialog card">
<div class="modal-header">
<h2>A comparative order of the proverbs of Ahiqar in the Syriac manuscript tradition</h2>
<CloseButton/>
</div>
<div class="modal-content">

<h3>Sigla</h3>

<table>
    <tr>
        <td>
            <ul>
                <li><strong>A:</strong> <a href="/manuscripts.html#aleppo-scaa-7-229" target="_blank">Aleppo SCAA 7/229</a></li>
                <li><strong>B:</strong> <a href="/manuscripts.html#paris-bibliotheque-nationale-de-france-ms-syr-434" target="_blank">BnF syr. 434</a></li>
                <li><strong>C:</strong> <a href="/manuscripts.html#cambridge-univ-cant-add-2020" target="_blank">Cambridge Add. 2020</a></li>
                <li><strong>D:</strong> <a href="/manuscripts.html#mosul-dfm-430" target="_blank">Mosul DFM 430</a></li>
                <li><strong>G:</strong> <a href="/manuscripts.html#ms-graffin" target="_blank">Ms. Graffin</a></li>
                <li><strong>H:</strong> <a href="/manuscripts.html#cambridge-ma-harvard-university-houghton-library-syr-80" target="_blank">Harvard syr. 80</a></li>
                <li><strong>I:</strong> <a href="/manuscripts.html#alqosh-notre-dame-des-semences-mss-syr-612-codex-207" target="_blank">Notre-Dame des Semences, mss. 612</a></li>
            </ul>
        </td>
        <td>
            <ul>
                <li><strong>K:</strong> <a href="/manuscripts.html#alqosh-notre-dame-des-semences-mss-syr-611-codex-205">Notre-Dame des Semences, mss. 611</a></li>
                <li><strong>M:</strong> <a href="/manuscripts.html#birmingham-mingana-syriac-433">Mingana syr. 433</a></li>
                <li><strong>N:</strong> <a href="/manuscripts.html#paris-bibliotheque-nationale-de-france-ms-syr-422-ms-pognon">BnF syr. 422</a></li>
                <li><strong>O:</strong> <a href="/manuscripts.html#london-brit-libr-or-2313">Oxford BL Or. 2313</a></li>
                <li><strong>P:</strong> <a href="/manuscripts.html#st-petersburg-sado-no-9">St. Petersburg, Sado 9</a></li>
                <li><strong>T:</strong> <a href="/manuscripts.html#bibliotheque-nationale-de-france-et-universitaire-de-strasbourg-ms-4122">Strasbourg S4122</a></li>
                <li><strong>U:</strong> <a href="/manuscripts.html#staatsbibliothek-zu-berlin-sachau-336">Sachau 336</a></li>
            </ul>
        </td>
    </tr>
</table>

<strong>Please note:</strong>

<ul>
    <li>Nos. 1–39: transribed according to Cambridge Add. 2020 (C)</li>
    <li>No. X1: transcribed according to Sachau 336 (U)</li>
    <li>No. X2: transcribed according to Harvard syr. 80 (H)</li>
    <li>No. X3: transcribed according to Harvard syr. 80 (H)</li>
</ul>

<table>
        <tr class="ro1">
            <th>
                <p>Nos.</p>
            </th>
            <th>
                <p>English Translation</p>
            </th>
            <th>
                <p>Syriac</p>
            </th>
            <th>
                <p>Biblical Parallels</p>
            </th>
            <th>
                <p><abbr class="siglum C">C</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum O">O</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum B">B</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum A">A</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum N">N</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum G">G</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum K">K</abbr> &amp; <abbr class="siglum I">I</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum U">U</abbr> &amp; <abbr class="siglum M">M</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum T">T</abbr>, <abbr class="siglum H">H</abbr> &amp; <abbr class="siglum P">P</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum D">D</abbr></p>
            </th>
            <th>
                <p><abbr class="siglum Q">Q</abbr></p>
            </th>
        </tr>
        <tr class="ro1">
            <td>
                <p>1</p>
            </td>
            <td>
                <p>My son, he who does not hear with his ears, they make him hear with the nape of his neck.</p>
            </td>
            <td>
                <p>ܒܪܝ ܡܿܢ ܕܠܐ ܫܡܥ ܡܼܢ ܐܕܢܘܗ̈ܝ ܡܼܢ ܒܣܬܪ ܩܕܠܗ ܡܫܡܥܝܼܢ ܠܗ</p>
            </td>
            <td> </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>(lost)</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p>1</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>2</p>
            </td>
            <td>
                <p>My son Nadan answered and said to me: “Why are you [so] angry against your son?” I answered and said
                    to him: “My son, I have set you on the throne of honour, but you have cast me down from my throne.
                    But my righteousness has saved me.</p>
            </td>
            <td>
                <p>ܥܢܼܐ ܢܕܢ ܒܪܝ ܘܐܡܪ ܠܝܼ ܠܡܢܐ ܐܬܼܚܡܬܼܬܿ ܥܠ ܒܪܟ ܥܢܿܝܬܼ ܘܐܡܪܬܼ ܠܗ ܐܢܐ ܒܪܝ ܥܠ ܟܘܼܪܣܝܐ ܕܐܝܼܩܪܐ ܐܘܬܿܒܼܬܟ
                    ܘܐܢܬܿ ܡܼܢ ܟܘܼܪܣܝ ܣܚܦܬܢܝ ܘܠܝܼ ܟܐܢܘܼܬܼܝ ܫܘܙܒܼܬܼܢܝ</p>
            </td>
            <td>
                <p>[A. 1 Petr 3,9: ܘܠܐܢܫ ܒܝܫܬܐ ܚܠܦ ܒܝܫܬܐ ܠܐ ܬܦܪܥܘܢ܂ ܘܐܦܠܐ ܨܘܚܝܬܐ ܚܠܦ ܨܘܚܝܬܐ܂ ܐܠܐ ܕܠܩܘܒܠܐ ܕܗܠܝܢ܆ ܗܘܝܬܘܢ
                    ܡܒܪܟܝܢ܂ ܠܗܕܐ ܓܝܪ ܐܬܩܪܝܬܘܢ܆ ܕܒܘܪܟܬܐ ܬܐܪܬܘܢ܂ // B. Rom 12,17: ܘܠܐ ܬܦܪܥܘܢ ܠܐܢܫ ܒܝܫܬܐ ܚܠܦ ܒܝܫܬܐ ܐܠܐ
                    ܢܬܒܛܠ ܠܟܘܢ ܕܬܥܒܕܘܢ ܛܒ̈ܬܐ ܩܕܡ ܒܢܝ̈ܢܫܐ ܟܠܗܘܢ܂ // C. 1 Thess 5,15: ܘܶܐܙܕܱܿܗ̱ܪܘ ܕܱܠܡܳܐ ܐ̱ܢܳܫ ܡܶܢܟܼܘܽܢ
                    ܒܻܝܫܬܴܐ ܚܠܳܦܼ ܒܻܿܝܫܬܴܿܐ ܢܶܦܼܪܘܽܥ܆ ܐܶܠܴܐ ܒܼܟܼܽܠܙܒܼܰܢܿ ܗܰܪ̱ܛܘ ܒܿܳܬܰܪ ܛܳܒܼܳܬܼ̈ܳܐ܁ ܠܘܳܬܼ ܚ̈ܕܼܳܕܼܶܐ܂
                    ܘܰܠܘܳܬܼ ܟܿܽܠܢܳܫ܂]</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>32</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>2</p>
            </td>
            <td>
                <p>2</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>3</p>
            </td>
            <td>
                <p>My son, you have been to me like a scorpion striking a rock. And it [the rock] says to it: “You have
                    struck at an unconcerned heart.” And it strikes at a needle, and they say to it: “You have struck at
                    a sting worse than yours.”</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܥܩܪܒܼܐ ܕܡܚܝܐ ܠܫܘܿܥܐ ܘܐܡܪ ܠܗܿ ܥܠ ܠܒܐ ܫܠܝܐ ܡܚܝܬܿܝ ܘܡܚܬ ܠܡܚܛܐ ܘܐܡܪܝܼܢ ܠܗ ܡܚܝܬܿܝ
                    ܠܥܘܼܩܣܐ ܕܒܼܝܼܫ ܡܼܢ ܕܝܼܠܟܼܝ</p>
            </td>
            <td> </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p>3</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>4</p>
            </td>
            <td>
                <p>My son, you have been to me like a goat standing over a red berry and eating it. And the red berry
                    said to him: "Why do you eat me, when you treat your skin with my root?" The goat said to it: "I eat
                    you during my life, and after my death they will pull you up by your roots".</p>
            </td>
            <td>
                <p>ܗܘܝܬܼ ܠܝܼ ܒܪܝ ܐܝܟ ܥܙܐ ܕܩܝܿܡܐ ܗܘܼܬܼ ܥܠ ܐܘܼܓܐ ܘܐܟܼܠܐ ܠܗ ܘܐܡܪ ܠܗܿ ܐܘܼܓܐ ܡܛܠ ܡܢܐ ܐܟܼܠܐ ܐܢܬܼܝ ܠܝܼ ܕܡܫܟܟܼܝ
                    ܒܥܩܪܝ ܦܠܚܿܝܼܢ ܠܗ ܐܡܪܐ ܠܗ ܥܙܐ ܐܟܠܬܟ ܒܚ̈ܝܝ ܘܒܼܡܘܬܿܝ ܡܼܢ ܥܩܪܟ ܢܥܩܪܘܼܢܟ</p>
            </td>
            <td> </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>X1</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>4</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>5</p>
            </td>
            <td>
                <p>My son, you have been to me like the onw who threw a stone at heaven, and it did not reach heaven,
                    but he received punishment from God.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܗܘܿ ܕܫܕܼܐ ܟܐܦܐ ܠܫܡܝܐ ܘܠܫܡܝܐ ܠܐ ܡܛܬ ܘܚܛܗܐ ܡܼܢ ܐܠܗܐ ܩܒܿܠ</p>
            </td>
            <td>
                <p>[Sir 27,25: ܒܝܬ ܪ̈ܫܝܥܐ ܠܐ ܬܬܒ܂ ܘܡܐ ܕܓܚܟܝܼܢ ܣܿܟܪ ܐܕܢ̈ܟ܂ ܕܓܠܿܐ ܪܐܙܐ ܡܘܒܕ ܗܝܡܢܘܬܗ܂ ܘܠܐ ܢܫܟܚ ܠܗ ܪܚܡܿܐ ܐܝܟ
                    ܢܦܫܗ܂]</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td> </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>X2</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p>5</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>6</p>
            </td>
            <td>
                <p>My son, you have been to me like the one who saw his companion shivering from cold and took a pitcher
                    of water and poured it over him.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܗܘܿ ܕܚܙܐ ܠܚܒܼܪܗ ܕܪܥܿܠ ܡܼܢ ܩܘܼܪܫܗ ܘܫܩܿܠ ܓܪܒܼܐ ܕܡ̈ܝܐ ܘܕܡܐ ܥܠܘܗܝ</p>
            </td>
            <td> </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td> </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>6</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>7</p>
            </td>
            <td>
                <p><span> Oh, my son, if you had killed me, you would have been able to stand in my place; but you
                        should know, my son, that even if the tail of the pig were to grow to seven ells, it would not
                        take the place of the horse, and even if its bristles were soft and woven, it would not ascend
                        to the body of a free man.</span></p>
            </td>
            <td>
                <p>ܠܘܝ ܕܝܢ ܒܪܝ ܡܐ ܕܩܛܠܬܢܝ ܡܫܟܚ ܗܘܼܝܬܿ ܕܬܼܩܘܼܡ ܒܕܘܼܟܬܼܝ ܗܘܝܬܿ ܕܝܢ ܝܕܥ ܐܢܬܿ ܒܪܝ ܕܐܠܘܼ ܢܼܪܒܿܐ ܕܘܼܢܒܐ
                    ܕܚܙܝܼܪܐ ܫܒܥ ܐܡܝܼ̈ܢ ܒܕܘܼܟܬܼ ܣܘܼܣܝܐ ܠܐ ܩܐܿܡ ܘܐܢ ܢܗܘܐ ܪܟܿܝܼܟ ܣܥܪܗ ܘܩܘܙ ܡܡܬܼܘܿܡ ܥܠ ܓܘܼܫܡܐ ܕܒܪ ܚܐܪ̈ܐ ܠܐ
                    ܣܠܿܩ</p>
            </td>
            <td> </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td> </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p>7</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>8</p>
            </td>
            <td>
                <p>My son, I intended that you should be in my place, that you should acquire my house and my wealth,
                    and that you should inherit them. But God was not pleased, and He did not hear your voice.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢܐ ܐܡܪܬܼ ܕܬܗܘܐ ܚܠܦܝ ܘܒܝܬܿܝ ܘܢܟܼ̈ܣܝ ܐܢܬܿ ܬܩܢܐ ܐܢܘܿܢ ܘܬܐܪܬܼ ܐܢܘܿܢ ܘܠܐܠܗܐ ܠܐ ܫܼܦܪ ܘܠܐ ܫܼܡܥ ܒܩܠܟ</p>
            </td>
            <td> </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td> </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>8</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>9</p>
            </td>
            <td>
                <p>My son, you have been to me like a lion that came upon a donkey in the morning of the day, and said
                    to him "Welcome, my Lord Cyrus". But the donkey said to him: "May the same welcome that you give me
                    be given to him, who tied me up last night, but did not tie my loins, lest I should see your face".
                </p>
            </td>
            <td>
                <p>ܗܘܝܬܼ ܠܝܼ ܒܪܝ ܐܝܟ ܐܪܝܐ ܕܦܓܥ ܒܚܡܪܐ ܡܼܢ ܨܦܪܗ ܕܝܘܡܐ ܘܐܡܪ ܠܗ ܬܐ ܒܫܠܡ ܡܪܝ ܩܘܼܪܝܼܣ ܗܘ ܕܝܢ ܐܡܪ ܠܗ ܐܝܟ ܫܠܡܐ
                    ܕܝܗܿܒܼ ܐܢܬܿ ܠܝܼ ܢܗܘܐ ܠܗܘܿ ܕܐܣܪܢܝ ܒܪܡܫܐ ܘܠܐ ܚܝܨܗ ܠܐܣܘܼܪܝ ܕܐܦܝ̈ܟ ܠܐ ܚܙܿܝܬܼ</p>
            </td>
            <td> </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td> </td>
            <td>
                <p>20</p>
            </td>
            <td> </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p>9</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>10</p>
            </td>
            <td>
                <p>My son, a snare was set on a dunghill, and a sparrow came and saw it and said to it: "What are you
                    doing here?" and the snare said to it: "I am praying to God". The sparrow said: "And this in your
                    mouth - what is it?" The snare said: "Bread for strangers". [Then] the sparrow approached to take
                    it, and [the snare] caught it by the neck. And while the sparrow was flapping, it said: "If this is
                    bread for strangers, may the God to whom you pray never answer your voice".</p>
            </td>
            <td>
                <p>ܒܪܝ ܦܚܐ ܨܠܐ ܗܘܼܐ ܒܩܩܠܬܐ ܘܐܬܼܐ ܨܦܪܐ ܚܙܼܝܗܝ ܘܐܡܪ ܠܿܗ ܡܢܐ ܥܒܿܕ ܐܢܬܿ ܗܪܟܐ ܐܡܪ ܠܗ ܦܚܐ ܠܐܠܗ ܡܨܠܐ ܐܢܐ ܐܡܪ ܠܗ
                    ܨܦܪܐ ܘܗܘܿ ܕܐܝܼܬܼ ܒܦܘܼܡܟ ܡܢܘ ܐܡܪ ܦܚܐ ܠܚܡ ܥܪ̈ܨܐ ܘܩܪܒܼ ܨܦܪܐ ܗܘܿ ܕܢܣܒܗ ܘܨܕܗ ܒܨܘܪܗ ܘܟܕ ܡܬܿܛܪܦ ܨܦܪܐ ܐܡܪ ܐܢ
                    ܗܢܿܘ ܠܚܡܐ ܕܥܪ̈ܨܐ ܐܠܗܐ ܕܡܨܠܐ ܐܢܬܿ ܠܗ ܠܐ ܢܫܡܥ ܠܩܠܟ</p>
            </td>
            <td> </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td> </td>
            <td>
                <p>24</p>
            </td>
            <td> </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p>10</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>11</p>
            </td>
            <td>
                <p>My son, I was like a bull tied to a lion, but the lion turned towards [it] and crushed it.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܒܪܝ ܐܝܟ ܬܘܪܐ ܕܐܣܝܼܪ ܥܡ ܐܪܝܐ ܘܐܬܼܦܢܝܼ ܐܪܝܐ ܘܬܒܼܪܗ</p>
            </td>
            <td> </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td> </td>
            <td>
                <p>X1</p>
            </td>
            <td> </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>X2</p>
            </td>
            <td>
                <p>(lost)</p>
            </td>
            <td>
                <p>12</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>12</p>
            </td>
            <td>
                <p>My son, you have been to me like a fleeting weevil that destroys the granaries of kings, but has no
                    hold on anything.</p>
            </td>
            <td>
                <p>ܘܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܩܠܡܐ ܕܥܒܼܘܼܪܐ ܕܚܪܒܬ ܐܘܨܪ̈ܐ ܕܡܠ̈ܟܿܐ ܘܗܝ ܒܡܕܡ ܠܐ ܚܫܝܼܒܐ</p>
            </td>
            <td> </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td> </td>
            <td>
                <p>36</p>
            </td>
            <td> </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>24</p>
            </td>
            <td>
                <p>X3</p>
            </td>
            <td> </td>
            <td>
                <p>17</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>13</p>
            </td>
            <td>
                <p>My son, you have been to me like a pot on which they have made golden handles, but the soot has not
                    been scraped off the bottom.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܩܕܪܐ ܕܥܒܕܘ ܠܗܿ ܐܕ̈ܢܐ ܕܕܗܒܼܐ ܘܗܝ ܐܫܬܼܗܿ ܡܼܢ ܫܘܼܚܪܐ ܠܐ ܡܬܼܓܪܕܝܐ</p>
            </td>
            <td> </td>
            <td>
                <p>13</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td> </td>
            <td>
                <p>38</p>
            </td>
            <td> </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td> </td>
            <td>
                <p>18</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>14</p>
            </td>
            <td>
                <p>My son, you have been to me like a ploughman who sowed twenty ears of barley in a field. And when he
                    reaped it, it was twenty ears. And he said to it: "What I have scattered, I have gathered: but thou
                    art a disgrace with thy evil name, in that thou hast made a peck out of a peck, while I live".</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܐܟܪܐ ܕܙܪܥ ܐܫܟܪܐ ܕܥܣܪܝܼܢ ܡܕ̈ܘܬܐ ܕܣܥܪ̈ܐ ܘܟܕ ܚܨܕܼܗܿ ܥܒܼܕܬ ܠܗ ܥܣܪܝܼܢ ܡܕ̈ܘܬܼܐ ܘܐܡܪ ܠܗܿ
                    ܗܘܿ ܡܕܡ ܕܒܕܪܬܼ ܟܢܫܬܿ ܐܠܐ ܒܗܿܬܐ ܐܢܬܿܝ ܒܫܡܟܼܝ ܒܝܼܫܐ ܕܡܘܕܝܐ ܡܘܕܝܐ ܥܒܕܬܿܝ ܘܐܢܐ ܚܝܐ ܐܢܐ</p>
            </td>
            <td> </td>
            <td>
                <p>14</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
            <td> </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>13</p>
            </td>
            <td> </td>
            <td>
                <p>20</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>15</p>
            </td>
            <td>
                <p>My son, you have been to me like a decoy bird, which could not save himself from death, and by his
                    voice he seduced his companions getting into the trap.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܐܪܐ ܕܨܦܪܐ ܗܘ ܠܢܦܫܗ ܡܼܢ ܡܘܬܐ ܠܐ ܡܫܘܙܒ ܘܠܩܠܗ ܚܒܼܪ̈ܘܗܝ ܡܛܒܥ</p>
            </td>
            <td> </td>
            <td>
                <p>15</p>
            </td>
            <td>
                <p>15</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>14</p>
            </td>
            <td> </td>
            <td>
                <p>23</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>16</p>
            </td>
            <td>
                <p>My son, you have been to me like a goat that leads its fellows to the slaughterhouse, but does not
                    save itself.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܬܝܫܐ ܕܡܥܠ ܚܒܪ̈ܘܗܝ ܠܒܝܬܼ ܛܒܚ̈ܐ ܘܗܘ ܠܢܦܫܗ ܠܐ ܡܫܘܙܒܼ</p>
            </td>
            <td> </td>
            <td>
                <p>16</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td>
                <p>16</p>
            </td>
            <td> </td>
            <td>
                <p>26</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>17</p>
            </td>
            <td>
                <p>My son, you have been to me like a dog that entered into the potter's oven to warm itself, and when
                    it was warm, it got up to bark at them.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܟܠܒܐ ܕܥܠܼ ܠܐܬܿܘܿܢܐ ܕܩܘܼ̈ܩܝܐ ܕܢܫܚܢ ܘܡܼܢ ܒܬܪ ܕܫܚܢ ܩܡ ܠܡܒܚ ܒܗܘܿܢ</p>
            </td>
            <td>
                <p>2 Petr 2,22: ܓܿܕܼܰܫ ܠܗܘܽܢ ܕܷܝܢ ܗܳܠܶܝܢ ܕܡܰܬܼܠܴܐ ܫܰܪܺܝܪܳܐ܆ ܕܿܟܼܰܠܒܴܿܐ ܕܼܰܗܦܼܰܟܼ ܥܰܠ ܬܿܝܘܽܒܼܶܗ܆
                    ܘܰܚܙܺܝܪܬܴܿܐ ܕܼܰܣܚܳܬܼ ܒܿܥܘܽܪܓܴܿܠܴܐ ܕܼܰܣܝܳܢܳܐ܀</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>29</p>
            </td>
            <td>
                <p>29</p>
            </td>
            <td>
                <p>28</p>
            </td>
            <td>
                <p>17</p>
            </td>
            <td> </td>
            <td>
                <p>29</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>18</p>
            </td>
            <td>
                <p>My son, you have been to me like a swine that had gone to the bathhouse, and when it saw a trench of
                    mud, it went down and bathed in it, calling out to its companions: "Come and bathe!"</p>
            </td>
            <td>
                <p>ܗܘܝܬܼ ܠܝܼ ܒܪܝ ܐܝܟ ܚܙܝܼܪܐ ܕܐܙܠ ܗܘܼܐ ܠܒܢ̈ܐ ܘܟܕ ܚܼܙܐ ܓܘܼܡܬܼܐ ܕܣܝܢܐ ܢܚܬܼ ܣܚܼܐ ܒܗܿ ܘܩܼܪܐ ܠܚܒܼܪ̈ܘܗܝ ܕܬܼܘ
                    ܣܚܼܘ</p>
            </td>
            <td> </td>
            <td>
                <p>18</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>31</p>
            </td>
            <td>
                <p>30</p>
            </td>
            <td>
                <p>18</p>
            </td>
            <td> </td>
            <td>
                <p>31</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>19</p>
            </td>
            <td>
                <p>My son, my finger was on your mouth, and your finger was on my eyes. Why did I raise you up, fox,
                    that your eyes should see apples?</p>
            </td>
            <td>
                <p>ܕܝܼܠܝ ܒܪܝ ܨܒܼܥܝ ܠܦܘܼܡܟ ܘܕܝܼܠܟ ܨܒܼܥܟ ܠܥܝ̈ܢܝ ܥܠ ܡܿܢ ܐܪܒܿܝܟ ܬܥܠܐ ܕܥܝܢ̈ܝܟ ܥܠ ܚܙܘܼܪ̈ܐ ܚܝܪܢ</p>
            </td>
            <td>
                <p>Prov 30,17: ܥܝܢܐ ܕܓܚܟܐ ܥܠ ܐܒܘܗܿ ܘܫܝܛܐ ܣܝܒܘܬܐ ܕܐܡܗܿ܂ ܢܚܨܘܢܗܿ ܥܘܪ̈ܒܐ ܕܢܚܠܐ܂ ܘܢܐܟܠܘܢܗܿ ܒܢ̈ܝ ܢܫܪܐ܀</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>32</p>
            </td>
            <td>
                <p>32</p>
            </td>
            <td>
                <p>33</p>
            </td>
            <td>
                <p>19</p>
            </td>
            <td> </td>
            <td>
                <p>32</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>20</p>
            </td>
            <td>
                <p>My son, the dog that eats his prey shall be the prey of the wolves, and the hand that is not
                    industrious shall be cut off from its shoulder, and the eye with which I cannot see shall be plucked
                    out by the raven.</p>
            </td>
            <td>
                <p>ܒܪܝ ܟܠܒܐ ܕܡܼܢ ܨܝܕܗ ܐܟܿܠ ܡܢܬܼܐ ܕܕܐܒ̈ܐ ܢܗܘܐ ܘܐܝܕܼܐ ܕܠܐ ܟܫܪܐ ܡܼܢ ܫܚܬܼܗܿ ܬܬܼܦܣܩ ܘܥܝܢܐ ܕܠܐ ܚܙܢܐ ܒܗܿ
                    ܥܘܼܪܒܼܐ ܢܚܨܝܼܗܿ</p>
            </td>
            <td> </td>
            <td>
                <p>20</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>34</p>
            </td>
            <td>
                <p>20</p>
            </td>
            <td> </td>
            <td>
                <p>34</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>21</p>
            </td>
            <td>
                <p>What good have you done me, my son, that I have remembered you and my soul has found comfort in you?
                </p>
            </td>
            <td>
                <p>ܡܢܐ ܛܒܼ ܥܒܕܬܿ ܠܝܼ ܒܪܝ ܕܐܬܿܕܟܪܬܟ ܘܐܬܿܬܢܚܬ ܒܟ ܢܦܫܝ</p>
            </td>
            <td> </td>
            <td>
                <p>21</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>X1</p>
            </td>
            <td>
                <p>X1</p>
            </td>
            <td>
                <p>35</p>
            </td>
            <td>
                <p>21</p>
            </td>
            <td> </td>
            <td>
                <p>X1</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>22</p>
            </td>
            <td>
                <p>My son, if the gods steal, by what shall they swear? And a lion that steals a piece of land, how will
                    it settle down and feed on it?</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܐܠ[ܗ̈]ܐ ܓܢܒܿܝܼܢ ܒܡܿܢ ܡܘܡܝܢ ܠܗܘܿܢ ܘܐܪܝܐ ܕܓܼܢܒܼ ܐܪܥܐ ܐܝܟܢܐ ܝܬܿܒܼ ܐܟܿܠ ܠܗ</p>
            </td>
            <td> </td>
            <td>
                <p>22</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>X1</p>
            </td>
            <td>
                <p>22</p>
            </td>
            <td> </td>
            <td>
                <p>36</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>23</p>
            </td>
            <td>
                <p>My son, I showed you the face of the king, and brought you to great honour, but you have chosen to do
                    me evil.</p>
            </td>
            <td>
                <p> ܐܢܐ ܒܪܝ ܐܦܝ̈ ܡܠܟܐ ܚܘܝܼܬܟ ܘܠܐܝܼܩܪܐ ܪܒܐ ܡܛܝܼܬܟ ܘܐܢܬܿ ܨܒܝܬܿ ܠܡܒܼܐܫܘܼ ܠܝܼ</p>
            </td>
            <td> </td>
            <td>
                <p>23</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>36</p>
            </td>
            <td>
                <p>23</p>
            </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>24</p>
            </td>
            <td>
                <p>My son, you were to me like a tree that said to its woodcutters: "If there had not been some of me in
                    your hands, you would not have fallen on me.</p>
            </td>
            <td>
                <p> ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܐܝܼܠܢܐ ܕܐܡܪ ܠܦܣܘܿܩܘ̈ܗܝ ܕܐܠܘܼܠܐ ܡܢܝ ܒܐܝܼܕܝ̈ܟܿܘܿܢ ܥܠܝ ܠܐ ܢܦܠܼܬܿܘܿܢ</p>
            </td>
            <td> </td>
            <td>
                <p>24</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>38</p>
            </td>
            <td> </td>
            <td>
                <p>38</p>
            </td>
            <td>
                <p>26</p>
            </td>
            <td> </td>
            <td>
                <p>38</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>25</p>
            </td>
            <td>
                <p>My son, you have been to me like the young swallows that fell out of their nest, and a cat caught
                    them and said to them: "Had it not been for me, great evil would have befallen you". They answered
                    and said to him: "Is this why you have put us in your mouth?"</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܒ̈ܢܝ ܣܢܘܿܢܝܼܬܼܐ ܕܢܦܠܼܘ ܡܼܢ ܩܢܗܘܿܢ ܘܩܒܿܠܬ ܐܢܘܿܢ ܟܟܼܘܼܫܬܐ ܘܐܡܪܬ ܠܗܘܿܢ ܕܐܠܘܼܠܐ ܐܢܐ
                    ܒܝܼܫܬܐ ܪܒܿܬܼܐ ܗܘܝܐ ܗܘܼܬܼ ܠܟܼܘܿܢ ܥܢܼܘ ܘܐܡܪܝܼܢ ܠܗܿ ܥܠ ܗܕܐ ܣܡܬܿܝܼܢܢ ܒܦܘܼܡܟܼܝ</p>
            </td>
            <td> </td>
            <td>
                <p>25</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
            <td>
                <p>27</p>
            </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>26</p>
            </td>
            <td>
                <p>My son, you have been to me like the cat to which they say: "Give up thine thieving, and thou shalt
                    go out and come in from the palace as thy soul pleaseth". And she answered and said to them: "Even
                    if I had eyes of silver and ears of gold, I would not stop stealing."</p>
            </td>
            <td>
                <p> ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܗܝ ܟܟܼܘܿܫܬܐ ܕܐܡܪܝܼܢ ܠܗܿ ܫܒܼܘܿܩܝ ܓܘܼܢܒܟܼܝ ܘܬܗܘܐ ܥܐܠܐ ܐܢܬܿܝ ܘܢܦܿܩܐ ܠܒܝܬܼ ܡܠܟܐ ܐܝܟ
                    ܕܨܒܼܝܐ ܢܦܫܟܼܝ ܘܥܢܬ ܘܐܡܪܬ ܠܗܘܿܢ ܐܠܐ ܢܗܘܘܿܢ ܠܝܼ ܥܝܢ̈ܐ ܕܣܐܡܐ ܘܐܕ̈ܢܐ ܕܣܐܡܐ ܐܢܐ ܓܘܼܢܒܿܝ ܠܐ ܫܒܼܩܐ ܐܢܐ</p>
            </td>
            <td> </td>
            <td>
                <p>26</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>28</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>27</p>
            </td>
            <td>
                <p>My son, you were to me like a snake that was tied to a bush and thrown into the river. And the wolf
                    saw them and said to them: "Evil rides on evil, and worse than either carries them away". The snake
                    said to him: "If you had come here, you would have given account of the goats and their kids."</p>
            </td>
            <td>
                <p> ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܚܘܝܐ ܕܪܟܼܝܼܒܼ ܗܘܼܐ ܥܠ ܣܢܝܐ ܘܫܕܐ ܒܢܗܪܐ ܘܚܙܼܐ ܐܢܘܿܢ ܕܐܒܼܐ ܘܐܡܪ ܠܗܘܿܢ ܕܒܼܝܼܫ ܥܠ
                    ܕܒܼܝܼܫ ܪܟܼܝܼܒܼ ܘܕܒܼܝܼܫ ܡܼܢ ܬܪܝܗܘܿܢ ܡܕܒܪ ܠܗܘܿܢ ܐܡܪ ܠܗ ܚܘܝܐ ܐܠܘܼ ܐܬܝܬܿ ܠܗܪܟܿܐ ܥܒܿܕ ܗܘܼܝܬܿ ܚܘܼܫܒܢܐ
                    ܕܥܙ̈ܐ ܘܕܒܼܢܝܗ̈ܝܢ</p>
            </td>
            <td> </td>
            <td>
                <p>27</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>29</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>28</p>
            </td>
            <td>
                <p>My son, I have seen a goat taken to the slaughterhouse, and because its time had not yet come, it
                    returned to its place and saw its children and its grandchildren. My son, I have seen foals that
                    have become murderers of their mothers.</p>
            </td>
            <td>
                <p>ܐܢܐ ܒܪܝ ܚܙܝܬܼ ܥܙܐ ܕܐܥܠܘܼܗܿ ܠܒܝܬܼ ܛܒܚ̈ܐ ܘܥܠ ܕܙܒܼܢܗܿ ܠܐ ܡܛܝܼ ܗܘܼܐ ܗܦܟܬ ܠܕܘܼܟܿܬܼܗܿ ܘܚܙܬ ܒܢ̈ܝܗܿ ܘܒ̈ܢܝ
                    ܒܢ̈ܝܗܿ ܒܪܝ ܐܢܐ ܚܙܿܝܬܼ ܥܝܼ̈ܠܐ ܕܗܘܘ ܩܛܘܿ̈ܠܐ ܠܐܡܗܬܼ̈ܗܘܿܢ</p>
            </td>
            <td> </td>
            <td>
                <p>28</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>30</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>29</p>
            </td>
            <td>
                <p>My son, I fed you with all that was good, but you, my son, fed me with bread of the soil, and you
                    were not satisfied.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢܐ ܟܠܡܕܡ ܕܒܣܝܼܡ ܐܘܟܿܠܬܟ ܘܐܢܬܿ ܒܪܝ ܠܚܡܐ ܒܥܦܪܐ ܐܘܟܿܠܬܢܝ ܘܠܐ ܣܒܼܥܬܼ</p>
            </td>
            <td> </td>
            <td>
                <p>29</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>33</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>30</p>
            </td>
            <td>
                <p>My son, I anointed you with sweet ointments, but you, my son, defiled my body with dust.</p>
            </td>
            <td>
                <p>ܐܢܐ ܒܪܝ ܡܫ̈ܚܢܐ ܡܒܣܡ̈ܐ ܡܫܚܬܟ ܘܐܢܬܿ ܒܪܝ ܓܘܼܫܡܝ ܒܥܦܪܐ ܚܒܿܠܬܝܗܝ</p>
            </td>
            <td> </td>
            <td>
                <p>30</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>34</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>31</p>
            </td>
            <td>
                <p>My son, I made your stature grow like a cedar, but you made me bend in my life, and you made me drunk
                    with your evil deeds.</p>
            </td>
            <td>
                <p>ܐܢܐ ܒܪܝ ܪܒܿܝܼܬ ܩܘܡܬܟ ܐܝܟ ܐܪܙܐ ܟܦܦܬܢܝ ܒܚ̈ܝ ܘܐܪܘܝܼܬܢܝ ܐܢܬܿ ܒܒܼܝܼ̈ܫܬܟ</p>
            </td>
            <td> </td>
            <td>
                <p>31</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>35</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>32</p>
            </td>
            <td>
                <p>My son, I raised you like a tower and said: "If my enemy should come upon me, I will go up and dwell
                    in thee," but thou, when thou sawest my enemy, didst yield to him.</p>
            </td>
            <td>
                <p> ܒܪܝ ܐܢܐ ܐܘܪܒܼܬܟ ܐܝܟ ܡܓܼܕܠܐ ܘܐܡܪ ܗܘܿܝܬܼ ܕܐܢ ܐܬܿܐ ܥܠܝ ܒܥܠܕܒܼܒܼܝ ܐܣܩ ܘܐܥܡܪ ܒܟ ܘܐܢܬܿ ܟܕ ܚܼܙܝܬܿ ܒܥܠܕܒܼܒܼܝ
                    ܐܪܟܿܢܬܿ ܩܕܼܡܘܗܝ</p>
            </td>
            <td> </td>
            <td>
                <p>32</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>X1</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>33</p>
            </td>
            <td>
                <p>My son, you were to me like a mole that came up out of the ground to get some warmth from the sun,
                    for it had no eyes.  An eagle saw him, smote him, and carried him off.</p>
            </td>
            <td>
                <p>ܗܘܝܬ ܠܝܼ ܒܪܝ ܐܝܟ ܚܘܼܠܕܼܐ ܕܣܠܿܩ ܡܼܢ ܓܘ ܐܪܥܐ ܕܢܩܒܿܠ ܠܫܡܫܐ ܥܠ ܕܠܝܬܿ ܠܗ ܥܝܢ̈ܐ ܘܚܙܼܝܗܝ ܢܫܪܐ ܘܡܚܼܝܗܝ ܘܚܛܦܗ
                </p>
            </td>
            <td> </td>
            <td>
                <p>33</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>36</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>34</p>
            </td>
            <td>
                <p>My son Nadan answered and said to me: "O my father Ahiqar! such a thing is far from you! Do unto me
                    according to thy mercy: for God forgives the iniquity of men; and thou, too, forgive me this folly:
                    and I will serve thy horses, and feed the swine that are in thy house: and I shall be called evil:
                    but thou devise no evil against me".</p>
            </td>
            <td>
                <p>ܥܢܼܐ ܢܕܢ ܒܪܝ ܘܐܡܪ ܠܝܼ ܚܣ ܠܟ ܡܼܢ ܗܠܝܢ ܐܒܼܝ ܐܚܝܼܩܪ ܐܝܟ ܪ̈ܚܡܝܟ ܥܒܕ ܥܡܝ ܐܦ ܐܠܗܐ ܓܝܪ ܚܛܿܝܢ ܐܢܫܐ ܘܫܒܿܩ
                    ܠܗܘܿܢ ܘܐܦ ܐܢܬܿ ܫܒܼܘܿܩ ܠܝܼ ܗܕܐ ܣܟܼܠܘܼܬܼܐ ܘܐܗܘܐ ܡܫܡܫ ܪ̈ܟܼܫܟ ܘܪܥܿܐ ܚܙܝܼܪ̈ܐ ܕܒܼܒܝܬܟ ܘܐܢܐ ܐܬܼܩܪܐ ܒܝܼܫܐ
                    ܘܐܢܬܿ ܠܐ ܬܚܫܘܿܒܼ ܥܠܝ ܒܝܼܫܬܐ</p>
            </td>
            <td> </td>
            <td>
                <p>34</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>38</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>35</p>
            </td>
            <td>
                <p>I answered and said to him: "O my son! thou hast been to me like a palm-tree which stood by a river,
                    and cast all its fruit into the river. When its owner came to cut it down, it said to him: 'Leave me
                    alone this year, and I will give you carobs.' His master said to him: 'Thou hast not been diligent
                    in that which is thy own: how canst thou be diligent in that which is not thy own?'"</p>
            </td>
            <td>
                <p>ܥܢܝܿܬܼ ܐܢܐ ܘܐܡܿܪܬܼ ܠܗ ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܗܘܿ ܕܘܠܐ ܕܩܐܿܡ ܗܘܼܐ ܥܠ ܢܗܪܐ ܘܟܼܠܗ ܐܒܗ ܒܢܗܪܐ ܫܕܐ ܗܘܐ ܘܟܕ ܐܬܼܐ
                    ܡܪܗ ܕܢܦܣܩܝܼܘܗܝ ܐܡܪ ܠܗ ܫܒܼܘܿܩܝܢܝ ܗܕܐ ܫܢܬܐ ܘܥܒܿܕܢܐ ܠܟ ܚܪ̈ܘܼܒܐ ܘܐܡܪ ܠܗ ܡܪܗ ܒܕܝܼܠܟ ܠܐ ܐܟܼܫܪܬܿ ܒܕܠܐ ܕܝܼܠܟ
                    ܐܝܟܢܐ ܬܟܼܫܪ</p>
            </td>
            <td> </td>
            <td>
                <p>35</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>36</p>
            </td>
            <td>
                <p>My son, they say to the wolf: "Why do you follow the sheep?" He said to them: "Their dust is very
                    good for my eyes". Again they took him to the schoolhouse, and his master said to him: "A, B," said
                    the wolf: "Kid, lamb".</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܕܐܒܼܐ ܐܡܪܝܼܢ ܠܗ ܡܛܠ ܡܢܐ ܒܬܪ ܥܢ̈ܐ ܡܗܠܟ ܐܢܬܿ ܐܡܪ ܠܗܘܿܢ ܣܓܿܝܼ ܡܘܬܪ ܚܝܠܗܿ ܠܥܝܢ̈ܝ ܘܬܼܘܼܒܼ ܐܥܠܘܼܗܝ
                    ܠܒܝܬܼ ܣܦܪܐ ܐܡܪ ܠ ܪܒܿܗ ܐܠܦ ܒܝܬܼ ܐܡܪ ܕܐܒܐ ܓܕܝܐ ܦܐܪܐ</p>
            </td>
            <td> </td>
            <td>
                <p>36</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>37</p>
            </td>
            <td>
                <p>My son, I have taught you that there is a God, but you rise up against good servants and beat those
                    who have not fumbled. And just as God has kept me alive because of my righteousness, so He will
                    destroy you because of your deeds.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢܐ ܐܠܦܬܟ ܕܐܝܼܬܼ ܐܠܗܐ ܘܐܢܬܿ ܥܠ ܥܒܼ̈ܕܐ ܛܒ̈ܐ ܩܐܡ ܐܢܬܿ ܘܡܢܓܿܕ ܐܢܬܿ ܠܗܘܿܢ ܕܠܐ ܣܟܼܠܘܼ ܘܐܝܟܢܐ ܕܠܝܼ ܐܚܝܼ
                    ܐܠܗܐ ܡܛܠ ܟܐܢܘܼܬܼܝ ܠܟ ܢܘܒܿܕ ܡܛܠ ܥܒ̈ܕܝܟ</p>
            </td>
            <td> </td>
            <td>
                <p>37</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>38</p>
            </td>
            <td>
                <p>My son, they put the head of the donkey over a dish at the table, and it rolled off and fell into the
                    dust. They say: "It was angry with itself because it was not honoured."</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܪܫܗ ܕܚܡܪܐ ܣܡܘܼܗܝ ܒܦܝܼܢܟܼܐ ܥܠ ܦܬܼܘܿܪܐ ܘܗܘ ܐܬܼܥܪܓܠ ܘܢܼܦܠ ܒܥܦܪܐ ܘܐܡܪܝܼܢ ܥܠ ܢܦܫܗ ܪܓܙ ܕܠܐ ܩܒܿܠ ܐܝܼܩܪܐ
                </p>
            </td>
            <td> </td>
            <td>
                <p>38</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>39</p>
            </td>
            <td>
                <p>My son, you have proved the saying which says: "Call him whom you have begotten as your son, and him
                    whom you have bought as your slave". My son, the saying is true which says "Take thy sister's son
                    under thy armpit and smite him against a stone". But God, who has kept me alive, will judge between
                    us.</p>
            </td>
            <td>
                <p>ܐܢܬܿ ܒܪܝ ܫܪܪܬܝܗܝ ܠܡܬܼܠܐ ܕܐܡܝܼܪ ܕܝܼܠܕܬܿ ܩܪܝܼ ܒܪܟ ܘܕܙܒܢܬܿ ܩܪܝܼ ܥܒܼܕܟ ܒܪܝ ܫܪܝܼܪܘܼ ܡܬܼܠܐ ܕܐܡܝܼܪ ܕܒܪ ܚܬܼܟ
                    ܐܚܘܿܕ ܬܚܝܬܼ ܫܚܬܼܟ ܘܠܟܐܦܐ ܛܪܘܿܦ ܐܠܐ ܐܠܗܐ ܗܘܿ ܕܐܚܝܢܝ ܢܕܘܼܢ ܒܝܢܬܢ</p>
            </td>
            <td> </td>
            <td>
                <p>39</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>X1</p>
            </td>
            <td>
                <p>My son, the old age of an eagle is better than the youth of a vulture</p>
            </td>
            <td>
                <p>ܒܸܪܝ ܛܵܒܼܐ ܣܲܝܒܿܘܲܬܹܗ ܕܢܹܫܵܪܐ ܡܼܢ ܥܠܲܝܡܘܼܬܼܗ ܕܟܼܘܼܕܪܵܐ܀</p>
            </td>
            <td> </td>
            <td>
                <p>X1</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>X2</p>
            </td>
            <td>
                <p>My son, you have been to me like a roe that is entangled in a noose, unable to save itself from
                    death. However, it lifts up its sweet and beautiful voice and makes them sink to murder.</p>
            </td>
            <td>
                <p>ܗܘܸܝܬܿ ܠܝ ܐܝܟ ܚܓܠܼܐ ܕܠܐ  ܡܦܿܨܐ ܢܦܫܗ ܡܼܢ ܡܘܬܐ܂ ܘܠܚܒܼܪ̈ܘܗܝ ܠܘܬܼܗ ܡܟܲܢܫ ܘܡܛܿܒܥ ܠܗܘܢ ܒܩܛܠܐ܂</p>
            </td>
            <td>
                <p>Prov 6,5: ܕܬܬܦܨܐ ܐܝܟ ܛܒܝܐ ܡܢ ܢܫܒܐ ܘܐܝܟ ܨܦܪܐ ܡܢ ܦܚܐ܂</p>
            </td>
            <td>
                <p>X2</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>X3</p>
            </td>
            <td>
                <p>My son, you have been to me like someone who rebukes the bulls and let them alive.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝ ܒܪܝ ܐܝܟ ܐ̄ܢܫ ܕܒܣܪ ܥܠ ܬܘ[ܪ̈]ܐ ܫܒܼܩ ܐܢܘܢ ܠܚܝܘ̈ܗܝ܂</p>
            </td>
            <td> </td>
            <td>
                <p>X3</p>
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
    </table>

</div>
</div>
</div>

## Tab
## Tab. 2: The distribution of the proverbs of Ahiqar in the Syriac manuscripts


<ModalTrigger modal-element="#table2" trigger-element="#table2-trigger" />
<div class="modal fade" id="table2" tabindex="-1" aria-hidden="true" role="dialog">
<div class="modal-dialog card">
<div class="modal-header">
<h2>Proverbs of Ahiqar – presence in the textual witnesses</h2>
<CloseButton/>
</div>
<div class="modal-content">

<h3>Sigla</h3>

<table>
    <tr>
        <td>
            <ul>
                <li><strong>A:</strong> <a href="/manuscripts.html#aleppo-scaa-7-229" target="_blank">Aleppo SCAA 7/229</a></li>
                <li><strong>B:</strong> <a href="/manuscripts.html#paris-bibliotheque-nationale-de-france-ms-syr-434" target="_blank">BnF syr. 434</a></li>
                <li><strong>C:</strong> <a href="/manuscripts.html#cambridge-univ-cant-add-2020" target="_blank">Cambridge Add. 2020</a></li>
                <li><strong>D:</strong> <a href="/manuscripts.html#mosul-dfm-430" target="_blank">Mosul DFM 430</a></li>
                <li><strong>G:</strong> <a href="/manuscripts.html#ms-graffin" target="_blank">Ms. Graffin</a></li>
                <li><strong>H:</strong> <a href="/manuscripts.html#cambridge-ma-harvard-university-houghton-library-syr-80" target="_blank">Harvard syr. 80</a></li>
                <li><strong>I:</strong> <a href="/manuscripts.html#alqosh-notre-dame-des-semences-mss-syr-612-codex-207" target="_blank">Notre-Dame des Semences, mss. 612</a></li>
            </ul>
        </td>
        <td>
            <ul>
                <li><strong>K:</strong> <a target="_blank" href="/manuscripts.html#alqosh-notre-dame-des-semences-mss-syr-611-codex-205">Notre-Dame des Semences, mss. 611</a></li>
                <li><strong>M:</strong> <a target="_blank" href="/manuscripts.html#birmingham-mingana-syriac-433">Mingana syr. 433</a></li>
                <li><strong>N:</strong> <a target="_blank" href="/manuscripts.html#paris-bibliotheque-nationale-de-france-ms-syr-422-ms-pognon">BnF syr. 422</a></li>
                <li><strong>O:</strong> <a target="_blank" href="/manuscripts.html#london-brit-libr-or-2313">Oxford BL Or. 2313</a></li>
                <li><strong>P:</strong> <a target="_blank" href="/manuscripts.html#st-petersburg-sado-no-9">St. Petersburg, Sado 9</a></li>
                <li><strong>T:</strong> <a target="_blank" href="/manuscripts.html#bibliotheque-nationale-de-france-et-universitaire-de-strasbourg-ms-4122">Strasbourg S4122</a></li>
                <li><strong>U:</strong> <a target="_blank" href="/manuscripts.html#staatsbibliothek-zu-berlin-sachau-336">Sachau 336</a></li>
            </ul>
        </td>
    </tr>
</table>

<strong>Please note:</strong>

<ul>
    <li>Nos. 1–39: transribed according to Cambridge Add. 2020 (C)</li>
    <li>No. X1: transcribed according to Sachau 336 (U)</li>
    <li>No. X2: transcribed according to Harvard syr. 80 (H)</li>
    <li>No. X3: transcribed according to Harvard syr. 80 (H)</li>
</ul>
    <table>
        <tr class="ro1">
            <th>
                <p>Nos.</p>
            </th>
            <th>
                <p>English Translation</p>
            </th>
            <th>
                <p>Syriac</p>
            </th>
            <th>
                <p>Appearances</p>
            </th>
            <th>
                <p>Textual witnesses</p>
            </th>
        </tr>
        <tr class="ro1">
            <td>
                <p>1</p>
            </td>
            <td>
                <p>My son, he who does not hear with his ears, they make him hear with the nape of his neck.</p>
            </td>
            <td>
                <p>ܒܪܝ ܡܿܢ ܕܠܐ ܫܡܥ ܡܼܢ ܐܕܢܘܗ̈ܝ ܡܼܢ ܒܣܬܪ ܩܕܠܗ ܡܫܡܥܝܼܢ ܠܗ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum B">B</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>2</p>
            </td>
            <td>
                <p>My son Nadan answered and said to me: “Why are you [so] angry against your son?” I answered and said
                    to him: “My son, I have set you on the throne of honour, but you have cast me down from my throne.
                    But my righteousness has saved me.</p>
            </td>
            <td>
                <p>ܥܢܼܐ ܢܕܢ ܒܪܝ ܘܐܡܪ ܠܝܼ ܠܡܢܐ ܐܬܼܚܡܬܼܬܿ ܥܠ ܒܪܟ ܥܢܿܝܬܼ ܘܐܡܪܬܼ ܠܗ ܐܢܐ ܒܪܝ ܥܠ ܟܘܼܪܣܝܐ ܕܐܝܼܩܪܐ ܐܘܬܿܒܼܬܟ
                    ܘܐܢܬܿ ܡܼܢ ܟܘܼܪܣܝ ܣܚܦܬܢܝ ܘܠܝܼ ܟܐܢܘܼܬܼܝ ܫܘܙܒܼܬܼܢܝ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>3</p>
            </td>
            <td>
                <p>My son, you have been to me like a scorpion striking a rock. And it [the rock] says to it: “You have
                    struck at an unconcerned heart.” And it strikes at a needle, and they say to it: “You have struck at
                    a sting worse than yours.”</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܥܩܪܒܼܐ ܕܡܚܝܐ ܠܫܘܿܥܐ ܘܐܡܪ ܠܗܿ ܥܠ ܠܒܐ ܫܠܝܐ ܡܚܝܬܿܝ ܘܡܚܬ ܠܡܚܛܐ ܘܐܡܪܝܼܢ ܠܗ ܡܚܝܬܿܝ
                    ܠܥܘܼܩܣܐ ܕܒܼܝܼܫ ܡܼܢ ܕܝܼܠܟܼܝ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>4</p>
            </td>
            <td>
                <p>My son, you have been to me like a goat standing over a red berry and eating it. And the red berry
                    said to him: "Why do you eat me, when you treat your skin with my root?" The goat said to it: "I eat
                    you during my life, and after my death they will pull you up by your roots".</p>
            </td>
            <td>
                <p>ܗܘܝܬܼ ܠܝܼ ܒܪܝ ܐܝܟ ܥܙܐ ܕܩܝܿܡܐ ܗܘܼܬܼ ܥܠ ܐܘܼܓܐ ܘܐܟܼܠܐ ܠܗ ܘܐܡܪ ܠܗܿ ܐܘܼܓܐ ܡܛܠ ܡܢܐ ܐܟܼܠܐ ܐܢܬܼܝ ܠܝܼ ܕܡܫܟܟܼܝ
                    ܒܥܩܪܝ ܦܠܚܿܝܼܢ ܠܗ ܐܡܪܐ ܠܗ ܥܙܐ ܐܟܠܬܟ ܒܚ̈ܝܝ ܘܒܼܡܘܬܿܝ ܡܼܢ ܥܩܪܟ ܢܥܩܪܘܼܢܟ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>5</p>
            </td>
            <td>
                <p>My son, you have been to me like the onw who threw a stone at heaven, and it did not reach heaven,
                    but he received punishment from God.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܗܘܿ ܕܫܕܼܐ ܟܐܦܐ ܠܫܡܝܐ ܘܠܫܡܝܐ ܠܐ ܡܛܬ ܘܚܛܗܐ ܡܼܢ ܐܠܗܐ ܩܒܿܠ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>6</p>
            </td>
            <td>
                <p>My son, you have been to me like the one who saw his companion shivering from cold and took a pitcher
                    of water and poured it over him.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܗܘܿ ܕܚܙܐ ܠܚܒܼܪܗ ܕܪܥܿܠ ܡܼܢ ܩܘܼܪܫܗ ܘܫܩܿܠ ܓܪܒܼܐ ܕܡ̈ܝܐ ܘܕܡܐ ܥܠܘܗܝ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>7</p>
            </td>
            <td>
                <p><span> Oh, my son, if you had killed me, you would have been able to stand in my place; but you
                        should know, my son, that even if the tail of the pig were to grow to seven ells, it would not
                        take the place of the horse, and even if its bristles were soft and woven, it would not ascend
                        to the body of a free man.</span></p>
            </td>
            <td>
                <p>ܠܘܝ ܕܝܢ ܒܪܝ ܡܐ ܕܩܛܠܬܢܝ ܡܫܟܚ ܗܘܼܝܬܿ ܕܬܼܩܘܼܡ ܒܕܘܼܟܬܼܝ ܗܘܝܬܿ ܕܝܢ ܝܕܥ ܐܢܬܿ ܒܪܝ ܕܐܠܘܼ ܢܼܪܒܿܐ ܕܘܼܢܒܐ
                    ܕܚܙܝܼܪܐ ܫܒܥ ܐܡܝܼ̈ܢ ܒܕܘܼܟܬܼ ܣܘܼܣܝܐ ܠܐ ܩܐܿܡ ܘܐܢ ܢܗܘܐ ܪܟܿܝܼܟ ܣܥܪܗ ܘܩܘܙ ܡܡܬܼܘܿܡ ܥܠ ܓܘܼܫܡܐ ܕܒܪ ܚܐܪ̈ܐ ܠܐ
                    ܣܠܿܩ</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>8</p>
            </td>
            <td>
                <p>My son, I intended that you should be in my place, that you should acquire my house and my wealth,
                    and that you should inherit them. But God was not pleased, and He did not hear your voice.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢܐ ܐܡܪܬܼ ܕܬܗܘܐ ܚܠܦܝ ܘܒܝܬܿܝ ܘܢܟܼ̈ܣܝ ܐܢܬܿ ܬܩܢܐ ܐܢܘܿܢ ܘܬܐܪܬܼ ܐܢܘܿܢ ܘܠܐܠܗܐ ܠܐ ܫܼܦܪ ܘܠܐ ܫܼܡܥ ܒܩܠܟ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>9</p>
            </td>
            <td>
                <p>My son, you have been to me like a lion that came upon a donkey in the morning of the day, and said
                    to him "Welcome, my Lord Cyrus". But the donkey said to him: "May the same welcome that you give me
                    be given to him, who tied me up last night, but did not tie my loins, lest I should see your face".
                </p>
            </td>
            <td>
                <p>ܗܘܝܬܼ ܠܝܼ ܒܪܝ ܐܝܟ ܐܪܝܐ ܕܦܓܥ ܒܚܡܪܐ ܡܼܢ ܨܦܪܗ ܕܝܘܡܐ ܘܐܡܪ ܠܗ ܬܐ ܒܫܠܡ ܡܪܝ ܩܘܼܪܝܼܣ ܗܘ ܕܝܢ ܐܡܪ ܠܗ ܐܝܟ ܫܠܡܐ
                    ܕܝܗܿܒܼ ܐܢܬܿ ܠܝܼ ܢܗܘܐ ܠܗܘܿ ܕܐܣܪܢܝ ܒܪܡܫܐ ܘܠܐ ܚܝܨܗ ܠܐܣܘܼܪܝ ܕܐܦܝ̈ܟ ܠܐ ܚܙܿܝܬܼ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>10</p>
            </td>
            <td>
                <p>My son, a snare was set on a dunghill, and a sparrow came and saw it and said to it: "What are you
                    doing here?" and the snare said to it: "I am praying to God". The sparrow said: "And this in your
                    mouth - what is it?" The snare said: "Bread for strangers". [Then] the sparrow approached to take
                    it, and [the snare] caught it by the neck. And while the sparrow was flapping, it said: "If this is
                    bread for strangers, may the God to whom you pray never answer your voice".</p>
            </td>
            <td>
                <p>ܒܪܝ ܦܚܐ ܨܠܐ ܗܘܼܐ ܒܩܩܠܬܐ ܘܐܬܼܐ ܨܦܪܐ ܚܙܼܝܗܝ ܘܐܡܪ ܠܿܗ ܡܢܐ ܥܒܿܕ ܐܢܬܿ ܗܪܟܐ ܐܡܪ ܠܗ ܦܚܐ ܠܐܠܗ ܡܨܠܐ ܐܢܐ ܐܡܪ ܠܗ
                    ܨܦܪܐ ܘܗܘܿ ܕܐܝܼܬܼ ܒܦܘܼܡܟ ܡܢܘ ܐܡܪ ܦܚܐ ܠܚܡ ܥܪ̈ܨܐ ܘܩܪܒܼ ܨܦܪܐ ܗܘܿ ܕܢܣܒܗ ܘܨܕܗ ܒܨܘܪܗ ܘܟܕ ܡܬܿܛܪܦ ܨܦܪܐ ܐܡܪ ܐܢ
                    ܗܢܿܘ ܠܚܡܐ ܕܥܪ̈ܨܐ ܐܠܗܐ ܕܡܨܠܐ ܐܢܬܿ ܠܗ ܠܐ ܢܫܡܥ ܠܩܠܟ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum B">B</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>11</p>
            </td>
            <td>
                <p>My son, I was like a bull tied to a lion, but the lion turned towards [it] and crushed it.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܒܪܝ ܐܝܟ ܬܘܪܐ ܕܐܣܝܼܪ ܥܡ ܐܪܝܐ ܘܐܬܼܦܢܝܼ ܐܪܝܐ ܘܬܒܼܪܗ</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>12</p>
            </td>
            <td>
                <p>My son, you have been to me like a fleeting weevil that destroys the granaries of kings, but has no
                    hold on anything.</p>
            </td>
            <td>
                <p>ܘܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܩܠܡܐ ܕܥܒܼܘܼܪܐ ܕܚܪܒܬ ܐܘܨܪ̈ܐ ܕܡܠ̈ܟܿܐ ܘܗܝ ܒܡܕܡ ܠܐ ܚܫܝܼܒܐ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>13</p>
            </td>
            <td>
                <p>My son, you have been to me like a pot on which they have made golden handles, but the soot has not
                    been scraped off the bottom.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܩܕܪܐ ܕܥܒܕܘ ܠܗܿ ܐܕ̈ܢܐ ܕܕܗܒܼܐ ܘܗܝ ܐܫܬܼܗܿ ܡܼܢ ܫܘܼܚܪܐ ܠܐ ܡܬܼܓܪܕܝܐ</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>14</p>
            </td>
            <td>
                <p>My son, you have been to me like a ploughman who sowed twenty ears of barley in a field. And when he
                    reaped it, it was twenty ears. And he said to it: "What I have scattered, I have gathered: but thou
                    art a disgrace with thy evil name, in that thou hast made a peck out of a peck, while I live".</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܐܟܪܐ ܕܙܪܥ ܐܫܟܪܐ ܕܥܣܪܝܼܢ ܡܕ̈ܘܬܐ ܕܣܥܪ̈ܐ ܘܟܕ ܚܨܕܼܗܿ ܥܒܼܕܬ ܠܗ ܥܣܪܝܼܢ ܡܕ̈ܘܬܼܐ ܘܐܡܪ ܠܗܿ
                    ܗܘܿ ܡܕܡ ܕܒܕܪܬܼ ܟܢܫܬܿ ܐܠܐ ܒܗܿܬܐ ܐܢܬܿܝ ܒܫܡܟܼܝ ܒܝܼܫܐ ܕܡܘܕܝܐ ܡܘܕܝܐ ܥܒܕܬܿܝ ܘܐܢܐ ܚܝܐ ܐܢܐ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>15</p>
            </td>
            <td>
                <p>My son, you have been to me like a decoy bird, which could not save himself from death, and by his
                    voice he seduced his companions getting into the trap.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܐܪܐ ܕܨܦܪܐ ܗܘ ܠܢܦܫܗ ܡܼܢ ܡܘܬܐ ܠܐ ܡܫܘܙܒ ܘܠܩܠܗ ܚܒܼܪ̈ܘܗܝ ܡܛܒܥ</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum A">A</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>16</p>
            </td>
            <td>
                <p>My son, you have been to me like a goat that leads its fellows to the slaughterhouse, but does not
                    save itself.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܬܝܫܐ ܕܡܥܠ ܚܒܪ̈ܘܗܝ ܠܒܝܬܼ ܛܒܚ̈ܐ ܘܗܘ ܠܢܦܫܗ ܠܐ ܡܫܘܙܒܼ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>17</p>
            </td>
            <td>
                <p>My son, you have been to me like a dog that entered into the potter's oven to warm itself, and when
                    it was warm, it got up to bark at them.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܟܠܒܐ ܕܥܠܼ ܠܐܬܿܘܿܢܐ ܕܩܘܼ̈ܩܝܐ ܕܢܫܚܢ ܘܡܼܢ ܒܬܪ ܕܫܚܢ ܩܡ ܠܡܒܚ ܒܗܘܿܢ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>18</p>
            </td>
            <td>
                <p>My son, you have been to me like a swine that had gone to the bathhouse, and when it saw a trench of
                    mud, it went down and bathed in it, calling out to its companions: "Come and bathe!"</p>
            </td>
            <td>
                <p>ܗܘܝܬܼ ܠܝܼ ܒܪܝ ܐܝܟ ܚܙܝܼܪܐ ܕܐܙܠ ܗܘܼܐ ܠܒܢ̈ܐ ܘܟܕ ܚܼܙܐ ܓܘܼܡܬܼܐ ܕܣܝܢܐ ܢܚܬܼ ܣܚܼܐ ܒܗܿ ܘܩܼܪܐ ܠܚܒܼܪ̈ܘܗܝ ܕܬܼܘ
                    ܣܚܼܘ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>19</p>
            </td>
            <td>
                <p>My son, my finger was on your mouth, and your finger was on my eyes. Why did I raise you up, fox,
                    that your eyes should see apples?</p>
            </td>
            <td>
                <p>ܕܝܼܠܝ ܒܪܝ ܨܒܼܥܝ ܠܦܘܼܡܟ ܘܕܝܼܠܟ ܨܒܼܥܟ ܠܥܝ̈ܢܝ ܥܠ ܡܿܢ ܐܪܒܿܝܟ ܬܥܠܐ ܕܥܝܢ̈ܝܟ ܥܠ ܚܙܘܼܪ̈ܐ ܚܝܪܢ</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>20</p>
            </td>
            <td>
                <p>My son, the dog that eats his prey shall be the prey of the wolves, and the hand that is not
                    industrious shall be cut off from its shoulder, and the eye with which I cannot see shall be plucked
                    out by the raven.</p>
            </td>
            <td>
                <p>ܒܪܝ ܟܠܒܐ ܕܡܼܢ ܨܝܕܗ ܐܟܿܠ ܡܢܬܼܐ ܕܕܐܒ̈ܐ ܢܗܘܐ ܘܐܝܕܼܐ ܕܠܐ ܟܫܪܐ ܡܼܢ ܫܚܬܼܗܿ ܬܬܼܦܣܩ ܘܥܝܢܐ ܕܠܐ ܚܙܢܐ ܒܗܿ
                    ܥܘܼܪܒܼܐ ܢܚܨܝܼܗܿ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum O">O</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>21</p>
            </td>
            <td>
                <p>What good have you done me, my son, that I have remembered you and my soul has found comfort in you?
                </p>
            </td>
            <td>
                <p>ܡܢܐ ܛܒܼ ܥܒܕܬܿ ܠܝܼ ܒܪܝ ܕܐܬܿܕܟܪܬܟ ܘܐܬܿܬܢܚܬ ܒܟ ܢܦܫܝ</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>22</p>
            </td>
            <td>
                <p>My son, if the gods steal, by what shall they swear? And a lion that steals a piece of land, how will
                    it settle down and feed on it?</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢ ܐܠ[ܗ̈]ܐ ܓܢܒܿܝܼܢ ܒܡܿܢ ܡܘܡܝܢ ܠܗܘܿܢ ܘܐܪܝܐ ܕܓܼܢܒܼ ܐܪܥܐ ܐܝܟܢܐ ܝܬܿܒܼ ܐܟܿܠ ܠܗ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>23</p>
            </td>
            <td>
                <p>My son, I showed you the face of the king, and brought you to great honour, but you have chosen to do
                    me evil.</p>
            </td>
            <td>
                <p> ܐܢܐ ܒܪܝ ܐܦܝ̈ ܡܠܟܐ ܚܘܝܼܬܟ ܘܠܐܝܼܩܪܐ ܪܒܐ ܡܛܝܼܬܟ ܘܐܢܬܿ ܨܒܝܬܿ ܠܡܒܼܐܫܘܼ ܠܝܼ</p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum U">U</abbr> (2x), <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>24</p>
            </td>
            <td>
                <p>My son, you were to me like a tree that said to its woodcutters: "If there had not been some of me in
                    your hands, you would not have fallen on me.</p>
            </td>
            <td>
                <p> ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܐܝܼܠܢܐ ܕܐܡܪ ܠܦܣܘܿܩܘ̈ܗܝ ܕܐܠܘܼܠܐ ܡܢܝ ܒܐܝܼܕܝ̈ܟܿܘܿܢ ܥܠܝ ܠܐ ܢܦܠܼܬܿܘܿܢ</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>25</p>
            </td>
            <td>
                <p>My son, you have been to me like the young swallows that fell out of their nest, and a cat caught
                    them and said to them: "Had it not been for me, great evil would have befallen you". They answered
                    and said to him: "Is this why you have put us in your mouth?"</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܒ̈ܢܝ ܣܢܘܿܢܝܼܬܼܐ ܕܢܦܠܼܘ ܡܼܢ ܩܢܗܘܿܢ ܘܩܒܿܠܬ ܐܢܘܿܢ ܟܟܼܘܼܫܬܐ ܘܐܡܪܬ ܠܗܘܿܢ ܕܐܠܘܼܠܐ ܐܢܐ
                    ܒܝܼܫܬܐ ܪܒܿܬܼܐ ܗܘܝܐ ܗܘܼܬܼ ܠܟܼܘܿܢ ܥܢܼܘ ܘܐܡܪܝܼܢ ܠܗܿ ܥܠ ܗܕܐ ܣܡܬܿܝܼܢܢ ܒܦܘܼܡܟܼܝ</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>26</p>
            </td>
            <td>
                <p>My son, you have been to me like the cat to which they say: "Give up thine thieving, and thou shalt
                    go out and come in from the palace as thy soul pleaseth". And she answered and said to them: "Even
                    if I had eyes of silver and ears of gold, I would not stop stealing."</p>
            </td>
            <td>
                <p> ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܗܝ ܟܟܼܘܿܫܬܐ ܕܐܡܪܝܼܢ ܠܗܿ ܫܒܼܘܿܩܝ ܓܘܼܢܒܟܼܝ ܘܬܗܘܐ ܥܐܠܐ ܐܢܬܿܝ ܘܢܦܿܩܐ ܠܒܝܬܼ ܡܠܟܐ ܐܝܟ
                    ܕܨܒܼܝܐ ܢܦܫܟܼܝ ܘܥܢܬ ܘܐܡܪܬ ܠܗܘܿܢ ܐܠܐ ܢܗܘܘܿܢ ܠܝܼ ܥܝܢ̈ܐ ܕܣܐܡܐ ܘܐܕ̈ܢܐ ܕܣܐܡܐ ܐܢܐ ܓܘܼܢܒܿܝ ܠܐ ܫܒܼܩܐ ܐܢܐ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum B">B</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>27</p>
            </td>
            <td>
                <p>My son, you were to me like a snake that was tied to a bush and thrown into the river. And the wolf
                    saw them and said to them: "Evil rides on evil, and worse than either carries them away". The snake
                    said to him: "If you had come here, you would have given account of the goats and their kids."</p>
            </td>
            <td>
                <p> ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܚܘܝܐ ܕܪܟܼܝܼܒܼ ܗܘܼܐ ܥܠ ܣܢܝܐ ܘܫܕܐ ܒܢܗܪܐ ܘܚܙܼܐ ܐܢܘܿܢ ܕܐܒܼܐ ܘܐܡܪ ܠܗܘܿܢ ܕܒܼܝܼܫ ܥܠ
                    ܕܒܼܝܼܫ ܪܟܼܝܼܒܼ ܘܕܒܼܝܼܫ ܡܼܢ ܬܪܝܗܘܿܢ ܡܕܒܪ ܠܗܘܿܢ ܐܡܪ ܠܗ ܚܘܝܐ ܐܠܘܼ ܐܬܝܬܿ ܠܗܪܟܿܐ ܥܒܿܕ ܗܘܼܝܬܿ ܚܘܼܫܒܢܐ
                    ܕܥܙ̈ܐ ܘܕܒܼܢܝܗ̈ܝܢ</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>28</p>
            </td>
            <td>
                <p>My son, I have seen a goat taken to the slaughterhouse, and because its time had not yet come, it
                    returned to its place and saw its children and its grandchildren. My son, I have seen foals that
                    have become murderers of their mothers.</p>
            </td>
            <td>
                <p>ܐܢܐ ܒܪܝ ܚܙܝܬܼ ܥܙܐ ܕܐܥܠܘܼܗܿ ܠܒܝܬܼ ܛܒܚ̈ܐ ܘܥܠ ܕܙܒܼܢܗܿ ܠܐ ܡܛܝܼ ܗܘܼܐ ܗܦܟܬ ܠܕܘܼܟܿܬܼܗܿ ܘܚܙܬ ܒܢ̈ܝܗܿ ܘܒ̈ܢܝ
                    ܒܢ̈ܝܗܿ ܒܪܝ ܐܢܐ ܚܙܿܝܬܼ ܥܝܼ̈ܠܐ ܕܗܘܘ ܩܛܘܿ̈ܠܐ ܠܐܡܗܬܼ̈ܗܘܿܢ</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>29</p>
            </td>
            <td>
                <p>My son, I fed you with all that was good, but you, my son, fed me with bread of the soil, and you
                    were not satisfied.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢܐ ܟܠܡܕܡ ܕܒܣܝܼܡ ܐܘܟܿܠܬܟ ܘܐܢܬܿ ܒܪܝ ܠܚܡܐ ܒܥܦܪܐ ܐܘܟܿܠܬܢܝ ܘܠܐ ܣܒܼܥܬܼ</p>
            </td>
            <td>
                <p>7</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>30</p>
            </td>
            <td>
                <p>My son, I anointed you with sweet ointments, but you, my son, defiled my body with dust.</p>
            </td>
            <td>
                <p>ܐܢܐ ܒܪܝ ܡܫ̈ܚܢܐ ܡܒܣܡ̈ܐ ܡܫܚܬܟ ܘܐܢܬܿ ܒܪܝ ܓܘܼܫܡܝ ܒܥܦܪܐ ܚܒܿܠܬܝܗܝ</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>31</p>
            </td>
            <td>
                <p>My son, I made your stature grow like a cedar, but you made me bend in my life, and you made me drunk
                    with your evil deeds.</p>
            </td>
            <td>
                <p>ܐܢܐ ܒܪܝ ܪܒܿܝܼܬ ܩܘܡܬܟ ܐܝܟ ܐܪܙܐ ܟܦܦܬܢܝ ܒܚ̈ܝ ܘܐܪܘܝܼܬܢܝ ܐܢܬܿ ܒܒܼܝܼ̈ܫܬܟ</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>32</p>
            </td>
            <td>
                <p>My son, I raised you like a tower and said: "If my enemy should come upon me, I will go up and dwell
                    in thee," but thou, when thou sawest my enemy, didst yield to him.</p>
            </td>
            <td>
                <p> ܒܪܝ ܐܢܐ ܐܘܪܒܼܬܟ ܐܝܟ ܡܓܼܕܠܐ ܘܐܡܪ ܗܘܿܝܬܼ ܕܐܢ ܐܬܿܐ ܥܠܝ ܒܥܠܕܒܼܒܼܝ ܐܣܩ ܘܐܥܡܪ ܒܟ ܘܐܢܬܿ ܟܕ ܚܼܙܝܬܿ ܒܥܠܕܒܼܒܼܝ
                    ܐܪܟܿܢܬܿ ܩܕܼܡܘܗܝ</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>33</p>
            </td>
            <td>
                <p>My son, you were to me like a mole that came up out of the ground to get some warmth from the sun,
                    for it had no eyes.  An eagle saw him, smote him, and carried him off.</p>
            </td>
            <td>
                <p>ܗܘܝܬ ܠܝܼ ܒܪܝ ܐܝܟ ܚܘܼܠܕܼܐ ܕܣܠܿܩ ܡܼܢ ܓܘ ܐܪܥܐ ܕܢܩܒܿܠ ܠܫܡܫܐ ܥܠ ܕܠܝܬܿ ܠܗ ܥܝܢ̈ܐ ܘܚܙܼܝܗܝ ܢܫܪܐ ܘܡܚܼܝܗܝ ܘܚܛܦܗ
                </p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>34</p>
            </td>
            <td>
                <p>My son Nadan answered and said to me: "O my father Ahiqar! such a thing is far from you! Do unto me
                    according to thy mercy: for God forgives the iniquity of men; and thou, too, forgive me this folly:
                    and I will serve thy horses, and feed the swine that are in thy house: and I shall be called evil:
                    but thou devise no evil against me".</p>
            </td>
            <td>
                <p>ܥܢܼܐ ܢܕܢ ܒܪܝ ܘܐܡܪ ܠܝܼ ܚܣ ܠܟ ܡܼܢ ܗܠܝܢ ܐܒܼܝ ܐܚܝܼܩܪ ܐܝܟ ܪ̈ܚܡܝܟ ܥܒܕ ܥܡܝ ܐܦ ܐܠܗܐ ܓܝܪ ܚܛܿܝܢ ܐܢܫܐ ܘܫܒܿܩ
                    ܠܗܘܿܢ ܘܐܦ ܐܢܬܿ ܫܒܼܘܿܩ ܠܝܼ ܗܕܐ ܣܟܼܠܘܼܬܼܐ ܘܐܗܘܐ ܡܫܡܫ ܪ̈ܟܼܫܟ ܘܪܥܿܐ ܚܙܝܼܪ̈ܐ ܕܒܼܒܝܬܟ ܘܐܢܐ ܐܬܼܩܪܐ ܒܝܼܫܐ
                    ܘܐܢܬܿ ܠܐ ܬܚܫܘܿܒܼ ܥܠܝ ܒܝܼܫܬܐ</p>
            </td>
            <td>
                <p>10</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>35</p>
            </td>
            <td>
                <p>I answered and said to him: "O my son! thou hast been to me like a palm-tree which stood by a river,
                    and cast all its fruit into the river. When its owner came to cut it down, it said to him: 'Leave me
                    alone this year, and I will give you carobs.' His master said to him: 'Thou hast not been diligent
                    in that which is thy own: how canst thou be diligent in that which is not thy own?'"</p>
            </td>
            <td>
                <p>ܥܢܝܿܬܼ ܐܢܐ ܘܐܡܿܪܬܼ ܠܗ ܗܘܝܬܿ ܠܝܼ ܒܪܝ ܐܝܟ ܗܘܿ ܕܘܠܐ ܕܩܐܿܡ ܗܘܼܐ ܥܠ ܢܗܪܐ ܘܟܼܠܗ ܐܒܗ ܒܢܗܪܐ ܫܕܐ ܗܘܐ ܘܟܕ ܐܬܼܐ
                    ܡܪܗ ܕܢܦܣܩܝܼܘܗܝ ܐܡܪ ܠܗ ܫܒܼܘܿܩܝܢܝ ܗܕܐ ܫܢܬܐ ܘܥܒܿܕܢܐ ܠܟ ܚܪ̈ܘܼܒܐ ܘܐܡܪ ܠܗ ܡܪܗ ܒܕܝܼܠܟ ܠܐ ܐܟܼܫܪܬܿ ܒܕܠܐ ܕܝܼܠܟ
                    ܐܝܟܢܐ ܬܟܼܫܪ</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>36</p>
            </td>
            <td>
                <p>My son, they say to the wolf: "Why do you follow the sheep?" He said to them: "Their dust is very
                    good for my eyes". Again they took him to the schoolhouse, and his master said to him: "A, B," said
                    the wolf: "Kid, lamb".</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܕܐܒܼܐ ܐܡܪܝܼܢ ܠܗ ܡܛܠ ܡܢܐ ܒܬܪ ܥܢ̈ܐ ܡܗܠܟ ܐܢܬܿ ܐܡܪ ܠܗܘܿܢ ܣܓܿܝܼ ܡܘܬܪ ܚܝܠܗܿ ܠܥܝܢ̈ܝ ܘܬܼܘܼܒܼ ܐܥܠܘܼܗܝ
                    ܠܒܝܬܼ ܣܦܪܐ ܐܡܪ ܠ ܪܒܿܗ ܐܠܦ ܒܝܬܼ ܐܡܪ ܕܐܒܐ ܓܕܝܐ ܦܐܪܐ</p>
            </td>
            <td>
                <p>12</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum B">B</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>37</p>
            </td>
            <td>
                <p>My son, I have taught you that there is a God, but you rise up against good servants and beat those
                    who have not fumbled. And just as God has kept me alive because of my righteousness, so He will
                    destroy you because of your deeds.</p>
            </td>
            <td>
                <p>ܒܪܝ ܐܢܐ ܐܠܦܬܟ ܕܐܝܼܬܼ ܐܠܗܐ ܘܐܢܬܿ ܥܠ ܥܒܼ̈ܕܐ ܛܒ̈ܐ ܩܐܡ ܐܢܬܿ ܘܡܢܓܿܕ ܐܢܬܿ ܠܗܘܿܢ ܕܠܐ ܣܟܼܠܘܼ ܘܐܝܟܢܐ ܕܠܝܼ ܐܚܝܼ
                    ܐܠܗܐ ܡܛܠ ܟܐܢܘܼܬܼܝ ܠܟ ܢܘܒܿܕ ܡܛܠ ܥܒ̈ܕܝܟ</p>
            </td>
            <td>
                <p>1</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>38</p>
            </td>
            <td>
                <p>My son, they put the head of the donkey over a dish at the table, and it rolled off and fell into the
                    dust. They say: "It was angry with itself because it was not honoured."</p>
            </td>
            <td>
                <p>ܒܪܝ ܠܪܫܗ ܕܚܡܪܐ ܣܡܘܼܗܝ ܒܦܝܼܢܟܼܐ ܥܠ ܦܬܼܘܿܪܐ ܘܗܘ ܐܬܼܥܪܓܠ ܘܢܼܦܠ ܒܥܦܪܐ ܘܐܡܪܝܼܢ ܥܠ ܢܦܫܗ ܪܓܙ ܕܠܐ ܩܒܿܠ ܐܝܼܩܪܐ
                </p>
            </td>
            <td>
                <p>9</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum G">G</abbr> <abbr class="siglum N">N</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>39</p>
            </td>
            <td>
                <p>My son, you have proved the saying which says: "Call him whom you have begotten as your son, and him
                    whom you have bought as your slave". My son, the saying is true which says "Take thy sister's son
                    under thy armpit and smite him against a stone". But God, who has kept me alive, will judge between
                    us.</p>
            </td>
            <td>
                <p>ܐܢܬܿ ܒܪܝ ܫܪܪܬܝܗܝ ܠܡܬܼܠܐ ܕܐܡܝܼܪ ܕܝܼܠܕܬܿ ܩܪܝܼ ܒܪܟ ܘܕܙܒܢܬܿ ܩܪܝܼ ܥܒܼܕܟ ܒܪܝ ܫܪܝܼܪܘܼ ܡܬܼܠܐ ܕܐܡܝܼܪ ܕܒܪ ܚܬܼܟ
                    ܐܚܘܿܕ ܬܚܝܬܼ ܫܚܬܼܟ ܘܠܟܐܦܐ ܛܪܘܿܦ ܐܠܐ ܐܠܗܐ ܗܘܿ ܕܐܚܝܢܝ ܢܕܘܼܢ ܒܝܢܬܢ</p>
            </td>
            <td>
                <p>11</p>
            </td>
            <td>
                <p><abbr class="siglum C">C</abbr> <abbr class="siglum A">A</abbr> <abbr class="siglum G">G</abbr> (2x), <abbr class="siglum K">K</abbr> (2x), <abbr class="siglum I">I</abbr> (2x), <abbr class="siglum N">N</abbr> (2x), <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr> <abbr class="siglum Q">Q</abbr> (2x)</p>
            </td>
        </tr>
        <tr class="ro1">
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>X1</p>
            </td>
            <td>
                <p>My son, the old age of an eagle is better than the youth of a vulture</p>
            </td>
            <td>
                <p>ܒܸܪܝ ܛܵܒܼܐ ܣܲܝܒܿܘܲܬܹܗ ܕܢܹܫܵܪܐ ܡܼܢ ܥܠܲܝܡܘܼܬܼܗ ܕܟܼܘܼܕܪܵܐ܀</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p><abbr class="siglum A">A</abbr> <abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum K">K</abbr> <abbr class="siglum I">I</abbr> <abbr class="siglum Q">Q</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>X2</p>
            </td>
            <td>
                <p>My son, you have been to me like a roe that is entangled in a noose, unable to save itself from
                    death. However, it lifts up its sweet and beautiful voice and makes them sink to murder.</p>
            </td>
            <td>
                <p>ܗܘܸܝܬܿ ܠܝ ܐܝܟ ܚܓܠܼܐ ܕܠܐ  ܡܦܿܨܐ ܢܦܫܗ ܡܼܢ ܡܘܬܐ܂ ܘܠܚܒܼܪ̈ܘܗܝ ܠܘܬܼܗ ܡܟܲܢܫ ܘܡܛܿܒܥ ܠܗܘܢ ܒܩܛܠܐ܂</p>
            </td>
            <td>
                <p>5</p>
            </td>
            <td>
                <p><abbr class="siglum U">U</abbr> <abbr class="siglum M">M</abbr> <abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
        <tr class="ro1">
            <td>
                <p>X3</p>
            </td>
            <td>
                <p>My son, you have been to me like someone who rebukes the bulls and let them alive.</p>
            </td>
            <td>
                <p>ܗܘܝܬܿ ܠܝ ܒܪܝ ܐܝܟ ܐ̄ܢܫ ܕܒܣܪ ܥܠ ܬܘ[ܪ̈]ܐ ܫܒܼܩ ܐܢܘܢ ܠܚܝܘ̈ܗܝ܂</p>
            </td>
            <td>
                <p>3</p>
            </td>
            <td>
                <p><abbr class="siglum T">T</abbr> <abbr class="siglum H">H</abbr> <abbr class="siglum P">P</abbr></p>
            </td>
        </tr>
    </table>
</div>
</div>
</div>
