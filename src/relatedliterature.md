---
home: false
title: Related Literature
layout: Layout
---

# {{ $frontmatter.title }}

This bibliography covers the literature on the Ahiqar story limited to its Arabic and Syriac branches.

## I. Text Editions resp. translations

### A. Syriac

* Braida, E. (2014). The Romance of Aḥiqar the Wise in the Neo-Aramaic MS London Sachau 9321. _Journal of the Canadian Society for Syriac Studies_, 14, 50–78. https://doi.org/10.31826/jcsss-2014-140105
* Braida, E. (2015). The Romance of Aḥiqar the Wise in the Neo-Aramaic MS London Sachau 9321: Part II. _Journal of the Canadian Society for Syriac Studies_, 15, 41–50. https://doi.org/10.31826/9781463236915-005
* Conybeare, F. C., Harris, J. R., & Smith Lewis, Agnes. (1898). _The Story of Aḥiḳar from the Syriac, Arabic, Armenian, Ethiopic, Greek and Slavonic Versions_. C. J. Clay and Sons. 
* Conybeare, F. C., Harris, J. R., & Smith Lewis, Agnes. (1913). _The Story of Aḥiḳar from the Aramaic, Syriac, Arabic, Armenian, Ethiopic, Old Turkish, Greek and Slavonic Versions (2nd ed.)_. Cambridge University Press. 
* Dillon, E. J. (1898). Ahikar the Wise: An ancient Hebrew Folk Story. _Contemporary Review_, 73, 362–386.
* Dolabani, P. Y. (1962). _Aḥīqar sāprā w-ḥakīmā_. Maṭbaʻtā d-Mardīn. 
* Ferrer, J., & Monferrer-Sala, J. P. (2006). _Historia y enseñanzas de Ahíqar o la antigua sabiduría oriental / edición, traducción y estudio. Studia Semitica. Series Minor: Vol. 2_. Servicio de Publicaciones, Universidad de Córdoba. 
* Goshen-Gottstein, M. H. (1965). _Wisdom of Aḥiqar, Syriac and Aramaic_. The Hebrew University of Jerusalem. 
* Grünberg, S. (1917). _Die weisen Sprüche des Achikar nach der syrischen Hs. Cod. Sachau Nr. 336 der Kgl. Bibliothek in Berlin herausgegeben und bearbeitet_. H. Itzkowski. 
* Gülten, Y., & Gülten, A. (2004). Ahikar'in ögutleri. Gerçeğe Doğru Kitapları. 
* Guzik, M. H. (1936). _Die Achikar-Erzählung nach der syrischen Handschrift Cod. Sachau Nr. 336 der Preussischen Staatsbibliothek in Berlin_. Renaissance. 
* Lahdo, A._ Weisheiten, Aphorismen und Sprichwörter des Ahiqars. Weise[r P]hilosoph, Minister und Berater der Assyrischen Könige Sargon II[.] (722-705 v. Chr.) und Sanh[e]rib (705-681 v. Chr.). Gelesen auf Suryoyo und Deutsch_ (A. Lahdo, Narr.).
* Lidzbarski, M. (1896). _Geschichten und Lieder aus den neu-aramäischen Handschriften der Königlichen Bibliothek zu Berlin. Beiträge zur Volks- und Völkerkunde: Vol. 4_. E. Felber. 
* Mūḥtas, R. (1941). _Kūnāšā d-tašʿīṯā u-maṯlē ḏ-Aḥīqar ḥakkīmā: ‘am mēmrē ḏ-pelōṭarkōs u-medem medem min pardaysā ḏ-aṿāhāṯā_. Mar Narsai Press. 
* Nau, François. (1909). _Histoire et Sagesse d Ahikar l Assyrien (fils d Anaël, neveu de Tobie): Traduction des versions syriaques avec les principales différences des versions arabes, arménienne, grecque, néo syriaque, slave et roumaine. Documents pour l étude de la Bible_. Letouzey et Ané. 
* Nau, F. (1918/1919). _Documents relatifs à Ahikar: Édition et traduction d un manuscrit de Mgr Graffin (C), avec les principales variantes d un manuscrit de M.H. Pognon (P). Édition de la partie récente du manuscrit de M.H. Pognon_. Revue de l‘Orient chrétien, 3(1) [21], 274–307 & 356–400.
* Nau, F. (1918/1919). _Histoire et Sagesse d'Ahikar: D'aprés le manuscrit de Berlin << Sachau 162 >>, fol. 86sq_. Revue De L'orient Chrétien, 3,1 [21], 148–160.
* Nau, F. (1920). _Documents relatifs à Ahikar: Édition et traduction d un manuscrit de Mgr Graffin (C), avec les principales variantes d un manuscrit de M.H. Pognon (P). Édition de la partie récente du manuscrit de M.H. Pognon_. Libraire Auguste Picard. 
* Pennacchietti, F. (2005). Il testo siriaco antico di Ahiqar. In R. Contini, Grottanelli, C. (Eds.), _Il saggio Ahiqar. Fortuna e trasformazioni di uno scritto sapienziale. Il testo più antico e le sue versioni_, pp. 193-226). Paideia Editrice.
* Salci-Hawsho, E. (2002). _Den vise Ahikar_. Syrianska Demokratiska Föreningen i Södertälje. 
* Talay, S. (2002). Die Geschichte und die Sprüche des Aḥiqar im neuaramäischen Dialekt von Mlaḥsô. In W. Arnold (Ed.), _"Sprich doch mit deinen Knechten aramäisch, wir verstehen es!": 60 Beiträge zur Semitistik. Festschrift für Otto Jastrow zum 60. Geburtstag_ (pp. 650–666). Harrassowitz.
* Talon, P. (2013). _Histoires syriaques: La sagesse d´Ahiqar et autres oeuvres syriaques. Nouvelles études orientales_. E.M.E. 
* Tronina, A., Starowieyski, M. (2011). _Apokryfy syryjskie: Historia i przysłowia Achikara, Grota Skarbów, Apokalipsa Pseudo-Metodego. Pisma apokryficzne: Vol. 6_. Wydawnictwo WAM.


### B. Arabic

* Charles, R. H. (1913). _The Apocrypha and Pseudepigrapha of the Old Testament in English: with critical and explanatory notes to the several books_. Clarendon Press. 
* Conybeare, F. C., Harris, J. R., & Smith Lewis, Agnes. (1898). _The Story of Aḥiḳar from the Syriac, Arabic, Armenian, Ethiopic, Greek and Slavonic Versions_. C. J. Clay and Sons. 
* Conybeare, F. C., Harris, J. R., & Smith Lewis, Agnes. (1913). _The Story of Aḥiḳar from the Aramaic, Syriac, Arabic, Armenian, Ethiopic, Old Turkish, Greek and Slavonic Versions_ (2nd ed.). Cambridge University Press. 
* Giaiero, R. (2005). Tre recensione arabi della storia di Hayqar. In R. Contini, Grottanelli, C. (Eds.), _Il saggio Ahiqar. Fortuna e trasformazioni di uno scritto sapienziale. Il testo più antico e le sue versioni_, pp. 227-254). Paideia Editrice.
* Kuhn, E. (1892). Zum weisen Akyrios. _Byzantinische Zeitschrift_, 1, 127-130.
* Leroy, L. (1908). Histoire d'Haikar le sage d'après les manuscrits arabes 3637 et 3656 de Paris (I). _Revue de l'orient Chrétien_, 13, 367–388.
* Leroy, L. (1909). Histoire d'Haikar le sage d'après les manuscrits arabes 3637 et 3656 de Paris (II). _Revue de l'orient Chrétien_, 14, 50-70 & 143-154.
* Lidzbarski, M. (1896). _Geschichten und Lieder aus den neu-aramäischen Handschriften der Königlichen Bibliothek zu Berlin. Beiträge zur Volks- und Völkerkunde: Vol. 4_. E. Felber. 
* Mahdi, M. (1994). _The Thousand and One Nights (Alf Layla wa-Layla). From the Earliest Known Sources. Arabic Text Edited with Introduction and Notes_ (Vols. 1-3). Brill. 
* Salhani, A. (1890). _Contes arabes: édités d'après un ms. de l'Université S. Joseph_. Impr. catholique. 


## II. Catalogue Entries

* Assemani, S., & Assemani, S. E. (1759). _Bibliothecae Apostolicae Vaticanae codicum manuscriptorum catalogus, in tres partes distributus, in quarum prima Orientales, in altera Graeci, in tertia Latini, Italici aliorumque Europaeorum idiomatum codices, vol. 3_. Maisonneuve. 
* Briquel-Chatonnet, F. (1997). _Manuscrits syriaque. de la Bibliothèque nationale de France (nos 356 - 435, entrés depuis 1911), de la bibliothèque Méjanes d'Aix-en-Provence, de la bibliothèque muncipale de Lyon et de la Bibliothèque nationale et universitaire de Strasbourg_. Bibliothèque nationale de France. 
* Browne, E. G. _A hand-list of the Muhammadan manuscripts, including all those written in the Arabic character, preserved in the Library of the University of Cambridge_. J. and C. F. Clay, at the University Press. 
* Del Río Sánchez, F. (2003). _Catalogue des manuscrits conservés dans la bibliothèque de l’archevêché grec-catholique d’Alep (Syrie)_. L. Reichelt. 
* Forshall, J. F., & Rosen, F. A. (1838). _Catalogus codicum manuscriptorum orientalium qui in Museo Britannico asservantur. Pars prima: codices syriacos et carshunicos amplectens_. Impensis curatorum Musei Britannici. 
* Goshen-Gottstein, M. H. (1979). _Syriac manuscripts in the Harvard College Library. A catalogue. Harvard Semitic Studies: Vol. 23_. Scholars Press. 
* Lidzbarski, M. (1896). _Die neu-aramäischen Handschriften der Königlichen Bibliothek zu Berlin. Ergänzungshefte zur Zeitschrift für Assyriologie, Semitistische Studien: 4/9,1-2_. E. Felber. 
* Mingana, A. (1933-1939). _Catalogue of the Mingana Collection of Manuscripts now in the Possession of the Trustees of the Woodbrooke Settlement (Vols. 1-3)_. W. Heffer & Sons. 
* Mingana, A. (1934). _Catalogue of the Arabic Manuscripts in the John Rylands Library_. Manchester University Press. 
* Perho, I. (2007), _Catalogue of Arabic manuscripts, codices arabici et codices arabici additamenta, vol. 2. Catalogue of Oriental Manuscripts, Xylographs, etc. in Danish Collections (COMDC) series: Vol. 5.2_. NIAS Press. 
* Proverbio, D. V. (2010). _Turcica Vaticana. Studi e Testi: Vol. 461_. Biblioteca Apostolica Vaticana. 
* Pertsch, W. (1883). _Die orientalischen Handschriften der Herzoglichen Bibliothek zu Gotha_. Druck der Kais. Kön. Hof- und Staatsdruckerei. 
* Sachau, E. (1899). _Verzeichniss der syrischen Handschriften der Königlichen Bibliothek zu Berlin (Vols. 1-2)_. A. Asher & Co. 
* Sarau, O., & Shedd, W. A. (1899). _Catalogue of the Syriac manuscripts in the Library of the Museum Association of Oroomiah College_. N.N. 
* Sbath, P. (1928). _Bibliothèque de Manuscrits Paul Sbath, Prêtre Syrien d'Alep vol. 1_. H. Friedrich et Co.
* Scher, A. (1906). _Notice sur les manuscrits syriaques conservés dans la bibliothèque du couvent des Chaldéens de Notre-Dame-des-Semences_. Imprimerie Nationale. 
* Slane, M. G. de. (1883-1895). _Catalogue des Manuscrits Arabes (Bibliothèque Nationale. Département des Manuscrits)_. Imprimerie Nationale. 
* Tisserant, E. (1924). Inventaire sommaire des manuscrits arabes du fonds Borgia à la Bibliothèque Vaticane, in _Miscellanea Francesco Ehrle. Scritti di storia e paleografia pubblicati sotto gli auspici di S. S. Pio XI in occasione dell'ottantesimo natalizio dell'E.mo cardinale Francesco Ehrle, V: Biblioteca ed Archivio Vaticano: Biblioteche diverse. Studi e Testi: Vol. 41_. Tipografia del senato. 
* Voorhoeve, P. (1980). _Handlist of Arabic manuscripts in the Library of the University of Leiden and other collections in the Netherlands_. Leiden University Press. 
* Vosté, J.‑M. (1929). _Catalogue de la Bibliothèque syro-chaldéenne du Couvent de Notre-Dame des Semences près d'Alqosh (Iraq)_. Bureaux of the «angelicum»/ P. Geuthner. 
* Witkam, J. (2007). _Inventory of the Oriental manuscripts in Leiden University Library, vol. 2_. Ter Lugt. 
* Wright, W. (1870-1872). _Catalogue of Syriac Manuscripts in the British Museum acquired since the year 1838 (Vols. 1-3)_. Longmann & Co. and Asher & Co. 


## III. Further Literature

* Adams, I. (1900). _Persia by a Persian. Personal experiences, manners, customs, habits, religious and social life in Persia_. N. N. 
* al-Baraṭlī, Q. H. (2007). _Taš‘ita d-Aḥiqar: Qiṣṣat Ahīqār_. Dar al-Mašriq al-Thaqāfīyah. 
* Arzhanov, Y. N. (2019). _Syriac Sayings of Greek Philosophers: A Study in Syriac Gnomologia with Edition and Translation. Corpus scriptorium Christianorum Orientalium, vol. 669, Subsidia, vol. 138_. Peeters. 
* Ayali-Darshan, N. (2018). The Sequence of Sir 4:26–27 in Light of Akkadian and Aramaic Texts from the Levant and Later Writings. _Zeitschrift für die alttestamentliche Wissenschaft_(130), 436–449.
* Bahnām, G. B. (1876). _Aḥīqār al-ḥākīm. Maṭbū‘āt maǧma‘ al-luġa as-suryānīya_. 
* Barton, G. A. (1900). The Story of Aḥiḳar and the Book of Daniel. _The American Journal of Semitic Languages and Literatures_(16), 242–247.
* Baumstark, A. (1922). A. Baumstark, _Geschichte der syrischen Literatur mit Ausschluß der christlich-palästinensischen Texte_. A. Marcus und E. Webers Verlag Dr. jur. Albert Ahn. 
* Bledsoe, S. A. (2013). Can Ahiqar Tell Us Anything about Personified Wisdom? _Journal of Biblical Literature_(132), 119–136.
* Bledsoe, S. A. (2014). The Relationship between the Elephantine Ahiqar Sayings and Later Recensions: A Preliminary Analysis of the Development and Diffusion of the Ahiqar tradition. In M.-C. Bornes-Varol & M.-S. Ortola (Eds.),_Aliento Échanges sapientiels en Méditerranée: Vol. 4. Enoncés sapientiels: traductions, traducteurs et contextes culturels_ (pp. 223–250). Presses de Nancy.
* Bousset, W. (1905). Beiträge zur Achikarlegende. _Zeitschrift für die neutestamentliche Wissenschaft_(6), 180–193.
* Braida, E. (2010). Il Romanzo del saggio Ahiqar: Una proposta stemmatica,". In _History of the ancient Near East Monographs: Vol. 10. Camsemud 2007: Proceedings of the 13th Italian Meeting of Afro-Asiatic Linguistics; held in Udine, May 21st - 24th, 2007_ (pp. 49–64). Sargon.
* Braida, E. (2012). Garshuni Manuscripts and Garshuni Notes in Syriac Manuscripts. _Parole De L’orient_(37), 181–198.
* Braida, E. (2014). Neo-Aramaic Garshuni: Observations Based on Manuscripts. _Journal of the Canadian Society for Syriac Studies_(17), 17–31.
* Braida, E., & Destefanis, S. (2008). 'I fratelli hanno bisogno dei fratelli e i re dei re'. Ipotesi sull’origine di un episodo delle versioni siriache e araba de Romanzo di Ahiqar. In A. Monti, F. A. Pennacchietti, F. Gallucci, & F. Musso (Eds.), _Essays in honour of Fabrizio Pennacchietti_ (pp. 35–41). Edizioni dell'Orso.
* Briquel-Chatonnet,, F. (2005). L’histoire et la sagesse d’Aḥiqar: fortune littéraire de la vie d’un dignitaire araméen à la cour assyrienne. In _Cahiers de la Societe Asiatique: N.S., 4. D'un Orient l'autre: Actes des Troisièmes Journées de l'Orient, Bordeaux, 2 - 4 octobre 2002_ (pp. 17–40). Peeters.
* Briquel-Chatonnet,, F. (2006). "Construis-moi un château dans le ciel': Remarques sur un motif de conte, d’Ahiqar à Thomas. _The Harp_(20), 55–64.
* Briquel-Chatonnet,, F. (2007). De l’Aḥiqar araméen à l’Aḥiqar syriaque: Les voies de transmission d’un roman. In G. S. Vashalomidze & L. Greisiger (Eds.), _Der Christliche Orient und seine Umwelt: Gesammelte Studien zu Ehren Jürgen Tubachs anläßlich seines 60. Geburtstages. Studies in oriental religions, vol. 56_ (pp. 51–57). Harrassowitz.
* Brock, S. P. (1968). A piece of wisdom literature in Syriac. _Journal of Semitic Studies_(13), 212–217.
* Brock, S. P. (1969). Notes on some Texts in the Mingana Collection. _Journal of Semitic Studie_s(14), 205–226.
* Brock, S. P. (2011). Ahiqar. In Brock, S. P. et al. (Ed.), _Gorgias encyclopedic dictionary of Syriac heritage_ (pp. 11–12). Gorgias Press.
* Burton, F. (1886). _Supplemental Nights to the Book of the Thousand Nights and a Night._ The Burton Club. 
* Caquot, A. (Ed.). (1981). _Alter Orient und Altes Testament: Vol. 212. Mélanges bibliques et orientaux en l'honneur de M. Henri Cazelles._ Butzon & Bercker. 
* Charlesworth, J. H. (Ed.). (1985). _The Old Testament pseudepigrapha._ Doubleday & Company. 
* Cheikho, L. (1910). _Kitāb al-ḥamāsah taʼlīf Abī ʻUbādah al-Walīd ibn ʻUbayd al-Buḥturī. Naqalahu ʻan al-nuskhah al-waḥīdah al-maḥfūẓah fī maktabat kullīyat Laydin wa-iʻtanā bi-ḍabṭihi al-ab Lūwīs Shaykhū al-yasūʻī._ Univ. St. Joseph, Beyrouth. 
* Cornill, H. (1875). _Das Buch der weisen Philisophen_. F. A. Brockhaus. 
* Cowley, E. (1923). _Aramaic Papyri of the Fifth Century BC_. Clarendon Press. 
* Degen, R. (1977). Achikar. In K. Ranke (Ed.), _Enzyklopädie des Märchens, Band 1: Handwörterbuch zur historischen und vergleichenden Erzählforschung_ (pp. 53–59). De Gruyter.
* Denis, A.‑M., & Haelewyck, J.-C. (2005). _Introduction à la littérature religieuse judéo-hellénistique: Pseudepigraphes de l'Ancien Testament (Vols. 1-2)_. Brepols. 
* Destefanis, S., & Braida, E. (2006). An Outline of the Romance and Proverbs of the Wise Ahiqar and its Modern Neo-Aramaic Version. In A. Monti (Ed.), _DOST critical studies: Vol. 1. Roads to knowledge: Hermeneutical and Lexical Probes_ (pp. 19–26). Edizioni dell'Orso.
* Destefanis, S., & Braida, E. (2010). ''i Proverbi di Ahiqar nella versione neoaramaica di Rubeyl Muhattas. Un’analisi comparativa delle sue fonti. In _History of the ancient Near East Monographs: Vol. 10. Camsemud 2007: Proceedings of the 13th Italian Meeting of Afro-Asiatic Linguistics; held in Udine, May 21st - 24th, 2007_ (pp. 221–228). Sargon.
* Dillon, E. J. (1898). Ahikar the Wise: An Ancient Hebrew Folk Story. _Contemporary Review_(73), 362–386.
* Eberhard, A. (1872). Fabulae Romanenses Graece. B.G. Teubner. 
* Ebied, R., & Al-Jeloo, N. (2010). Some Further Letters in Syriac, Neo-Aramaic and Arabic Addressed to Eduard Sachau by Jeremiah Shāmīr. _Journal of Assyrian Academic Studies_(24), 1–45.
* Furayḥah, A. (1962). _Aḥīqār: Ḥakīm Min Al-Sharq Al-Adná Al-Qadīm_. al-Jāmiʻah al-Amīrikīyah, Manshūrāt Kullīyat al-ʻUlūm wa-al-Adāb. 
* Gaster, M. (1900). Contributions to the History of Aḥiḳar and Nadan. Journal of the Royal Asiatic Society(32), 301–319.
* Greenfield, C. (1998). The Wisdom of Ahiqar. In J. A. Emerton, J. Day, R. P. Gordon, & H. G. M. Williamson (Eds.), _Wisdom in ancient Israel: Essays in honour of J. A. Emerton_ (pp. 43–52). Cambridge University Press.
* Greenfield, C., & Delcor, M. (1981). Ahiqar in the book of Tobit. In A. Caquot (Ed.), _Alter Orient und Altes Testament: Vol. 212. Mélanges bibliques et orientaux en l'honneur de M. Henri Cazelles_ (pp. 329–336). Butzon & Bercker.
* Grotanelli, C. (1987). Aesop in Babylon. In _Berliner Beiträge zum Vorderen Orient. Mesopotamien und seine Nachbarn: Politische u. kulturelle Wechselbeziehungen im alten Vorderasien vom 4. bis 1. Jahrtausend v. Chr_ (pp. 555–572). Reimer.
* Halévy, J. (1912). Un document judeo-arameen d'Elephantine. _Revue Sémitique d'épigraphie et d'histoire Ancienne_(20), 153–164.
* Heller, B., & Stillmann, N. A. (1896). Luḳmān. In _Encyclopédie de l'Islam_. Vol. 5 (pp. 817–820). Brill.
* Hicks, D. (1925). _Lives of the Eminent Philosophers. Vol. 1_. W. Heinemann. 
* Holm, T. L. (2014). Memories of Sennacherib in Aramaic Tradition. In I. Kalimi & S. Richardson (Eds.), Brill online books and journals: Vol. 71. Sennacherib at the Gates of Jerusalem: Story, history and historiography (pp. 295–323). Brill.
* Contini, Riccardo. (2005). R. Contini, Grottanelli, C., Eds., _Il saggio Ahiqar, Studi Biblici_. Paideia Editrice. 
* Folmer, M. (2022). _Elephantine Revisited: New Insights Into the Judean Community and Its Neighbors_. Penn State University Press. 
* Jagié, V. (1892). Der Weise Akyrios. _Byzantinische Zeitschrift_(1), 107–127.
* Karouby, L. (2013). _Histoire et Sagesse d’Aḥiqar l’Assyrien” ou l’Ummānu sans descendance: invariance et variations, de l’Antiquité au XVIIIe siècle_. Uunpublished Dissertation. 
* Khan, G. (2016). _The Neo-Aramaic Dialect of the Assyrian Christians of Urmi. Studies in Semitic Languages and Linguistics: Vol. 86_. Brill. 
* Kottsieper, I. (1990). _Die Sprache der Aḥiqarsprüche. Beihefte zur Zeitschrift für die alttestamentliche Wissenschaft: Vol. 194_. De Gruyter. 
* Kottsieper, I. (1991). _Die Geschichte und die Sprüche des weisen Achiqar. Texte aus der Umwelt des Alten Testaments: Vol. 3,2_. Mohn. 
* Kraeling, G. H. (1969). _The Brooklyn Museum Aramaic papyri: new documents of the fifth century B.C. from the Jewish colony at Elephantine_. Repr. Arno Press. 
* Küchler, M. (1979). _Frühjüdische Weisheitstraditionen: Zum Fortgang weisheitlichen Denkens im Bereich des frühjüdischen Jahweglaubens. Orbis Biblicus et Orientalis: Vol. 26_. Universitätsverlag/ Vandenhoeck und Ruprecht. 
* Kuhn. E. (1892). Zum weisen Akyrios. _Byzantinische Zeitschrift_(1), 127–130.
* Lidzbarski, M. (1894). Zum weisen Achikâr. _Zeitschrift der Deutschen Morgenländischen Gesellschaft_(48), 671–675.
* Lindenberger, J. M. (1985). Ahiqar: A New Translation and Introduction. In J. H. Charlesworth (Ed.), _The Old Testament pseudepigrapha / Edited by J.H. Charlesworth /Bd 1: Vol. 2. Expansions of the "Old Testament" and legends, wisdom and philosophical literature, prayers, psalms and odes, fragments of lost Judeo-Hellenistic works_ (pp. 479–507). Darton Longman & Todd.
Littmann, E. (1923). _Tausendundeine Nacht in der arabischen Literatur_. Mohr. 
* Lourié, B. (2013). The Syriac Aḥiqar, Its Slavonic Version, and the Relics of the Three Youths in Babylon. _Slověne_(2), 64–117.
* Lourié, B. (2015). Direct Translations into Slavonic from Syriac: A Preliminary List. In C. Diddi (Ed.), _ΠΟΛΥΙΣΤΩΡ. Scripta slavica Mario Capaldo dicata_ (pp. 161–168). Индрик.
* Lüdtke, W. (1911). Beiträge zu slavischen Apocryphen. _Zeitschrift für die alttestamentliche Wissenschaft_(31), 218–240.
* Marc, P. (1902). Die Achikarsage: Ein Versuch zur Gruppierung der Quellen. _Studien Zur vergleichenden Literaturgeschichte_(2), 393–411.
* Meißner, B. _Das Märchen vom weisen Achiqar. Der Alte Orient: Vol. 16,2_. Hinrichs. 
* Meißner, B. (1894). Quellenuntersuchungen zur Haikargeschichte. _Zeitschrift der Deutschen Morgenländischen Gesellschaft_(48), 171–197.
* Meyer, E. (1912). _Der Papyrusfund von Elephantine: Dokumente einer jüdischen Gemeinde aus der Perserzeit und das älteste erhaltene Buch der Weltliteratur_. Hinrichs. 
* Minov, S. (2019). Syriac. In A. Ḳuliḳ (Ed.), _A Guide to Early Jewish Texts and Traditions in Christian Transmission_ (pp. 95–138). Oxford University Press.
Moore, J. D. (2021). _Literary Depictions of the Scribal Profession in the Story of Ahiqar and Jeremiah 36_. De Gruyter. 
* Nau, F. (1906). Note sur trois anciennes traductions de la légende d’Ahikar et de la Chronique d’Édesse. In _Actes du XIVe Congrès international des Orientalistes, Alger 1897. Première partie: Procès-verbaux_ (p. 69). E. Leroux.
* Nau, F. (1907). Le mot ’arhe’ dans Ahikar et Bar Bahlul. _Journal Asiatique_(10), 149–159.
* Nau, F. (1912). Ahiqar et les papyrus d'Éléphantine. _Revue Biblique_(2), 68–79.
* Nau, F. (1914). Préceptes anonyms et histoire d’Ahiqar. _Revue De L'orient Chrétien_(19), 209–214.
Niditch, S. (1976). A Test Case for Formal Variants in Proverbs. _Journal of Jewish Studies_(27), 192–194.
* Niehr, H. (2007). _Aramäischer Aḥiqar. Jüdische Schriften aus hellenistisch-römischer Zeit - Neue Folge: Vol. 2,2_. Gütersloher Verlagshaus. 
* Nöldeke, T. (1907). Die aramäischen Papyri von Assuan. _Zeitschrift für Assyriologie und Verwandte Gebiete_(20), 130–150.
* Nöldeke, T. (1913). _Untersuchungen zum Achiqar Roman. Abhandlungen der Königlichen Gesellschaft der Wissenschaften zu Göttingen, Philologisch Historische Klasse NF: Vol. 14,4_. Weidmannsche Buchhandlung. 
* Nöldeke, T. (1914-1915). כרכושת ܟܟܘܫܬܐ. _Zeitschrift für Assyriologie und Verwandte Gebiete_(29), 239–246.
* Sachau, E. (1911). _Aramäische Papyrus und Ostraka aus einer jüdischen Militär-Kolonie zu Elephantine. Altorientalische Sprachdenkmäler des 5. Jahrhunderts vor Chr_. Hinrichs. 
* Simonsen, D. (1894). Zu S. 185–186. _Zeitschrift der Deutschen Morgenländischen Gesellschaft_(48), 698.
* Smend, R. (1908). Alter und Herkunft des Achikar-Romans und sein Verhältnis zu Aesop. _Beihefte Zur Zeitschrift für die alttestamentliche Wissenschaft_(13), 57–125.
* Talia, S. (2015). The Story of Ahiqar in Syriac and Neo Aramaic: A Textual, Linguistic and Historical Comparison. _Parole De L’orient_(40), 13–27.
* Weigl, M. (2010). _Die aramäischen Achikar-Sprüche aus Elephantine und die alttestamentliche Weisheitsliteratur. Beihefte zur Zeitschrift für die alttestamentliche Wissenschaft: Vol. 399_. De Gruyter. 
* Yellin, A. (1924). Notes on the Syriac Version of the Story of Ahikar as Edited by J. Rendel Harris. _Jewish Quarterly Review NS_(15), 119–121.
* Zieme, P. (2016). The Tale of Ahikar according to a Garshuni Turkish Manuscript of the John Rylands University Library. In L. Tang & D. W. Winkler (Eds.), _Orientalia - patristica - oecumenica: Vol. 9. Winds of Jingjiao: Studies on Syriac Christianity in China and Central Asia_ (pp. 131–147). LIT.


## How to Cite This Page

Birol, Simon and Aly Elrefaei. „Related Literature“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/relatedliterature.html.
