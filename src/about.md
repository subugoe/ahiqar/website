---
home: false
title: About
layout: Layout
---

# {{ $frontmatter.title }}

This historical-critical edition of the Syriac and Arabic Ahiqar texts is a project funded by the German Research Foundation. It is hosted by the [Faculty of Theology of the University of Göttingen](https://www.uni-goettingen.de/en/the+faculty/55363.html) and by the [Göttingen State and University Library (SUB)](https://www.sub.uni-goettingen.de/en/). The texts were encoded by the researchers using the [TextGrid Lab](https://textgrid.de/en/web/guest). The SUB Technical Team developed and provided a TEI schema and advised on encoding. For the technical realization of the Digital Edition, a generic TextAPI was developed to deliver the data and the TIDO Viewer to display the data.

## Management

### General

- [Prof. Dr. Reinhard G. Kratz](https://www.uni-goettingen.de/en/56085.html) ([Faculty of Theology of the University of Göttingen](https://www.uni-goettingen.de/en/the+faculty/55363.html))
- [Prof. Dr. Sebastian Günther](https://www.uni-goettingen.de/de/professor+dr.+sebastian+g%C3%BCnther/639814.html) ([Institute of Arabic and Islamic Studies of the University of Göttingen](https://www.uni-goettingen.de/en/635988.html))

#### Technical

[Zeki Mustafa Doğan](https://www.sub.uni-goettingen.de/en/contact/staff-a-z/staff-details/person/zeki-mustafa-dogan/) (Head of [Digital Library, Göttingen State and University Library](https://www.sub.uni-goettingen.de/kontakt/abteilungen-a-z/abteilungs-und-gruppendetails/abteilunggruppe/digitale-bibliothek/))

### Team

#### Arabic and Karshuni Edition

[Dr. Aly Elrefaei](https://www.uni-goettingen.de/de/dr-aly-elrefaei/545664.html) (Faculty of Theology)

#### Syriac Edition

[Simon Birol](https://uni-goettingen.de/de/56764.html) (Faculty of Theology)

#### Technical

*Staff listed are members of SUB Göttingen*

- [Mohamed Awad Abdelrahman Amer](https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/mohamed-awad-abdelrahman-amer/)
- [Tillmann Dönicke](https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/tillmann-doenicke/)
- [Mathias Goebel](https://www.sub.uni-goettingen.de/en/contact/staff-a-z/staff-details/person/mathias-goebel/)
- [Kay Liewald](https://www.sub.uni-goettingen.de/en/contact/staff-a-z/staff-details/person/kay-liewald/)
- [Orlin Malkja](https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/orlin-malkja/)
- [Paul Pestov](https://www.sub.uni-goettingen.de/en/contact/staff-a-z/staff-details/person/paul-pestov/)
- [Rafael Maria Raschkowski](https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/rafael-maria-raschkowski/)
- [Michelle Weidling](https://www.sub.uni-goettingen.de/en/contact/staff-a-z/staff-details/person/michelle-weidling/)

#### Consulting

- [PD Dr. Ingo Kottsieper](https://www.uni-muenster.de/EvTheol/personen/kottsieper.html) ([Faculty of Theology of the University of Münster](https://www.uni-muenster.de/EvTheol/), Research Center [Qumran Digital](http://www.qwb.adw-goettingen.gwdg.de/) of the Academy of Science Göttingen)

*Staff listed are members of SUB Göttingen*

- [Uwe Sikora](https://www.sub.uni-goettingen.de/en/contact/staff-a-z/staff-details/person/uwe-sikora/)
- [Christian Mahnke](https://www.sub.uni-goettingen.de/en/contact/staff-a-z/staff-details/person/christian-mahnke/)
- [Service for Digital Editions](https://www.sub.uni-goettingen.de/en/digital-library/service-digitale-editionen/)

## Contact

<Contact/>
