---
title: Translation of the Syriac Version
---

# {{ $frontmatter.title }}

### Introductory remarks

Translations of all the textual witnesses will begin as soon as possible. However, at this stage of the project we are publishing the translation of the most important textual witness for each tradition. For the Syriac branch, this is Cambridge Add. 2020.

Its textual version belongs to the Urmia tradition (cf. [Stemmata](stemmata.html)) and it is the oldest textual witness to transmit the Ahiqar story in its complete form in Syriac. Compared to the other manuscripts of this branch, Cambridge Add. 2020 is not a contaminated manuscript and does not contain missing text modules or additions that can be found in most of the other textual witnesses. Furthermore, there is no certainty that parts of this textual witness have been re-translated from Arabic into Syriac. This translation is based on the earlier translation by Harris (published in Conybeare et al. 1913) and editorial additions have been placed in brackets.

### Translation

#### First narrative section

**By the power of our Lord Jesus Christ, we begin to write the story. Again, by the divine power, I write the proverbs resp. the story of Ahiqar, sage and scribe of Sennacherib the king of Assyria and Nineveh.**

In the twentieth year of Sennacherib, son of Esarhaddon, king of Assyria and Nineveh, I, Ahiqar, was the king’s scribe. When I was young it was said to me that 'No son will be born to you'; the wealth that I had acquired was (too) great to be told. I had sixty wives and I built sixty castles for them, but I had no son. So, **(fol. 66v)** I, Ahiqar, built me a great altar, all of wood, and I set fire to it and put good aloe on it, and thus I said: “O Lord, my God, when I shall die and leave no son behind, what will the people say about me? (They will say this:) ‘Behold, Ahiqar the just and the good and the worshipper of God: he is dead, and has left no son to bury him, nor any daughter, nor his possessions, as a cursed (man) who has no heir.’

But I pray thee, (O) God, that I may have a male child, so that when I die he may put dust on my eyes". And I heard this voice: "O Ahiqar, wise scribe! all that thou hast asked of me, I have given thee: and I have left thee without sons: it shall suffice thee: be not grieved: but behold, Nadan, the son of thy sister, shall be a son unto thee. While he is growing up, you shall be able to teach him everything. And when I heard this, I was grieved again, and I said: "O my Lord, God! Is it that Thou wilt give me Nadan, the son of my sister, for a son, so that when I die he may throw dust on my eyes? And again I received no further answer. So I obeyed his command and took Nadan, my sister's son, as my son. Because he was young, I gave him eight wet-nurses, and I raised my son on honey, and made him lie on carpets, and clothed him in fine linen and purple; and my son grew and shot up like a cedar tree. When my son was grown up, I taught him literature and wisdom: when the king came (back) from the place where he had gone, he called me and said to me: "O Ahiqar, the wise scribe and master of my thoughts, when you grow old and die, who will come after you to serve me as you did?

I answered and said to him: "Live forever, my lord the king! I have a son who is as wise as I am, as knowledgeable as I am, and educated. The king said to me: "Bring him and let me see him. If he is able to stand before me, I will set you free in peace, and you shall live out your old age in honour until the end of your days. Then I brought my son Nadan and presented him to the king. When the king, my lord, saw him, he said: "This day shall be a blessed day **(fol. 67r)** in the sight of God, so that Ahiqar, who made an effort before my father Esarhaddon and before me, shall be rewarded, and I will put his son in my court while he is still alive, and he shall live out his days in peace. So I, Ahiqar, bowed down before the king and said "Live forever, my lord the king! And as I have endeavoured before thy father and before thee hitherto, so be thou merciful to the youth of this my son, that thy favour which thou hast bestowed on me may be doubled on him. When the king heard this, he gave me his right (hand), and I, Ahiqar, bowed down before the king. Nor did I rest from teaching my son until I had filled him with knowledge as with bread and water. This is what I said to him:

#### Sayings

[1] Hear, my son Nadan, and come to my understanding, and consider my words as the words of God.

[2] My son Nadan, when you have heard a word, let it die in your heart, and do not reveal it to anyone, lest it become a hot coal in your mouth and burn you and you brand yourself with disgrace and complain angrily against God.

[3] My son, do not tell all that you hear and do not reveal all that you see.

[4] My son, do not loosen a bond that is sealed, nor seal one that is loosened.

[5] My son, do not raise your eyes and look at a woman who is bedizened and painted; do not desire her in your heart. For if you give her all that is in your hands, you will find no benefit in her, and you will be guilty of a sin against God.

[6] My son, do not commit adultery with your friend's wife, so that others may not commit adultery with your wife.

[7] My son, do not be in a hurry like the almond tree, which is the first to blossom, but whose fruit is the last to ripen [lit: be eaten]. Rather, be balanced and prudent, like the mulberry tree, which blooms last, but whose fruit is the first to be eaten.

[8] My son, lower your eyes and lower your voice, and look from under your eyelids, for a house is not built by a loud voice; for if a house were built by a loud voice, a donkey would build two houses in one day; and if the plough were driven by sheer force, its share would never be loosed from a camel's armpit.

[9] My son, it is better to roll over stones with a wise man **(fol. 67v)** than to drink wine with a fool.

[10] My son, pour your wine on the graves of the righteous rather than drink it with the wicked.

[11] My son, you will not be defiled with a wise man, nor will you be wise with a defiled man.

[12] My son, associate yourself with a wise man in order to become wise like him, and do not associate yourself with a loquacious and talkative man in order not to be numbered with him.

[13] My son, while you have shoes on your feet, tread down the thorns and make a path for your sons and grandsons.

[14] My son, the rich man has eaten a serpent, and they are saying: "He ate it for medicine". But the poor ate it, and they were saying: "He ate it for his hunger".

[15] My son, eat your portion and do not reproach your friends.

[16] My son, do not even eat bread with him who is not ashamed.

[17] My son, do not envy your enemy's happiness, nor rejoice in his misfortune.

[18] My son, do not go near a whispering woman or one whose voice is loud.

[19] My son, do not follow the beauty of a woman, nor desire her in your heart, for the [true] beauty of a woman is her mind, and the word of her mouth is her ornament.

[20] My son, when your enemy meets you with evil, meet him with wisdom.

[21] My Son, the wicked falls and does not rise, but the righteous is not shaken, for God is with him.

[22] My son, do not withhold your son from the chastisement, for the chastisement of a boy is like dung in the garden, and like a bridle for a donkey or any beast, and like a fetter on the foot of a donkey.

[23] My son, subdue your son while he is a boy, before he becomes stronger than you and rebels against you, and you are ashamed of all his evil deeds.

[24] My son, get a strong bull and a donkey with good hooves, but do not get a runaway slave or a thieving maid, lest they cause you to lose all that you have acquired.

[25] My son, the words of a liar are like fat sparrows, and he who is without understanding eats them [i.e. believes them].

[26] My son, do not bring upon yourself the curses of your father and mother, so that you may rejoice in the blessings of your children.

[27] My son, do not go unarmed on the way, for you do not know when your enemy may come upon you.

[28] My son, as a tree is adorned with its branches and **(fol. 68r)** fruit, and a mountain with trees, so is a man adorned with his wife and children; and a man without brothers, wife and children is despised and scorned by his enemies; and he is likened to a tree by the roadside, which every passer-by plucks and every beast of the field tears off its leaves.

[29] My son, do not say, "My lord is a fool, and I am wise," but take him in his faults, and you shall be loved.

[30] My son, do not count yourself wise if others do not count you wise.

[31] My son, do not lie with your words before your Lord, lest He despise you and say to you: "Get out of my sight!"

[32] My son, let your words be true, so that your Lord may say to you: "Come near to Me," and you shall live.

[33] My son, do not revile God on the day of your affliction, in order that We may not be angry with you when He hears you.
[34] My son, do not treat your slave better than his fellow, for you do not know which of them you will need in the end.
[35] My son, smite with stones the dog that leaves its master and follows you.
[36] My son, the flock of many tracks will become the prey of the wolves.

[37] My son, judge righteously in your youth, that you may have honour in your old age.

[38] My son, sweeten your tongue and make the opening of your mouth savoury, for the tail of a dog gives him bread, but his mouth blows.

[39] My son, do not let your neighbour tread on your foot, lest he tread on your throat.

[40] My son, strike a man with a wise speech, so that it may be in his heart like a fever in summer: for even if you strike the fool with many rods, he will not understand.

[41] My son, send a wise man, and give him no orders: but if thou wilt send a fool, go thy way, and do not send him.

[42] My son, test your son with bread and water, and then leave your property and goods in his hands.

[43] My son, withdraw first from a marriage, and do not tarry for pleasant ointments, lest they should become bruises in your head.

[44] My son, he whose hand is full is called wise and honourable; and he whose hand is scarce is called foolish **(fol. 68v)** and weak.

[45] My son, I have carried salt, and I have turned over lead; but I have seen nothing heavier than a debt which a man has to pay without having borrowed.

[46] My son, I have borne iron and rolled stones, but they did not weigh so heavily on me as a man who settles in his father-in-law's house.
[47] My son, teach your son hunger and thirst, so that he may manage his house according to what his eye sees.

[48] My son, a blind man is better than one who is blind in his heart, for the blind of the eyes quickly learn the way and walk in it, but the blind of the heart leave the right way and go astray.

[49] My son, better is a friend who is near than a brother who is far away; and better is a good name than much beauty, for a good name lasts forever, but beauty decays and fades.

[50] My son, better is death than life for a man who has no rest, and better is the voice of wailing in the ears of a fool than singing and joy.

[51.] My son, better is a leg in your hand than a goose in another's pot, and better is a sheep near you than a cow far away, and better is a sparrow in your hand than thousands on the wing, and better is poverty that gathers than wealth that is scattered, and better is woollen clothing on you than fine linen and silk of others.

[52] My son, hold back a word in your heart, and that will make you feel well, for once you have exchanged your word, you will have lost your friend.

[53] My son, do not let a word go out of your mouth until you have taken counsel in your heart, for it is better for a man to stumble in his heart than to stumble with his tongue.

[54] My son, when you hear an evil word, bury it seven cubits deep in the ground.

[55] My son, do not linger where there is strife, for out of strife comes murder.

[56] My son, he who does not execute a just judgement angers God.

[57] My son, do not go away from your father's friend, lest your (own) friend join you.

[58] My son, do not go down into the garden of the nobles, nor approach the daughters of the nobles.

[59] My son, help your friend before the ruler, that you may save him from the lion.

[60] My son, do not rejoice over your enemy when he dies.

[61] My son, when you see a man who is older **(fol. 69r)** than you, stand up before him.

[62] My son, if the waters were to rise up without a bottom, and the sparrow fly without wings, and the raven become white as snow, and the bitter become sweet as honey, then the fool would become wise.

[63] My son, if you are a priest of God, be on guard before Him, and come before Him in purity, and do not depart from His presence.

[64] My son, to whom God does good, honour him also.

[65] My son, do not quarrel with anyone on his day, nor stand against a river in its flow.

[66] My son, the human eye is like a well of water, and it is not satisfied with riches until it is filled with dust.

[67] My son, if you wish to be wise, keep your tongue from lying and your hand from stealing, and you will be wise.

[68] My son, be not a mediator in a woman's marriage, for if it goes badly for her, she will curse you; and if it goes well for her, she will not remember you.

[69] My son, he who is elegant in dress is also elegant in speech, and he who is despicable in dress is also despicable in speech.

[70] My son, if you discover a find in front of an idol, offer [the idol] its share of it.

[71] My son, neither the hand that was satiated and is (now) hungry will give, nor the one that was hungry and is (now) satiated.

[72] My son, our eyes must not look at a beautiful woman, nor must we gaze at beauty that does not belong to you, for many have perished because of a woman's beauty, and love for her is like a burning fire.

[73] My son, let the wise beat you with many blows, but do not let the fool soothe you with sweet perfume.

[74] My son, do not let your foot run after your friend, lest he be sated with you and hate you.

[75] My son, put not a golden ring on your finger when you have nothing, lest the fools mock you.

#### Second narrative section

This is the teaching which Ahiqar gave to Nadan, the son of his sister. But I, Ahiqar, supposed that all that I had taught Nadan was in his heart, and that he stood in my place at the king's gate; and I did not know that my son Nadan did not heed my words, but scattered them like the wind; and he returned and said that my father Ahiqar is old and stands at the door of his grave; and his mind **(fol. 69v)** has withdrawn and his intelligence is diminished. My son Nadan began to beat, slay and destroy my servants; and he showed no mercy to my industrious, beloved and excellent servants and maidservants; he slew my horses and hewed my good mules.

So when I saw that my son Nadan was doing abominable things, I answered and said to him: "Nadan, my son, do not come near my possessions; my son, it is said: 'What the hand has not acquired, the eye has not spared.'" I showed my lord Sennacherib all these things, and my lord said: "As long as Ahiqar lives, no one shall have power over his possessions". When my son Nadan saw his brother Nebuzardan standing in my house, he was very angry and said this: "My father Ahiqar has grown old, and his wisdom is gone, and his wise words are despised, unless he gives his possessions to Nebuzardan my brother, and removes me from his house. "

When I, Ahiqar, heard these words, I said: "Woe to you, my wisdom, which my son Nadan has defiled, and my words which he has despised". When my son Nadan heard this, he was angry, and went to the king's gate, and evil was in his heart. And he sat down, and wrote two letters to two of the kings of Sennacherib, my lord: one to Akhi, the son of Ḥamselim, king of Persia and Elam:

From Ahiqar, scribe and keeper of the seal of Sennacherib, king of Assyria and Nineveh: Greetings. When this letter reaches you, arise and come to me in Assyria, and I will bring you to Assyria, and you shall take the kingdom without war.
And he wrote another letter as follows: 'To Pharaoh king of Egypt, from Ahiqar the scribe and keeper of the seal of Sennacherib of Assyria and Nineveh: Greetings. When this letter reaches you, get up and come to me on the 25th day of the month 'Āb in the plain of the Eagles, which is to the south, and I will bring you to Nineveh without war, and you shall take the kingdom. **(fol. 70r)**

And he made these writings of his like to my (own) handwriting; and he sealed them in the king's palace, and went (his way). He also wrote another letter to me, as if from my lord, King Sennacherib; and he wrote it as follows: 'From Sennacherib the king to Ahiqar, my scribe and keeper of the seal: Greetings. When this letter reaches you, gather all the forces to the mountain called Ṣīṣ, and go out from there and meet me in the plain of the Eagles, which is to the south, on the 25th day of the month of Āb. When you see me coming towards you, array your forces against me like a man ready for battle, for the messengers of Pharaoh the king of Egypt have come to me to see what power I have.
My son Nadan sent this letter to me by the hands of two of the king's servants. Then my son Nadan took the writings that he had written, as one who had found them (recently), and read them before the king. When my lord the king heard them, he lamented and said: "O God! in what have I sinned against Ahiqar that he should do this to me?" My son Nadan answered and said to the king: "My lord, be not irritated, nor be angry. Arise, and let us go to the Plain of the Eagles on the day that is written by him in the letter. If it is true, everything you command will be done. (So) my son Nadan led the king, my lord, and they came to me in the plain of the eagles, and they found me there, with a great army assembled with me. When I saw the king, I set my forces against him, as it was written in the letter. When the king saw this, he was very afraid. (Then) my son Nadan answered and said to him: "Do not be afraid, my lord the king. Go back in peace to your inner chamber, and Ahiqar will be brought to you". So my lord the king returned to his house. My son Nadan came to me and said: "All that you have done, you have done well; and the king has praised you much; and he commands you to dismiss your forces, and let each go to his own country and place, and come to me (the king) alone". **(fol. 70v)**
Then I came before the king. When he saw me, he said to me: "You have come, Ahiqar, my scribe and schoolmate from Assyria and Nineveh, whom I made honourable. But you turned back and became one of my enemies. He gave me the letters written in my name and sealed with the seal of my ring. When I read them, my tongue began to stammer, and my limbs were paralysed; and I sought for a single word of wisdom, and I found none. My son Nadan answered and said to me: "Get thee out of the presence of the king, thou foolish old man! And let your hands be bound with chains and your feet with iron". Then Sennacherib the king turned his face away from me, and he talked with Nabusemakh, my poor companion, and said to him: "Get up, and slay Ahiqar, and cut off his head a hundred ells from his body".

So I fell on my face to the ground and worshipped the king, saying "My lord, the king, live forever. Then, my lord, if you wish to kill me, your will shall be done. But I know that I have not sinned against you. But command, my lord the king, that they kill me at the door of my house, and that they bury my body. The king said to my poor companion, Nabusemakh: "Go, kill Ahiqar at the door of his house and bury his body". Then I, Ahiqar, sent to Eshfagni, my wife, that she should bring forth a thousand and one maidens of the daughters of my tribe, and that they should put on sackcloth, and that they should weep and lament and mourn for me. Let them come to me, and let them make a lamentation for me before I die, and you, prepare bread and a feast* and a banquet for Nabusemakh, my poor companion, and the Parthians with him, and go out to meet them, receive them, and bring them into my house. I too will enter the house as a guest. Eshfagni, my wife, because she was exceedingly wise, understood all that I said to her, and did as I commanded her **(fol. 71r)**.
She came out to meet them, and brought them into my house, and they ate bread, and she served them - with her own hand - until they went to their places and fell asleep from drunkenness. Then I, Ahiqar, went in and said to Nabusemakh: "Look to God and remember the love that was between us, brother. Do not grieve over my death, but remember that Esarhaddon the father of Sennacherib also delivered you to me to kill you, but I did not kill you. For I knew that there was no cause for offence in you, and I kept you alive until the king desired you; and when I brought you before him, he gave me great presents, and I carried away many presents from him. And now keep me alive, and repay me this kindness, lest it should go abroad that I have not been slain, and that the king has done you harm. Behold, I have in my prison a slave, whose name is Marzifan, and he is about to die. Clothe him in my robes, and stir up the Parthians against him, and they will slay him. I will not die, for I have done nothing wrong.

When I spoke these words, even Nabusemakh, my poor companion, was very sorry for me, and he took my clothes and put them on the slave who was in the prison. And he stirred up the Parthians, and they stood up while they tasted their wine, and killed him, and took his head a hundred ells from his body, and gave his body to be buried. Then the news went out in Assyria and Nineveh that Ahikar the scribe had been killed. Nabusemakh, my poor companion, and Eshfagni, my wife, arose and made for me a hiding place in the ground, three cubits wide and five cubits high, under the threshold of the door of my house.

So they gave me bread and water, and went and told Sennacherib the king that Ahikar the scribe was dead. When the men heard this, they wept; and the women scratched their faces, saying: "Alas for you, Ahiqar the wise scribe, **(fol. 71v)** repairer of the breaches of our land! We shall never have another like you.

Then Sennacherib the king called my son Nadan and said to him: "Go and mourn for your father Ahiqar, and come to me. And when Nadan my son came, he made no mourning for me, nor any remembrance at all; but he gathered together worthless and lewd companions, and set them at my table with singing and great rejoicing; and he stripped off my beloved servants and maidservants, and flogged them without mercy. Nor was he ashamed of my wife Eshfagni, but he sought to do with her what men do with women. And I, Ahiqar, was cast into darkness in the pit below, and I heard the voices of my bakers, cooks and butlers weeping and groaning in my house. And after a few days Nabusemakh, my poor companion, came and opened the prison over me and comforted me and set bread and water before me. And I said to him: When you go out from me, remember me in the sight of God, and say: "O God [of] justice and righteousness! send mercy on the earth! hear the voice of Ahiqar, thy servant, and remember that he sacrificed to thee fattened oxen like suckling lambs: now he is cast into a dark pit where he sees no space of light: willst thou not deliver him who crieth unto thee? My Lord, hear the voice of my companion!"
When Pharaoh king of Egypt heard that I, Ahiqar, had been killed, he was very glad, and he wrote a letter to Sennacherib as follows: "Pharaoh, king of Egypt, to Sennacherib, king of Assyria and Nineveh: Greetings. I want to build a castle between heaven and earth, (wherefore) I will seek out and send to me from your kingdom a man who is a skilled architect, that he may give me answers concerning all **(fol. 72r)** that I shall ask him. And if you will send me such a man, I will collect and send you the tribute of Egypt for three years; and if you will not send me a man who will give me an answer to all that I ask him, then (you must) collect and send me the tribute of Assyria and Nineveh for three years by the hands of these ambassadors who have come to you". When this letter had been read to the king, he called all the nobles and nobles of his kingdom and said to them: "Who of you will go to Egypt to give the king an answer to all his questions, to build him the palace he desires, and to bring back the tribute of Egypt for three years?

When the nobles heard this, they answered and said to the king "My lord the king, you know that not only in your years, but also in the years of your father Esarhaddon, Ahiqar the scribe used to settle such matters. And even now behold his son Nadan! He, too, is being instructed in the knowledge and wisdom of his father. When my son Nadan heard this, he cried out to the king and said: "The gods (themselves) are not able to do such things, not to speak of human beings!" When the king heard these words, he was very indignant, and he got down from his throne, sat down on the ground, and said "Woe to you, Ahiqar the Wise, that I have killed you for the words of a boy! Who will give you to me this time? I would give him your weight in gold".
When Nabusemakh, my poor companion, heard these words, he fell down before the king and said to him: "My lord the king, he that condemneth the commandment of his lord is guilty of death: and I, my lord, have condemned the commandment of thy kingdom. So order them to crucify me. For Ahiqar, whom you commanded me to kill, is still alive". When the king heard this, he answered and said: "Speak, speak, Nabusemakh, speak, O thou diligent and good man, who knew not evil. If it is indeed as you say, show me Ahiqar alive, and I will give you gifts of silver, the weight of a hundred talents, and of purple, the weight of fifty talents. Nabusemakh said to him: **(fol. 72v)** "Swear to me, my lord the king, that if no other sins of mine are found before you, this (sin) shall not be remembered (against me)". So the king gave him his right hand in this matter. And that same hour the king rode in his chariot, and came to me in haste, and opened my prison upon me, and I got up, and came, and prostrated myself before the king - while the hair of my head had grown down to my shoulders, and my beard reached my breast, and my body was covered with dust, and my nails were long like (those of) an eagle.

When the king saw me, he wept and was ashamed to speak to me, and in great pain he said to me: "Ahiqar, I have not sinned against thee, but thy son, whom thou hast raised up, has sinned against thee". So I answered and said to him: "I have seen your face, my lord, and there is no evil in my heart. The king said to me: "Go to your house, Ahiqar, and shave off your hair, and wash your body, and recover your strength for forty days, and then come to me.

Then I went to my house, and I was in my house for thirty days, and when my body was recovered, I came to the king, and the king answered and said to me: "See, Ahiqar, what Pharaoh the king of Egypt has written to me!" And I answered and said to him: "O my lord the king! you must not worry about this matter. I will go to Egypt and build for him a palace, and I will answer him all his questions, and I will bring with me the tribute of Egypt for three years". When the king heard this, he was glad with great joy; he made a great feast and sacrificed great offerings, and gave gifts to me and to Nabusemakh, my poor companion, and set him at the head of all.

Then I wrote a letter to Eshfagni, my wife(, as follows): "When this letter reaches you, order my hunters to catch two young eaglets for me: and order my cotton workers to make me ropes of cotton; **(fol. 73r)** the length of each of them shall be a thousand ells, and their thickness shall be the thickness of a little finger; tell the carpenters that they shall make for me birdcages for the eaglet; and deliver (them) to Ubael and Tabshalem, the two boys who do not (yet) know how to speak, and they shall teach them to say this: "Give mud, mortar, tiles, and bricks to the builders who are idle". So I restrained them. When we came to Egypt, I went to the king's gate, and his nobles told the king, "The man whom the king of Assyria sent has come". The king ordered and gave me a place to rest, and the next day I went in before him and worshipped him and greeted him.


The king answered and said to me: "What is your name?" And I said to him: "Abiqam is my name, one of the despised ants of the kingdom. The king answered and said to me: "Am I so despised by your lord that he has sent me a despised ant of his kingdom? Go, Abiqam, to your lodging, and rise early in the morning and come to me. Then the king commanded his nobles: "So the king dressed himself in fine white linen and sat on his throne. He commanded, and I came into his presence, and he said to me: "To what am I similar, Abiqam, and to what are my nobles similar?" I answered and said to him: "My lord the king, you are like Baal, and your nobles are like his priests". And again he said to me: "Go **(fol. 73v)** to your lodgings and come to me tomorrow". The king commanded his nobles: "For tomorrow you shall put on robes of white cotton," and this king (himself) put on white robes and sat on his throne. And he commanded, and I came into his presence, and he said to me: "To what am I similar, Abiqam, and to what are my nobles similar?" I said to him: "My lord the king, thou art like the sun, and thy nobles are like its rays". Again he said to me: "Go to your lodgings and come to me tomorrow". Again the king commanded his nobles: "Tomorrow clothe yourselves in black," and the king put on purple. He commanded, and I came to him, and he said to me: "What am I like, Abiqam, and what are my nobles like?"
I said to him: "My lord the king, you are like the moon, and your nobles like the stars". And again he said to me: "Go to your house and come to me tomorrow". And the king commanded his nobles: "Tomorrow dress yourselves in dyed and variegated garments, and let the doors of the palace be covered with red hangings". The king himself dressed in fine needlework. He commanded, and I came into his presence, and he said to me: "What am I like, Abiqam? and my nobles, what are they like?" And I said to him: "My lord the king, you are like the month of Nisan, and your nobles are like its flowers". Then the king said to me: "The first time you compared me to Bel, and my nobles to his priests. The second time you compared me to the sun, and my nobles to its rays. The third time you compared me to the moon, and my nobles to the stars. The fourth time you compared me to Nisan, and my nobles to its flowers. Now tell me, Abiqam, what is your Lord like?" And I answered and said to him: "Be it far from you, my lord the king, that I should mention my lord Sennacherib while you are sitting. My lord Sennacherib is like (?) and his nobles to the lightning that is in the clouds. When he wills, he makes the rain and the dew (and) the hail; and when he thunders, he hinders the sun from rising, and its rays from being seen **(fol. 74r)**; and he will hinder Bel from going in and out in the street, and his nobles from being seen. And he will keep the moon from rising and the stars from appearing".
When the king heard this, he became very angry and said to me: "By the life of your lord, I beseech you, tell me your name!" I answered and said to him: "I am Ahiqar, the scribe and seal of Sennacherib, king of Assyria and Nineveh". The king said to me: "Did I not hear that your lord killed you?" And I said to him: "Till now I am alive, my lord the king, and God has saved me from what my hands did not do". The king said to me:  "Go, Ahiqar, to your house, and come to me tomorrow, and tell me a word that has not been heard by me, nor by any of my nobles, nor in the city of my kingdom". So I sat down and meditated in my heart and wrote the following letter: "From Pharaoh, King of Egypt, to Sennacherib, King of Assyria and Nineveh, Greetings. Kings need kings and brothers need brothers. And at this time my gifts are diminishing, for silver is lacking in my treasuries: Please send me from your treasuries 900 talents of silver, and in a little while I will restore them to their place". So I rolled up this letter and held it in my hands. And the king commanded, and I came into his presence, and I said to him: "Perhaps there is a word in this letter which you have never heard before". And when I read it before him and his nobles, they cried out, as the king had commanded, and said: "Surely we have all heard this, and it is so". So I said to them: "Behold, Egypt owes nine hundred talents to Assyria". When the king heard this, he was astonished.
Then he said to me: "I want to build a castle between the earth and the sky, and its height from the earth shall be a thousand ells". **(Fol. 74v)** Then I let out the eaglets, and tied the ropes to their feet, and set the boys on them; and they said: "Give mud, mortar, tiles, and bricks to the builders who are idle; the master will yokes them together". When the king saw this, he was astonished. Then I, Ahiqar, took a rod and beat the king's nobles until they all fled. Then the king became angry with me and said to me: "You rave wildly, Ahiqar, who is able to carry anything up to these boys?" And I said to him:" As for the matter of Sennacherib, my lord, you say nothing, but it was that he built two castles in one day". The king said to me: "Get out of the castle, Ahiqar, and go to your lodging, and come to me in the morning.


When it was morning, I came before him, and he said to me: "Tell me, Ahikar, what is this? The horse of your lord neighs in Assyria, and our mares hear his voice here, and their foals miscarry". So I went out from the presence of the king, and I told my servants to catch me a cat, and I whipped it in the streets of the city. When the Egyptians saw this, they went and said to the king: "Ahiqar has done evil to our people and has made us a laughing stock. For he has caught a cat and is whipping it in the streets of our city". The king sent for me and summoned me, and I came to him. He said to me: "In what way do you offend us?" And I answered and said to him: "O my lord the king, live for ever! This cat has done me serious harm in no small way, for my lord had entrusted me with a rooster, and its voice was very beautiful, and when it crowed I understood that my lord wanted me, and I went to my lord's gate. And that night this cat **(fol. 75r)** went to Assyria and tore off the head of this cock and returned". The king answered and said to me: "As far as I can see, Ahiqar, since you have grown old you have become mad. For it is 360 parasangs from here to Assyria; and how can you say that in a single night this cat went and cut off the head of the cock and returned?" So I said to him: "And if it is 360 parasangs from Egypt to Assyria, how is it that your mares here hear the voice of my Lord's horse and miscarry their foals?"
When the king heard this, he was very sad* and said to me: "Ahiqar, tell me this parable: On the top of a pillar there are twelve cedars; in each cedar there are thirty wheels, and in each wheel there are two ropes, one white and one black". I answered and said to him: "O my lord the king! the cattle-breeders of our land know this parable which thou hast told. The pillar you told me about is the year; the twelve cedars are the twelve months of the year; the thirty wheels are the thirty days of the month; and the two ropes, one white and the other black, are the day and the night".

Again he said to me: "Ahiqar, make me five ropes from the sand of the river. And I said to him: "Command, O my Lord, bring me a rope of sand from your treasury, and I will make one like it". So he said to me: "Unless you do this, I will not give you the tribute of Egypt". So I sat down and pondered in my heart what I should do. I went out of the king's palace and made five holes in the east wall of the palace. When the sun entered the holes, I scattered sand in them; and the path of the sun began to appear as if (the sand) were twisted in the holes. Then I said to the king: "Please, my lord, take up these, and I will twine others in their place". When the king and his nobles saw it, they were astonished. **(fol. 75v)**
Again the king commanded me to bring the upper part of a broken millstone, and he said to me, "Ahiqar, sew up this broken millstone for us". So I went and brought a lower millstone and threw it down before the king and said to him: "O my lord the king, since I am a stranger here, and have not the tools of my trade with me, command the cobblers to cut me straps of this lower millstone, which is the companion of the upper part of a millstone, and I will sew them up at once". When the king heard this, he laughed and said: "The day on which Ahiqar was born shall be blessed before the God of Egypt; and since I have seen you alive, I will make it a great day and a feast". So he gave me the tribute of Egypt for three years, and I returned immediately and came to my lord, King Sennacherib, and he came out to meet me and received me.

He made a great day of it, and made me the head of his household; and he answered and said to me "Ask what you will, Ahiqar," and I worshipped the king and said: "O my lord the king! whatever thou wilt give me, give it to Nabusemakh, my poor companion, for he has given me my life; and as for me, my lord, command them to give me my son Nadan, that I may teach him another lesson. For he has forgotten my former teaching". So the king commanded them to bring me my son Nadan, and the king said to me: "Go, Ahiqar, and do whatever you like with your son Nadan, for no one will save his body from your hands".

Then I took Nadan my son, and brought him into my house, and bound him with an iron chain, the weight of which was twenty talents, and cast him into vessels, and put an iron band round his neck, and struck him a thousand blows on his shoulders, and a thousand and one on his loins, and put him in the porch of the door of my court, and gave him bread by weight, and water by measure. **(fol. 76r)** I gave him to my servant Nabuel to guard him, and I said to my servant "Write down on a tablet what I say to my son Nadan when I go in and when I come out". I answered and said to my son Nadan (as follows):
#### Parables

[1] My son, he who does not hear with his ears, they make him hear with his neck.

[2] My son Nadan answered and said to me: "Why are you angry with your son?" I answered and said to him: "My son, I have set you on the throne of honour, but you have cast me down from my throne. But my righteousness has saved me".

[3] My son, you have been to me like a scorpion striking a rock. And it [the rock] says to it: "You have struck at an unaffected heart". And it strikes a needle, and they say to it: "You have struck at a sting worse than yours".

[4] My son, you have been to me like a goat standing over a red berry and eating it. And the red berry said to him: "Why do you eat me, when you treat your skin with my root?" The goat said to it: "I eat you during my life, and after my death they will pull you up by your roots".

[5] My son, you have been to me like the one who threw a stone at heaven, and it did not reach heaven, but he received punishment from God.

[6] My son, you were like the one who saw his companion shivering with cold and took a pitcher of water and poured it over him.

[7] Oh, my son, if you had killed me, you would have been able to stand in my place; but you should know, my son, that even if the tail of the pig were to grow to seven ells, it would not take the place of the horse, and even if its bristles were soft and woven, it would not ascend to the body of a free man.

[8] My son, I intended that you should be in my place, that you should acquire my house and my wealth, and that you should inherit them. But God was not pleased, and He did not hear your voice.

[9] My son, you have been to me like a lion that came upon a donkey in the morning of the day, and said to him "Welcome, my Lord Cyrus". But the donkey said to him: "May the same welcome that you give me be given to him **(fol. 76v)** who tied me up last night, but did not tie my loins, lest I should see your face".

[10] My son, a snare was set on a dunghill, and a sparrow came and saw it and said to it: "What are you doing here?" and the snare said to it: "I am praying to God". The sparrow said: "And this in your mouth - what is it?" The snare said: "Bread for strangers". [Then] the sparrow approached to take it, and [the snare] caught it by the neck. And while the sparrow was flapping, it said: "If this is bread for strangers, may the God to whom you pray never answer your voice".

[11] My son, I was like a bull tied to a lion, but the lion turned and crushed it.

[12] My son, you have been to me like a fleeting weevil that destroys the granaries of kings, but has no hold on anything.

[13] My son, you have been to me like a pot on which they have made golden handles, but the soot has not been scraped off the bottom.

[14] My son, you have been to me like a ploughman who sowed twenty ears of barley in a field. And when he reaped it, it was twenty ears. And he said to it: "What I have scattered, I have gathered: but thou art a disgrace with thy evil name, in that thou hast made a peck out of a peck, while I live".

[15] My son, you have been to me like a decoy bird, which does not save itself from death, and whose voice destroys its companions.

[16] My son, you have been to me like a goat that leads its fellows to the slaughterhouse, but does not save itself.

[17] My son, you have been to me like a dog that went into the potter's oven to warm itself, and when it was warm, it got up to bark at them.

[18.] My son, you have been to me like a swine that had gone to the bathhouse, and when it saw a trench of mud, it went down and bathed in it, calling out to its companions: "Come and bathe!"

[19] My son! My finger was on your mouth, and your finger was on my eyes. Why did I raise you up, fox, that your eyes should see apples?

[20] My son, the dog that eats his prey shall be the prey of the wolves, and the hand that is not industrious shall be cut off from its shoulder, **(fol. 77r)** and the eye with which I cannot see shall be plucked out by the raven.

[21] What good have you done me, my son, that I have remembered you and my soul has found comfort in you?

[22] My son, if the gods steal, by whom shall they swear? And a lion that steals a piece of land, how will it settle down and feed on it?

[23] My son, I showed you the face of the king, and I gave you great honour, but you wanted to do me evil.

[24] My son, you were to me like a tree that said to its woodcutters: "If there had not been some of me in your hands, you would not have fallen on me.

[25] My son, you have been to me like the young swallows that fell out of their nest, and a cat [or weasel] caught them and said to them: "Had it not been for me, great evil would have befallen you". They answered and said to him: "Is this why you have put us in your mouth?"

[26] O my son! you have been to me like the cat to whom they say: "Give up thine thieving, and thou shalt go out and come in from the king's palace as thy soul pleaseth". And she answered and said to them: "Even if I had eyes of silver and ears of gold, I would not stop stealing".

[27] My son, you were to me like a snake that was tied to a bush and thrown into the river. And the wolf saw them and said to them: "Evil rides on evil, and worse than either carries them away". The serpent said to him: "If you had come here, you would have given account of the goats and their kids".

[28] My son, I have seen a goat taken to the slaughterhouse, and because its time had not yet come, it returned to its place and saw its children and its grandchildren. My son, I have seen foals that have become murderers of their mothers.
[29] My son, I fed you with all that was good, but you, my son, fed me with bread of the soil, and you were not satisfied.

[30] My son, I anointed you with sweet ointments, but you, my son, defiled my body with dust.

[31] My son, I made your stature grow like a cedar, but you made me bend in my life, and you made me drunk with your evil deeds.

[32] My son, I raised you like a tower and said: "If **(fol. 77v)** my enemy should come upon me, I will go up and dwell in thee," but thou, when thou sawest my enemy, didst yield to him.

[33] My son, you were to me like a mole that came up out of the ground to get some warmth from the sun, for it had no eyes.  An eagle saw him, smote him, and carried him off.

[34] My son Nadan answered and said to me: "O my father Ahiqar! such a thing is far from you! Do unto me according to thy mercy: for God forgives the iniquity of men; and thou, too, forgive me this folly: and I will serve thy horses, and feed the swine that are in thy house: and I shall be called evil: but thou devise no evil against me".

[35] I answered and said to him: "O my son! thou hast been to me like a palm-tree which stood by a river, and cast all its fruit into the river. When its owner came to cut it down, it said to him: 'Leave me alone this year, and I will give you carobs.' His master said to him: 'Thou hast not been diligent in that which is thy own: how canst thou be diligent in that which is not thy own?'"

[36] My son, they say to the wolf: "Why do you follow the sheep?" He said to them: "Their dust is very good for my eyes". Again they took him to the schoolhouse, and his master said to him: "A, B," said the wolf: "Kid, lamb".

[37] My son, I have taught you that there is a God, but you rise up against good servants and beat those who have not fumbled. And just as God has kept me alive because of my righteousness, so He will destroy you because of your deeds.

[38] My son! they put the head of the donkey over a dish at the table, and it rolled off and fell into the dust. They say: "It was angry with itself because it was not honoured".

[39] My son, you have proved the saying which says: "Call him whom you have begotten as your son, and him whom you have bought as your slave". My son! the saying is true which says "Take thy sister's son under thy armpit and smite him against a stone". But God, who has kept me alive, will judge between us.

#### Third narrative section

At that hour Nadan swelled up like a bladder and died. To the one who does good, the good **(fol. 78r)** shall be rewarded, and to the one who does evil, the evil shall be rewarded. And he who digs a pit for his friend, fills it with his (own) stature. Glory be to God, and His mercy be upon us. Amen.

The proverbs of Ahiqar, the wise man and scribe of Sennacherib, king of Assyria and Nineveh, are finished.



## How to Cite This Page

Birol, Simon. „Translation of the Syriac Version“. Ahiqar. The Story of Ahiqar in Its Syriac and Arabic Tradition, [date], ahiqar.uni-goettingen.de/syriactranslation.html.
