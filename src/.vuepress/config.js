import dotenv from 'dotenv-flow';
dotenv.config();

import { viteBundler } from '@vuepress/bundler-vite'
import { defineUserConfig } from 'vuepress'
import { defaultTheme } from '@vuepress/theme-default'

export default defineUserConfig({
  bundler: viteBundler(),
  lang: 'en-US',
  head: [
    ['meta', { name: 'charset', content: 'UTF-8' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['meta', { name: 'viewport', content: 'width=device-width,initial-scale=1' }],
    ['link', { rel: "apple-touch-icon", sizes: "180x180", href: "/assets/favicons/android-chrome-192x192.png"}],
    ['link', { rel: "icon", type: "image/png", sizes: "32x32", href: "/assets/favicons/favicon-32x32.png"}],
    ['link', { rel: "icon", type: "image/png", sizes: "16x16", href: "/assets/favicons/favicon-16x16.png"}],
    ['link', { rel: "shortcut icon", href: "/assets/favicons/favicon.ico"}],
    ],
  locales: {
    '/': {
      lang: 'en-US',
      title: 'Ahiqar',
      description: 'Ahiqar - The Syriac and Arabic Ahiqar Texts'
    },
  },
  theme: defaultTheme({
    editLinkText: '',
    lastUpdated: false,
    contributors: false,
    navbar: [
      {
        text: 'Meta Edition',
        children: [
          { text: 'Edition Guidelines', link: '/editionguidelines.html' },
          { text: 'Textual Criticism', link: 'textualcriticism.html' },
          { text: 'Motifs', link: '/motifs.html' },
          { text: 'Manuscripts', link: '/manuscripts.html' },
          { text: 'Sayings', link: '/sayings.html' },
          { text: 'Proverbs', link: '/proverbs.html' },
          { text: 'Collation', link: '/collation.html' },
          { text: 'Stemmata', link: '/stemmata.html' },
          { text: 'Related Literature', link: '/relatedliterature.html' },
        ],
      },
      {
        text: 'Editions',
        children: [
          { text: 'Syriac Texts', link: '/syriac.html' },
          { text: 'Arabic and Karshuni Texts', link: '/arabic-karshuni.html' },
          { text: 'Neo-Aramaic Texts', link: '/neo-aramaic.html' },
        ],
      },
      {
        text: 'Translations',
        children: [
          { text: 'Translation of the Syriac version', link: '/syriactranslation.html' },
          { text: 'Translation of the Arabic version', link: '/arabictranslation.html' },
        ],
      },
      {
        text: 'Project',
        children: [
          { text: 'About', link: '/about.html' },
          { text: 'Technical Documentation', link: '/technicaldocumentation.html' },
        ],
      },
      {
        text: 'Search',
        link: '/search.html',
      },
    ],
  }),
  plugins: [],
  markdown: {
    anchor: { permalink: false },
    toc: { includeLevel: [1, 2] },
    extendMarkdown: md => {
      md.use(require('markdown-it-attrs'))
    }
  },
});
