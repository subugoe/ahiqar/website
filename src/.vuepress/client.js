import { defineClientConfig } from '@vuepress/client';
import Banner from './components/Banner.vue';
import CloseButton from "./components/CloseButton.vue";
import Contact from "./components/Contact.vue";
import CookieBanner from './components/CookieBanner.vue';
import Cta from "./components/Cta.vue";
import Footer from "./components/Footer.vue";
import ModalTrigger from "./components/ModalTrigger.vue";
import SearchInput from "./components/SearchInput.vue";
import SearchPage from "./components/SearchPage.vue";
import TidoPage from "./components/TidoPage.vue";
import TidoPageTranslation from "./components/TidoPageTranslation.vue";
import Layout from "./layouts/Layout.vue";
// import Modal from "bootstrap/js/src/modal.js";

const matomoSiteId = parseInt(import.meta.env?.VITE_APP_MATOMO_SITE_ID);

export default defineClientConfig({
  async enhance({ app, router }) {
    app.component('Banner', Banner);
    app.component('Cta', Cta);
    app.component('Footer', Footer);
    app.component('Contact', Contact);
    app.component('SearchPage', SearchPage);
    app.component('SearchInput', SearchInput);
    app.component('CloseButton', CloseButton);
    app.component('CookieBanner', CookieBanner);
    if (!__VUEPRESS_SSR__) {
      await import('tido/dist/tido.js');
      await import('tido/dist/tido.css');
      app.component('TidoPage', TidoPage);
      app.component('TidoPageTranslation', TidoPageTranslation);
      app.component('ModalTrigger', ModalTrigger);
      if(matomoSiteId){
        const { default: VueMatomo } = await import('vue-matomo');
        app.use(VueMatomo, {
          host: 'https://matomo.gwdg.de/',
          siteId: matomoSiteId,
          router: router,
          enableLinkTracking: true,
          trackInitialView: true,
        });
      }
    }
  },
  layouts: {
    Layout,
  },
})
