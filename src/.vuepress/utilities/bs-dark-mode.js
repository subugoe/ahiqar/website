const themeSwitcherSelector = '.toggle-color-mode-button';

const getPreferredTheme = () => {
  return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'
}

export const setTheme = function (theme) {
  if (theme === 'auto' && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    document.documentElement.setAttribute('data-bs-theme', 'dark')
  } else {
    document.documentElement.setAttribute('data-bs-theme', theme)
  }
}

// setTheme(getPreferredTheme())

export const setupDarkModeListeners = () => {
  window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', () => {
    setTheme(getPreferredTheme())
  })

  document.querySelectorAll(themeSwitcherSelector)
    .forEach(toggle => {
      toggle.addEventListener('click', () => {
        const theme = document.documentElement.getAttribute('data-bs-theme') === 'light' ? 'dark' : 'light';
        setTheme(theme)
      })
    })
}


