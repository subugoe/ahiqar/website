const tidoConfig = {
    collection: "https://ahiqar.uni-goettingen.de/textapi/ahiqar/arabic-karshuni/collection.json",
    container: "#viewer",
    colors: {
        primary: "#234ea6"
    },
    labels: {
        item: "Sheet",
        manifest: "Manuscript"
    },
    panels: [
        {
            label: "contents_and_metadata",
            views: [
                {
                    id: "tree",
                    label: "contents",
                    connector: {
                        id: 1
                    }
                },
                {
                    id: "metadata",
                    label: "metadata",
                    connector: {
                        id: 2,
                        options: {
                            collection: {
                                all: true
                            },
                            manifest: {
                                all: true
                            },
                            item: {
                                all: true
                            }
                        }
                    }
                }
            ]
        },
        {
            label: "image",
            views: [
                {
                    id: "image",
                    label: "Image",
                    connector: {
                        id: 3
                    }
                }]
        },
        {
            label: "text",
            views: [
                {
                    id: "text1",
                    label: "Transcription",
                    default: true,
                    connector: {
                        id: 4,
                        options: {
                            type: "transcription"
                        }
                    }
                },
                {
                    id: "text2",
                    label: "Transliteration",
                    connector: {
                        id: 4,
                        options: {
                            type: "transliteration"
                        }
                    }
                },
                {
                    id: "text3",
                    label: "Translation",
                    connector: {
                        id: 4,
                        options: {
                            type: "translation"
                        }
                    }
                }
            ]
        },
        {
            label: "annotations",
            views: [
                {
                    id: "annotations1",
                    label: "Editorial",
                    connector: {
                        id: 5,
                        options: {
                            types: [
                                {
                                    name: "Person",
                                    icon: "person"
                                },
                                {
                                    name: "Place",
                                    icon: "marker"
                                },
                                {
                                    name: "Editorial Comment",
                                    icon: "chat"
                                },
                                {
                                    name: "Reference",
                                    icon: "externalLink"
                                }
                            ]
                        }
                    }
                },
                {
                    id: "annotations2",
                    label: "Motif",
                    connector: {
                        id: 5,
                        options: {
                            types: [
                                {
                                    name: "Motif",
                                    icon: "pen"
                                }
                            ]
                        }
                    }
                },
                {
                    "id": "annotations3",
                    "label": "Variants",
                    "connector": {
                      "id": 6
                    }
                }
            ]
        }
    ],
    translations: {
        en: {
            contents_and_metadata: "Contents & Metadata",
            next_item: "Next Sheet",
            previous_item: "Previous Sheet",
            next_manifest: "Next Manuscript",
            previous_manifest: "Previous Manuscript",
            item: "Sheet"
          }
    }
};

export {
    tidoConfig
};
