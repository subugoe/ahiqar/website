import axios from 'axios';

class ApiService {
  constructor(baseURL) {
    this.http = axios.create({
      baseURL: baseURL,
      timeout: 5000,
    });
    this.http.interceptors.response.use(
      (response) => response.data,
      (error) => {
        return Promise.reject(error);
      },
    );
  }

  async search(value, from, size) {
    return this.request('/search', 'POST', {
      query: {
        simple_query_string: {
          query: value
        }
      },
      from,
      size
    })
  }

  async request (url, method, data, params) {
    return await this.http.request({
      url,
      method,
      data,
      params,
    });
  }
}

export default new ApiService(import.meta.env?.VITE_APP_BASE_URL_API);
