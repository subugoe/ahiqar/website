---
home: true
heroText: Search
tagline: You can search the text of any edition by just typing a search term.
---
<div class="container">

Additionally you can start a more elaborated search by using operators like "AND", "NOT", "OR". Wildcards "\*" are available as well. Please dive into the [full documentation at Lucene](https://lucene.apache.org/core/8_9_0/queryparser/org/apache/lucene/queryparser/classic/package-summary.html) for further information on how to use it (e. g. proximity, regular expressions, ranges and more).

Please note: In the current state, the search engine is only able to identify exact matches (in spelling and vocalization).

<ClientOnly>
    <SearchPage/>
</ClientOnly>
</div>
