---
title: Motifs
layout: Layout
hide_sidebar: true
---

# {{ $frontmatter.title }}

In 1979, Max Küchler began his work on Ahiqar, which he published in his study _Frühjüdische Weisheitstraditionen_ (Fribourg/Göttingen 1979). He started with a collection of themes in the Ahiqar story in the form of notes. In connection with this, we have collected and marked the central motifs and themes of the story as they appear in the various textual witnesses. This process aids in clarifying, correcting, and expanding the history of motifs in this story. Below is a brief description of each motif:


| Letter  | Keyword                                   | Category                                      | Description                                                         |
|---------|-------------------------------------------|-----------------------------------------------|---------------------------------------------------------------------|
| A       | Adherence to wisdom                       | Wisdom, Knowledge                             | The necessity and Priority of being wise                            |
|         | Air Castle                                | Riddle                                        | The riddle of the air castle                                        |
| B       | Beauty/Caducity                           | Women, caducity, Social relationship          | The relationship to women and the caducity of their beauty          |
|         | Burden/Debt                               | Commitment                                    | The state of a person with commitments                              |
| C       | Caution against powerful                  | Social relationship                           | The relationship to powerful persons or forces                      |
| D       | Death age                                 | Caducity, Social standing, Death, Honour      | The relationship to older persons and the caducity of human beings  |
|         | Discernment                               | Reflection, Behaviour                         | The righteous assessment of a situation                             |
|         | Disciplining of son(s)                    | Social relationship, Family                   | The instruction and education of one\'s sons                        |
| E       | Enemies                                   | Social relationship                           | The relationship and attitude to adversial persons                  |
| G       | Good name                                 | Social standing, Honour                       | The income of wise and righteous deeds                              |
| I       | Image king                                | Politics, King                                | The image of the Assyrian king                                      |
|         | Intrigue                                  | Politics                                      | Nadan\'s actions against Ahiqar                                     |
| K       | Keepind secrets                           | Social relationship, Loyalty                  | How to handle secrets                                               |
| L       | Loyal obligation to God(s)                | God, Loyality                                 | Allegiance and relation to God(s)                                   |
| M       | Meekness showing respect                  | Social relationship                           | To behave with respect and meekness                                 |
| O       | Obeying parents                           | Social relationship, Loyalty                  | Relationship to parents                                             |
| P       | Parable animals                           | Parables, Animals                             | Parables including references to animals                            |
|         | Parable plants                            | Parables, Plants                              | Parables including references to plants                             |
| Q       | Quarrel                                   | Social relationship, Social behaviour         | How to handle conflicts and quarrels                                |
| R       | Richness poverty                          | Social position                               | The significance of richness resp. poverty                          |
|         | Royal challenges                          | Politics                                      | Political tasks for the king                                        |
| S       | Self-examination, self-reflection         | Self-examination                              | Reflection on one\'s attitude and behaviour                         |
|         | Social contacts family                    | Social relationship                           | Social contacts with persons within the family                      |
|         | Social contacts no family                 | Social relationship                           | Social contacts with persons outside the family                     |
|         | Successful courtier                       | Politics                                      | Rehabilitation of Ahiqar                                            |
| T       | Temptress women                           | Social relationship, Women                    | Relationship to temptress women                                     |
|         | Treatment slaves                          | Social relationship                           | Relationship and treatment of slaves and maidservants               |
|         | Truth lying                               | Social behaviour                              | Attitude towards truth or falsehood                                 |
| W       | Wise/fool/sinful behaviour resp. persons  | Wisdom, Social behaviour, Social relationship | Relation to wise, fool and sinful persons and their behaviour       |
